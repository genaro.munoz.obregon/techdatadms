"use strict";
(self["webpackChunkdocument_management"] = self["webpackChunkdocument_management"] || []).push([["main"],{

/***/ 90158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _layout_app_layout_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./layout/app-layout/main-layout/main-layout.component */ 9899);
/* harmony import */ var _core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/security/auth.guard */ 50319);
/* harmony import */ var _user_my_profile_my_profile_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user/my-profile/my-profile.component */ 76811);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ 55041);
/* harmony import */ var _company_profile_company_profile_resolver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./company-profile/company-profile.resolver */ 31539);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);








const routes = [{
  path: '',
  component: _app_component__WEBPACK_IMPORTED_MODULE_3__.AppComponent,
  resolve: {
    profile: _company_profile_company_profile_resolver__WEBPACK_IMPORTED_MODULE_4__.CompanyProfileResolver
  },
  children: [{
    path: 'login',
    loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_login_login_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./login/login.module */ 80107)).then(m => m.LoginModule)
  }, {
    path: '',
    component: _layout_app_layout_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_0__.LayoutComponent,
    children: [{
      path: '',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_paginator_mjs-node_modules_angular_material_fe-42cafe"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_checkbox_mjs"), __webpack_require__.e("default-src_app_core_domain-classes_dayOfWeek_enum_ts-src_app_core_domain-classes_frequency_e-a11b28"), __webpack_require__.e("default-src_app_core_domain-classes_document-header_ts-src_app_core_domain-classes_document-r-f10612"), __webpack_require__.e("common"), __webpack_require__.e("src_app_document-library_document-library_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./document-library/document-library.module */ 55304)).then(m => m.DocumentLibraryModule)
    }, {
      path: 'my-profile',
      component: _user_my_profile_my_profile_component__WEBPACK_IMPORTED_MODULE_2__.MyProfileComponent,
      canActivate: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard]
    }, {
      path: 'dashboard',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_paginator_mjs-node_modules_angular_material_fe-42cafe"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_card_mjs"), __webpack_require__.e("common"), __webpack_require__.e("src_app_dashboard_dashboard_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./dashboard/dashboard.module */ 34814)).then(m => m.DashboardModule)
    }, {
      path: 'pages',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("src_app_page_page_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./page/page.module */ 85115)).then(m => m.PageModule)
    }, {
      path: 'roles',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_checkbox_mjs"), __webpack_require__.e("default-src_app_core_services_action_service_ts-src_app_core_services_page_service_ts-node_mo-eb24e9"), __webpack_require__.e("common"), __webpack_require__.e("src_app_role_role_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./role/role.module */ 37321)).then(m => m.RoleModule)
    }, {
      path: 'users',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_checkbox_mjs"), __webpack_require__.e("default-src_app_core_services_action_service_ts-src_app_core_services_page_service_ts-node_mo-eb24e9"), __webpack_require__.e("src_app_user_user_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./user/user.module */ 88524)).then(m => m.UserModule)
    }, {
      path: 'categories',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("src_app_category_category_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./category/category.module */ 26914)).then(m => m.CategoryModule)
    }, {
      path: 'documents',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_paginator_mjs-node_modules_angular_material_fe-42cafe"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_checkbox_mjs"), __webpack_require__.e("default-src_app_core_domain-classes_dayOfWeek_enum_ts-src_app_core_domain-classes_frequency_e-a11b28"), __webpack_require__.e("default-src_app_core_domain-classes_document-header_ts-src_app_core_domain-classes_document-r-f10612"), __webpack_require__.e("src_app_document_document_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./document/document.module */ 7209)).then(m => m.DocumentModule)
    }, {
      path: 'document-audit-trails',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_paginator_mjs-node_modules_angular_material_fe-42cafe"), __webpack_require__.e("src_app_document-audit-trail_document-audit-trail_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./document-audit-trail/document-audit-trail.module */ 54647)).then(m => m.DocumentAuditTrailModule)
    }, {
      path: 'login-audit',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_paginator_mjs-node_modules_angular_material_fe-42cafe"), __webpack_require__.e("common"), __webpack_require__.e("src_app_login-audit_login-audit_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./login-audit/login-audit.module */ 28443)).then(m => m.LoginAuditModule)
    }, {
      path: 'notifications',
      canLoad: [_core_security_auth_guard__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_paginator_mjs-node_modules_angular_material_fe-42cafe"), __webpack_require__.e("src_app_notification_notification_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./notification/notification.module */ 22154)).then(m => m.NotificationModule)
    }, {
      path: 'reminders',
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_paginator_mjs-node_modules_angular_material_fe-42cafe"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_checkbox_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_card_mjs"), __webpack_require__.e("default-src_app_core_domain-classes_dayOfWeek_enum_ts-src_app_core_domain-classes_frequency_e-a11b28"), __webpack_require__.e("common"), __webpack_require__.e("src_app_reminder_reminder_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./reminder/reminder.module */ 78784)).then(m => m.ReminderModule)
    }, {
      path: 'email-smtp',
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("src_app_email-smtp-setting_email-smtp-setting_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./email-smtp-setting/email-smtp-setting.module */ 80194)).then(m => m.EmailSmtpSettingModule)
    }, {
      path: 'company-profile',
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_card_mjs"), __webpack_require__.e("src_app_company-profile_company-profile_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./company-profile/company-profile.module */ 63765)).then(m => m.CompanyProfileModule)
    }, {
      path: 'languages',
      loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_checkbox_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_card_mjs"), __webpack_require__.e("src_app_languages_languages_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./languages/languages.module */ 43798)).then(m => m.LanguagesModule)
    }, {
      path: '**',
      redirectTo: '/'
    }]
  }]
}];
class AppRoutingModule {}
AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) {
  return new (t || AppRoutingModule)();
};
AppRoutingModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
  type: AppRoutingModule
});
AppRoutingModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
  imports: [_angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'top',
    useHash: false
  }), _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](AppRoutingModule, {
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule]
  });
})();

/***/ }),

/***/ 55041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ 38699);
/* harmony import */ var _core_services_translation_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/services/translation.service */ 16107);
/* harmony import */ var _core_security_security_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/security/security.service */ 40130);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ 34497);
/* harmony import */ var _shared_loading_indicator_loading_indicator_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/loading-indicator/loading-indicator.component */ 23096);








class AppComponent {
  constructor(_router, translate, translationService, route, securityService, titleService) {
    this._router = _router;
    this.translate = translate;
    this.translationService = translationService;
    this.route = route;
    this.securityService = securityService;
    this.titleService = titleService;
    this.setProfile();
    this.companyProfileSubscription();
    translate.addLangs(['en']);
    translate.setDefaultLang('en');
    this.setLanguage();
    this._router.events.subscribe(routerEvent => {
      if (routerEvent instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__.NavigationStart) {
        this.currentUrl = routerEvent.url.substring(routerEvent.url.lastIndexOf('/') + 1);
      }
      if (routerEvent instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__.NavigationEnd) {
        /* empty */
      }
      window.scrollTo(0, 0);
    });
  }
  setProfile() {
    this.route.data.subscribe(data => {
      if (data.profile) {
        this.securityService.updateProfile(data.profile);
      }
    });
  }
  companyProfileSubscription() {
    this.securityService.companyProfile.subscribe(profile => {
      if (profile) {
        this.titleService.setTitle(profile.title);
      }
    });
  }
  setLanguage() {
    const currentLang = this.translationService.getSelectedLanguage();
    if (currentLang) {
      this.translationService.setLanguage(currentLang).subscribe(() => {});
    } else {
      const browserLang = this.translate.getBrowserLang();
      const lang = browserLang.match(/en|es|ar|ru|cn|ja|ko|fr/) ? browserLang : 'en';
      this.translationService.setLanguage(lang).subscribe(() => {});
    }
  }
}
AppComponent.ɵfac = function AppComponent_Factory(t) {
  return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__.TranslateService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_core_services_translation_service__WEBPACK_IMPORTED_MODULE_0__.TranslationService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_core_security_security_service__WEBPACK_IMPORTED_MODULE_1__.SecurityService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__.Title));
};
AppComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({
  type: AppComponent,
  selectors: [["app-root"]],
  decls: 2,
  vars: 0,
  template: function AppComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](0, "app-loading-indicator")(1, "router-outlet");
    }
  },
  dependencies: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterOutlet, _shared_loading_indicator_loading_indicator_component__WEBPACK_IMPORTED_MODULE_2__.LoadingIndicatorComponent],
  styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 36747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule),
/* harmony export */   "createTranslateLoader": () => (/* binding */ createTranslateLoader)
/* harmony export */ });
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core/core.module */ 40294);
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shared/shared.module */ 44466);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/platform-browser */ 34497);
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/platform-browser/animations */ 37146);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ 90158);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ 55041);
/* harmony import */ var _layout_header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./layout/header/header.component */ 17876);
/* harmony import */ var _layout_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./layout/sidebar/sidebar.component */ 20129);
/* harmony import */ var _layout_app_layout_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./layout/app-layout/main-layout/main-layout.component */ 9899);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ngx-translate/core */ 38699);
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ngx-translate/http-loader */ 58319);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var ngx_scrollbar__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ngx-scrollbar */ 24203);
/* harmony import */ var _core_interceptor_http_interceptor_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @core/interceptor/http-interceptor.module */ 35980);
/* harmony import */ var _shared_loading_indicator_pending_interceptor_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shared/loading-indicator/pending-interceptor.module */ 96181);
/* harmony import */ var _core_services_window_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @core/services/window.service */ 11877);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-toastr */ 94817);
/* harmony import */ var _store_app_store_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./store/app-store.module */ 33007);
/* harmony import */ var _shared_loading_indicator_loading_indicator_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @shared/loading-indicator/loading-indicator.module */ 72025);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @environments/environment */ 92340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/core */ 22560);
























function createTranslateLoader(http) {
  return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_13__.TranslateHttpLoader(http, `${_environments_environment__WEBPACK_IMPORTED_MODULE_12__.environment.apiUrl}api/i18n/`);
}
class AppModule {}
AppModule.ɵfac = function AppModule_Factory(t) {
  return new (t || AppModule)();
};
AppModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵɵdefineNgModule"]({
  type: AppModule,
  bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__.AppComponent]
});
AppModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵɵdefineInjector"]({
  providers: [_core_services_window_service__WEBPACK_IMPORTED_MODULE_9__.WINDOW_PROVIDERS, {
    provide: _angular_common__WEBPACK_IMPORTED_MODULE_15__.APP_BASE_HREF,
    useValue: '/'
  }],
  imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_16__.BrowserModule, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_17__.BrowserAnimationsModule, _app_routing_module__WEBPACK_IMPORTED_MODULE_2__.AppRoutingModule, _angular_common_http__WEBPACK_IMPORTED_MODULE_18__.HttpClientModule, ngx_scrollbar__WEBPACK_IMPORTED_MODULE_19__.NgScrollbarModule, _ngx_translate_core__WEBPACK_IMPORTED_MODULE_20__.TranslateModule.forRoot({
    loader: {
      provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_20__.TranslateLoader,
      useFactory: createTranslateLoader,
      deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_18__.HttpClient]
    }
  }), _core_core_module__WEBPACK_IMPORTED_MODULE_0__.CoreModule, _shared_loading_indicator_loading_indicator_module__WEBPACK_IMPORTED_MODULE_11__.LoadingIndicatorModule, _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__.SharedModule, ngx_toastr__WEBPACK_IMPORTED_MODULE_21__.ToastrModule.forRoot(), _angular_common_http__WEBPACK_IMPORTED_MODULE_18__.HttpClientModule, _core_interceptor_http_interceptor_module__WEBPACK_IMPORTED_MODULE_7__.HttpInterceptorModule, _store_app_store_module__WEBPACK_IMPORTED_MODULE_10__.AppStoreModule, _shared_loading_indicator_pending_interceptor_module__WEBPACK_IMPORTED_MODULE_8__.PendingInterceptorModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵɵsetNgModuleScope"](AppModule, {
    declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__.AppComponent, _layout_header_header_component__WEBPACK_IMPORTED_MODULE_4__.HeaderComponent, _layout_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_5__.SidebarComponent, _layout_app_layout_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_6__.LayoutComponent],
    imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_16__.BrowserModule, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_17__.BrowserAnimationsModule, _app_routing_module__WEBPACK_IMPORTED_MODULE_2__.AppRoutingModule, _angular_common_http__WEBPACK_IMPORTED_MODULE_18__.HttpClientModule, ngx_scrollbar__WEBPACK_IMPORTED_MODULE_19__.NgScrollbarModule, _ngx_translate_core__WEBPACK_IMPORTED_MODULE_20__.TranslateModule, _core_core_module__WEBPACK_IMPORTED_MODULE_0__.CoreModule, _shared_loading_indicator_loading_indicator_module__WEBPACK_IMPORTED_MODULE_11__.LoadingIndicatorModule, _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__.SharedModule, ngx_toastr__WEBPACK_IMPORTED_MODULE_21__.ToastrModule, _angular_common_http__WEBPACK_IMPORTED_MODULE_18__.HttpClientModule, _core_interceptor_http_interceptor_module__WEBPACK_IMPORTED_MODULE_7__.HttpInterceptorModule, _store_app_store_module__WEBPACK_IMPORTED_MODULE_10__.AppStoreModule, _shared_loading_indicator_pending_interceptor_module__WEBPACK_IMPORTED_MODULE_8__.PendingInterceptorModule]
  });
})();

/***/ }),

/***/ 83607:
/*!***********************************!*\
  !*** ./src/app/base.component.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BaseComponent": () => (/* binding */ BaseComponent)
/* harmony export */ });
/* harmony import */ var SubSink__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! SubSink */ 99663);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


class BaseComponent {
  constructor() {
    this.sub$ = new SubSink__WEBPACK_IMPORTED_MODULE_0__.SubSink();
  }
  ngOnDestroy() {
    this.sub$.unsubscribe();
  }
}
BaseComponent.ɵfac = function BaseComponent_Factory(t) {
  return new (t || BaseComponent)();
};
BaseComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
  type: BaseComponent,
  selectors: [["app-base"]],
  decls: 0,
  vars: 0,
  template: function BaseComponent_Template(rf, ctx) {},
  encapsulation: 2
});

/***/ }),

/***/ 31539:
/*!*************************************************************!*\
  !*** ./src/app/company-profile/company-profile.resolver.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CompanyProfileResolver": () => (/* binding */ CompanyProfileResolver)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 10745);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 59295);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ 51353);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @environments/environment */ 92340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _company_profile_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./company-profile.service */ 91076);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);






class CompanyProfileResolver {
  constructor(companyProfileService, router) {
    this.companyProfileService = companyProfileService;
    this.router = router;
  }
  resolve(route, state) {
    return this.companyProfileService.getCompanyProfile().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.take)(1), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.mergeMap)(companyProfile => {
      if (companyProfile) {
        if (companyProfile.languages) {
          companyProfile.languages.forEach(lan => {
            lan.imageUrl = `${_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}${lan.imageUrl}`;
          });
        }
        return (0,rxjs__WEBPACK_IMPORTED_MODULE_4__.of)(companyProfile);
      } else {
        this.router.navigate(['/dashboard']);
        return null;
      }
    }));
  }
}
CompanyProfileResolver.ɵfac = function CompanyProfileResolver_Factory(t) {
  return new (t || CompanyProfileResolver)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](_company_profile_service__WEBPACK_IMPORTED_MODULE_1__.CompanyProfileService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.Router));
};
CompanyProfileResolver.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjectable"]({
  token: CompanyProfileResolver,
  factory: CompanyProfileResolver.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 91076:
/*!************************************************************!*\
  !*** ./src/app/company-profile/company-profile.service.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CompanyProfileService": () => (/* binding */ CompanyProfileService)
/* harmony export */ });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 53158);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/error-handler/common-http-error.service */ 48032);




class CompanyProfileService {
  constructor(http, commonHttpErrorService) {
    this.http = http;
    this.commonHttpErrorService = commonHttpErrorService;
  }
  getCompanyProfile() {
    const url = `companyProfile`;
    return this.http.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  updateCompanyProfile(companyProfile) {
    const url = `companyProfile`;
    return this.http.post(url, companyProfile).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
}
CompanyProfileService.ɵfac = function CompanyProfileService_Factory(t) {
  return new (t || CompanyProfileService)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_0__.CommonHttpErrorService));
};
CompanyProfileService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({
  token: CompanyProfileService,
  factory: CompanyProfileService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 95991:
/*!***************************************************************!*\
  !*** ./src/app/core/common-dialog/common-dialog.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CommonDialogComponent": () => (/* binding */ CommonDialogComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ 57822);




class CommonDialogComponent {
  constructor(dialogRef) {
    this.dialogRef = dialogRef;
  }
  clickHandler(data) {
    this.dialogRef.close(data);
  }
  closeDialog() {
    this.dialogRef.close();
  }
}
CommonDialogComponent.ɵfac = function CommonDialogComponent_Factory(t) {
  return new (t || CommonDialogComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__.MatDialogRef));
};
CommonDialogComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
  type: CommonDialogComponent,
  selectors: [["app-common-dialog"]],
  decls: 13,
  vars: 1,
  consts: [[1, "d-flex"], ["mat-dialog-title", ""], ["mat-icon-button", "", 1, "close-button", "ms-auto", 3, "click"], ["color", "warn", 1, "close-icon"], ["mat-dialog-actions", ""], [1, "btn", "btn-success", "btn-sm", "m-r-10", 3, "click"], [1, "fas", "fa-check"], [1, "btn", "btn-danger", "btn-sm", 3, "click"], [1, "fas", "fa-times-circle"]],
  template: function CommonDialogComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0)(1, "h2", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommonDialogComponent_Template_button_click_3_listener() {
        return ctx.closeDialog();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-icon", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "close");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4)(7, "button", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommonDialogComponent_Template_button_click_7_listener() {
        return ctx.clickHandler(true);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "i", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Yes");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommonDialogComponent_Template_button_click_10_listener() {
        return ctx.clickHandler(false);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " Cancel");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.primaryMessage);
    }
  },
  dependencies: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__.MatDialogTitle, _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__.MatDialogActions, _angular_material_button__WEBPACK_IMPORTED_MODULE_2__.MatIconButton, _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__.MatIcon],
  styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 44603:
/*!*************************************************************!*\
  !*** ./src/app/core/common-dialog/common-dialog.service.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CommonDialogService": () => (/* binding */ CommonDialogService)
/* harmony export */ });
/* harmony import */ var _common_dialog_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common-dialog.component */ 95991);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ 31484);



class CommonDialogService {
  constructor(dialog) {
    this.dialog = dialog;
    this.dialogConfig = {
      disableClose: false,
      width: '50%',
      height: '',
      position: {
        top: '',
        bottom: '',
        left: '',
        right: ''
      }
    };
  }
  deleteConformationDialog(message) {
    const dialogRef = this.dialog.open(_common_dialog_component__WEBPACK_IMPORTED_MODULE_0__.CommonDialogComponent, this.dialogConfig);
    dialogRef.componentInstance.primaryMessage = message;
    return dialogRef.afterClosed();
  }
}
CommonDialogService.ɵfac = function CommonDialogService_Factory(t) {
  return new (t || CommonDialogService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__.MatDialog));
};
CommonDialogService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
  token: CommonDialogService,
  factory: CommonDialogService.ɵfac
});

/***/ }),

/***/ 40294:
/*!*************************************!*\
  !*** ./src/app/core/core.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CoreModule": () => (/* binding */ CoreModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/tooltip */ 6896);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _shared_loading_indicator_loading_indicator_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared/loading-indicator/loading-indicator.module */ 72025);
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @shared/shared.module */ 44466);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _common_dialog_common_dialog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./common-dialog/common-dialog.service */ 44603);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _common_dialog_common_dialog_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./common-dialog/common-dialog.component */ 95991);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);












class CoreModule {}
CoreModule.ɵfac = function CoreModule_Factory(t) {
  return new (t || CoreModule)();
};
CoreModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({
  type: CoreModule
});
CoreModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({
  providers: [_common_dialog_common_dialog_service__WEBPACK_IMPORTED_MODULE_2__.CommonDialogService],
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule, _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__.MatDialogModule, _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule, _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__.SharedModule, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__.MatTooltipModule, _shared_loading_indicator_loading_indicator_module__WEBPACK_IMPORTED_MODULE_0__.LoadingIndicatorModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_10__.MatIconModule, _angular_material_button__WEBPACK_IMPORTED_MODULE_11__.MatButtonModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](CoreModule, {
    declarations: [_common_dialog_common_dialog_component__WEBPACK_IMPORTED_MODULE_3__.CommonDialogComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule, _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__.MatDialogModule, _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule, _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__.SharedModule, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__.MatTooltipModule, _shared_loading_indicator_loading_indicator_module__WEBPACK_IMPORTED_MODULE_0__.LoadingIndicatorModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_10__.MatIconModule, _angular_material_button__WEBPACK_IMPORTED_MODULE_11__.MatButtonModule]
  });
})();

/***/ }),

/***/ 47954:
/*!***********************************************************!*\
  !*** ./src/app/core/domain-classes/document-operation.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DocumentOperation": () => (/* binding */ DocumentOperation)
/* harmony export */ });
var DocumentOperation;
(function (DocumentOperation) {
  DocumentOperation["Read"] = "Read";
  DocumentOperation["Created"] = "Created";
  DocumentOperation["Modified"] = "Modified";
  DocumentOperation["Deleted"] = "Deleted";
  DocumentOperation["Add_Permission"] = "Add_Permission";
  DocumentOperation["Remove_Permission"] = "Remove_Permission";
  DocumentOperation["Send_Email"] = "Send_Email";
  DocumentOperation["Download"] = "Download";
})(DocumentOperation || (DocumentOperation = {}));

/***/ }),

/***/ 32760:
/*!***********************************************************!*\
  !*** ./src/app/core/domain-classes/reminder-frequency.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReminderFrequency": () => (/* binding */ ReminderFrequency),
/* harmony export */   "reminderFrequencies": () => (/* binding */ reminderFrequencies)
/* harmony export */ });
class ReminderFrequency {}
const reminderFrequencies = [{
  id: 0,
  name: 'Daily'
}, {
  id: 1,
  name: 'Weekly'
}, {
  id: 2,
  name: 'Monthly'
}, {
  id: 3,
  name: 'Quarterly'
}, {
  id: 4,
  name: 'Half Yearly'
}, {
  id: 5,
  name: 'Yearly'
}];

/***/ }),

/***/ 61301:
/*!**************************************************!*\
  !*** ./src/app/core/domain-classes/user-auth.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Authorisation": () => (/* binding */ Authorisation),
/* harmony export */   "User": () => (/* binding */ User),
/* harmony export */   "UserAuth": () => (/* binding */ UserAuth)
/* harmony export */ });
class UserAuth {
  constructor() {
    this.isAuthenticated = false;
    this.claims = [];
  }
}
class Authorisation {}
class User {}

/***/ }),

/***/ 48032:
/*!*****************************************************************!*\
  !*** ./src/app/core/error-handler/common-http-error.service.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CommonHttpErrorService": () => (/* binding */ CommonHttpErrorService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 25474);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


class CommonHttpErrorService {
  constructor() {}
  handleError(httpErrorResponse) {
    const errors = [];
    for (const [, value] of Object.entries(httpErrorResponse.error)) {
      errors.push(value);
    }
    const customError = {
      statusText: httpErrorResponse.statusText,
      code: httpErrorResponse.status,
      messages: errors,
      friendlyMessage: 'Error from service',
      error: httpErrorResponse.error
    };
    console.error(httpErrorResponse);
    return (0,rxjs__WEBPACK_IMPORTED_MODULE_0__.throwError)(customError);
  }
}
CommonHttpErrorService.ɵfac = function CommonHttpErrorService_Factory(t) {
  return new (t || CommonHttpErrorService)();
};
CommonHttpErrorService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
  token: CommonHttpErrorService,
  factory: CommonHttpErrorService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 35980:
/*!*************************************************************!*\
  !*** ./src/app/core/interceptor/http-interceptor.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HttpInterceptorModule": () => (/* binding */ HttpInterceptorModule),
/* harmony export */   "HttpRequestInterceptor": () => (/* binding */ HttpRequestInterceptor)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @environments/environment */ 92340);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 19337);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ 94817);
/* harmony import */ var _core_security_security_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/security/security.service */ 40130);








class HttpRequestInterceptor {
  /**
   *
   */
  constructor(router, toastrService, securityService) {
    this.router = router;
    this.toastrService = toastrService;
    this.securityService = securityService;
  }
  intercept(req, next) {
    const token = localStorage.getItem('bearerToken');
    const baseUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl;
    if (req.url.lastIndexOf('i18n') > -1) {
      return next.handle(req);
    }
    let url = req.url.lastIndexOf('api') > -1 ? req.url : 'api/' + req.url;
    const lastChar = url.substring(url.length - 1);
    if (lastChar == '/') {
      url = url.substring(0, url.length - 1);
    }
    if (token) {
      const newReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + token),
        url: `${baseUrl}${url}`
      });
      return next.handle(newReq).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.tap)(
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      () => {}, err => {
        if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpErrorResponse) {
          if (err.status === 401) {
            this.securityService.logout();
            this.router.navigate(['login']);
          } else if (err.status === 403) {
            this.toastrService.error(`you don't have permission to perform this access.`);
          } else if (err.error && Array.isArray(err.error) && err.error.length >= 0) {
            this.toastrService.error(err.error[0]);
          } else if (err.error && Object.entries(err.error)?.length > 0) {
            const errors = [];
            for (const [key, value] of Object.entries(err.error)) {
              if (value) {
                errors.push(value);
              }
            }
            this.toastrService.error(errors.join('\n'));
          } else if (err.message) {
            this.toastrService.error(err.message);
          }
        }
      }));
    } else {
      const newReq = req.clone({
        url: `${baseUrl}${url}`
      });
      return next.handle(newReq).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.tap)(
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      () => {}, err => {
        if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpErrorResponse) {
          if (err.status === 401) {
            this.securityService.logout();
            this.router.navigate(['login']);
          } else if (err.status === 403) {
            this.toastrService.error(`you don't have permission to perform this access.`);
          } else if (err.status === 409) {
            this.toastrService.error(err.error.messages[0]);
          }
        }
      }));
    }
  }
}
HttpRequestInterceptor.ɵfac = function HttpRequestInterceptor_Factory(t) {
  return new (t || HttpRequestInterceptor)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_6__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_core_security_security_service__WEBPACK_IMPORTED_MODULE_1__.SecurityService));
};
HttpRequestInterceptor.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({
  token: HttpRequestInterceptor,
  factory: HttpRequestInterceptor.ɵfac
});
class HttpInterceptorModule {}
HttpInterceptorModule.ɵfac = function HttpInterceptorModule_Factory(t) {
  return new (t || HttpInterceptorModule)();
};
HttpInterceptorModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({
  type: HttpInterceptorModule
});
HttpInterceptorModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({
  providers: [{
    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HTTP_INTERCEPTORS,
    useClass: HttpRequestInterceptor,
    multi: true
  }]
});

/***/ }),

/***/ 50319:
/*!*********************************************!*\
  !*** ./src/app/core/security/auth.guard.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthGuard": () => (/* binding */ AuthGuard)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _security_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./security.service */ 40130);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ 94817);




class AuthGuard {
  constructor(securityService, router, toastr) {
    this.securityService = securityService;
    this.router = router;
    this.toastr = toastr;
  }
  canActivate(next,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  state) {
    if (this.securityService.isUserAuthenticate()) {
      const claimType = next.data["claimType"];
      if (claimType) {
        if (!this.securityService.hasClaim(claimType)) {
          this.toastr.error(`You don't have right to access this page`);
          this.router.navigate(['/']);
          return false;
        }
      }
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
  canActivateChild(next, state) {
    // Get property name on security object to check
    // let claimType: string = next.data['claimType'];
    if (this.securityService.isUserAuthenticate()) {
      const claimType = next.data["claimType"];
      if (claimType) {
        if (!this.securityService.hasClaim(claimType)) {
          this.toastr.error(`You don't have right to access this page `);
          return false;
        }
      }
      return true;
    } else {
      this.router.navigate(['login'], {
        queryParams: {
          returnUrl: state.url
        }
      });
      return false;
    }
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  canLoad(route) {
    if (this.securityService.isUserAuthenticate()) {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
}
AuthGuard.ɵfac = function AuthGuard_Factory(t) {
  return new (t || AuthGuard)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_security_service__WEBPACK_IMPORTED_MODULE_0__.SecurityService), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_3__.ToastrService));
};
AuthGuard.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
  token: AuthGuard,
  factory: AuthGuard.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 40130:
/*!***************************************************!*\
  !*** ./src/app/core/security/security.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SecurityService": () => (/* binding */ SecurityService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 76317);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 19337);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 53158);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 21339);
/* harmony import */ var _domain_classes_user_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../domain-classes/user-auth */ 61301);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @environments/environment */ 92340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _core_services_clone_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @core/services/clone.service */ 28265);
/* harmony import */ var _error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../error-handler/common-http-error.service */ 48032);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 60124);









class SecurityService {
  get SecurityObject() {
    return this.securityObject$.asObservable();
  }
  get companyProfile() {
    return this._companyProfile$;
  }
  constructor(http, clonerService, commonHttpErrorService, router) {
    this.http = http;
    this.clonerService = clonerService;
    this.commonHttpErrorService = commonHttpErrorService;
    this.router = router;
    this.securityObject = new _domain_classes_user_auth__WEBPACK_IMPORTED_MODULE_0__.UserAuth();
    this.securityObject$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.BehaviorSubject(null);
    this._companyProfile$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.BehaviorSubject(null);
  }
  isUserAuthenticate() {
    if (this.securityObject?.user?.userName && this.securityObject?.authorisation?.token) {
      setTimeout(() => {
        this.refreshToken();
      }, 1000);
      return true;
    } else {
      return this.parseSecurityObj();
    }
  }
  login(entity) {
    // Initialize security object
    // this.resetSecurityObject();
    return this.http.post('auth/login', entity).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.tap)(resp => {
      this.tokenTime = new Date();
      this.securityObject = this.clonerService.deepClone(resp);
      this.securityObject.tokenTime = new Date();
      localStorage.setItem('authObj', JSON.stringify(this.securityObject));
      localStorage.setItem('bearerToken', this.securityObject.authorisation.token);
      this.securityObject$.next(resp);
      this.refreshToken();
    })).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.catchError)(this.commonHttpErrorService.handleError));
  }
  refreshToken() {
    const currentDate = new Date();
    currentDate.setMinutes(currentDate.getMinutes() - _environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.tokenExpiredTimeInMin);
    let diffTime;
    if (!this.clearTimeOutData) {
      clearTimeout(this.clearTimeOutData);
    }
    if (!this.tokenTime) {
      diffTime = 1000;
      this.tokenTime = new Date();
    } else {
      diffTime = Math.abs(this.tokenTime.getTime() - currentDate.getTime());
    }
    this.clearTimeOutData = setTimeout(() => {
      clearTimeout(this.clearTimeOutData);
      this.refresh().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.delay)(1000)).subscribe(userAuth => {
        this.tokenTime = new Date();
        this.securityObject = this.clonerService.deepClone(userAuth);
        this.securityObject.tokenTime = new Date();
        localStorage.setItem('authObj', JSON.stringify(this.securityObject));
        localStorage.setItem('bearerToken', this.securityObject.authorisation.token);
        this.securityObject$.next(userAuth);
        this.refreshToken();
      });
    }, diffTime);
  }
  refresh() {
    return this.http.post('auth/refresh', {}).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.tap)(resp => {
      this.securityObject = this.clonerService.deepClone(resp);
      this.securityObject.tokenTime = new Date();
      localStorage.setItem('authObj', JSON.stringify(this.securityObject));
      localStorage.setItem('bearerToken', this.securityObject.authorisation.token);
      this.securityObject$.next(resp);
    })).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.catchError)(this.commonHttpErrorService.handleError));
  }
  parseSecurityObj() {
    const securityObjectString = localStorage.getItem('authObj');
    if (!securityObjectString) {
      return false;
    }
    const secuObj = JSON.parse(securityObjectString);
    this.tokenTime = new Date(secuObj.tokenTime);
    this.securityObject = this.clonerService.deepClone(secuObj);
    if (this.securityObject.user.userName && this.securityObject.authorisation.token) {
      this.securityObject$.next(this.securityObject);
      return true;
    }
    return false;
  }
  logout() {
    this.resetSecurityObject();
  }
  updateProfile(companyProfile) {
    if (companyProfile.logoUrl) {
      companyProfile.logoUrl = `${_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.apiUrl}${companyProfile.logoUrl}`;
    }
    if (companyProfile.bannerUrl) {
      companyProfile.bannerUrl = `${_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.apiUrl}${companyProfile.bannerUrl}`;
    }
    this._companyProfile$.next(companyProfile);
  }
  resetSecurityObject() {
    this.securityObject = {
      isAuthenticated: false,
      claims: [],
      user: {
        id: '',
        firstName: '',
        lastName: '',
        phoneNumber: '',
        userName: '',
        email: ''
      },
      status: '',
      authorisation: {
        token: '',
        type: ''
      },
      tokenTime: new Date()
    };
    localStorage.removeItem('authObj');
    localStorage.removeItem('bearerToken');
    this.securityObject$.next(null);
    this.router.navigate(['/login']);
  }
  // This method can be called a couple of different ways
  // *hasClaim="'claimType'"  // Assumes claimValue is true
  // *hasClaim="'claimType:value'"  // Compares claimValue to value
  // *hasClaim="['claimType1','claimType2:value','claimType3']"
  // tslint:disable-next-line: typedef
  hasClaim(claimType, claimValue) {
    let ret = false;
    // See if an array of values was passed in.
    if (typeof claimType === 'string') {
      ret = this.isClaimValid(claimType, claimValue);
    } else {
      const claims = claimType;
      if (claims) {
        // tslint:disable-next-line: prefer-for-of
        for (let index = 0; index < claims.length; index++) {
          ret = this.isClaimValid(claims[index]);
          // If one is successful, then let them in
          if (ret) {
            break;
          }
        }
      }
    }
    // return true;
    return ret;
  }
  isClaimValid(claimType, claimValue) {
    let ret = false;
    let auth = null;
    // Retrieve security object
    auth = this.securityObject;
    if (auth) {
      // See if the claim type has a value
      // *hasClaim="'claimType:value'"
      if (claimType.indexOf(':') >= 0) {
        const words = claimType.split(':');
        claimType = words[0].toLowerCase();
        claimValue = words[1];
      } else {
        claimType = claimType.toLowerCase();
        // Either get the claim value, or assume 'true'
        claimValue = claimValue ? claimValue : 'true';
      }
      // Attempt to find the claim
      ret = auth.claims.find(c => c.toLowerCase() == claimType) != null;
    }
    return ret;
  }
  getUserDetail() {
    const userJson = localStorage.getItem('authObj');
    return JSON.parse(userJson);
  }
  setUserDetail(user) {
    this.securityObject = this.clonerService.deepClone(user);
    localStorage.setItem('authObj', JSON.stringify(this.securityObject));
  }
}
SecurityService.ɵfac = function SecurityService_Factory(t) {
  return new (t || SecurityService)(_angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_9__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵinject"](_core_services_clone_service__WEBPACK_IMPORTED_MODULE_2__.ClonerService), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵinject"](_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_3__.CommonHttpErrorService), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_10__.Router));
};
SecurityService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdefineInjectable"]({
  token: SecurityService,
  factory: SecurityService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 66214:
/*!******************************************************!*\
  !*** ./src/app/core/services/breakpoints.service.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BreakpointsService": () => (/* binding */ BreakpointsService)
/* harmony export */ });
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/layout */ 83278);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 76317);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);




class BreakpointsService {
  constructor(breakpointObserver) {
    this.breakpointObserver = breakpointObserver;
    this.isMobile$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject(false);
    this.isTablet$ = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject(false);
    this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__.Breakpoints.Handset).subscribe(result => {
      this.isMobile$.next(result.matches);
    });
    this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__.Breakpoints.Tablet).subscribe(result => {
      this.isTablet$.next(result.matches);
    });
  }
  observe(value) {
    return this.breakpointObserver.observe(value);
  }
}
BreakpointsService.ɵfac = function BreakpointsService_Factory(t) {
  return new (t || BreakpointsService)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__.BreakpointObserver));
};
BreakpointsService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({
  token: BreakpointsService,
  factory: BreakpointsService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 28265:
/*!************************************************!*\
  !*** ./src/app/core/services/clone.service.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ClonerService": () => (/* binding */ ClonerService)
/* harmony export */ });
/* harmony import */ var clone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! clone */ 75482);
/* harmony import */ var clone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(clone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


class ClonerService {
  deepClone(value) {
    return clone__WEBPACK_IMPORTED_MODULE_0__(value);
  }
}
ClonerService.ɵfac = function ClonerService_Factory(t) {
  return new (t || ClonerService)();
};
ClonerService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
  token: ClonerService,
  factory: ClonerService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 50690:
/*!*************************************************!*\
  !*** ./src/app/core/services/common.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CommonService": () => (/* binding */ CommonService)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 10745);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 53158);
/* harmony import */ var _core_domain_classes_reminder_frequency__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/domain-classes/reminder-frequency */ 32760);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/error-handler/common-http-error.service */ 48032);







class CommonService {
  constructor(httpClient, commonHttpErrorService) {
    this.httpClient = httpClient;
    this.commonHttpErrorService = commonHttpErrorService;
  }
  getUsers() {
    const url = `user`;
    return this.httpClient.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
  getUsersForDropdown() {
    const url = `user-dropdown`;
    return this.httpClient.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
  getRoles() {
    const url = `role`;
    return this.httpClient.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
  getRolesForDropdown() {
    const url = 'role-dropdown';
    return this.httpClient.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
  getMyReminder(id) {
    const url = `reminder/${id}/myreminder`;
    return this.httpClient.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
  getReminder(id) {
    const url = `reminder/${id}`;
    return this.httpClient.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
  addDocumentAuditTrail(documentAuditTrail) {
    const url = `documentAuditTrail`;
    return this.httpClient.post(url, documentAuditTrail).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
    //return this.httpClient.post<DocumentAuditTrail>('documentAuditTrail',documentAuditTrail);
  }

  downloadDocument(documentId, isVersion) {
    const url = `document/${documentId}/download/${isVersion} `;
    return this.httpClient.get(url, {
      reportProgress: true,
      observe: 'events',
      responseType: 'blob'
    });
  }
  isDownloadFlag(documentId, isPermission) {
    const url = `document/${documentId}/isDownloadFlag/isPermission/${isPermission}`;
    return this.httpClient.get(url);
  }
  getDocumentToken(documentId) {
    const url = `documentToken/${documentId}/token`;
    return this.httpClient.get(url);
  }
  deleteDocumentToken(token) {
    const url = `documentToken/${token}`;
    return this.httpClient.delete(url);
  }
  readDocument(documentId, isVersion) {
    const url = `document/${documentId}/readText/${isVersion}`;
    return this.httpClient.get(url);
  }
  getReminderFrequency() {
    return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)(_core_domain_classes_reminder_frequency__WEBPACK_IMPORTED_MODULE_0__.reminderFrequencies);
  }
  getAllRemindersForCurrentUser(resourceParams) {
    const url = 'reminder/all/currentuser';
    const customParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpParams().set('fields', resourceParams.fields ? resourceParams.fields : '').set('orderBy', resourceParams.orderBy ? resourceParams.orderBy : '').set('pageSize', resourceParams.pageSize.toString()).set('skip', resourceParams.skip.toString()).set('searchQuery', resourceParams.searchQuery ? resourceParams.searchQuery : '').set('subject', resourceParams.subject ? resourceParams.subject : '').set('message', resourceParams.message ? resourceParams.message : '').set('frequency', resourceParams.frequency ? resourceParams.frequency : '');
    return this.httpClient.get(url, {
      params: customParams,
      observe: 'response'
    });
  }
  deleteReminderCurrentUser(reminderId) {
    const url = `reminder/currentuser/${reminderId}`;
    return this.httpClient.delete(url);
  }
}
CommonService.ɵfac = function CommonService_Factory(t) {
  return new (t || CommonService)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](_core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_1__.CommonHttpErrorService));
};
CommonService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjectable"]({
  token: CommonService,
  factory: CommonService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 77524:
/*!***************************************************!*\
  !*** ./src/app/core/services/language.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LanguageService": () => (/* binding */ LanguageService)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngx-translate/core */ 38699);


class LanguageService {
  constructor(translate) {
    this.translate = translate;
    this.languages = ['en', 'es', 'de'];
    let browserLang;
    translate.addLangs(this.languages);
    if (localStorage.getItem('lang')) {
      browserLang = localStorage.getItem('lang');
    } else {
      browserLang = translate.getBrowserLang();
    }
    translate.use(browserLang.match(/en|es|de/) ? browserLang : 'en');
  }
  setLanguage(lang) {
    this.translate.use(lang);
    localStorage.setItem('lang', lang);
  }
}
LanguageService.ɵfac = function LanguageService_Factory(t) {
  return new (t || LanguageService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__.TranslateService));
};
LanguageService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
  token: LanguageService,
  factory: LanguageService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 16107:
/*!******************************************************!*\
  !*** ./src/app/core/services/translation.service.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TranslationService": () => (/* binding */ TranslationService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 10745);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ 38699);



const LOCALIZATION_LOCAL_STORAGE_KEY = 'language';
class TranslationService {
  constructor(translate) {
    this.translate = translate;
    // Private properties
    this.langIds = [];
  }
  loadTranslations(...args) {
    const locales = [...args];
    locales.forEach(locale => {
      // use setTranslation() with the third argument set to true
      // to append translations instead of replacing them
      this.translate.setTranslation(locale.lang, locale.data, true);
      this.langIds.push(locale.lang);
    });
    // add new languages to the list
    this.translate.addLangs(this.langIds);
  }
  setLanguage(lang) {
    try {
      if (lang) {
        localStorage.setItem(LOCALIZATION_LOCAL_STORAGE_KEY, lang);
        return this.translate.use(lang);
      }
    } catch {
      return (0,rxjs__WEBPACK_IMPORTED_MODULE_0__.of)(null);
    }
    return (0,rxjs__WEBPACK_IMPORTED_MODULE_0__.of)(null);
  }
  removeLanguage() {
    try {
      localStorage.removeItem(LOCALIZATION_LOCAL_STORAGE_KEY);
    } catch {}
  }
  /**
   * Returns selected language
   */
  getSelectedLanguage() {
    try {
      if (localStorage) {
        return localStorage.getItem(LOCALIZATION_LOCAL_STORAGE_KEY) || this.translate.getDefaultLang();
      }
    } catch {
      return null;
    }
  }
  getValue(key) {
    return this.translate.instant(key);
  }
}
TranslationService.ɵfac = function TranslationService_Factory(t) {
  return new (t || TranslationService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__.TranslateService));
};
TranslationService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
  token: TranslationService,
  factory: TranslationService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 11877:
/*!*************************************************!*\
  !*** ./src/app/core/services/window.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BrowserWindowRef": () => (/* binding */ BrowserWindowRef),
/* harmony export */   "WINDOW": () => (/* binding */ WINDOW),
/* harmony export */   "WINDOW_PROVIDERS": () => (/* binding */ WINDOW_PROVIDERS),
/* harmony export */   "WindowRef": () => (/* binding */ WindowRef),
/* harmony export */   "browserWindowProvider": () => (/* binding */ browserWindowProvider),
/* harmony export */   "windowFactory": () => (/* binding */ windowFactory),
/* harmony export */   "windowProvider": () => (/* binding */ windowProvider)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* eslint-disable @typescript-eslint/ban-types */


/* Create a new injection token for injecting the window into a component. */
const WINDOW = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.InjectionToken('WindowToken');
/* Define abstract class for obtaining reference to the global window object. */
class WindowRef {
  get nativeWindow() {
    throw new Error('Not implemented.');
  }
}
/* Define class that implements the abstract class and returns the native window object. */
class BrowserWindowRef extends WindowRef {
  constructor() {
    super();
  }
  get nativeWindow() {
    return window;
  }
}
/* Create an factory function that returns the native window object. */
function windowFactory(browserWindowRef, platformId) {
  if ((0,_angular_common__WEBPACK_IMPORTED_MODULE_1__.isPlatformBrowser)(platformId)) {
    return browserWindowRef.nativeWindow;
  }
  return new Object();
}
/* Create a injectable provider for the WindowRef token that uses the BrowserWindowRef class. */
const browserWindowProvider = {
  provide: WindowRef,
  useClass: BrowserWindowRef
};
/* Create an injectable provider that uses the windowFactory function for returning the native window object. */
const windowProvider = {
  provide: WINDOW,
  useFactory: windowFactory,
  deps: [WindowRef, _angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID]
};
/* Create an array of providers. */
const WINDOW_PROVIDERS = [browserWindowProvider, windowProvider];

/***/ }),

/***/ 98447:
/*!*********************************************!*\
  !*** ./src/app/core/utils/random-string.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "randomString": () => (/* binding */ randomString)
/* harmony export */ });
function randomString(length = 36) {
  let random = '';
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < length; i++) {
    random += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return random;
}

/***/ }),

/***/ 9899:
/*!************************************************************************!*\
  !*** ./src/app/layout/app-layout/main-layout/main-layout.component.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LayoutComponent": () => (/* binding */ LayoutComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../header/header.component */ 17876);
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../sidebar/sidebar.component */ 20129);




class LayoutComponent {
  constructor() {}
}
LayoutComponent.ɵfac = function LayoutComponent_Factory(t) {
  return new (t || LayoutComponent)();
};
LayoutComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
  type: LayoutComponent,
  selectors: [["app-main-layout"]],
  decls: 3,
  vars: 0,
  template: function LayoutComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "app-header")(1, "app-sidebar")(2, "router-outlet");
    }
  },
  dependencies: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterOutlet, _header_header_component__WEBPACK_IMPORTED_MODULE_0__.HeaderComponent, _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_1__.SidebarComponent],
  encapsulation: 2
});

/***/ }),

/***/ 17876:
/*!***************************************************!*\
  !*** ./src/app/layout/header/header.component.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeaderComponent": () => (/* binding */ HeaderComponent)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _core_services_window_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/services/window.service */ 11877);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _core_services_language_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/services/language.service */ 77524);
/* harmony import */ var _core_security_security_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @core/security/security.service */ 40130);
/* harmony import */ var _core_services_translation_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @core/services/translation.service */ 16107);
/* harmony import */ var src_app_notification_notification_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/notification/notification.service */ 75769);
/* harmony import */ var ngx_scrollbar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-scrollbar */ 24203);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/menu */ 88589);
/* harmony import */ var _shared_components_feather_icons_feather_icons_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/components/feather-icons/feather-icons.component */ 61676);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ngx-translate/core */ 38699);
/* harmony import */ var _shared_pipes_truncate_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/pipes/truncate.pipe */ 52821);
/* harmony import */ var _shared_pipes_utc_to_localtime_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/pipes/utc-to-localtime.pipe */ 18017);
















function HeaderComponent_img_7_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](0, "img", 45);
  }
}
function HeaderComponent_img_8_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](0, "img", 46);
  }
  if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("src", ctx_r2.logoImage, _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵsanitizeUrl"]);
  }
}
const _c0 = function (a0) {
  return {
    "active": a0
  };
};
function HeaderComponent_div_20_Template(rf, ctx) {
  if (rf & 1) {
    const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](0, "div", 47)(1, "button", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵlistener"]("click", function HeaderComponent_div_20_Template_button_click_1_listener() {
      const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵrestoreView"](_r11);
      const item_r9 = restoredCtx.$implicit;
      const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵresetView"](ctx_r10.setNewLanguageRefresh(item_r9.code));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](2, "img", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](3, "span", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()()();
  }
  if (rf & 2) {
    const item_r9 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpureFunction1"](3, _c0, item_r9.active === true));
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpropertyInterpolate"]("src", item_r9.imageUrl, _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate"](item_r9.name);
  }
}
function HeaderComponent_span_24_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](0, "span", 51);
  }
}
function HeaderComponent_button_39_span_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](0, "span", 57)(1, "i", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](2, "description");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()();
  }
}
function HeaderComponent_button_39_span_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](0, "span", 57)(1, "i", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](2, "event_available");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()();
  }
}
function HeaderComponent_button_39_ng_container_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](1, "span", 59)(2, "b")(3, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipe"](5, "limitTo");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](6, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipe"](8, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const notification_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipeBind2"](5, 2, notification_r12.documentName, "25"), "");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipeBind1"](8, 5, "DOCUMENT_PERMISSION_GRANTED_TO_YOU"), " ");
  }
}
function HeaderComponent_button_39_ng_container_5_ng_container_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipe"](2, "limitTo");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const notification_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate1"](" ::", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipeBind2"](2, 1, notification_r12.documentName, "25"), " ");
  }
}
function HeaderComponent_button_39_ng_container_5_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](1, "small", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](3, HeaderComponent_button_39_ng_container_5_ng_container_3_Template, 3, 4, "ng-container", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const notification_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate1"]("", notification_r12.message, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngIf", notification_r12.documentId);
  }
}
function HeaderComponent_button_39_Template(rf, ctx) {
  if (rf & 1) {
    const _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](0, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵlistener"]("click", function HeaderComponent_button_39_Template_button_click_0_listener() {
      const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵrestoreView"](_r22);
      const notification_r12 = restoredCtx.$implicit;
      const ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵresetView"](ctx_r21.viewNotification(notification_r12));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](1, HeaderComponent_button_39_span_1_Template, 3, 0, "span", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](2, HeaderComponent_button_39_span_2_Template, 3, 0, "span", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](3, "span", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](4, HeaderComponent_button_39_ng_container_4_Template, 9, 7, "ng-container", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](5, HeaderComponent_button_39_ng_container_5_Template, 4, 2, "ng-container", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](6, "span", 55)(7, "i", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](8, "access_time");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipe"](10, "utcToLocalTime");
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()()();
  }
  if (rf & 2) {
    const notification_r12 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngIf", notification_r12.documentId);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngIf", !notification_r12.documentId);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngIf", !notification_r12.message && notification_r12.documentId);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngIf", notification_r12.message);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipeBind2"](10, 5, notification_r12.createdDate, "shortDate"), " ");
  }
}
const _c1 = function () {
  return ["/"];
};
const _c2 = function () {
  return ["/notifications"];
};
class HeaderComponent {
  constructor(document, window, renderer, elementRef, router, languageService, securityService, translationService, cd, notificationService) {
    this.document = document;
    this.window = window;
    this.renderer = renderer;
    this.elementRef = elementRef;
    this.router = router;
    this.languageService = languageService;
    this.securityService = securityService;
    this.translationService = translationService;
    this.cd = cd;
    this.notificationService = notificationService;
    this.isNavbarCollapsed = true;
    this.isNavbarShow = true;
    this.countryName = [];
    this.isFullScreen = false;
    this.appUserAuth = null;
    this.newNotificationCount = 0;
    this.notifications = [];
    this.refereshReminderTimeInMinute = 10;
    this.logoImage = '';
    this.isUnReadNotification = false;
    this.languages = [];
  }
  onWindowScroll() {
    this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    // if (offset > 50) {
    //   this.isNavbarShow = true;
    // } else {
    //   this.isNavbarShow = false;
    // }
  }

  ngOnInit() {
    this.setTopLogAndName();
    this.getNotification();
    this.setDefaultLanguage();
    this.companyProfileSubscription();
  }
  companyProfileSubscription() {
    this.securityService.companyProfile.subscribe(profile => {
      if (profile) {
        this.logoImage = profile.logoUrl;
        this.languages = profile.languages;
        this.setDefaultLanguage();
      }
    });
  }
  setDefaultLanguage() {
    const lang = this.translationService.getSelectedLanguage();
    if (lang) {
      this.setLanguageWithRefresh(lang);
    }
  }
  setLanguageWithRefresh(lang) {
    this.languages.forEach(language => {
      if (language.code === lang) {
        language.active = true;
        this.language = language;
      } else {
        language.active = false;
      }
    });
    this.translationService.setLanguage(lang);
    this.defaultFlag = this.languages.find(c => c.code == lang)?.imageUrl;
  }
  setNewLanguageRefresh(lang) {
    this.translationService.setLanguage(lang).subscribe(response => {
      this.setLanguageWithRefresh(response['LANGUAGE']);
    });
  }
  setTopLogAndName() {
    this.securityService.SecurityObject.subscribe(c => {
      if (c) {
        this.appUserAuth = c;
      }
    });
  }
  getNotification() {
    if (!this.securityService.isUserAuthenticate()) {
      return;
    }
    this.notificationService.getNotification().subscribe(notifications => {
      this.newNotificationCount = notifications.filter(c => !c.isRead).length;
      this.notifications = notifications;
      this.isUnReadNotification = this.notifications.some(n => !n.isRead);
      this.cd.detectChanges();
      setTimeout(() => {
        this.getNotification();
      }, this.refereshReminderTimeInMinute * 60 * 1000);
    });
  }
  markAllAsReadNotification() {
    this.notificationService.markAllAsRead().subscribe(() => {
      this.getNotification();
    });
  }
  mobileMenuSidebarOpen(event, className) {
    const hasClass = event.target.classList.contains(className);
    if (hasClass) {
      this.renderer.removeClass(this.document.body, className);
    } else {
      this.renderer.addClass(this.document.body, className);
    }
  }
  callSidemenuCollapse() {
    const hasClass = this.document.body.classList.contains('side-closed');
    if (hasClass) {
      this.renderer.removeClass(this.document.body, 'side-closed');
      this.renderer.removeClass(this.document.body, 'submenu-closed');
    } else {
      this.renderer.addClass(this.document.body, 'side-closed');
      this.renderer.addClass(this.document.body, 'submenu-closed');
    }
  }
  viewNotification(notification) {
    if (!notification.isRead) {
      this.markAsReadNotification(notification.id);
    }
    if (notification.documentId) {
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['reminders']);
    }
  }
  markAsReadNotification(id) {
    this.notificationService.markAsRead(id).subscribe(() => {
      this.getNotification();
    });
  }
  onProfileClick() {
    this.router.navigate(['my-profile']);
  }
  logout() {
    this.securityService.logout();
  }
}
HeaderComponent.ɵfac = function HeaderComponent_Factory(t) {
  return new (t || HeaderComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_9__.DOCUMENT), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](_core_services_window_service__WEBPACK_IMPORTED_MODULE_0__.WINDOW), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_8__.Renderer2), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_8__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_10__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](_core_services_language_service__WEBPACK_IMPORTED_MODULE_1__.LanguageService), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](_core_security_security_service__WEBPACK_IMPORTED_MODULE_2__.SecurityService), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](_core_services_translation_service__WEBPACK_IMPORTED_MODULE_3__.TranslationService), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_8__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdirectiveInject"](src_app_notification_notification_service__WEBPACK_IMPORTED_MODULE_4__.NotificationService));
};
HeaderComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdefineComponent"]({
  type: HeaderComponent,
  selectors: [["app-header"]],
  hostBindings: function HeaderComponent_HostBindings(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵlistener"]("scroll", function HeaderComponent_scroll_HostBindingHandler() {
        return ctx.onWindowScroll();
      }, false, _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵresolveWindow"]);
    }
  },
  decls: 60,
  vars: 41,
  consts: [[1, "navbar", 3, "ngClass"], ["navbar", ""], [1, "container-fluid"], [1, "navbar-header"], ["href", "#", "onClick", "return false;", "aria-expanded", "false", 1, "navbar-toggle", "collapsed", 3, "click"], ["href", "#", "onClick", "return false;", 1, "bars", 3, "click"], [1, "navbar-brand", 3, "routerLink"], ["src", "assets/images/logo.png", "alt", "logo", "class", "logo-lg", 4, "ngIf"], ["alt", "logo", "class", "logo-lg", 3, "src", 4, "ngIf"], [1, "collapse", "navbar-collapse", 3, "ngClass"], [1, "pull-left", "collapse-menu-icon"], [1, "menuBtn"], ["mat-button", "", 1, "sidemenu-collapse", "nav-notification-icons", 3, "click"], [3, "icon"], [1, "nav", "navbar-nav", "navbar-right"], [1, "nav-item"], ["mat-button", "", 1, "lang-dropdown", "nav-notification-icons", 3, "matMenuTriggerFor"], ["height", "16", 3, "src"], [1, "lang-item-menu"], ["languagemenu", "matMenu"], ["class", "lang-item", 4, "ngFor", "ngForOf"], ["ngbDropdown", "", 1, "nav-item"], ["mat-button", "", 1, "nav-notification-icons", 3, "matMenuTriggerFor"], ["class", "label-count bg-orange", 4, "ngIf"], [1, "nfc-menu"], ["notificationMenu", "matMenu"], [1, "nfc-header"], [1, "mb-0"], [1, "nfc-mark-as-read", "cursor-pointer", 3, "click"], [1, "nfc-dropdown"], ["visibility", "hover", 2, "height", "350px"], [1, "noti-list", "header-menu"], [1, "menu"], ["mat-menu-item", "", 3, "click", 4, "ngFor", "ngForOf"], [1, "nfc-footer"], [1, "nfc-read-all", 3, "routerLink"], [1, "nav-item", "user_profile"], ["mat-button", "", 3, "matMenuTriggerFor"], ["ngbDropdownToggle", "", 1, ""], ["src", "assets/images/user.jpg", "width", "32", "height", "32", "alt", "User", 1, "rounded-circle"], [1, "profile-menu"], ["profilemenu", "matMenu"], [1, "noti-list"], [1, "user_dw_menu"], ["mat-menu-item", "", 3, "click"], ["src", "assets/images/logo.png", "alt", "logo", 1, "logo-lg"], ["alt", "logo", 1, "logo-lg", 3, "src"], [1, "lang-item"], ["mat-menu-item", "", 1, "dropdown-item", "lang-item-list", 3, "ngClass", "click"], ["height", "12", 1, "flag-img", 3, "src"], [1, "align-middle"], [1, "label-count", "bg-orange"], ["class", "table-img msg-user", 4, "ngIf"], [1, "menu-info"], [4, "ngIf"], [1, "menu-desc", "mt-1"], [1, "material-icons"], [1, "table-img", "msg-user"], [1, "material-icons-two-tone", "nfc-type-icon", "nfc-green"], [1, "menu-title"]],
  template: function HeaderComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](0, "nav", 0, 1)(2, "div", 2)(3, "div", 3)(4, "a", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵlistener"]("click", function HeaderComponent_Template_a_click_4_listener() {
        return ctx.isNavbarCollapsed = !ctx.isNavbarCollapsed;
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](5, "a", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵlistener"]("click", function HeaderComponent_Template_a_click_5_listener($event) {
        return ctx.mobileMenuSidebarOpen($event, "overlay-open");
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](6, "a", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](7, HeaderComponent_img_7_Template, 1, 0, "img", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](8, HeaderComponent_img_8_Template, 1, 1, "img", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](9, "div", 9)(10, "ul", 10)(11, "li", 11)(12, "button", 12);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_12_listener() {
        return ctx.callSidemenuCollapse();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](13, "app-feather-icons", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](14, "ul", 14)(15, "li", 15)(16, "button", 16);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](17, "img", 17);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](18, "mat-menu", 18, 19);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](20, HeaderComponent_div_20_Template, 5, 5, "div", 20);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](21, "li", 21)(22, "button", 22);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](23, "app-feather-icons", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](24, HeaderComponent_span_24_Template, 1, 0, "span", 23);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](25, "mat-menu", 24, 25)(27, "div", 26)(28, "h5", 27);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](29);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipe"](30, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](31, "a", 28);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵlistener"]("click", function HeaderComponent_Template_a_click_31_listener() {
        return ctx.markAllAsReadNotification();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](32);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipe"](33, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](34, "div", 29)(35, "ng-scrollbar", 30)(36, "div", 31)(37, "div", 32)(38, "div");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtemplate"](39, HeaderComponent_button_39_Template, 11, 8, "button", 33);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()()()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](40, "div", 34)(41, "a", 35);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](42);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipe"](43, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](44, "li", 36)(45, "button", 37)(46, "div", 38);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](47, "img", 39);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](48, "mat-menu", 40, 41)(50, "div", 42)(51, "div", 32)(52, "div", 43)(53, "button", 44);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_53_listener() {
        return ctx.onProfileClick();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](54, "app-feather-icons", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](55);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementStart"](56, "button", 44);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_56_listener() {
        return ctx.logout();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelement"](57, "app-feather-icons", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtext"](58);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipe"](59, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵelementEnd"]()()()()()()()()()();
    }
    if (rf & 2) {
      const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵreference"](19);
      const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵreference"](26);
      const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵreference"](49);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngClass", ctx.isNavbarShow ? "active" : "inactive");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](6);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpureFunction0"](39, _c1));
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngIf", !ctx.logoImage);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngIf", ctx.logoImage);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngClass", ctx.isNavbarCollapsed ? "" : "show");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵclassMap"]("header-icon");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("icon", "menu");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("matMenuTriggerFor", _r3);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpropertyInterpolate"]("src", ctx.defaultFlag, _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵsanitizeUrl"]);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngForOf", ctx.languages);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("matMenuTriggerFor", _r6);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵclassMap"]("header-icon");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("icon", "bell");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngIf", ctx.isUnReadNotification);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipeBind1"](30, 31, "NOTIFICATIONS"));
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipeBind1"](33, 33, "MARK_ALL_AS_READ"), " ");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](7);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("ngForOf", ctx.notifications);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpureFunction0"](40, _c2));
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipeBind1"](43, 35, "VIEW_ALL"), " ");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("matMenuTriggerFor", _r8);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](9);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵclassMap"]("user-menu-icons");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("icon", "user");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate2"](" ", ctx.appUserAuth == null ? null : ctx.appUserAuth.user == null ? null : ctx.appUserAuth.user.firstName, " ", ctx.appUserAuth == null ? null : ctx.appUserAuth.user == null ? null : ctx.appUserAuth.user.lastName, " ");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵclassMap"]("user-menu-icons");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵproperty"]("icon", "log-out");
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵpipeBind1"](59, 37, "LOGOUT"), " ");
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_9__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_9__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_9__.NgIf, _angular_router__WEBPACK_IMPORTED_MODULE_10__.RouterLink, ngx_scrollbar__WEBPACK_IMPORTED_MODULE_11__.NgScrollbar, _angular_material_button__WEBPACK_IMPORTED_MODULE_12__.MatButton, _angular_material_menu__WEBPACK_IMPORTED_MODULE_13__.MatMenu, _angular_material_menu__WEBPACK_IMPORTED_MODULE_13__.MatMenuItem, _angular_material_menu__WEBPACK_IMPORTED_MODULE_13__.MatMenuTrigger, _shared_components_feather_icons_feather_icons_component__WEBPACK_IMPORTED_MODULE_5__.FeatherIconsComponent, _ngx_translate_core__WEBPACK_IMPORTED_MODULE_14__.TranslatePipe, _shared_pipes_truncate_pipe__WEBPACK_IMPORTED_MODULE_6__.TruncatePipe, _shared_pipes_utc_to_localtime_pipe__WEBPACK_IMPORTED_MODULE_7__.UTCToLocalTime],
  styles: [".logo-lg[_ngcontent-%COMP%] {\n  max-height: 45px !important;\n}\n\n.navbar-brand[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  line-height: 40px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvbGF5b3V0L2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwyQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSiIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dvLWxnIHtcclxuICAgIG1heC1oZWlnaHQ6IDQ1cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLm5hdmJhci1icmFuZCBzcGFue1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbn0iXSwic291cmNlUm9vdCI6IiJ9 */"]
});

/***/ }),

/***/ 39617:
/*!*************************************************!*\
  !*** ./src/app/layout/sidebar/sidebar-items.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ROUTES": () => (/* binding */ ROUTES)
/* harmony export */ });
const ROUTES = [{
  path: 'dashboard',
  title: 'DASHBOARD',
  icon: 'layers',
  class: '',
  groupTitle: false,
  claims: ['DASHBOARD_VIEW_DASHBOARD'],
  submenu: []
}, {
  path: '',
  title: 'ASSIGNED_DOCUMENTS',
  icon: 'list',
  class: '',
  groupTitle: false,
  claims: [],
  submenu: []
}, {
  path: 'documents',
  title: 'ALL_DOCUMENTS',
  icon: 'file-text',
  class: '',
  groupTitle: false,
  claims: ['ALL_DOCUMENTS_VIEW_DOCUMENTS'],
  submenu: []
}, {
  path: 'categories',
  title: 'DOCUMENT_CATEGORIES',
  icon: 'file',
  class: '',
  groupTitle: false,
  claims: ['DOCUMENT_CATEGORY_MANAGE_DOCUMENT_CATEGORY'],
  submenu: []
}, {
  path: 'document-audit-trails',
  title: 'DOCUMENTS_AUDIT_TRAIL',
  icon: 'activity',
  class: '',
  groupTitle: false,
  claims: ['DOCUMENT_AUDIT_TRAIL_VIEW_DOCUMENT_AUDIT_TRAIL'],
  submenu: []
}, {
  path: 'roles',
  title: 'ROLES',
  icon: 'users',
  class: '',
  groupTitle: false,
  claims: ['ROLE_VIEW_ROLES'],
  submenu: []
}, {
  path: 'users',
  title: 'USERS',
  icon: 'user',
  class: '',
  groupTitle: false,
  claims: ['USER_VIEW_USERS'],
  submenu: []
}, {
  path: 'roles/users',
  title: 'ROLE_USER',
  icon: 'user-check',
  class: '',
  groupTitle: false,
  claims: ['USER_ASSIGN_USER_ROLE'],
  submenu: []
}, {
  path: 'reminders',
  title: 'REMINDER',
  icon: 'bell',
  class: '',
  groupTitle: false,
  claims: ['REMINDER_VIEW_REMINDERS'],
  submenu: []
}, {
  path: 'login-audit',
  title: 'LOGIN_AUDITS',
  icon: 'log-in',
  class: '',
  groupTitle: false,
  claims: ['LOGIN_AUDIT_VIEW_LOGIN_AUDIT_LOGS'],
  submenu: []
}, {
  path: '',
  title: 'SETTINGS',
  icon: 'settings',
  class: 'menu-toggle',
  groupTitle: false,
  claims: ['EMAIL_MANAGE_SMTP_SETTINGS', 'SETTING_MANAGE_PROFILE', 'SETTING_MANAGE_LANGUAGE'],
  submenu: [{
    path: 'email-smtp',
    title: 'SMTP_SETTING',
    icon: 'mail',
    class: 'ml-menu',
    groupTitle: false,
    claims: ['EMAIL_MANAGE_SMTP_SETTINGS'],
    submenu: []
  }, {
    path: 'company-profile',
    title: 'COMPANY_PROFILE',
    icon: 'mail',
    class: 'ml-menu',
    groupTitle: false,
    claims: ['SETTING_MANAGE_PROFILE'],
    submenu: []
  }, {
    path: 'languages',
    title: 'LANGUAGES',
    icon: '',
    class: 'ml-menu',
    groupTitle: false,
    submenu: [],
    claims: ['SETTING_MANAGE_LANGUAGE']
  }]
}];

/***/ }),

/***/ 20129:
/*!*****************************************************!*\
  !*** ./src/app/layout/sidebar/sidebar.component.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SidebarComponent": () => (/* binding */ SidebarComponent)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _sidebar_items__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sidebar-items */ 39617);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var ngx_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-scrollbar */ 24203);
/* harmony import */ var angular_feather__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-feather */ 73918);
/* harmony import */ var _shared_has_claim_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/has-claim.directive */ 79785);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ 38699);
/* eslint-disable @typescript-eslint/no-unused-vars */










function SidebarComponent_ng_container_5_ng_container_1_div_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipeBind1"](2, 1, sidebarItem_r1.title));
  }
}
const _c0 = function (a0) {
  return [a0];
};
function SidebarComponent_ng_container_5_ng_container_1_a_3_Template(rf, ctx) {
  if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "a", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function SidebarComponent_ng_container_5_ng_container_1_a_3_Template_a_click_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r9);
      const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).$implicit;
      const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r7.callToggleMenu($event, sidebarItem_r1.submenu.length));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i-feather", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "span", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipe"](4, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("routerLink", sidebarItem_r1.class === "" ? _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction1"](6, _c0, sidebarItem_r1.path) : null)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction1"](8, _c0, sidebarItem_r1.class));
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("name", sidebarItem_r1.icon);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipeBind1"](4, 4, sidebarItem_r1.title), " ");
  }
}
const _c1 = function () {
  return {
    exact: true
  };
};
function SidebarComponent_ng_container_5_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "li", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, SidebarComponent_ng_container_5_ng_container_1_div_2_Template, 3, 3, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](3, SidebarComponent_ng_container_5_ng_container_1_a_3_Template, 5, 10, "a", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("routerLinkActiveOptions", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction0"](4, _c1))("routerLinkActive", sidebarItem_r1.submenu.length !== 0 ? "active" : "active-top");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", sidebarItem_r1.groupTitle === true);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", !sidebarItem_r1.groupTitle);
  }
}
function SidebarComponent_ng_container_5_ng_container_2_li_1_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](3).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipeBind1"](2, 1, sidebarItem_r1.title));
  }
}
function SidebarComponent_ng_container_5_ng_container_2_li_1_a_2_Template(rf, ctx) {
  if (rf & 1) {
    const _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "a", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function SidebarComponent_ng_container_5_ng_container_2_li_1_a_2_Template_a_click_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r19);
      const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](3).$implicit;
      const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r17.callToggleMenu($event, sidebarItem_r1.submenu.length));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i-feather", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "span", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipe"](4, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](3).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("routerLink", sidebarItem_r1.class === "" ? _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction1"](6, _c0, sidebarItem_r1.path) : null)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction1"](8, _c0, sidebarItem_r1.class));
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("name", sidebarItem_r1.icon);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipeBind1"](4, 4, sidebarItem_r1.title), " ");
  }
}
function SidebarComponent_ng_container_5_ng_container_2_li_1_ul_3_ng_container_1_li_1_Template(rf, ctx) {
  if (rf & 1) {
    const _r26 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "li", 17)(1, "a", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function SidebarComponent_ng_container_5_ng_container_2_li_1_ul_3_ng_container_1_li_1_Template_a_click_1_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r26);
      const sidebarSubItem1_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
      const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](5);
      return _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresetView"](ctx_r24.callToggleMenu($event, sidebarSubItem1_r22.submenu.length));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipe"](3, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const sidebarSubItem1_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("routerLinkActive", sidebarSubItem1_r22.submenu.length > 0 ? "" : "active");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("routerLink", sidebarSubItem1_r22.submenu.length > 0 ? null : _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction1"](6, _c0, sidebarSubItem1_r22.path))("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction1"](8, _c0, sidebarSubItem1_r22.class));
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpipeBind1"](3, 4, sidebarSubItem1_r22.title), " ");
  }
}
function SidebarComponent_ng_container_5_ng_container_2_li_1_ul_3_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, SidebarComponent_ng_container_5_ng_container_2_li_1_ul_3_ng_container_1_li_1_Template, 4, 10, "li", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const sidebarSubItem1_r22 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("hasClaim", sidebarSubItem1_r22.claims);
  }
}
function SidebarComponent_ng_container_5_ng_container_2_li_1_ul_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "ul", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, SidebarComponent_ng_container_5_ng_container_2_li_1_ul_3_ng_container_1_Template, 2, 1, "ng-container", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](3).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", sidebarItem_r1.submenu);
  }
}
function SidebarComponent_ng_container_5_ng_container_2_li_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "li", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, SidebarComponent_ng_container_5_ng_container_2_li_1_div_1_Template, 3, 3, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, SidebarComponent_ng_container_5_ng_container_2_li_1_a_2_Template, 5, 10, "a", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](3, SidebarComponent_ng_container_5_ng_container_2_li_1_ul_3_Template, 2, 1, "ul", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("routerLinkActiveOptions", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction0"](5, _c1))("routerLinkActive", sidebarItem_r1.submenu.length !== 0 ? "active" : "active-top");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", sidebarItem_r1.groupTitle === true);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", !sidebarItem_r1.groupTitle);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", sidebarItem_r1.submenu.length > 0);
  }
}
function SidebarComponent_ng_container_5_ng_container_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, SidebarComponent_ng_container_5_ng_container_2_li_1_Template, 4, 6, "li", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const sidebarItem_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("hasClaim", sidebarItem_r1.claims);
  }
}
function SidebarComponent_ng_container_5_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, SidebarComponent_ng_container_5_ng_container_1_Template, 4, 5, "ng-container", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, SidebarComponent_ng_container_5_ng_container_2_Template, 2, 1, "ng-container", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const sidebarItem_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", sidebarItem_r1.claims.length == 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", sidebarItem_r1.claims.length > 0);
  }
}
class SidebarComponent {
  constructor(document, renderer, elementRef, router) {
    this.document = document;
    this.renderer = renderer;
    this.elementRef = elementRef;
    this.router = router;
    this.headerHeight = 60;
    this.routerObj = this.router.events.subscribe(event => {
      if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__.NavigationEnd) {
        // close sidebar on mobile screen after menu select
        this.renderer.removeClass(this.document.body, 'overlay-open');
      }
    });
  }
  windowResizecall() {
    this.setMenuHeight();
    this.checkStatuForResize(false);
  }
  onGlobalClick(event) {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.renderer.removeClass(this.document.body, 'overlay-open');
    }
  }
  ngOnInit() {
    this.sidebarItems = _sidebar_items__WEBPACK_IMPORTED_MODULE_0__.ROUTES;
    this.initLeftSidebar();
    this.bodyTag = this.document.body;
  }
  ngOnDestroy() {
    this.routerObj.unsubscribe();
  }
  initLeftSidebar() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const _this = this;
    // Set menu height
    _this.setMenuHeight();
    _this.checkStatuForResize(true);
  }
  setMenuHeight() {
    this.innerHeight = window.innerHeight;
    const height = this.innerHeight - this.headerHeight;
    this.listMaxHeight = height + '';
    this.listMaxWidth = '500px';
  }
  isOpen() {
    return this.bodyTag.classList.contains('overlay-open');
  }
  checkStatuForResize(firstTime) {
    if (window.innerWidth < 1170) {
      this.renderer.addClass(this.document.body, 'ls-closed');
    } else {
      this.renderer.removeClass(this.document.body, 'ls-closed');
    }
  }
  mouseHover() {
    const body = this.elementRef.nativeElement.closest('body');
    if (body.classList.contains('submenu-closed')) {
      this.renderer.addClass(this.document.body, 'side-closed-hover');
      this.renderer.removeClass(this.document.body, 'submenu-closed');
    }
  }
  mouseOut() {
    const body = this.elementRef.nativeElement.closest('body');
    if (body.classList.contains('side-closed-hover')) {
      this.renderer.removeClass(this.document.body, 'side-closed-hover');
      this.renderer.addClass(this.document.body, 'submenu-closed');
    }
  }
  callToggleMenu(event, length) {
    if (length > 0) {
      const parentElement = event.target.closest('li');
      const activeClass = parentElement?.classList.contains('active');
      if (activeClass) {
        this.renderer.removeClass(parentElement, 'active');
      } else {
        this.renderer.addClass(parentElement, 'active');
      }
    }
  }
}
SidebarComponent.ɵfac = function SidebarComponent_Factory(t) {
  return new (t || SidebarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_4__.DOCUMENT), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_2__.Renderer2), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_2__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router));
};
SidebarComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
  type: SidebarComponent,
  selectors: [["app-sidebar"]],
  hostBindings: function SidebarComponent_HostBindings(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("resize", function SidebarComponent_resize_HostBindingHandler($event) {
        return ctx.windowResizecall($event);
      }, false, _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresolveWindow"])("mousedown", function SidebarComponent_mousedown_HostBindingHandler($event) {
        return ctx.onGlobalClick($event);
      }, false, _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵresolveDocument"]);
    }
  },
  decls: 6,
  vars: 3,
  consts: [["id", "leftsidebar", 1, "sidebar", 3, "mouseenter", "mouseleave"], [1, "menu"], ["visibility", "hover"], ["id", "sidebarnav", 1, "list"], [4, "ngFor", "ngForOf"], [4, "ngIf"], [3, "routerLinkActiveOptions", "routerLinkActive"], ["class", "header", 4, "ngIf"], ["class", "menu-top", 3, "routerLink", "ngClass", "click", 4, "ngIf"], [1, "header"], [1, "menu-top", 3, "routerLink", "ngClass", "click"], [1, "sidebarIcon", 3, "name"], [1, "hide-menu"], [3, "routerLinkActiveOptions", "routerLinkActive", 4, "hasClaim"], ["class", "ml-menu", 4, "ngIf"], [1, "ml-menu"], [3, "routerLinkActive", 4, "hasClaim"], [3, "routerLinkActive"], [3, "routerLink", "ngClass", "click"]],
  template: function SidebarComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div")(1, "aside", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("mouseenter", function SidebarComponent_Template_aside_mouseenter_1_listener() {
        return ctx.mouseHover();
      })("mouseleave", function SidebarComponent_Template_aside_mouseleave_1_listener() {
        return ctx.mouseOut();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "div", 1)(3, "ng-scrollbar", 2)(4, "ul", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](5, SidebarComponent_ng_container_5_Template, 3, 2, "ng-container", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()()()();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵstyleProp"]("height", ctx.listMaxHeight + "px");
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx.sidebarItems);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, _angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterLink, _angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterLinkActive, ngx_scrollbar__WEBPACK_IMPORTED_MODULE_5__.NgScrollbar, angular_feather__WEBPACK_IMPORTED_MODULE_6__.FeatherComponent, _shared_has_claim_directive__WEBPACK_IMPORTED_MODULE_1__.HasClaimDirective, _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__.TranslatePipe],
  styles: [".menu-top[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvbGF5b3V0L3NpZGViYXIvc2lkZWJhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7QUFDSiIsInNvdXJjZXNDb250ZW50IjpbIi5tZW51LXRvcCB7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn0iXSwic291cmNlUm9vdCI6IiJ9 */"]
});

/***/ }),

/***/ 75769:
/*!******************************************************!*\
  !*** ./src/app/notification/notification.service.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NotificationService": () => (/* binding */ NotificationService)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 53158);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/error-handler/common-http-error.service */ 48032);





class NotificationService {
  constructor(httpClient, commonHttpErrorService) {
    this.httpClient = httpClient;
    this.commonHttpErrorService = commonHttpErrorService;
  }
  getNotification() {
    const url = `userNotification/notification`;
    return this.httpClient.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  getNotifications(resource) {
    const url = `userNotification/notifications`;
    const customParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpParams().set('fields', resource.fields).set('orderBy', resource.orderBy).set('pageSize', resource.pageSize.toString()).set('skip', resource.skip.toString()).set('searchQuery', resource.searchQuery).set('categoryId', resource.categoryId).set('name', resource.name).set('id', resource.id.toString()).set('createdBy', resource.createdBy.toString());
    return this.httpClient.get(url, {
      params: customParams,
      observe: 'response'
    }).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  markAsRead(id) {
    const url = `userNotification/MarkAsRead`;
    return this.httpClient.post(url, {
      id
    }).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  markAllAsRead() {
    const url = `UserNotification/MarkAllAsRead`;
    return this.httpClient.post(url, null).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
}
NotificationService.ɵfac = function NotificationService_Factory(t) {
  return new (t || NotificationService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_0__.CommonHttpErrorService));
};
NotificationService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
  token: NotificationService,
  factory: NotificationService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 68842:
/*!*****************************************************************!*\
  !*** ./src/app/shared/audio-preview/audio-preview.component.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AudioPreviewComponent": () => (/* binding */ AudioPreviewComponent)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var src_app_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/base.component */ 83607);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @shared/overlay-panel/overlay-panel-ref */ 3681);
/* harmony import */ var _core_services_common_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @core/services/common.service */ 50690);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);







const _c0 = ["playerEl"];
function AudioPreviewComponent_div_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "mat-spinner");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
  }
}
class AudioPreviewComponent extends src_app_base_component__WEBPACK_IMPORTED_MODULE_0__.BaseComponent {
  constructor(overlayRef, commonService) {
    super();
    this.overlayRef = overlayRef;
    this.commonService = commonService;
    this.isLoading = false;
  }
  ngOnChanges(changes) {
    if (changes['document']) {
      this.getDocument();
    }
  }
  getDocument() {
    this.isLoading = true;
    this.sub$.sink = this.commonService.downloadDocument(this.document.documentId, this.document.isVersion).subscribe(data => {
      if (data.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpEventType.Response) {
        this.isLoading = false;
        if (this.htmlSource && this.player().hasChildNodes()) {
          this.player().removeChild(this.htmlSource);
        }
        const imgageFile = new Blob([data.body], {
          type: data.body.type
        });
        this.htmlSource = document.createElement('source');
        this.htmlSource.src = URL.createObjectURL(imgageFile);
        this.htmlSource.type = data.body.type;
        this.player().pause();
        this.player().load();
        this.player().appendChild(this.htmlSource);
        this.player().play();
      }
    }, err => {
      this.isLoading = false;
    });
  }
  player() {
    return this.playerEl.nativeElement;
  }
  onCancel() {
    this.overlayRef.close();
  }
}
AudioPreviewComponent.ɵfac = function AudioPreviewComponent_Factory(t) {
  return new (t || AudioPreviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_1__.OverlayPanelRef), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_core_services_common_service__WEBPACK_IMPORTED_MODULE_2__.CommonService));
};
AudioPreviewComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({
  type: AudioPreviewComponent,
  selectors: [["app-audio-preview"]],
  viewQuery: function AudioPreviewComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵviewQuery"](_c0, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵloadQuery"]()) && (ctx.playerEl = _t.first);
    }
  },
  inputs: {
    document: "document"
  },
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵInheritDefinitionFeature"], _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵNgOnChangesFeature"]],
  decls: 4,
  vars: 1,
  consts: [[1, "audio-div"], ["controls", "controls"], ["playerEl", ""], ["class", "loading-shade", 4, "ngIf"], [1, "loading-shade"]],
  template: function AudioPreviewComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "audio", 1, 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, AudioPreviewComponent_div_3_Template, 2, 0, "div", 3);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.isLoading);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_5__.NgIf, _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__.MatProgressSpinner],
  styles: [".audio-div[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  min-height: calc(100vh - 60px);\n  background: rgba(15, 15, 15, 0.9490196078);\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hhcmVkL2F1ZGlvLXByZXZpZXcvYXVkaW8tcHJldmlldy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLDhCQUFBO0VBQ0EsMENBQUE7QUFDSiIsInNvdXJjZXNDb250ZW50IjpbIi5hdWRpby1kaXYge1xyXG4gICAgZGlzcGxheSAgICAgICAgOiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb24gOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zICAgIDogY2VudGVyO1xyXG4gICAgdGV4dC1hbGlnbiAgICAgOiBjZW50ZXI7XHJcbiAgICBtaW4taGVpZ2h0ICAgICA6IGNhbGMoMTAwdmggLSA2MHB4KTtcclxuICAgIGJhY2tncm91bmQgICAgIDogIzBmMGYwZmYyO1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
});

/***/ }),

/***/ 46235:
/*!***************************************************************!*\
  !*** ./src/app/shared/base-preview/base-preview.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BasePreviewComponent": () => (/* binding */ BasePreviewComponent)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _core_domain_classes_document_operation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/domain-classes/document-operation */ 47954);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @environments/environment */ 92340);
/* harmony import */ var _shared_overlay_panel_overlay_panel_data__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @shared/overlay-panel/overlay-panel-data */ 55854);
/* harmony import */ var src_app_base_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/base.component */ 83607);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _shared_overlay_panel_overlay_panel_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @shared/overlay-panel/overlay-panel.service */ 96670);
/* harmony import */ var _core_services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @core/services/common.service */ 50690);
/* harmony import */ var _core_services_clone_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/services/clone.service */ 28265);
/* harmony import */ var _shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @shared/overlay-panel/overlay-panel-ref */ 3681);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-toastr */ 94817);
/* harmony import */ var _core_services_translation_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @core/services/translation.service */ 16107);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);
/* harmony import */ var _image_preview_image_preview_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../image-preview/image-preview.component */ 3046);
/* harmony import */ var _pdf_viewer_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../pdf-viewer/pdf-viewer.component */ 88948);
/* harmony import */ var _text_preview_text_preview_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../text-preview/text-preview.component */ 22638);
/* harmony import */ var _office_viewer_office_viewer_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../office-viewer/office-viewer.component */ 52812);
/* harmony import */ var _audio_preview_audio_preview_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../audio-preview/audio-preview.component */ 68842);
/* harmony import */ var _video_preview_video_preview_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../video-preview/video-preview.component */ 96352);




















function BasePreviewComponent_span_5_Template(rf, ctx) {
  if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementStart"](0, "span", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵlistener"]("click", function BasePreviewComponent_span_5_Template_span_click_0_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵrestoreView"](_r9);
      const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵresetView"](ctx_r8.downloadDocument(ctx_r8.currentDoc));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelement"](1, "i", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementEnd"]();
  }
}
function BasePreviewComponent_app_image_preview_7_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelement"](0, "app-image-preview", 10);
  }
  if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("document", ctx_r1.currentDoc);
  }
}
function BasePreviewComponent_app_office_viewer_8_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelement"](0, "app-office-viewer", 10);
  }
  if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("document", ctx_r2.currentDoc);
  }
}
function BasePreviewComponent_app_pdf_viewer_9_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelement"](0, "app-pdf-viewer", 10);
  }
  if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("document", ctx_r3.currentDoc);
  }
}
function BasePreviewComponent_app_text_preview_10_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelement"](0, "app-text-preview", 10);
  }
  if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("document", ctx_r4.currentDoc);
  }
}
function BasePreviewComponent_app_audio_preview_11_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelement"](0, "app-audio-preview", 10);
  }
  if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("document", ctx_r5.currentDoc);
  }
}
function BasePreviewComponent_app_video_preview_12_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelement"](0, "app-video-preview", 10);
  }
  if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("document", ctx_r6.currentDoc);
  }
}
function BasePreviewComponent_div_13_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementStart"](0, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelement"](1, "mat-spinner");
    _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementEnd"]();
  }
}
class BasePreviewComponent extends src_app_base_component__WEBPACK_IMPORTED_MODULE_3__.BaseComponent {
  constructor(overlay, commonService, data, clonerService, overlayRef, toastrService, translationService) {
    super();
    this.overlay = overlay;
    this.commonService = commonService;
    this.data = data;
    this.clonerService = clonerService;
    this.overlayRef = overlayRef;
    this.toastrService = toastrService;
    this.translationService = translationService;
    this.type = '';
    this.isLoading = false;
    this.isDownloadFlag = false;
  }
  ngOnInit() {
    this.onDocumentView(this.data);
  }
  closeToolbar() {
    this.overlayRef.close();
  }
  onDocumentView(document) {
    this.currentDoc = this.clonerService.deepClone(document);
    const allowExtesions = _environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.allowExtesions;
    const allowTypeExtenstion = allowExtesions.find(c => c.extentions.find(ext => ext === document.extension));
    this.type = allowTypeExtenstion ? allowTypeExtenstion.type : '';
    this.getIsDownloadFlag(document);
    this.addDocumentTrail(document.isVersion ? document.id : document.documentId, _core_domain_classes_document_operation__WEBPACK_IMPORTED_MODULE_0__.DocumentOperation.Read.toString());
  }
  addDocumentTrail(documentId, operation) {
    const objDocumentAuditTrail = {
      documentId: documentId,
      operationName: operation
    };
    this.sub$.sink = this.commonService.addDocumentAuditTrail(objDocumentAuditTrail).subscribe(c => {});
  }
  getIsDownloadFlag(document) {
    this.sub$.sink = this.commonService.isDownloadFlag(this.data.isVersion ? document.id : document.documentId, this.data.isFromPreview).subscribe(c => {
      this.isDownloadFlag = c;
    });
  }
  downloadDocument(documentView) {
    this.sub$.sink = this.commonService.downloadDocument(this.currentDoc.documentId, documentView.isVersion).subscribe(event => {
      if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_16__.HttpEventType.Response) {
        this.addDocumentTrail(documentView.isVersion ? documentView.id : documentView.documentId, _core_domain_classes_document_operation__WEBPACK_IMPORTED_MODULE_0__.DocumentOperation.Download.toString());
        this.downloadFile(event, documentView);
      }
    }, error => {
      this.toastrService.error(this.translationService.getValue('ERROR_WHILE_DOWNLOADING_DOCUMENT'));
    });
  }
  downloadFile(data, documentView) {
    const downloadedFile = new Blob([data.body], {
      type: data.body.type
    });
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
    a.download = this.data.name;
    a.href = URL.createObjectURL(downloadedFile);
    a.target = '_blank';
    a.click();
    document.body.removeChild(a);
  }
}
BasePreviewComponent.ɵfac = function BasePreviewComponent_Factory(t) {
  return new (t || BasePreviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdirectiveInject"](_shared_overlay_panel_overlay_panel_service__WEBPACK_IMPORTED_MODULE_4__.OverlayPanel), _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdirectiveInject"](_core_services_common_service__WEBPACK_IMPORTED_MODULE_5__.CommonService), _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdirectiveInject"](_shared_overlay_panel_overlay_panel_data__WEBPACK_IMPORTED_MODULE_2__.OVERLAY_PANEL_DATA), _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdirectiveInject"](_core_services_clone_service__WEBPACK_IMPORTED_MODULE_6__.ClonerService), _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdirectiveInject"](_shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_7__.OverlayPanelRef), _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_17__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdirectiveInject"](_core_services_translation_service__WEBPACK_IMPORTED_MODULE_8__.TranslationService));
};
BasePreviewComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵdefineComponent"]({
  type: BasePreviewComponent,
  selectors: [["app-base-preview"]],
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵInheritDefinitionFeature"]],
  decls: 14,
  vars: 10,
  consts: [[1, "file-preview-toolbar", "d-flex"], [1, "back-arrow"], [1, "fas", "fa-arrow-left", 3, "click"], [1, "file-name", "flex-grow-1"], ["class", "back-arrow", 3, "click", 4, "ngIf"], [3, "ngSwitch"], [3, "document", 4, "ngSwitchCase"], ["class", "loading-shade", 4, "ngIf"], [1, "back-arrow", 3, "click"], [1, "fas", "fa-arrow-down"], [3, "document"], [1, "loading-shade"]],
  template: function BasePreviewComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementStart"](0, "div", 0)(1, "span", 1)(2, "i", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵlistener"]("click", function BasePreviewComponent_Template_i_click_2_listener() {
        return ctx.closeToolbar();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementStart"](3, "span", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtext"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtemplate"](5, BasePreviewComponent_span_5_Template, 2, 0, "span", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementContainerStart"](6, 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtemplate"](7, BasePreviewComponent_app_image_preview_7_Template, 1, 1, "app-image-preview", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtemplate"](8, BasePreviewComponent_app_office_viewer_8_Template, 1, 1, "app-office-viewer", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtemplate"](9, BasePreviewComponent_app_pdf_viewer_9_Template, 1, 1, "app-pdf-viewer", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtemplate"](10, BasePreviewComponent_app_text_preview_10_Template, 1, 1, "app-text-preview", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtemplate"](11, BasePreviewComponent_app_audio_preview_11_Template, 1, 1, "app-audio-preview", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtemplate"](12, BasePreviewComponent_app_video_preview_12_Template, 1, 1, "app-video-preview", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵelementContainerEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtemplate"](13, BasePreviewComponent_div_13_Template, 2, 0, "div", 7);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵtextInterpolate"](ctx.currentDoc == null ? null : ctx.currentDoc.name);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("ngIf", ctx.isDownloadFlag);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("ngSwitch", ctx.type);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("ngSwitchCase", "image");
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("ngSwitchCase", "office");
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("ngSwitchCase", "pdf");
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("ngSwitchCase", "text");
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("ngSwitchCase", "audio");
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("ngSwitchCase", "video");
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_15__["ɵɵproperty"]("ngIf", ctx.isLoading);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_18__.NgIf, _angular_common__WEBPACK_IMPORTED_MODULE_18__.NgSwitch, _angular_common__WEBPACK_IMPORTED_MODULE_18__.NgSwitchCase, _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_19__.MatProgressSpinner, _image_preview_image_preview_component__WEBPACK_IMPORTED_MODULE_9__.ImagePreviewComponent, _pdf_viewer_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_10__.PdfViewerComponent, _text_preview_text_preview_component__WEBPACK_IMPORTED_MODULE_11__.TextPreviewComponent, _office_viewer_office_viewer_component__WEBPACK_IMPORTED_MODULE_12__.OfficeViewerComponent, _audio_preview_audio_preview_component__WEBPACK_IMPORTED_MODULE_13__.AudioPreviewComponent, _video_preview_video_preview_component__WEBPACK_IMPORTED_MODULE_14__.VideoPreviewComponent],
  styles: ["[_nghost-%COMP%]     #aligner {\n  display: none !important;\n  visibility: hidden !important;\n}\n\n.file-preview-toolbar[_ngcontent-%COMP%] {\n  top: 0;\n  left: 0;\n  display: flex;\n  width: 100%;\n  align-items: center;\n  color: var(--be-accent-contrast);\n  height: 60px;\n  padding: 0 15px;\n  background: #673ab7;\n  z-index: 10;\n}\n\n.back-arrow[_ngcontent-%COMP%] {\n  color: white;\n  font-size: x-large;\n  font-weight: bold;\n  cursor: pointer;\n  margin-right: 10px;\n}\n\n.download-button[_ngcontent-%COMP%] {\n  position: absolute;\n  right: 30px;\n}\n\n.file-name[_ngcontent-%COMP%] {\n  color: #fff;\n  margin-left: 10px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hhcmVkL2Jhc2UtcHJldmlldy9iYXNlLXByZXZpZXcuY29tcG9uZW50LnNjc3MiLCJ3ZWJwYWNrOi8vLi9zcmMvYXNzZXRzL3N0eWxlcy9jb2xvci5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0Usd0JBQUE7RUFDQSw2QkFBQTtBQUFGOztBQUlBO0VBQ0UsTUFBQTtFQUNBLE9BQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0NBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQ1hhO0VEWWIsV0FBQTtBQURGOztBQUlBO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUFERjs7QUFJQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtBQURGOztBQUlBO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0FBREYiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICcuLi8uLi8uLi9hc3NldHMvc3R5bGVzL2NvbG9yLnNjc3MnO1xyXG46aG9zdCA6Om5nLWRlZXAgI2FsaWduZXIge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxuICB2aXNpYmlsaXR5OiBoaWRkZW4gIWltcG9ydGFudDtcclxufVxyXG5cclxuXHJcbi5maWxlLXByZXZpZXctdG9vbGJhciB7XHJcbiAgdG9wICAgICAgICA6IDA7XHJcbiAgbGVmdCAgICAgICA6IDA7XHJcbiAgZGlzcGxheSAgICA6IGZsZXg7XHJcbiAgd2lkdGggICAgICA6IDEwMCU7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBjb2xvciAgICAgIDogdmFyKC0tYmUtYWNjZW50LWNvbnRyYXN0KTtcclxuICBoZWlnaHQgICAgIDogNjBweDtcclxuICBwYWRkaW5nICAgIDogMCAxNXB4O1xyXG4gIGJhY2tncm91bmQgOiAkdGV4dC1wcmltYXJ5O1xyXG4gIHotaW5kZXggICAgOiAxMDtcclxufVxyXG5cclxuLmJhY2stYXJyb3cge1xyXG4gIGNvbG9yOiAjZmZmZjtcclxuICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuLmRvd25sb2FkLWJ1dHRvbiB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0ICAgOiAzMHB4O1xyXG59XHJcblxyXG4uZmlsZS1uYW1lIHtcclxuICBjb2xvciAgICAgIDogI2ZmZjtcclxuICBtYXJnaW4tbGVmdDogMTBweDtcclxufVxyXG4iLCIkYm9keS1iZzogI2VhZjBmNztcclxuJHByaW1hcnk6ICM2NzNhYjc7XHJcbiR3aGl0ZTogI2ZmZmZmZjtcclxuJGdyYXk6ICM0NzQ3NDc7XHJcbiRsaWdodC1ncmF5OiAjZGNkZGVhO1xyXG4kdGV4dC1wcmltYXJ5OiAjNjczYWI3O1xyXG4kcHJpbWFyeS1ncmF5OiM2NzNhYjdjYztcclxuJGNvbG9yX29yYW5nZTogI2ZmYTQ0MjtcclxuJGNvbG9yX2JsdWU6ICM0YjY0ZmY7XHJcbiRjb2xvcl9yZWQ6ICNmZjRiNGI7XHJcbiRjb2xvcl9ncmVlbjogIzQwYzk1MjsiXSwic291cmNlUm9vdCI6IiJ9 */"]
});

/***/ }),

/***/ 61676:
/*!****************************************************************************!*\
  !*** ./src/app/shared/components/feather-icons/feather-icons.component.ts ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FeatherIconsComponent": () => (/* binding */ FeatherIconsComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var angular_feather__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-feather */ 73918);


class FeatherIconsComponent {
  constructor() {
    // constructor
  }
}
FeatherIconsComponent.ɵfac = function FeatherIconsComponent_Factory(t) {
  return new (t || FeatherIconsComponent)();
};
FeatherIconsComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
  type: FeatherIconsComponent,
  selectors: [["app-feather-icons"]],
  inputs: {
    icon: "icon",
    class: "class"
  },
  decls: 1,
  vars: 3,
  consts: [[3, "name"]],
  template: function FeatherIconsComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i-feather", 0);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.class);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("name", ctx.icon);
    }
  },
  dependencies: [angular_feather__WEBPACK_IMPORTED_MODULE_1__.FeatherComponent],
  styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 27545:
/*!*************************************************************************!*\
  !*** ./src/app/shared/components/feather-icons/feather-icons.module.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FeatherIconsModule": () => (/* binding */ FeatherIconsModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _feather_icons_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./feather-icons.component */ 61676);
/* harmony import */ var angular_feather__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-feather */ 73918);
/* harmony import */ var angular_feather_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-feather/icons */ 31744);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);






class FeatherIconsModule {}
FeatherIconsModule.ɵfac = function FeatherIconsModule_Factory(t) {
  return new (t || FeatherIconsModule)();
};
FeatherIconsModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
  type: FeatherIconsModule
});
FeatherIconsModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, angular_feather__WEBPACK_IMPORTED_MODULE_3__.FeatherModule.pick(angular_feather_icons__WEBPACK_IMPORTED_MODULE_4__.allIcons), angular_feather__WEBPACK_IMPORTED_MODULE_3__.FeatherModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](FeatherIconsModule, {
    declarations: [_feather_icons_component__WEBPACK_IMPORTED_MODULE_0__.FeatherIconsComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, angular_feather__WEBPACK_IMPORTED_MODULE_3__.FeatherModule],
    exports: [_feather_icons_component__WEBPACK_IMPORTED_MODULE_0__.FeatherIconsComponent, angular_feather__WEBPACK_IMPORTED_MODULE_3__.FeatherModule]
  });
})();

/***/ }),

/***/ 79785:
/*!***********************************************!*\
  !*** ./src/app/shared/has-claim.directive.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HasClaimDirective": () => (/* binding */ HasClaimDirective)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _core_security_security_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/security/security.service */ 40130);


class HasClaimDirective {
  set hasClaim(claimType) {
    if (this.securityService.hasClaim(claimType)) {
      // Add template to DOM
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      // Remove template from DOM
      this.viewContainer.clear();
    }
  }
  constructor(templateRef, viewContainer, securityService) {
    this.templateRef = templateRef;
    this.viewContainer = viewContainer;
    this.securityService = securityService;
  }
}
HasClaimDirective.ɵfac = function HasClaimDirective_Factory(t) {
  return new (t || HasClaimDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__.TemplateRef), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__.ViewContainerRef), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_core_security_security_service__WEBPACK_IMPORTED_MODULE_0__.SecurityService));
};
HasClaimDirective.ɵdir = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
  type: HasClaimDirective,
  selectors: [["", "hasClaim", ""]],
  inputs: {
    hasClaim: "hasClaim"
  }
});

/***/ }),

/***/ 3046:
/*!*****************************************************************!*\
  !*** ./src/app/shared/image-preview/image-preview.component.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ImagePreviewComponent": () => (/* binding */ ImagePreviewComponent)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 21339);
/* harmony import */ var src_app_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/base.component */ 83607);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @shared/overlay-panel/overlay-panel-ref */ 3681);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ 34497);
/* harmony import */ var _core_services_common_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @core/services/common.service */ 50690);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);









function ImagePreviewComponent_div_0_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "img", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("src", ctx_r0.imageUrl, _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsanitizeUrl"])("alt", ctx_r0.document.name);
  }
}
function ImagePreviewComponent_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "mat-spinner");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
  }
}
class ImagePreviewComponent extends src_app_base_component__WEBPACK_IMPORTED_MODULE_0__.BaseComponent {
  constructor(overlayRef, sanitizer, ref, commonService) {
    super();
    this.overlayRef = overlayRef;
    this.sanitizer = sanitizer;
    this.ref = ref;
    this.commonService = commonService;
    this.isLoading = false;
  }
  ngOnInit() {}
  ngOnChanges(changes) {
    if (changes['document']) {
      this.getImage();
    }
  }
  getImage() {
    this.isLoading = true;
    this.sub$.sink = this.commonService.downloadDocument(this.document.documentId, this.document.isVersion).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.delay)(500)).subscribe(data => {
      this.isLoading = false;
      if (data.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpEventType.Response) {
        const imgageFile = new Blob([data.body], {
          type: data.body.type
        });
        this.imageUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(imgageFile));
        this.ref.markForCheck();
      }
    }, err => {
      this.isLoading = false;
    });
  }
  onCancel() {
    this.overlayRef.close();
  }
}
ImagePreviewComponent.ɵfac = function ImagePreviewComponent_Factory(t) {
  return new (t || ImagePreviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_1__.OverlayPanelRef), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__.DomSanitizer), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_3__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_core_services_common_service__WEBPACK_IMPORTED_MODULE_2__.CommonService));
};
ImagePreviewComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({
  type: ImagePreviewComponent,
  selectors: [["app-image-preview"]],
  inputs: {
    document: "document"
  },
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵInheritDefinitionFeature"], _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵNgOnChangesFeature"]],
  decls: 2,
  vars: 2,
  consts: [["class", "center-img m-5", 4, "ngIf"], ["class", "loading-shade", 4, "ngIf"], [1, "center-img", "m-5"], [1, "img-fluid", "custom-img", 3, "src", "alt"], [1, "loading-shade"]],
  template: function ImagePreviewComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](0, ImagePreviewComponent_div_0_Template, 2, 2, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ImagePreviewComponent_div_1_Template, 2, 0, "div", 1);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.imageUrl);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.isLoading);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_7__.NgIf, _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_8__.MatProgressSpinner],
  styles: [".center-img[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.custom-img[_ngcontent-%COMP%] {\n  max-height: calc(100vh - 160px);\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hhcmVkL2ltYWdlLXByZXZpZXcvaW1hZ2UtcHJldmlldy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0FBQ0Y7O0FBRUE7RUFDRSwrQkFBQTtBQUNGIiwic291cmNlc0NvbnRlbnQiOlsiLmNlbnRlci1pbWcge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmN1c3RvbS1pbWcge1xyXG4gIG1heC1oZWlnaHQ6IGNhbGMoMTAwdmggLSAxNjBweCk7XHJcbn0iXSwic291cmNlUm9vdCI6IiJ9 */"]
});

/***/ }),

/***/ 23096:
/*!*************************************************************************!*\
  !*** ./src/app/shared/loading-indicator/loading-indicator.component.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoadingIndicatorComponent": () => (/* binding */ LoadingIndicatorComponent)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 78947);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 45508);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _pending_interceptor_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pending-interceptor.service */ 40052);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/progress-bar */ 51294);






function LoadingIndicatorComponent_div_0_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "mat-progress-bar", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
  }
}
class LoadingIndicatorComponent {
  constructor(pendingRequestInterceptorService) {
    this.pendingRequestInterceptorService = pendingRequestInterceptorService;
    this.filteredUrlPatterns = [];
    this.debounceDelay = 100;
    this.entryComponent = null;
    this.subscription = this.pendingRequestInterceptorService.pendingRequestsStatus.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.debounce)(this.handleDebounce.bind(this))).subscribe(hasPendingRequests => this.isSpinnerVisible = hasPendingRequests);
  }
  ngOnInit() {
    if (!(this.filteredUrlPatterns instanceof Array)) {
      throw new TypeError('`filteredUrlPatterns` must be an array.');
    }
    if (!!this.filteredUrlPatterns.length) {
      this.filteredUrlPatterns.forEach(e => {
        this.pendingRequestInterceptorService.filteredUrlPatterns.push(new RegExp(e));
      });
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  handleDebounce(hasPendingRequests) {
    if (hasPendingRequests) {
      return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.timer)(this.debounceDelay);
    }
    return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.timer)(0);
  }
}
LoadingIndicatorComponent.ɵfac = function LoadingIndicatorComponent_Factory(t) {
  return new (t || LoadingIndicatorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_pending_interceptor_service__WEBPACK_IMPORTED_MODULE_0__.PendingInterceptorService));
};
LoadingIndicatorComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
  type: LoadingIndicatorComponent,
  selectors: [["app-loading-indicator"]],
  inputs: {
    backgroundColor: "backgroundColor",
    filteredUrlPatterns: "filteredUrlPatterns",
    debounceDelay: "debounceDelay",
    entryComponent: "entryComponent"
  },
  decls: 1,
  vars: 1,
  consts: [["class", "progress", 4, "ngIf"], [1, "progress"], ["mode", "indeterminate"]],
  template: function LoadingIndicatorComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, LoadingIndicatorComponent_div_0_Template, 2, 0, "div", 0);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.isSpinnerVisible);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_5__.MatProgressBar],
  styles: [".progress[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  height: 5px;\n  z-index: 9999;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hhcmVkL2xvYWRpbmctaW5kaWNhdG9yL2xvYWRpbmctaW5kaWNhdG9yLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7QUFDRiIsInNvdXJjZXNDb250ZW50IjpbIi5wcm9ncmVzcyB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNXB4O1xyXG4gIHotaW5kZXg6IDk5OTk7XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 72025:
/*!**********************************************************************!*\
  !*** ./src/app/shared/loading-indicator/loading-indicator.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoadingIndicatorModule": () => (/* binding */ LoadingIndicatorModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/progress-bar */ 51294);
/* harmony import */ var _loading_indicator_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loading-indicator.component */ 23096);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);




class LoadingIndicatorModule {}
LoadingIndicatorModule.ɵfac = function LoadingIndicatorModule_Factory(t) {
  return new (t || LoadingIndicatorModule)();
};
LoadingIndicatorModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
  type: LoadingIndicatorModule
});
LoadingIndicatorModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_3__.MatProgressBarModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](LoadingIndicatorModule, {
    declarations: [_loading_indicator_component__WEBPACK_IMPORTED_MODULE_0__.LoadingIndicatorComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_3__.MatProgressBarModule],
    exports: [_loading_indicator_component__WEBPACK_IMPORTED_MODULE_0__.LoadingIndicatorComponent]
  });
})();

/***/ }),

/***/ 96181:
/*!************************************************************************!*\
  !*** ./src/app/shared/loading-indicator/pending-interceptor.module.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PendingInterceptorModule": () => (/* binding */ PendingInterceptorModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _pending_interceptor_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pending-interceptor.service */ 40052);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);





const PendingInterceptorServiceExistingProvider = {
  provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HTTP_INTERCEPTORS,
  useExisting: _pending_interceptor_service__WEBPACK_IMPORTED_MODULE_0__.PendingInterceptorService,
  multi: true
};
class PendingInterceptorModule {}
PendingInterceptorModule.ɵfac = function PendingInterceptorModule_Factory(t) {
  return new (t || PendingInterceptorModule)();
};
PendingInterceptorModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
  type: PendingInterceptorModule
});
PendingInterceptorModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
  providers: [PendingInterceptorServiceExistingProvider, _pending_interceptor_service__WEBPACK_IMPORTED_MODULE_0__.PendingInterceptorServiceFactoryProvider],
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PendingInterceptorModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule]
  });
})();

/***/ }),

/***/ 40052:
/*!*************************************************************************!*\
  !*** ./src/app/shared/loading-indicator/pending-interceptor.service.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PendingInterceptorService": () => (/* binding */ PendingInterceptorService),
/* harmony export */   "PendingInterceptorServiceFactory": () => (/* binding */ PendingInterceptorServiceFactory),
/* harmony export */   "PendingInterceptorServiceFactoryProvider": () => (/* binding */ PendingInterceptorServiceFactoryProvider)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 26067);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 25474);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 50635);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ 53158);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 32313);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);





class PendingInterceptorService {
  get pendingRequests() {
    return this._pendingRequests;
  }
  get pendingRequestsStatus() {
    return this._pendingRequestsStatus.asObservable();
  }
  get filteredUrlPatterns() {
    return this._filteredUrlPatterns;
  }
  constructor(router) {
    this._pendingRequests = 0;
    this._pendingRequestsStatus = new rxjs__WEBPACK_IMPORTED_MODULE_0__.ReplaySubject(1);
    this._filteredUrlPatterns = [];
    router.events.subscribe(event => {
      if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__.NavigationStart) {
        this._pendingRequestsStatus.next(true);
      }
      if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__.NavigationError || event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__.NavigationEnd || event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__.NavigationCancel) {
        this._pendingRequestsStatus.next(false);
      }
    });
  }
  intercept(req, next) {
    const shouldBypass = this.shouldBypass(req.url);
    if (!shouldBypass) {
      this._pendingRequests++;
      if (1 === this._pendingRequests) {
        this._pendingRequestsStatus.next(true);
      }
    }
    return next.handle(req).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.map)(event => {
      return event;
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.catchError)(error => {
      return (0,rxjs__WEBPACK_IMPORTED_MODULE_4__.throwError)(error);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.finalize)(() => {
      if (!shouldBypass) {
        this._pendingRequests--;
        if (0 === this._pendingRequests) {
          this._pendingRequestsStatus.next(false);
        }
      }
    }));
  }
  shouldBypass(url) {
    return this._filteredUrlPatterns.some(e => {
      return e.test(url);
    });
  }
}
PendingInterceptorService.ɵfac = function PendingInterceptorService_Factory(t) {
  return new (t || PendingInterceptorService)(_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__.Router));
};
PendingInterceptorService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjectable"]({
  token: PendingInterceptorService,
  factory: PendingInterceptorService.ɵfac
});
function PendingInterceptorServiceFactory(router) {
  return new PendingInterceptorService(router);
}
let PendingInterceptorServiceFactoryProvider = {
  provide: PendingInterceptorService,
  useFactory: PendingInterceptorServiceFactory,
  deps: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.Router]
};

/***/ }),

/***/ 94872:
/*!*******************************************!*\
  !*** ./src/app/shared/material.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MaterialModule": () => (/* binding */ MaterialModule)
/* harmony export */ });
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/core */ 59121);
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-mask */ 80446);
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/tooltip */ 6896);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/list */ 6517);
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/menu */ 88589);
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button-toggle */ 19837);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/form-field */ 75074);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/slide-toggle */ 84714);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);













const materialModules = [_angular_material_button__WEBPACK_IMPORTED_MODULE_0__.MatButtonModule, _angular_material_input__WEBPACK_IMPORTED_MODULE_1__.MatInputModule, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatListModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__.MatIconModule, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_4__.MatTooltipModule, _angular_material_core__WEBPACK_IMPORTED_MODULE_5__.MatNativeDateModule, ngx_mask__WEBPACK_IMPORTED_MODULE_6__.NgxMaskModule.forRoot(), _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_7__.MatButtonToggleModule, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__.MatFormFieldModule, _angular_material_menu__WEBPACK_IMPORTED_MODULE_9__.MatMenuModule, _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_10__.MatSlideToggleModule];
class MaterialModule {}
MaterialModule.ɵfac = function MaterialModule_Factory(t) {
  return new (t || MaterialModule)();
};
MaterialModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineNgModule"]({
  type: MaterialModule
});
MaterialModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineInjector"]({
  imports: [materialModules, _angular_material_button__WEBPACK_IMPORTED_MODULE_0__.MatButtonModule, _angular_material_input__WEBPACK_IMPORTED_MODULE_1__.MatInputModule, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatListModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__.MatIconModule, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_4__.MatTooltipModule, _angular_material_core__WEBPACK_IMPORTED_MODULE_5__.MatNativeDateModule, ngx_mask__WEBPACK_IMPORTED_MODULE_6__.NgxMaskModule, _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_7__.MatButtonToggleModule, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__.MatFormFieldModule, _angular_material_menu__WEBPACK_IMPORTED_MODULE_9__.MatMenuModule, _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_10__.MatSlideToggleModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵsetNgModuleScope"](MaterialModule, {
    imports: [_angular_material_button__WEBPACK_IMPORTED_MODULE_0__.MatButtonModule, _angular_material_input__WEBPACK_IMPORTED_MODULE_1__.MatInputModule, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatListModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__.MatIconModule, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_4__.MatTooltipModule, _angular_material_core__WEBPACK_IMPORTED_MODULE_5__.MatNativeDateModule, ngx_mask__WEBPACK_IMPORTED_MODULE_6__.NgxMaskModule, _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_7__.MatButtonToggleModule, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__.MatFormFieldModule, _angular_material_menu__WEBPACK_IMPORTED_MODULE_9__.MatMenuModule, _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_10__.MatSlideToggleModule],
    exports: [_angular_material_button__WEBPACK_IMPORTED_MODULE_0__.MatButtonModule, _angular_material_input__WEBPACK_IMPORTED_MODULE_1__.MatInputModule, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatListModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__.MatIconModule, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_4__.MatTooltipModule, _angular_material_core__WEBPACK_IMPORTED_MODULE_5__.MatNativeDateModule, ngx_mask__WEBPACK_IMPORTED_MODULE_6__.NgxMaskModule, _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_7__.MatButtonToggleModule, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__.MatFormFieldModule, _angular_material_menu__WEBPACK_IMPORTED_MODULE_9__.MatMenuModule, _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_10__.MatSlideToggleModule]
  });
})();

/***/ }),

/***/ 52812:
/*!*****************************************************************!*\
  !*** ./src/app/shared/office-viewer/office-viewer.component.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OfficeViewerComponent": () => (/* binding */ OfficeViewerComponent)
/* harmony export */ });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @environments/environment */ 92340);
/* harmony import */ var src_app_base_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/base.component */ 83607);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _core_services_common_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @core/services/common.service */ 50690);
/* harmony import */ var _shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @shared/overlay-panel/overlay-panel-ref */ 3681);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);







const _c0 = ["iframe"];
function OfficeViewerComponent_iframe_0_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](0, "iframe", 3, 4);
  }
}
function OfficeViewerComponent_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "img", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "h5");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3, "Microsoft Office Viewer does not support the localhost. please host your Project on domain and try again.");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]()();
  }
}
function OfficeViewerComponent_div_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "mat-spinner");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
  }
}
class OfficeViewerComponent extends src_app_base_component__WEBPACK_IMPORTED_MODULE_1__.BaseComponent {
  constructor(commonService, overlayRef) {
    super();
    this.commonService = commonService;
    this.overlayRef = overlayRef;
    this.isLive = true;
    this.isLoading = false;
    this.token = '';
  }
  ngOnInit() {
    if (_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl.indexOf('localhost') >= 0) {
      this.isLive = false;
    }
  }
  ngAfterViewInit() {
    if (this.isLive) {
      this.getDocumentToken();
    }
  }
  getDocumentToken() {
    this.isLoading = true;
    this.sub$.sink = this.commonService.getDocumentToken(this.document.documentId).subscribe(token => {
      this.token = token['result'];
      const host = location.host;
      const protocal = location.protocol;
      const url = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl === '/' ? `${protocal}//${host}/` : _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl;
      this.iframe.nativeElement.src = 'https://view.officeapps.live.com/op/embed.aspx?src=' + encodeURIComponent(`${url}api/document/${this.document.documentId}/officeviewer?token=${this.token}&isVersion=${this.document.isVersion}`);
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    });
  }
  onCancel() {
    this.overlayRef.close();
  }
  ngOnDestroy() {
    if (this.isLive) {
      this.sub$.sink = this.commonService.deleteDocumentToken(this.token).subscribe(() => {
        super.ngOnDestroy();
      });
    } else {
      super.ngOnDestroy();
    }
  }
}
OfficeViewerComponent.ɵfac = function OfficeViewerComponent_Factory(t) {
  return new (t || OfficeViewerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_core_services_common_service__WEBPACK_IMPORTED_MODULE_2__.CommonService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_3__.OverlayPanelRef));
};
OfficeViewerComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({
  type: OfficeViewerComponent,
  selectors: [["app-office-viewer"]],
  viewQuery: function OfficeViewerComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵviewQuery"](_c0, 5);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵloadQuery"]()) && (ctx.iframe = _t.first);
    }
  },
  inputs: {
    document: "document"
  },
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵInheritDefinitionFeature"]],
  decls: 3,
  vars: 3,
  consts: [["class", "preview-object", "style", "width: 100%;height: 89%;", 4, "ngIf"], ["style", "text-align: center;", 4, "ngIf"], ["class", "loading-shade", 4, "ngIf"], [1, "preview-object", 2, "width", "100%", "height", "89%"], ["iframe", ""], [2, "text-align", "center"], ["src", "assets/images/file-preview.png", "alt", "", 1, "m-5"], [1, "loading-shade"]],
  template: function OfficeViewerComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](0, OfficeViewerComponent_iframe_0_Template, 2, 0, "iframe", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, OfficeViewerComponent_div_1_Template, 4, 0, "div", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](2, OfficeViewerComponent_div_2_Template, 2, 0, "div", 2);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.isLive);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.isLive);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.isLoading);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_5__.NgIf, _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__.MatProgressSpinner],
  styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 23276:
/*!****************************************************************************!*\
  !*** ./src/app/shared/overlay-panel/fullscreen-overlay-scroll-strategy.ts ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FullscreenOverlayScrollStrategy": () => (/* binding */ FullscreenOverlayScrollStrategy)
/* harmony export */ });
class FullscreenOverlayScrollStrategy {
  attach() {}
  enable() {
    document.documentElement.classList.add('be-fullscreen-overlay-scrollblock');
  }
  disable() {
    document.documentElement.classList.remove('be-fullscreen-overlay-scrollblock');
  }
}

/***/ }),

/***/ 55854:
/*!************************************************************!*\
  !*** ./src/app/shared/overlay-panel/overlay-panel-data.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OVERLAY_PANEL_DATA": () => (/* binding */ OVERLAY_PANEL_DATA)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);

const OVERLAY_PANEL_DATA = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.InjectionToken('OVERLAY_PANEL_DATA');

/***/ }),

/***/ 3681:
/*!***********************************************************!*\
  !*** ./src/app/shared/overlay-panel/overlay-panel-ref.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OverlayPanelRef": () => (/* binding */ OverlayPanelRef)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 80228);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 59295);
/* harmony import */ var _core_utils_random_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/utils/random-string */ 98447);



class OverlayPanelRef {
  constructor(overlayRef) {
    this.overlayRef = overlayRef;
    this.id = (0,_core_utils_random_string__WEBPACK_IMPORTED_MODULE_0__.randomString)(15);
    this.value = new rxjs__WEBPACK_IMPORTED_MODULE_1__.Subject();
  }
  isOpen() {
    return this.overlayRef && this.overlayRef.hasAttached();
  }
  close() {
    this.overlayRef && this.overlayRef.dispose();
  }
  emitValue(value) {
    this.value.next(value);
  }
  valueChanged() {
    return this.value.asObservable();
  }
  getPanelEl() {
    return this.overlayRef.overlayElement;
  }
  updatePosition() {
    return this.overlayRef.updatePosition();
  }
  afterClosed() {
    return this.overlayRef.detachments().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.take)(1));
  }
}

/***/ }),

/***/ 96670:
/*!***************************************************************!*\
  !*** ./src/app/shared/overlay-panel/overlay-panel.service.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OverlayPanel": () => (/* binding */ OverlayPanel)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/portal */ 17520);
/* harmony import */ var _overlay_panel_ref__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./overlay-panel-ref */ 3681);
/* harmony import */ var _overlay_panel_data__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./overlay-panel-data */ 55854);
/* harmony import */ var _fullscreen_overlay_scroll_strategy__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fullscreen-overlay-scroll-strategy */ 23276);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 60116);
/* harmony import */ var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/keycodes */ 28456);
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/overlay */ 25895);
/* harmony import */ var _core_services_breakpoints_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @core/services/breakpoints.service */ 66214);










const DEFAULT_CONFIG = {
  hasBackdrop: true,
  closeOnBackdropClick: true,
  panelClass: 'overlay-panel'
};
class OverlayPanel {
  constructor(overlay, breakpoints, injector) {
    this.overlay = overlay;
    this.breakpoints = breakpoints;
    this.injector = injector;
  }
  open(cmp, userConfig) {
    const config = Object.assign({}, DEFAULT_CONFIG, userConfig);
    const cdkConfig = {
      positionStrategy: this.getPositionStrategy(config),
      hasBackdrop: config.hasBackdrop,
      panelClass: config.panelClass,
      backdropClass: config.backdropClass,
      scrollStrategy: this.getScrollStrategy(config),
      disposeOnNavigation: true
    };
    if (config.width) cdkConfig.width = config.width;
    if (config.height) cdkConfig.height = config.height;
    if (config.maxHeight) cdkConfig.maxHeight = config.maxHeight;
    if (config.maxWidth) cdkConfig.maxWidth = config.maxWidth;
    const overlayRef = this.overlay.create(cdkConfig);
    const overlayPanelRef = new _overlay_panel_ref__WEBPACK_IMPORTED_MODULE_0__.OverlayPanelRef(overlayRef);
    const portal = cmp instanceof _angular_core__WEBPACK_IMPORTED_MODULE_4__.TemplateRef ? new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_5__.TemplatePortal(cmp, config.viewContainerRef, config.data) : new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_5__.ComponentPortal(cmp, config.viewContainerRef, this.createInjector(config, overlayPanelRef));
    overlayPanelRef.componentRef = overlayRef.attach(portal);
    if (config.closeOnBackdropClick) {
      overlayRef.backdropClick().subscribe(() => overlayPanelRef.close());
      overlayRef.keydownEvents().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.filter)(e => e.keyCode === _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_7__.ESCAPE)).subscribe(() => overlayPanelRef.close());
    }
    return overlayPanelRef;
  }
  getScrollStrategy(config) {
    if (config.fullScreen) {
      return new _fullscreen_overlay_scroll_strategy__WEBPACK_IMPORTED_MODULE_2__.FullscreenOverlayScrollStrategy();
    } else if (config.scrollStrategy === 'close') {
      return this.overlay.scrollStrategies.close();
    } else {
      return null;
    }
  }
  createInjector(config, dialogRef) {
    const injectionTokens = new WeakMap();
    injectionTokens.set(_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_0__.OverlayPanelRef, dialogRef);
    injectionTokens.set(_overlay_panel_data__WEBPACK_IMPORTED_MODULE_1__.OVERLAY_PANEL_DATA, config.data || null);
    return new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_5__.PortalInjector(this.injector, injectionTokens);
  }
  getPositionStrategy(config) {
    if (config.positionStrategy) {
      return config.positionStrategy;
    }
    const position = this.breakpoints.isMobile$.value ? config.mobilePosition || config.position : config.position;
    if (config.origin === 'global' || this.positionIsGlobal(position)) {
      return this.getGlobalPositionStrategy(position);
    } else {
      return this.getConnectedPositionStrategy(position, config.origin);
    }
  }
  positionIsGlobal(position) {
    return position === 'center' || !Array.isArray(position);
  }
  getGlobalPositionStrategy(position) {
    if (position === 'center') {
      return this.overlay.position().global().centerHorizontally().centerVertically();
    } else {
      const global = this.overlay.position().global();
      Object.keys(position).forEach(key => {
        global[key](position[key]);
      });
      return global;
    }
  }
  getConnectedPositionStrategy(position, origin) {
    return this.overlay.position().flexibleConnectedTo(origin).withPositions(position).withPush(true).withViewportMargin(5);
  }
}
OverlayPanel.ɵfac = function OverlayPanel_Factory(t) {
  return new (t || OverlayPanel)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_8__.Overlay), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_core_services_breakpoints_service__WEBPACK_IMPORTED_MODULE_3__.BreakpointsService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector));
};
OverlayPanel.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({
  token: OverlayPanel,
  factory: OverlayPanel.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 88948:
/*!***********************************************************!*\
  !*** ./src/app/shared/pdf-viewer/pdf-viewer.component.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PdfViewerComponent": () => (/* binding */ PdfViewerComponent)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var src_app_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/base.component */ 83607);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _core_services_common_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/services/common.service */ 50690);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-extended-pdf-viewer */ 59547);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);







function PdfViewerComponent_ng_template_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 3)(1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](2, "pdf-toggle-sidebar")(3, "pdf-page-number")(4, "pdf-zoom-out")(5, "pdf-zoom-in")(6, "pdf-zoom-dropdown")(7, "pdf-rotate-page");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](9, "pdf-toggle-secondary-toolbar");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]()()();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵclassProp"]("invisible", false);
  }
}
function PdfViewerComponent_div_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "mat-spinner");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
  }
}
class PdfViewerComponent extends src_app_base_component__WEBPACK_IMPORTED_MODULE_0__.BaseComponent {
  constructor(commonService) {
    super();
    this.commonService = commonService;
    this.loadingTime = 2000;
    this.documentUrl = null;
    this.isLoading = false;
  }
  ngOnChanges(changes) {
    if (changes['document']) {
      this.getDocument();
    }
  }
  getDocument() {
    this.isLoading = true;
    this.sub$.sink = this.commonService.downloadDocument(this.document.documentId, this.document.isVersion).subscribe(event => {
      if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpEventType.Response) {
        this.isLoading = false;
        this.downloadFile(event);
      }
    }, err => {
      this.isLoading = false;
    });
  }
  downloadFile(data) {
    this.documentUrl = new Blob([data.body], {
      type: data.body.type
    });
  }
}
PdfViewerComponent.ɵfac = function PdfViewerComponent_Factory(t) {
  return new (t || PdfViewerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_core_services_common_service__WEBPACK_IMPORTED_MODULE_1__.CommonService));
};
PdfViewerComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
  type: PdfViewerComponent,
  selectors: [["app-pdf-viewer"]],
  inputs: {
    document: "document"
  },
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵNgOnChangesFeature"]],
  decls: 4,
  vars: 19,
  consts: [["height", "91vh", 3, "src", "showToolbar", "showSidebarButton", "showFindButton", "showPagingButtons", "showZoomButtons", "showPresentationModeButton", "showOpenFileButton", "showPrintButton", "showDownloadButton", "showBookmarkButton", "showSecondaryToolbarButton", "showRotateButton", "showHandToolButton", "showScrollingButton", "showSpreadButton", "showPropertiesButton", "useBrowserLocale"], ["customCheckboxZoomToolbar", ""], ["class", "loading-shade", 4, "ngIf"], ["id", "toolbarViewer"], ["id", "toolbarViewerMiddle"], [1, "float:right"], [1, "loading-shade"]],
  template: function PdfViewerComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ngx-extended-pdf-viewer", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, PdfViewerComponent_ng_template_1_Template, 10, 2, "ng-template", null, 1, _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplateRefExtractor"]);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](3, PdfViewerComponent_div_3_Template, 2, 0, "div", 2);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("src", ctx.documentUrl)("showToolbar", true)("showSidebarButton", true)("showFindButton", true)("showPagingButtons", true)("showZoomButtons", true)("showPresentationModeButton", true)("showOpenFileButton", false)("showPrintButton", false)("showDownloadButton", false)("showBookmarkButton", false)("showSecondaryToolbarButton", true)("showRotateButton", true)("showHandToolButton", true)("showScrollingButton", true)("showSpreadButton", false)("showPropertiesButton", false)("useBrowserLocale", false);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.isLoading);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__.PdfZoomDropdownComponent, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__.PdfRotatePageComponent, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__.PdfToggleSidebarComponent, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__.PdfToggleSecondaryToolbarComponent, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__.PdfPageNumberComponent, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__.PdfZoomInComponent, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__.PdfZoomOutComponent, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__.NgxExtendedPdfViewerComponent, _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__.MatProgressSpinner],
  styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 24586:
/*!**********************************************!*\
  !*** ./src/app/shared/pipes/pipes.module.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PipesModule": () => (/* binding */ PipesModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _truncate_pipe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./truncate.pipe */ 52821);
/* harmony import */ var _reminder_frequency_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reminder-frequency.pipe */ 8475);
/* harmony import */ var _utc_to_localtime_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utc-to-localtime.pipe */ 18017);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);





class PipesModule {}
PipesModule.ɵfac = function PipesModule_Factory(t) {
  return new (t || PipesModule)();
};
PipesModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({
  type: PipesModule
});
PipesModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](PipesModule, {
    declarations: [_truncate_pipe__WEBPACK_IMPORTED_MODULE_0__.TruncatePipe, _reminder_frequency_pipe__WEBPACK_IMPORTED_MODULE_1__.ReminderFrequencyPipe, _utc_to_localtime_pipe__WEBPACK_IMPORTED_MODULE_2__.UTCToLocalTime],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule],
    exports: [_truncate_pipe__WEBPACK_IMPORTED_MODULE_0__.TruncatePipe, _reminder_frequency_pipe__WEBPACK_IMPORTED_MODULE_1__.ReminderFrequencyPipe, _utc_to_localtime_pipe__WEBPACK_IMPORTED_MODULE_2__.UTCToLocalTime]
  });
})();

/***/ }),

/***/ 8475:
/*!*********************************************************!*\
  !*** ./src/app/shared/pipes/reminder-frequency.pipe.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReminderFrequencyPipe": () => (/* binding */ ReminderFrequencyPipe)
/* harmony export */ });
/* harmony import */ var _core_domain_classes_reminder_frequency__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/domain-classes/reminder-frequency */ 32760);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


class ReminderFrequencyPipe {
  transform(value, ...args) {
    const reminderFrequency = _core_domain_classes_reminder_frequency__WEBPACK_IMPORTED_MODULE_0__.reminderFrequencies.find(c => c.id == value);
    if (reminderFrequency) {
      return reminderFrequency.name;
    }
    return '';
  }
}
ReminderFrequencyPipe.ɵfac = function ReminderFrequencyPipe_Factory(t) {
  return new (t || ReminderFrequencyPipe)();
};
ReminderFrequencyPipe.ɵpipe = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefinePipe"]({
  name: "frequency",
  type: ReminderFrequencyPipe,
  pure: true
});

/***/ }),

/***/ 52821:
/*!***********************************************!*\
  !*** ./src/app/shared/pipes/truncate.pipe.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TruncatePipe": () => (/* binding */ TruncatePipe)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);

// tslint:disable-next-line:use-pipe-transform-interface
class TruncatePipe {
  transform(value, args) {
    if (!value) return '';
    const limit = args ? parseInt(args, 10) : 100;
    const trail = '...';
    return value.length > limit ? value.substring(0, limit) + trail : value;
  }
}
TruncatePipe.ɵfac = function TruncatePipe_Factory(t) {
  return new (t || TruncatePipe)();
};
TruncatePipe.ɵpipe = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
  name: "limitTo",
  type: TruncatePipe,
  pure: true
});

/***/ }),

/***/ 18017:
/*!*******************************************************!*\
  !*** ./src/app/shared/pipes/utc-to-localtime.pipe.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UTCToLocalTime": () => (/* binding */ UTCToLocalTime)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);

var UTCToLocalTimeFormat;
(function (UTCToLocalTimeFormat) {
  UTCToLocalTimeFormat["FULL"] = "full";
  UTCToLocalTimeFormat["SHORT"] = "short";
  UTCToLocalTimeFormat["SHORT_DATE"] = "shortDate";
  UTCToLocalTimeFormat["SHORT_TIME"] = "shortTime";
})(UTCToLocalTimeFormat || (UTCToLocalTimeFormat = {}));
class UTCToLocalTime {
  transform(utcDate, format) {
    const browserLanuges = navigator.language;
    if (!utcDate) {
      return '';
    }
    if (format === UTCToLocalTimeFormat.SHORT) {
      const date = new Date(utcDate).toLocaleDateString(browserLanuges);
      const time = new Date(utcDate).toLocaleTimeString(browserLanuges);
      return `${date} ${time}`;
      //  return moment.utc(utcDate).format("MM/DD/YYYY hh:mm:ss");
    } else if (format === UTCToLocalTimeFormat.SHORT_DATE) {
      const date = new Date(utcDate).toLocaleDateString(browserLanuges);
      return `${date}`;
    } else if (format === UTCToLocalTimeFormat.SHORT_TIME) {
      const time = new Date(utcDate).toLocaleTimeString(browserLanuges);
      return `${time}`;
    } else {
      const date = new Date(utcDate).toLocaleDateString(browserLanuges);
      const time = new Date(utcDate).toLocaleTimeString(browserLanuges);
      return `${date} ${time}`;
    }
  }
}
UTCToLocalTime.ɵfac = function UTCToLocalTime_Factory(t) {
  return new (t || UTCToLocalTime)();
};
UTCToLocalTime.ɵpipe = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
  name: "utcToLocalTime",
  type: UTCToLocalTime,
  pure: true
});

/***/ }),

/***/ 44466:
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharedModule": () => (/* binding */ SharedModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/cdk/overlay */ 25895);
/* harmony import */ var ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-extended-pdf-viewer */ 59547);
/* harmony import */ var _base_preview_base_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base-preview/base-preview.component */ 46235);
/* harmony import */ var _has_claim_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./has-claim.directive */ 79785);
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pipes/pipes.module */ 24586);
/* harmony import */ var _image_preview_image_preview_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./image-preview/image-preview.component */ 3046);
/* harmony import */ var _office_viewer_office_viewer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./office-viewer/office-viewer.component */ 52812);
/* harmony import */ var _pdf_viewer_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pdf-viewer/pdf-viewer.component */ 88948);
/* harmony import */ var _text_preview_text_preview_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./text-preview/text-preview.component */ 22638);
/* harmony import */ var _audio_preview_audio_preview_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./audio-preview/audio-preview.component */ 68842);
/* harmony import */ var _video_preview_video_preview_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./video-preview/video-preview.component */ 96352);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ngx-translate/core */ 38699);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./material.module */ 94872);
/* harmony import */ var _components_feather_icons_feather_icons_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/feather-icons/feather-icons.module */ 27545);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);



















class SharedModule {}
SharedModule.ɵfac = function SharedModule_Factory(t) {
  return new (t || SharedModule)();
};
SharedModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineNgModule"]({
  type: SharedModule
});
SharedModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineInjector"]({
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_12__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.ReactiveFormsModule, _angular_router__WEBPACK_IMPORTED_MODULE_14__.RouterModule, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_15__.OverlayModule, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_16__.NgxExtendedPdfViewerModule, _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_2__.PipesModule, _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_17__.MatProgressSpinnerModule, _angular_common__WEBPACK_IMPORTED_MODULE_12__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.ReactiveFormsModule, _angular_router__WEBPACK_IMPORTED_MODULE_14__.RouterModule, _material_module__WEBPACK_IMPORTED_MODULE_9__.MaterialModule, _components_feather_icons_feather_icons_module__WEBPACK_IMPORTED_MODULE_10__.FeatherIconsModule, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_15__.OverlayModule, _ngx_translate_core__WEBPACK_IMPORTED_MODULE_18__.TranslateModule, _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_2__.PipesModule]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵsetNgModuleScope"](SharedModule, {
    declarations: [_has_claim_directive__WEBPACK_IMPORTED_MODULE_1__.HasClaimDirective, _image_preview_image_preview_component__WEBPACK_IMPORTED_MODULE_3__.ImagePreviewComponent, _base_preview_base_preview_component__WEBPACK_IMPORTED_MODULE_0__.BasePreviewComponent, _pdf_viewer_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_5__.PdfViewerComponent, _text_preview_text_preview_component__WEBPACK_IMPORTED_MODULE_6__.TextPreviewComponent, _office_viewer_office_viewer_component__WEBPACK_IMPORTED_MODULE_4__.OfficeViewerComponent, _audio_preview_audio_preview_component__WEBPACK_IMPORTED_MODULE_7__.AudioPreviewComponent, _video_preview_video_preview_component__WEBPACK_IMPORTED_MODULE_8__.VideoPreviewComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_12__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.ReactiveFormsModule, _angular_router__WEBPACK_IMPORTED_MODULE_14__.RouterModule, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_15__.OverlayModule, ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_16__.NgxExtendedPdfViewerModule, _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_2__.PipesModule, _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_17__.MatProgressSpinnerModule],
    exports: [_angular_common__WEBPACK_IMPORTED_MODULE_12__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_13__.ReactiveFormsModule, _angular_router__WEBPACK_IMPORTED_MODULE_14__.RouterModule, _material_module__WEBPACK_IMPORTED_MODULE_9__.MaterialModule, _components_feather_icons_feather_icons_module__WEBPACK_IMPORTED_MODULE_10__.FeatherIconsModule, _has_claim_directive__WEBPACK_IMPORTED_MODULE_1__.HasClaimDirective, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_15__.OverlayModule, _image_preview_image_preview_component__WEBPACK_IMPORTED_MODULE_3__.ImagePreviewComponent, _base_preview_base_preview_component__WEBPACK_IMPORTED_MODULE_0__.BasePreviewComponent, _audio_preview_audio_preview_component__WEBPACK_IMPORTED_MODULE_7__.AudioPreviewComponent, _video_preview_video_preview_component__WEBPACK_IMPORTED_MODULE_8__.VideoPreviewComponent, _ngx_translate_core__WEBPACK_IMPORTED_MODULE_18__.TranslateModule, _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_2__.PipesModule]
  });
})();

/***/ }),

/***/ 22638:
/*!***************************************************************!*\
  !*** ./src/app/shared/text-preview/text-preview.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TextPreviewComponent": () => (/* binding */ TextPreviewComponent)
/* harmony export */ });
/* harmony import */ var src_app_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/base.component */ 83607);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _core_services_common_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/services/common.service */ 50690);
/* harmony import */ var _shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @shared/overlay-panel/overlay-panel-ref */ 3681);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);






function TextPreviewComponent_ng_container_1_pre_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "pre");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const line_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](line_r2);
  }
}
function TextPreviewComponent_ng_container_1_div_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
  }
}
function TextPreviewComponent_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, TextPreviewComponent_ng_container_1_pre_1_Template, 2, 1, "pre", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, TextPreviewComponent_ng_container_1_div_2_Template, 2, 0, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const line_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", line_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !line_r2);
  }
}
function TextPreviewComponent_div_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "mat-spinner");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
  }
}
class TextPreviewComponent extends src_app_base_component__WEBPACK_IMPORTED_MODULE_0__.BaseComponent {
  constructor(commonService, overlayRef) {
    super();
    this.commonService = commonService;
    this.overlayRef = overlayRef;
    this.textLines = [];
    this.isLoading = false;
  }
  ngOnChanges(changes) {
    if (changes['document']) {
      this.readDocument();
    }
  }
  readDocument() {
    this.isLoading = true;
    this.sub$.sink = this.commonService.readDocument(this.document.documentId, this.document.isVersion).subscribe(data => {
      this.isLoading = false;
      this.textLines = data['result'];
    }, err => {
      this.isLoading = false;
    });
  }
  onCancel() {
    this.overlayRef.close();
  }
}
TextPreviewComponent.ɵfac = function TextPreviewComponent_Factory(t) {
  return new (t || TextPreviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_core_services_common_service__WEBPACK_IMPORTED_MODULE_1__.CommonService), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_2__.OverlayPanelRef));
};
TextPreviewComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({
  type: TextPreviewComponent,
  selectors: [["app-text-preview"]],
  inputs: {
    document: "document"
  },
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵInheritDefinitionFeature"], _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵNgOnChangesFeature"]],
  decls: 3,
  vars: 2,
  consts: [[1, "m-5", "text-main-div"], [4, "ngFor", "ngForOf"], ["class", "loading-shade", 4, "ngIf"], [4, "ngIf"], [1, "loading-shade"]],
  template: function TextPreviewComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, TextPreviewComponent_ng_container_1_Template, 3, 2, "ng-container", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, TextPreviewComponent_div_2_Template, 2, 0, "div", 2);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngForOf", ctx.textLines);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.isLoading);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_5__.MatProgressSpinner],
  styles: [".text-div[_ngcontent-%COMP%] {\n  word-wrap: break-word;\n}\n\n.text-main-div[_ngcontent-%COMP%] {\n  overflow: auto;\n  height: 75vh;\n  background-color: #fff;\n  padding: 20px;\n}\n\npre[_ngcontent-%COMP%] {\n  display: block;\n  white-space: pre-wrap;\n  word-wrap: break-word;\n  margin: 0;\n  padding: 0px !important;\n  font-size: 1rem;\n  font-family: inherit;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hhcmVkL3RleHQtcHJldmlldy90ZXh0LXByZXZpZXcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBQTtBQUNGOztBQUVBO0VBQ0UsY0FBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7QUFDRjs7QUFFQTtFQUNFLGNBQUE7RUFDQSxxQkFBQTtFQUNBLHFCQUFBO0VBQ0EsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyIudGV4dC1kaXYge1xyXG4gIHdvcmQtd3JhcDogYnJlYWstd29yZDtcclxufVxyXG5cclxuLnRleHQtbWFpbi1kaXYge1xyXG4gIG92ZXJmbG93ICAgICAgICA6IGF1dG87XHJcbiAgaGVpZ2h0ICAgICAgICAgIDogNzV2aDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbn1cclxuXHJcbnByZSB7XHJcbiAgZGlzcGxheSAgICA6IGJsb2NrO1xyXG4gIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcclxuICB3b3JkLXdyYXAgIDogYnJlYWstd29yZDtcclxuICBtYXJnaW4gICAgIDogMDtcclxuICBwYWRkaW5nICAgIDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgZm9udC1zaXplICA6IDFyZW07XHJcbiAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XHJcbn0iXSwic291cmNlUm9vdCI6IiJ9 */"]
});

/***/ }),

/***/ 96352:
/*!*****************************************************************!*\
  !*** ./src/app/shared/video-preview/video-preview.component.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VideoPreviewComponent": () => (/* binding */ VideoPreviewComponent)
/* harmony export */ });
/* harmony import */ var _shared_audio_preview_audio_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared/audio-preview/audio-preview.component */ 68842);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @shared/overlay-panel/overlay-panel-ref */ 3681);
/* harmony import */ var _core_services_common_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @core/services/common.service */ 50690);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);






function VideoPreviewComponent_div_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "mat-spinner");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
  }
}
class VideoPreviewComponent extends _shared_audio_preview_audio_preview_component__WEBPACK_IMPORTED_MODULE_0__.AudioPreviewComponent {
  constructor(overlayRef, commonService) {
    super(overlayRef, commonService);
    this.overlayRef = overlayRef;
    this.commonService = commonService;
  }
  ngOnChanges(changes) {
    if (changes['document']) {
      this.getDocument();
    }
  }
}
VideoPreviewComponent.ɵfac = function VideoPreviewComponent_Factory(t) {
  return new (t || VideoPreviewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_shared_overlay_panel_overlay_panel_ref__WEBPACK_IMPORTED_MODULE_1__.OverlayPanelRef), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_core_services_common_service__WEBPACK_IMPORTED_MODULE_2__.CommonService));
};
VideoPreviewComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({
  type: VideoPreviewComponent,
  selectors: [["app-video-preview"]],
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵInheritDefinitionFeature"], _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵNgOnChangesFeature"]],
  decls: 4,
  vars: 1,
  consts: [[1, "video-div"], ["controls", "controls"], ["playerEl", ""], ["class", "loading-shade", 4, "ngIf"], [1, "loading-shade"]],
  template: function VideoPreviewComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "video", 1, 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, VideoPreviewComponent_div_3_Template, 2, 0, "div", 3);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.isLoading);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_5__.MatProgressSpinner],
  styles: [".video-div[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  min-height: calc(100vh - 60px);\n  background: rgba(15, 15, 15, 0.9490196078);\n}\n\nvideo[_ngcontent-%COMP%] {\n  width: 65vw;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvc2hhcmVkL3ZpZGVvLXByZXZpZXcvdmlkZW8tcHJldmlldy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLDhCQUFBO0VBQ0EsMENBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSiIsInNvdXJjZXNDb250ZW50IjpbIi52aWRlby1kaXYge1xyXG4gICAgZGlzcGxheSAgICAgICAgOiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb24gOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zICAgIDogY2VudGVyO1xyXG4gICAgdGV4dC1hbGlnbiAgICAgOiBjZW50ZXI7XHJcbiAgICBtaW4taGVpZ2h0ICAgICA6IGNhbGMoMTAwdmggLSA2MHB4KTs7XHJcbiAgICBiYWNrZ3JvdW5kICAgICA6ICMwZjBmMGZmMjtcclxufVxyXG5cclxudmlkZW8ge1xyXG4gICAgd2lkdGg6IDY1dnc7XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 33007:
/*!*******************************************!*\
  !*** ./src/app/store/app-store.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppStoreModule": () => (/* binding */ AppStoreModule)
/* harmony export */ });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ 23488);
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/effects */ 5405);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @environments/environment */ 92340);
/* harmony import */ var _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store-devtools */ 55242);
/* harmony import */ var _entity_metadata__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./entity-metadata */ 92552);
/* harmony import */ var _ngrx_data__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngrx/data */ 90781);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);











class AppStoreModule {}
AppStoreModule.ɵfac = function AppStoreModule_Factory(t) {
  return new (t || AppStoreModule)();
};
AppStoreModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
  type: AppStoreModule
});
AppStoreModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
  imports: [_ngrx_store__WEBPACK_IMPORTED_MODULE_3__.StoreModule.forRoot({}), _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__.EffectsModule.forRoot([]), _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.production ? [] : _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_5__.StoreDevtoolsModule.instrument(), _ngrx_data__WEBPACK_IMPORTED_MODULE_6__.EntityDataModule.forRoot(_entity_metadata__WEBPACK_IMPORTED_MODULE_1__.entityConfig)]
});
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AppStoreModule, {
    imports: [_ngrx_store__WEBPACK_IMPORTED_MODULE_3__.StoreRootModule, _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__.EffectsRootModule, _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_5__.StoreDevtoolsModule, _ngrx_data__WEBPACK_IMPORTED_MODULE_6__.EntityDataModule]
  });
})();

/***/ }),

/***/ 92552:
/*!******************************************!*\
  !*** ./src/app/store/entity-metadata.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "entityConfig": () => (/* binding */ entityConfig)
/* harmony export */ });
const entityMetadata = {
  Page: {},
  Action: {},
  PageAction: {},
  Category: {}
};
const pluralNames = {
  Category: 'Categories'
};
const entityConfig = {
  entityMetadata,
  pluralNames
};

/***/ }),

/***/ 92384:
/*!*******************************************************************!*\
  !*** ./src/app/user/change-password/change-password.component.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChangePasswordComponent": () => (/* binding */ ChangePasswordComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var src_app_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/base.component */ 83607);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../user.service */ 1584);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ 94817);
/* harmony import */ var _core_security_security_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @core/security/security.service */ 40130);
/* harmony import */ var _core_services_translation_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @core/services/translation.service */ 16107);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-translate/core */ 38699);












function ChangePasswordComponent_div_15_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](2, 1, "CURRENT_PASSWORD_IS_REQUIRED"), " ");
  }
}
function ChangePasswordComponent_div_15_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, ChangePasswordComponent_div_15_div_1_Template, 3, 3, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r0.changePasswordForm.get("oldPasswordPassword").errors == null ? null : ctx_r0.changePasswordForm.get("oldPasswordPassword").errors["required"]);
  }
}
function ChangePasswordComponent_div_21_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](2, 1, "PASSWORD_IS_REQUIRED"), " ");
  }
}
function ChangePasswordComponent_div_21_div_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](2, 1, "YOU_HAVE_TO_ENTER_AT_LEAST_DIGIT"), " ");
  }
}
function ChangePasswordComponent_div_21_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, ChangePasswordComponent_div_21_div_1_Template, 3, 3, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](2, ChangePasswordComponent_div_21_div_2_Template, 3, 3, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r1.changePasswordForm.get("password").errors == null ? null : ctx_r1.changePasswordForm.get("password").errors["required"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r1.changePasswordForm.get("password").errors == null ? null : ctx_r1.changePasswordForm.get("password").errors["minlength"]);
  }
}
function ChangePasswordComponent_div_27_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](2, 1, "CONFIRM_PASSWORD_IS_REQUIRED"), " ");
  }
}
function ChangePasswordComponent_div_27_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, ChangePasswordComponent_div_27_div_1_Template, 3, 3, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r2.changePasswordForm.get("confirmPassword").errors == null ? null : ctx_r2.changePasswordForm.get("confirmPassword").errors["required"]);
  }
}
function ChangePasswordComponent_div_28_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](2, 1, "PASSWORDS_DO_NOT_MATCH"), " ");
  }
}
class ChangePasswordComponent extends src_app_base_component__WEBPACK_IMPORTED_MODULE_0__.BaseComponent {
  constructor(userService, fb, dialogRef, data, toastrService, securityService, translationService) {
    super();
    this.userService = userService;
    this.fb = fb;
    this.dialogRef = dialogRef;
    this.data = data;
    this.toastrService = toastrService;
    this.securityService = securityService;
    this.translationService = translationService;
  }
  ngOnInit() {
    this.createChangePasswordForm();
    this.changePasswordForm.get('email').setValue(this.data.user.userName);
  }
  createChangePasswordForm() {
    this.changePasswordForm = this.fb.group({
      email: [{
        value: '',
        disabled: true
      }],
      oldPasswordPassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required]],
      password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.minLength(6)]],
      confirmPassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required]]
    }, {
      validator: this.checkPasswords
    });
  }
  checkPasswords(group) {
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value;
    return pass === confirmPass ? null : {
      notSame: true
    };
  }
  changePassword() {
    if (this.changePasswordForm.valid) {
      this.sub$.sink = this.userService.changePassword(this.createBuildObject()).subscribe(d => {
        this.toastrService.success(this.translationService.getValue('SUCCESSFULLY_CHANGED_PASSWORD'));
        this.securityService.logout();
        this.dialogRef.close();
      });
    } else {
      this.changePasswordForm.markAllAsTouched();
    }
  }
  createBuildObject() {
    return {
      email: '',
      oldPassword: this.changePasswordForm.get('oldPasswordPassword').value,
      newPassword: this.changePasswordForm.get('password').value,
      userName: this.changePasswordForm.get('email').value
    };
  }
  onNoClick() {
    this.dialogRef.close();
  }
}
ChangePasswordComponent.ɵfac = function ChangePasswordComponent_Factory(t) {
  return new (t || ChangePasswordComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_user_service__WEBPACK_IMPORTED_MODULE_1__.UserService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_5__.UntypedFormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__.MatDialogRef), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__.MAT_DIALOG_DATA), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_7__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_core_security_security_service__WEBPACK_IMPORTED_MODULE_2__.SecurityService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_core_services_translation_service__WEBPACK_IMPORTED_MODULE_3__.TranslationService));
};
ChangePasswordComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({
  type: ChangePasswordComponent,
  selectors: [["app-change-password"]],
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵInheritDefinitionFeature"]],
  decls: 38,
  vars: 26,
  consts: [["mat-dialog-title", ""], ["mat-dialog-content", ""], [3, "formGroup"], [1, "col-md-12", "m-b-10"], [1, "form-label"], ["formControlName", "email", "type", "email", 1, "form-control"], ["autocomplete", "new-password", "formControlName", "oldPasswordPassword", "type", "password", 1, "form-control"], [4, "ngIf"], ["autocomplete", "new-password", "formControlName", "password", "type", "password", 1, "form-control"], [1, "col-md-12"], ["autocomplete", "new-password", "formControlName", "confirmPassword", "type", "password", 1, "form-control"], ["class", "text-danger", 4, "ngIf"], ["mat-dialog-actions", ""], ["cdkFocusInitial", "", 1, "btn", "btn-success", "btn-sm", "m-r-10", 3, "click"], [1, "fas", "fa-save"], [1, "btn", "btn-danger", "btn-sm", 3, "click"], [1, "fas", "fa-times-circle"], [1, "text-danger"]],
  template: function ChangePasswordComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "h1", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](2, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "div", 1)(4, "form", 2)(5, "div", 3)(6, "label", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](7);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](8, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](9, "input", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "div", 3)(11, "label", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](12);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](13, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](14, "input", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](15, ChangePasswordComponent_div_15_Template, 2, 1, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "div", 3)(17, "label", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](18);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](19, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](20, "input", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](21, ChangePasswordComponent_div_21_Template, 3, 2, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](22, "div", 9)(23, "label", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](24);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](25, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](26, "input", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](27, ChangePasswordComponent_div_27_Template, 2, 1, "div", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](28, ChangePasswordComponent_div_28_Template, 3, 3, "div", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](29, "div", 12)(30, "button", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function ChangePasswordComponent_Template_button_click_30_listener() {
        return ctx.changePassword();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](31, "i", 14);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](32);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](33, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](34, "button", 15);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function ChangePasswordComponent_Template_button_click_34_listener() {
        return ctx.onNoClick();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](35, "i", 16);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](36);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipe"](37, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]()();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](2, 12, "CHANGE_PASSWORD"));
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.changePasswordForm);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](8, 14, "EMAIL"));
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](13, 16, "CURRENT_PASSWORD"));
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.changePasswordForm.get("oldPasswordPassword").touched && ctx.changePasswordForm.get("oldPasswordPassword").errors);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](19, 18, "NEW_PASSWORD"));
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.changePasswordForm.get("password").touched && ctx.changePasswordForm.get("password").errors);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](25, 20, "CONFIRM_PASSWORD"));
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.changePasswordForm.get("confirmPassword").touched && ctx.changePasswordForm.get("confirmPassword").errors);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.changePasswordForm.get("confirmPassword").touched && ctx.changePasswordForm.hasError("notSame"));
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](33, 22, "SAVE"), "");
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpipeBind1"](37, 24, "CANCEL"), "");
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.NgIf, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroupDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControlName, _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__.MatDialogTitle, _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__.MatDialogContent, _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__.MatDialogActions, _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__.TranslatePipe],
  styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 76811:
/*!*********************************************************!*\
  !*** ./src/app/user/my-profile/my-profile.component.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MyProfileComponent": () => (/* binding */ MyProfileComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var src_app_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/base.component */ 83607);
/* harmony import */ var _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../change-password/change-password.component */ 92384);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.service */ 1584);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ 94817);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _core_security_security_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @core/security/security.service */ 40130);
/* harmony import */ var _core_services_translation_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @core/services/translation.service */ 16107);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _shared_components_feather_icons_feather_icons_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/components/feather-icons/feather-icons.component */ 61676);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-translate/core */ 38699);














function MyProfileComponent_div_29_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipeBind1"](2, 1, "FIRST_NAME_IS_REQUIRED"), " ");
  }
}
function MyProfileComponent_div_29_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtemplate"](1, MyProfileComponent_div_29_div_1_Template, 3, 3, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("ngIf", ctx_r0.userForm.get("firstName").errors == null ? null : ctx_r0.userForm.get("firstName").errors["required"]);
  }
}
function MyProfileComponent_div_36_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipeBind1"](2, 1, "LAST_NAME_IS_REQUIRED"), " ");
  }
}
function MyProfileComponent_div_36_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtemplate"](1, MyProfileComponent_div_36_div_1_Template, 3, 3, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("ngIf", ctx_r1.userForm.get("lastName").errors == null ? null : ctx_r1.userForm.get("lastName").errors["required"]);
  }
}
function MyProfileComponent_div_43_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipeBind1"](2, 1, "MOBILE_IS_REQUIRED"), " ");
  }
}
function MyProfileComponent_div_43_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtemplate"](1, MyProfileComponent_div_43_div_1_Template, 3, 3, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("ngIf", ctx_r2.userForm.get("phoneNumber").errors == null ? null : ctx_r2.userForm.get("phoneNumber").errors["required"]);
  }
}
const _c0 = function () {
  return ["/"];
};
class MyProfileComponent extends src_app_base_component__WEBPACK_IMPORTED_MODULE_0__.BaseComponent {
  constructor(router, fb, userService, toastrService, dialog, securityService, translationService) {
    super();
    this.router = router;
    this.fb = fb;
    this.userService = userService;
    this.toastrService = toastrService;
    this.dialog = dialog;
    this.securityService = securityService;
    this.translationService = translationService;
  }
  ngOnInit() {
    this.createUserForm();
    this.user = this.securityService.getUserDetail();
    if (this.user) {
      this.userForm.patchValue(this.user.user);
    }
  }
  createUserForm() {
    this.userForm = this.fb.group({
      id: [''],
      firstName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required]],
      lastName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required]],
      email: [{
        value: '',
        disabled: true
      }, [_angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.email]],
      phoneNumber: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required]]
    });
  }
  updateProfile() {
    if (this.userForm.valid) {
      const user = this.createBuildObject();
      this.sub$.sink = this.userService.updateUserProfile(user).subscribe(user => {
        this.user.user.firstName = user.firstName;
        this.user.user.lastName = user.lastName;
        this.user.user.phoneNumber = user.phoneNumber;
        this.toastrService.success(this.translationService.getValue('PROFILE_UPDATED_SUCCESSFULLY'));
        this.securityService.setUserDetail(this.user);
        this.router.navigate(['/']);
      });
    } else {
      this.userForm.markAllAsTouched();
    }
  }
  createBuildObject() {
    const user = {
      id: this.userForm.get('id').value,
      firstName: this.userForm.get('firstName').value,
      lastName: this.userForm.get('lastName').value,
      email: this.userForm.get('email').value,
      phoneNumber: this.userForm.get('phoneNumber').value,
      userName: this.userForm.get('email').value
    };
    return user;
  }
  changePassword() {
    this.dialog.open(_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_1__.ChangePasswordComponent, {
      width: '350px',
      data: Object.assign({}, this.user)
    });
  }
}
MyProfileComponent.ɵfac = function MyProfileComponent_Factory(t) {
  return new (t || MyProfileComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_8__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_7__.UntypedFormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_user_service__WEBPACK_IMPORTED_MODULE_2__.UserService), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_9__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__.MatDialog), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_core_security_security_service__WEBPACK_IMPORTED_MODULE_3__.SecurityService), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_core_services_translation_service__WEBPACK_IMPORTED_MODULE_4__.TranslationService));
};
MyProfileComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
  type: MyProfileComponent,
  selectors: [["app-my-profile"]],
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
  decls: 56,
  vars: 24,
  consts: [[1, "content"], [1, "content-block"], [1, "block-header"], [1, "breadcrumb-main"], [1, "row"], [1, "col-6"], [1, "breadcrumb-title"], [1, "breadcrumb-list"], [1, "breadcrumb-item", "bcrumb-1"], [1, "btn", "btn-outline-success", "btn-sm", 3, "click"], [3, "icon"], [1, "col-xl-12", "col-lg-12", "col-md-12", "col-sm-12"], [1, "card"], [1, "body"], [3, "formGroup"], [1, "col-md-6"], [1, "form-group"], ["formControlName", "firstName", "type", "text", 1, "form-control"], [4, "ngIf"], ["formControlName", "lastName", "type", "text", 1, "form-control"], ["formControlName", "phoneNumber", "type", "text", 1, "form-control"], ["formControlName", "email", "type", "email", 1, "form-control"], [1, "m-t-10", "col-md-12"], ["cdkFocusInitial", "", 1, "btn", "btn-success", "btn-sm", "m-r-10", 3, "click"], [1, "fas", "fa-save"], ["type", "button", 1, "btn", "btn-danger", "btn-sm", 3, "routerLink"], [1, "fas", "fa-times-circle"], ["class", "text-danger", 4, "ngIf"], [1, "text-danger"]],
  template: function MyProfileComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](0, "section", 0)(1, "div", 1)(2, "div", 2)(3, "div", 3)(4, "div", 4)(5, "div", 5)(6, "div", 6)(7, "h2");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](8);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipe"](9, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](10, "div", 5)(11, "ul", 7)(12, "li", 8)(13, "a", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵlistener"]("click", function MyProfileComponent_Template_a_click_13_listener() {
        return ctx.changePassword();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](14, "app-feather-icons", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](15);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipe"](16, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]()()()()()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](17, "div", 4)(18, "div", 11)(19, "div", 12)(20, "div", 13)(21, "form", 14)(22, "div", 4)(23, "div", 15)(24, "div", 16)(25, "label");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](26);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipe"](27, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](28, "input", 17);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtemplate"](29, MyProfileComponent_div_29_Template, 2, 1, "div", 18);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](30, "div", 15)(31, "div", 16)(32, "label");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](33);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipe"](34, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](35, "input", 19);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtemplate"](36, MyProfileComponent_div_36_Template, 2, 1, "div", 18);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](37, "div", 15)(38, "div", 16)(39, "label");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](40);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipe"](41, "translate");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](42, "input", 20);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtemplate"](43, MyProfileComponent_div_43_Template, 2, 1, "div", 18);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](44, "div", 15)(45, "div", 16)(46, "label");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](47, "Email");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](48, "input", 21);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](49, "div", 22)(50, "button", 23);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵlistener"]("click", function MyProfileComponent_Template_button_click_50_listener() {
        return ctx.updateProfile();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](51, "i", 24);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](52, " Save");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](53, "button", 25);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](54, "i", 26);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](55, " Cancel");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]()()()()()()()()()();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](8);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipeBind1"](9, 13, "PROFILE"));
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](6);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵclassMap"]("btn-success");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("icon", "key");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipeBind1"](16, 15, "CHANGE_PASSWORD"), " ");
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](6);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("formGroup", ctx.userForm);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipeBind1"](27, 17, "FIRST_NAME"));
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("ngIf", ctx.userForm.get("firstName").touched && ctx.userForm.get("firstName").errors);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipeBind1"](34, 19, "LAST_NAME"));
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("ngIf", ctx.userForm.get("lastName").touched && ctx.userForm.get("lastName").errors);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpipeBind1"](41, 21, "MOBILE_NUMBER"));
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("ngIf", ctx.userForm.get("phoneNumber").touched && ctx.userForm.get("phoneNumber").errors);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](10);
      _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵpureFunction0"](23, _c0));
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.NgIf, _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterLink, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormGroupDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormControlName, _shared_components_feather_icons_feather_icons_component__WEBPACK_IMPORTED_MODULE_5__.FeatherIconsComponent, _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__.TranslatePipe],
  styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
});

/***/ }),

/***/ 1584:
/*!**************************************!*\
  !*** ./src/app/user/user.service.ts ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserService": () => (/* binding */ UserService)
/* harmony export */ });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 53158);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/error-handler/common-http-error.service */ 48032);




class UserService {
  constructor(httpClient, commonHttpErrorService) {
    this.httpClient = httpClient;
    this.commonHttpErrorService = commonHttpErrorService;
  }
  updateUser(user) {
    const url = `user/${user.id}`;
    return this.httpClient.put(url, user).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  addUser(user) {
    const url = `user`;
    return this.httpClient.post(url, user).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  deleteUser(id) {
    const url = `user/${id}`;
    return this.httpClient.delete(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  getUser(id) {
    const url = `user/${id}`;
    return this.httpClient.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  updateUserClaim(userClaims, userId) {
    const url = `userClaim/${userId}`;
    return this.httpClient.put(url, {
      userClaims
    }).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  resetPassword(user) {
    const url = `user/resetpassword`;
    return this.httpClient.post(url, user).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  changePassword(user) {
    const url = `user/changepassword`;
    return this.httpClient.post(url, user).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
  updateUserProfile(user) {
    const url = `users/profile`;
    return this.httpClient.put(url, user).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.catchError)(this.commonHttpErrorService.handleError));
  }
}
UserService.ɵfac = function UserService_Factory(t) {
  return new (t || UserService)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_0__.CommonHttpErrorService));
};
UserService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({
  token: UserService,
  factory: UserService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 92340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
  production: false,
  apiUrl: 'http://localhost:8000/',
  tokenExpiredTimeInMin: 50,
  allowExtesions: [{
    type: 'office',
    extentions: ['doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx']
  }, {
    type: 'pdf',
    extentions: ['pdf']
  }, {
    type: 'image',
    extentions: ['jpg', 'jpeg', 'png', 'gif', 'tiff', 'psd', 'bmp', 'webp', 'raw', 'bmp', 'heif', 'indd', 'svg', 'ai', 'eps']
  }, {
    type: 'text',
    extentions: ['txt', 'csv']
  }, {
    type: 'audio',
    extentions: ['3gp', 'aa', 'aac', 'aax', 'act', 'aiff', 'alac', 'amr', 'ape', 'au', 'awb', 'dss', 'dvf', 'flac', 'gsm', 'iklx', 'ivs', 'm4a', 'm4b', 'm4p', 'mmf', 'mp3', 'mpc', 'msv', 'nmf', 'ogg', 'oga', 'mogg', 'opus', 'org', 'ra', 'rm', 'raw', 'rf64', 'sln', 'tta', 'voc', 'vox', 'wav', 'wma', 'wv']
  }, {
    type: 'video',
    extentions: ['webm', 'flv', 'vob', 'ogv', 'ogg', 'drc', 'avi', 'mts', 'm2ts', 'wmv', 'yuv', 'viv', 'mp4', 'm4p', '3pg', 'flv', 'f4v', 'f4a']
  }]
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

/***/ }),

/***/ 14431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ 34497);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 36747);


_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__.platformBrowser().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule).catch(err => console.error(err));

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(14431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map