(self["webpackChunkdocument_management"] = self["webpackChunkdocument_management"] || []).push([["default-src_app_core_domain-classes_dayOfWeek_enum_ts-src_app_core_domain-classes_frequency_e-a11b28"],{

/***/ 12851:
/*!*******************************************************!*\
  !*** ./src/app/core/domain-classes/dayOfWeek.enum.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DayOfWeek": () => (/* binding */ DayOfWeek)
/* harmony export */ });
var DayOfWeek;
(function (DayOfWeek) {
  DayOfWeek[DayOfWeek["Sunday"] = 0] = "Sunday";
  DayOfWeek[DayOfWeek["Monday"] = 1] = "Monday";
  DayOfWeek[DayOfWeek["Tuesday"] = 2] = "Tuesday";
  DayOfWeek[DayOfWeek["Wednesday"] = 3] = "Wednesday";
  DayOfWeek[DayOfWeek["Thursday"] = 4] = "Thursday";
  DayOfWeek[DayOfWeek["Friday"] = 5] = "Friday";
  DayOfWeek[DayOfWeek["Saturday"] = 6] = "Saturday";
})(DayOfWeek || (DayOfWeek = {}));

/***/ }),

/***/ 44598:
/*!*******************************************************!*\
  !*** ./src/app/core/domain-classes/frequency.enum.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Frequency": () => (/* binding */ Frequency)
/* harmony export */ });
var Frequency;
(function (Frequency) {
  Frequency[Frequency["Daily"] = 0] = "Daily";
  Frequency[Frequency["Weekly"] = 1] = "Weekly";
  Frequency[Frequency["Monthly"] = 2] = "Monthly";
  Frequency[Frequency["Quarterly"] = 3] = "Quarterly";
  Frequency[Frequency["HalfYearly"] = 4] = "HalfYearly";
  Frequency[Frequency["Yearly"] = 5] = "Yearly";
})(Frequency || (Frequency = {}));

/***/ }),

/***/ 71357:
/*!*****************************************************!*\
  !*** ./src/app/core/domain-classes/quarter.enum.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Quarter": () => (/* binding */ Quarter)
/* harmony export */ });
var Quarter;
(function (Quarter) {
  Quarter[Quarter["Quarter1"] = 0] = "Quarter1";
  Quarter[Quarter["Quarter2"] = 1] = "Quarter2";
  Quarter[Quarter["Quarter3"] = 2] = "Quarter3";
  Quarter[Quarter["Quarter4"] = 3] = "Quarter4";
})(Quarter || (Quarter = {}));

/***/ }),

/***/ 75519:
/*!***********************************************************!*\
  !*** ./src/app/core/domain-classes/resource-parameter.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ResourceParameter": () => (/* binding */ ResourceParameter)
/* harmony export */ });
class ResourceParameter {
  constructor() {
    this.fields = '';
    this.orderBy = '';
    this.searchQuery = '';
    this.pageSize = 30;
    this.skip = 0;
    this.name = '';
    this.totalCount = 0;
    this.metaTags = '';
  }
}

/***/ }),

/***/ 53037:
/*!**********************************************!*\
  !*** ./src/app/reminder/reminder.service.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReminderService": () => (/* binding */ ReminderService)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 53158);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @core/error-handler/common-http-error.service */ 48032);





class ReminderService {
  constructor(httpClient, commonHttpErrorService) {
    this.httpClient = httpClient;
    this.commonHttpErrorService = commonHttpErrorService;
  }
  getReminders(resourceParams) {
    const url = 'reminder/all';
    const customParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpParams().set('fields', resourceParams.fields ? resourceParams.fields : '').set('orderBy', resourceParams.orderBy ? resourceParams.orderBy : '').set('pageSize', resourceParams.pageSize.toString()).set('skip', resourceParams.skip.toString()).set('searchQuery', resourceParams.searchQuery ? resourceParams.searchQuery : '').set('subject', resourceParams.subject ? resourceParams.subject : '').set('message', resourceParams.message ? resourceParams.message : '').set('frequency', resourceParams.frequency ? resourceParams.frequency : '');
    return this.httpClient.get(url, {
      params: customParams,
      observe: 'response'
    });
  }
  addReminder(reminder) {
    const url = `reminder`;
    return this.httpClient.post(url, reminder).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
  addDocumentReminder(reminder) {
    const url = `reminder/document`;
    return this.httpClient.post(url, reminder).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
  updateReminder(reminder) {
    const url = `reminder/${reminder.id}`;
    return this.httpClient.put(url, reminder).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
  deleteReminder(id) {
    const url = `reminder/${id}`;
    return this.httpClient.delete(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(this.commonHttpErrorService.handleError));
  }
}
ReminderService.ɵfac = function ReminderService_Factory(t) {
  return new (t || ReminderService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_core_error_handler_common_http_error_service__WEBPACK_IMPORTED_MODULE_0__.CommonHttpErrorService));
};
ReminderService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
  token: ReminderService,
  factory: ReminderService.ɵfac,
  providedIn: 'root'
});

/***/ }),

/***/ 16901:
/*!*****************************************!*\
  !*** ./node_modules/dayjs/dayjs.min.js ***!
  \*****************************************/
/***/ (function(module) {

!function (t, e) {
   true ? module.exports = e() : 0;
}(this, function () {
  "use strict";

  var t = 1e3,
    e = 6e4,
    n = 36e5,
    r = "millisecond",
    i = "second",
    s = "minute",
    u = "hour",
    a = "day",
    o = "week",
    f = "month",
    h = "quarter",
    c = "year",
    d = "date",
    l = "Invalid Date",
    $ = /^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[Tt\s]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/,
    y = /\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g,
    M = {
      name: "en",
      weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
      months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
      ordinal: function (t) {
        var e = ["th", "st", "nd", "rd"],
          n = t % 100;
        return "[" + t + (e[(n - 20) % 10] || e[n] || e[0]) + "]";
      }
    },
    m = function (t, e, n) {
      var r = String(t);
      return !r || r.length >= e ? t : "" + Array(e + 1 - r.length).join(n) + t;
    },
    v = {
      s: m,
      z: function (t) {
        var e = -t.utcOffset(),
          n = Math.abs(e),
          r = Math.floor(n / 60),
          i = n % 60;
        return (e <= 0 ? "+" : "-") + m(r, 2, "0") + ":" + m(i, 2, "0");
      },
      m: function t(e, n) {
        if (e.date() < n.date()) return -t(n, e);
        var r = 12 * (n.year() - e.year()) + (n.month() - e.month()),
          i = e.clone().add(r, f),
          s = n - i < 0,
          u = e.clone().add(r + (s ? -1 : 1), f);
        return +(-(r + (n - i) / (s ? i - u : u - i)) || 0);
      },
      a: function (t) {
        return t < 0 ? Math.ceil(t) || 0 : Math.floor(t);
      },
      p: function (t) {
        return {
          M: f,
          y: c,
          w: o,
          d: a,
          D: d,
          h: u,
          m: s,
          s: i,
          ms: r,
          Q: h
        }[t] || String(t || "").toLowerCase().replace(/s$/, "");
      },
      u: function (t) {
        return void 0 === t;
      }
    },
    g = "en",
    D = {};
  D[g] = M;
  var p = function (t) {
      return t instanceof _;
    },
    S = function t(e, n, r) {
      var i;
      if (!e) return g;
      if ("string" == typeof e) {
        var s = e.toLowerCase();
        D[s] && (i = s), n && (D[s] = n, i = s);
        var u = e.split("-");
        if (!i && u.length > 1) return t(u[0]);
      } else {
        var a = e.name;
        D[a] = e, i = a;
      }
      return !r && i && (g = i), i || !r && g;
    },
    w = function (t, e) {
      if (p(t)) return t.clone();
      var n = "object" == typeof e ? e : {};
      return n.date = t, n.args = arguments, new _(n);
    },
    O = v;
  O.l = S, O.i = p, O.w = function (t, e) {
    return w(t, {
      locale: e.$L,
      utc: e.$u,
      x: e.$x,
      $offset: e.$offset
    });
  };
  var _ = function () {
      function M(t) {
        this.$L = S(t.locale, null, !0), this.parse(t);
      }
      var m = M.prototype;
      return m.parse = function (t) {
        this.$d = function (t) {
          var e = t.date,
            n = t.utc;
          if (null === e) return new Date(NaN);
          if (O.u(e)) return new Date();
          if (e instanceof Date) return new Date(e);
          if ("string" == typeof e && !/Z$/i.test(e)) {
            var r = e.match($);
            if (r) {
              var i = r[2] - 1 || 0,
                s = (r[7] || "0").substring(0, 3);
              return n ? new Date(Date.UTC(r[1], i, r[3] || 1, r[4] || 0, r[5] || 0, r[6] || 0, s)) : new Date(r[1], i, r[3] || 1, r[4] || 0, r[5] || 0, r[6] || 0, s);
            }
          }
          return new Date(e);
        }(t), this.$x = t.x || {}, this.init();
      }, m.init = function () {
        var t = this.$d;
        this.$y = t.getFullYear(), this.$M = t.getMonth(), this.$D = t.getDate(), this.$W = t.getDay(), this.$H = t.getHours(), this.$m = t.getMinutes(), this.$s = t.getSeconds(), this.$ms = t.getMilliseconds();
      }, m.$utils = function () {
        return O;
      }, m.isValid = function () {
        return !(this.$d.toString() === l);
      }, m.isSame = function (t, e) {
        var n = w(t);
        return this.startOf(e) <= n && n <= this.endOf(e);
      }, m.isAfter = function (t, e) {
        return w(t) < this.startOf(e);
      }, m.isBefore = function (t, e) {
        return this.endOf(e) < w(t);
      }, m.$g = function (t, e, n) {
        return O.u(t) ? this[e] : this.set(n, t);
      }, m.unix = function () {
        return Math.floor(this.valueOf() / 1e3);
      }, m.valueOf = function () {
        return this.$d.getTime();
      }, m.startOf = function (t, e) {
        var n = this,
          r = !!O.u(e) || e,
          h = O.p(t),
          l = function (t, e) {
            var i = O.w(n.$u ? Date.UTC(n.$y, e, t) : new Date(n.$y, e, t), n);
            return r ? i : i.endOf(a);
          },
          $ = function (t, e) {
            return O.w(n.toDate()[t].apply(n.toDate("s"), (r ? [0, 0, 0, 0] : [23, 59, 59, 999]).slice(e)), n);
          },
          y = this.$W,
          M = this.$M,
          m = this.$D,
          v = "set" + (this.$u ? "UTC" : "");
        switch (h) {
          case c:
            return r ? l(1, 0) : l(31, 11);
          case f:
            return r ? l(1, M) : l(0, M + 1);
          case o:
            var g = this.$locale().weekStart || 0,
              D = (y < g ? y + 7 : y) - g;
            return l(r ? m - D : m + (6 - D), M);
          case a:
          case d:
            return $(v + "Hours", 0);
          case u:
            return $(v + "Minutes", 1);
          case s:
            return $(v + "Seconds", 2);
          case i:
            return $(v + "Milliseconds", 3);
          default:
            return this.clone();
        }
      }, m.endOf = function (t) {
        return this.startOf(t, !1);
      }, m.$set = function (t, e) {
        var n,
          o = O.p(t),
          h = "set" + (this.$u ? "UTC" : ""),
          l = (n = {}, n[a] = h + "Date", n[d] = h + "Date", n[f] = h + "Month", n[c] = h + "FullYear", n[u] = h + "Hours", n[s] = h + "Minutes", n[i] = h + "Seconds", n[r] = h + "Milliseconds", n)[o],
          $ = o === a ? this.$D + (e - this.$W) : e;
        if (o === f || o === c) {
          var y = this.clone().set(d, 1);
          y.$d[l]($), y.init(), this.$d = y.set(d, Math.min(this.$D, y.daysInMonth())).$d;
        } else l && this.$d[l]($);
        return this.init(), this;
      }, m.set = function (t, e) {
        return this.clone().$set(t, e);
      }, m.get = function (t) {
        return this[O.p(t)]();
      }, m.add = function (r, h) {
        var d,
          l = this;
        r = Number(r);
        var $ = O.p(h),
          y = function (t) {
            var e = w(l);
            return O.w(e.date(e.date() + Math.round(t * r)), l);
          };
        if ($ === f) return this.set(f, this.$M + r);
        if ($ === c) return this.set(c, this.$y + r);
        if ($ === a) return y(1);
        if ($ === o) return y(7);
        var M = (d = {}, d[s] = e, d[u] = n, d[i] = t, d)[$] || 1,
          m = this.$d.getTime() + r * M;
        return O.w(m, this);
      }, m.subtract = function (t, e) {
        return this.add(-1 * t, e);
      }, m.format = function (t) {
        var e = this,
          n = this.$locale();
        if (!this.isValid()) return n.invalidDate || l;
        var r = t || "YYYY-MM-DDTHH:mm:ssZ",
          i = O.z(this),
          s = this.$H,
          u = this.$m,
          a = this.$M,
          o = n.weekdays,
          f = n.months,
          h = function (t, n, i, s) {
            return t && (t[n] || t(e, r)) || i[n].slice(0, s);
          },
          c = function (t) {
            return O.s(s % 12 || 12, t, "0");
          },
          d = n.meridiem || function (t, e, n) {
            var r = t < 12 ? "AM" : "PM";
            return n ? r.toLowerCase() : r;
          },
          $ = {
            YY: String(this.$y).slice(-2),
            YYYY: this.$y,
            M: a + 1,
            MM: O.s(a + 1, 2, "0"),
            MMM: h(n.monthsShort, a, f, 3),
            MMMM: h(f, a),
            D: this.$D,
            DD: O.s(this.$D, 2, "0"),
            d: String(this.$W),
            dd: h(n.weekdaysMin, this.$W, o, 2),
            ddd: h(n.weekdaysShort, this.$W, o, 3),
            dddd: o[this.$W],
            H: String(s),
            HH: O.s(s, 2, "0"),
            h: c(1),
            hh: c(2),
            a: d(s, u, !0),
            A: d(s, u, !1),
            m: String(u),
            mm: O.s(u, 2, "0"),
            s: String(this.$s),
            ss: O.s(this.$s, 2, "0"),
            SSS: O.s(this.$ms, 3, "0"),
            Z: i
          };
        return r.replace(y, function (t, e) {
          return e || $[t] || i.replace(":", "");
        });
      }, m.utcOffset = function () {
        return 15 * -Math.round(this.$d.getTimezoneOffset() / 15);
      }, m.diff = function (r, d, l) {
        var $,
          y = O.p(d),
          M = w(r),
          m = (M.utcOffset() - this.utcOffset()) * e,
          v = this - M,
          g = O.m(this, M);
        return g = ($ = {}, $[c] = g / 12, $[f] = g, $[h] = g / 3, $[o] = (v - m) / 6048e5, $[a] = (v - m) / 864e5, $[u] = v / n, $[s] = v / e, $[i] = v / t, $)[y] || v, l ? g : O.a(g);
      }, m.daysInMonth = function () {
        return this.endOf(f).$D;
      }, m.$locale = function () {
        return D[this.$L];
      }, m.locale = function (t, e) {
        if (!t) return this.$L;
        var n = this.clone(),
          r = S(t, e, !0);
        return r && (n.$L = r), n;
      }, m.clone = function () {
        return O.w(this.$d, this);
      }, m.toDate = function () {
        return new Date(this.valueOf());
      }, m.toJSON = function () {
        return this.isValid() ? this.toISOString() : null;
      }, m.toISOString = function () {
        return this.$d.toISOString();
      }, m.toString = function () {
        return this.$d.toUTCString();
      }, M;
    }(),
    T = _.prototype;
  return w.prototype = T, [["$ms", r], ["$s", i], ["$m", s], ["$H", u], ["$W", a], ["$M", f], ["$y", c], ["$D", d]].forEach(function (t) {
    T[t[1]] = function (e) {
      return this.$g(e, t[0], t[1]);
    };
  }), w.extend = function (t, e) {
    return t.$i || (t(e, _, w), t.$i = !0), w;
  }, w.locale = S, w.isDayjs = p, w.unix = function (t) {
    return w(1e3 * t);
  }, w.en = D[g], w.Ls = D, w.p = {}, w;
});

/***/ }),

/***/ 45965:
/*!*************************************************!*\
  !*** ./node_modules/dayjs/plugin/localeData.js ***!
  \*************************************************/
/***/ (function(module) {

!function (n, e) {
   true ? module.exports = e() : 0;
}(this, function () {
  "use strict";

  return function (n, e, t) {
    var r = e.prototype,
      o = function (n) {
        return n && (n.indexOf ? n : n.s);
      },
      u = function (n, e, t, r, u) {
        var i = n.name ? n : n.$locale(),
          a = o(i[e]),
          s = o(i[t]),
          f = a || s.map(function (n) {
            return n.slice(0, r);
          });
        if (!u) return f;
        var d = i.weekStart;
        return f.map(function (n, e) {
          return f[(e + (d || 0)) % 7];
        });
      },
      i = function () {
        return t.Ls[t.locale()];
      },
      a = function (n, e) {
        return n.formats[e] || function (n) {
          return n.replace(/(\[[^\]]+])|(MMMM|MM|DD|dddd)/g, function (n, e, t) {
            return e || t.slice(1);
          });
        }(n.formats[e.toUpperCase()]);
      },
      s = function () {
        var n = this;
        return {
          months: function (e) {
            return e ? e.format("MMMM") : u(n, "months");
          },
          monthsShort: function (e) {
            return e ? e.format("MMM") : u(n, "monthsShort", "months", 3);
          },
          firstDayOfWeek: function () {
            return n.$locale().weekStart || 0;
          },
          weekdays: function (e) {
            return e ? e.format("dddd") : u(n, "weekdays");
          },
          weekdaysMin: function (e) {
            return e ? e.format("dd") : u(n, "weekdaysMin", "weekdays", 2);
          },
          weekdaysShort: function (e) {
            return e ? e.format("ddd") : u(n, "weekdaysShort", "weekdays", 3);
          },
          longDateFormat: function (e) {
            return a(n.$locale(), e);
          },
          meridiem: this.$locale().meridiem,
          ordinal: this.$locale().ordinal
        };
      };
    r.localeData = function () {
      return s.bind(this)();
    }, t.localeData = function () {
      var n = i();
      return {
        firstDayOfWeek: function () {
          return n.weekStart || 0;
        },
        weekdays: function () {
          return t.weekdays();
        },
        weekdaysShort: function () {
          return t.weekdaysShort();
        },
        weekdaysMin: function () {
          return t.weekdaysMin();
        },
        months: function () {
          return t.months();
        },
        monthsShort: function () {
          return t.monthsShort();
        },
        longDateFormat: function (e) {
          return a(n, e);
        },
        meridiem: n.meridiem,
        ordinal: n.ordinal
      };
    }, t.months = function () {
      return u(i(), "months");
    }, t.monthsShort = function () {
      return u(i(), "monthsShort", "months", 3);
    }, t.weekdays = function (n) {
      return u(i(), "weekdays", null, null, n);
    }, t.weekdaysShort = function (n) {
      return u(i(), "weekdaysShort", "weekdays", 3, n);
    }, t.weekdaysMin = function (n) {
      return u(i(), "weekdaysMin", "weekdays", 2, n);
    };
  };
});

/***/ }),

/***/ 87772:
/*!******************************************************!*\
  !*** ./node_modules/dayjs/plugin/localizedFormat.js ***!
  \******************************************************/
/***/ (function(module) {

!function (e, t) {
   true ? module.exports = t() : 0;
}(this, function () {
  "use strict";

  var e = {
    LTS: "h:mm:ss A",
    LT: "h:mm A",
    L: "MM/DD/YYYY",
    LL: "MMMM D, YYYY",
    LLL: "MMMM D, YYYY h:mm A",
    LLLL: "dddd, MMMM D, YYYY h:mm A"
  };
  return function (t, o, n) {
    var r = o.prototype,
      i = r.format;
    n.en.formats = e, r.format = function (t) {
      void 0 === t && (t = "YYYY-MM-DDTHH:mm:ssZ");
      var o = this.$locale().formats,
        n = function (t, o) {
          return t.replace(/(\[[^\]]+])|(LTS?|l{1,4}|L{1,4})/g, function (t, n, r) {
            var i = r && r.toUpperCase();
            return n || o[r] || e[r] || o[i].replace(/(\[[^\]]+])|(MMMM|MM|DD|dddd)/g, function (e, t, o) {
              return t || o.slice(1);
            });
          });
        }(t, void 0 === o ? {} : o);
      return i.call(this, n);
    };
  };
});

/***/ }),

/***/ 34596:
/*!****************************************************!*\
  !*** ./node_modules/dayjs/plugin/objectSupport.js ***!
  \****************************************************/
/***/ (function(module) {

!function (t, n) {
   true ? module.exports = n() : 0;
}(this, function () {
  "use strict";

  return function (t, n, e) {
    var i = n.prototype,
      r = function (t) {
        var n,
          r = t.date,
          o = t.utc,
          a = {};
        if (!((n = r) instanceof Date || n instanceof Array || i.$utils().u(n) || "Object" !== n.constructor.name)) {
          if (!Object.keys(r).length) return new Date();
          var u = o ? e.utc() : e();
          Object.keys(r).forEach(function (t) {
            var n, e;
            a[(n = t, e = i.$utils().p(n), "date" === e ? "day" : e)] = r[t];
          });
          var c = a.day || (a.year || a.month >= 0 ? 1 : u.date()),
            s = a.year || u.year(),
            d = a.month >= 0 ? a.month : a.year || a.day ? 0 : u.month(),
            f = a.hour || 0,
            b = a.minute || 0,
            h = a.second || 0,
            y = a.millisecond || 0;
          return o ? new Date(Date.UTC(s, d, c, f, b, h, y)) : new Date(s, d, c, f, b, h, y);
        }
        return r;
      },
      o = i.parse;
    i.parse = function (t) {
      t.date = r.bind(this)(t), o.bind(this)(t);
    };
    var a = i.set,
      u = i.add,
      c = i.subtract,
      s = function (t, n, e, i) {
        void 0 === i && (i = 1);
        var r = Object.keys(n),
          o = this;
        return r.forEach(function (e) {
          o = t.bind(o)(n[e] * i, e);
        }), o;
      };
    i.set = function (t, n) {
      return n = void 0 === n ? t : n, "Object" === t.constructor.name ? s.bind(this)(function (t, n) {
        return a.bind(this)(n, t);
      }, n, t) : a.bind(this)(t, n);
    }, i.add = function (t, n) {
      return "Object" === t.constructor.name ? s.bind(this)(u, t, n) : u.bind(this)(t, n);
    }, i.subtract = function (t, n) {
      return "Object" === t.constructor.name ? s.bind(this)(u, t, n, -1) : c.bind(this)(t, n);
    };
  };
});

/***/ }),

/***/ 21859:
/*!******************************************!*\
  !*** ./node_modules/dayjs/plugin/utc.js ***!
  \******************************************/
/***/ (function(module) {

!function (t, i) {
   true ? module.exports = i() : 0;
}(this, function () {
  "use strict";

  var t = "minute",
    i = /[+-]\d\d(?::?\d\d)?/g,
    e = /([+-]|\d\d)/g;
  return function (s, f, n) {
    var u = f.prototype;
    n.utc = function (t) {
      var i = {
        date: t,
        utc: !0,
        args: arguments
      };
      return new f(i);
    }, u.utc = function (i) {
      var e = n(this.toDate(), {
        locale: this.$L,
        utc: !0
      });
      return i ? e.add(this.utcOffset(), t) : e;
    }, u.local = function () {
      return n(this.toDate(), {
        locale: this.$L,
        utc: !1
      });
    };
    var o = u.parse;
    u.parse = function (t) {
      t.utc && (this.$u = !0), this.$utils().u(t.$offset) || (this.$offset = t.$offset), o.call(this, t);
    };
    var r = u.init;
    u.init = function () {
      if (this.$u) {
        var t = this.$d;
        this.$y = t.getUTCFullYear(), this.$M = t.getUTCMonth(), this.$D = t.getUTCDate(), this.$W = t.getUTCDay(), this.$H = t.getUTCHours(), this.$m = t.getUTCMinutes(), this.$s = t.getUTCSeconds(), this.$ms = t.getUTCMilliseconds();
      } else r.call(this);
    };
    var a = u.utcOffset;
    u.utcOffset = function (s, f) {
      var n = this.$utils().u;
      if (n(s)) return this.$u ? 0 : n(this.$offset) ? a.call(this) : this.$offset;
      if ("string" == typeof s && (s = function (t) {
        void 0 === t && (t = "");
        var s = t.match(i);
        if (!s) return null;
        var f = ("" + s[0]).match(e) || ["-", 0, 0],
          n = f[0],
          u = 60 * +f[1] + +f[2];
        return 0 === u ? 0 : "+" === n ? u : -u;
      }(s), null === s)) return this;
      var u = Math.abs(s) <= 16 ? 60 * s : s,
        o = this;
      if (f) return o.$offset = u, o.$u = 0 === s, o;
      if (0 !== s) {
        var r = this.$u ? this.toDate().getTimezoneOffset() : -1 * this.utcOffset();
        (o = this.local().add(u + r, t)).$offset = u, o.$x.$localOffset = r;
      } else o = this.utc();
      return o;
    };
    var h = u.format;
    u.format = function (t) {
      var i = t || (this.$u ? "YYYY-MM-DDTHH:mm:ss[Z]" : "");
      return h.call(this, i);
    }, u.valueOf = function () {
      var t = this.$utils().u(this.$offset) ? 0 : this.$offset + (this.$x.$localOffset || this.$d.getTimezoneOffset());
      return this.$d.valueOf() - 6e4 * t;
    }, u.isUTC = function () {
      return !!this.$u;
    }, u.toISOString = function () {
      return this.toDate().toISOString();
    }, u.toString = function () {
      return this.toDate().toUTCString();
    };
    var l = u.toDate;
    u.toDate = function (t) {
      return "s" === t && this.$offset ? n(this.format("YYYY-MM-DD HH:mm:ss:SSS")).toDate() : l.call(this);
    };
    var c = u.diff;
    u.diff = function (t, i, e) {
      if (t && this.$u === t.$u) return c.call(this, t, i, e);
      var s = this.local(),
        f = n(t).local();
      return c.call(s, f, i, e);
    };
  };
});

/***/ }),

/***/ 52922:
/*!***********************************************************!*\
  !*** ./node_modules/@angular/material/fesm2020/radio.mjs ***!
  \***********************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MAT_RADIO_DEFAULT_OPTIONS": () => (/* binding */ MAT_RADIO_DEFAULT_OPTIONS),
/* harmony export */   "MAT_RADIO_DEFAULT_OPTIONS_FACTORY": () => (/* binding */ MAT_RADIO_DEFAULT_OPTIONS_FACTORY),
/* harmony export */   "MAT_RADIO_GROUP": () => (/* binding */ MAT_RADIO_GROUP),
/* harmony export */   "MAT_RADIO_GROUP_CONTROL_VALUE_ACCESSOR": () => (/* binding */ MAT_RADIO_GROUP_CONTROL_VALUE_ACCESSOR),
/* harmony export */   "MatRadioButton": () => (/* binding */ MatRadioButton),
/* harmony export */   "MatRadioChange": () => (/* binding */ MatRadioChange),
/* harmony export */   "MatRadioGroup": () => (/* binding */ MatRadioGroup),
/* harmony export */   "MatRadioModule": () => (/* binding */ MatRadioModule),
/* harmony export */   "_MatRadioButtonBase": () => (/* binding */ _MatRadioButtonBase),
/* harmony export */   "_MatRadioGroupBase": () => (/* binding */ _MatRadioGroupBase)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser/animations */ 22560);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/core */ 59121);
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/a11y */ 24218);
/* harmony import */ var _angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/coercion */ 48971);
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ 11755);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 94666);











/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
// Increasing integer for generating unique ids for radio components.
const _c0 = ["input"];
const _c1 = ["*"];
let nextUniqueId = 0;
/** Change event object emitted by radio button and radio group. */
class MatRadioChange {
  constructor( /** The radio button that emits the change event. */
  source, /** The value of the radio button. */
  value) {
    this.source = source;
    this.value = value;
  }
}
/**
 * Provider Expression that allows mat-radio-group to register as a ControlValueAccessor. This
 * allows it to support [(ngModel)] and ngControl.
 * @docs-private
 */
const MAT_RADIO_GROUP_CONTROL_VALUE_ACCESSOR = {
  provide: _angular_forms__WEBPACK_IMPORTED_MODULE_0__.NG_VALUE_ACCESSOR,
  useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.forwardRef)(() => MatRadioGroup),
  multi: true
};
/**
 * Injection token that can be used to inject instances of `MatRadioGroup`. It serves as
 * alternative token to the actual `MatRadioGroup` class which could cause unnecessary
 * retention of the class and its component metadata.
 */
const MAT_RADIO_GROUP = new _angular_core__WEBPACK_IMPORTED_MODULE_1__.InjectionToken('MatRadioGroup');
const MAT_RADIO_DEFAULT_OPTIONS = new _angular_core__WEBPACK_IMPORTED_MODULE_1__.InjectionToken('mat-radio-default-options', {
  providedIn: 'root',
  factory: MAT_RADIO_DEFAULT_OPTIONS_FACTORY
});
function MAT_RADIO_DEFAULT_OPTIONS_FACTORY() {
  return {
    color: 'accent'
  };
}
/**
 * Base class with all of the `MatRadioGroup` functionality.
 * @docs-private
 */
class _MatRadioGroupBase {
  /** Name of the radio button group. All radio buttons inside this group will use this name. */
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
    this._updateRadioButtonNames();
  }
  /** Whether the labels should appear after or before the radio-buttons. Defaults to 'after' */
  get labelPosition() {
    return this._labelPosition;
  }
  set labelPosition(v) {
    this._labelPosition = v === 'before' ? 'before' : 'after';
    this._markRadiosForCheck();
  }
  /**
   * Value for the radio-group. Should equal the value of the selected radio button if there is
   * a corresponding radio button with a matching value. If there is not such a corresponding
   * radio button, this value persists to be applied in case a new radio button is added with a
   * matching value.
   */
  get value() {
    return this._value;
  }
  set value(newValue) {
    if (this._value !== newValue) {
      // Set this before proceeding to ensure no circular loop occurs with selection.
      this._value = newValue;
      this._updateSelectedRadioFromValue();
      this._checkSelectedRadioButton();
    }
  }
  _checkSelectedRadioButton() {
    if (this._selected && !this._selected.checked) {
      this._selected.checked = true;
    }
  }
  /**
   * The currently selected radio button. If set to a new radio button, the radio group value
   * will be updated to match the new selected button.
   */
  get selected() {
    return this._selected;
  }
  set selected(selected) {
    this._selected = selected;
    this.value = selected ? selected.value : null;
    this._checkSelectedRadioButton();
  }
  /** Whether the radio group is disabled */
  get disabled() {
    return this._disabled;
  }
  set disabled(value) {
    this._disabled = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_2__.coerceBooleanProperty)(value);
    this._markRadiosForCheck();
  }
  /** Whether the radio group is required */
  get required() {
    return this._required;
  }
  set required(value) {
    this._required = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_2__.coerceBooleanProperty)(value);
    this._markRadiosForCheck();
  }
  constructor(_changeDetector) {
    this._changeDetector = _changeDetector;
    /** Selected value for the radio group. */
    this._value = null;
    /** The HTML name attribute applied to radio buttons in this group. */
    this._name = `mat-radio-group-${nextUniqueId++}`;
    /** The currently selected radio button. Should match value. */
    this._selected = null;
    /** Whether the `value` has been set to its initial value. */
    this._isInitialized = false;
    /** Whether the labels should appear after or before the radio-buttons. Defaults to 'after' */
    this._labelPosition = 'after';
    /** Whether the radio group is disabled. */
    this._disabled = false;
    /** Whether the radio group is required. */
    this._required = false;
    /** The method to be called in order to update ngModel */
    this._controlValueAccessorChangeFn = () => {};
    /**
     * onTouch function registered via registerOnTouch (ControlValueAccessor).
     * @docs-private
     */
    this.onTouched = () => {};
    /**
     * Event emitted when the group value changes.
     * Change events are only emitted when the value changes due to user interaction with
     * a radio button (the same behavior as `<input type-"radio">`).
     */
    this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_1__.EventEmitter();
  }
  /**
   * Initialize properties once content children are available.
   * This allows us to propagate relevant attributes to associated buttons.
   */
  ngAfterContentInit() {
    // Mark this component as initialized in AfterContentInit because the initial value can
    // possibly be set by NgModel on MatRadioGroup, and it is possible that the OnInit of the
    // NgModel occurs *after* the OnInit of the MatRadioGroup.
    this._isInitialized = true;
  }
  /**
   * Mark this group as being "touched" (for ngModel). Meant to be called by the contained
   * radio buttons upon their blur.
   */
  _touch() {
    if (this.onTouched) {
      this.onTouched();
    }
  }
  _updateRadioButtonNames() {
    if (this._radios) {
      this._radios.forEach(radio => {
        radio.name = this.name;
        radio._markForCheck();
      });
    }
  }
  /** Updates the `selected` radio button from the internal _value state. */
  _updateSelectedRadioFromValue() {
    // If the value already matches the selected radio, do nothing.
    const isAlreadySelected = this._selected !== null && this._selected.value === this._value;
    if (this._radios && !isAlreadySelected) {
      this._selected = null;
      this._radios.forEach(radio => {
        radio.checked = this.value === radio.value;
        if (radio.checked) {
          this._selected = radio;
        }
      });
    }
  }
  /** Dispatch change event with current selection and group value. */
  _emitChangeEvent() {
    if (this._isInitialized) {
      this.change.emit(new MatRadioChange(this._selected, this._value));
    }
  }
  _markRadiosForCheck() {
    if (this._radios) {
      this._radios.forEach(radio => radio._markForCheck());
    }
  }
  /**
   * Sets the model value. Implemented as part of ControlValueAccessor.
   * @param value
   */
  writeValue(value) {
    this.value = value;
    this._changeDetector.markForCheck();
  }
  /**
   * Registers a callback to be triggered when the model value changes.
   * Implemented as part of ControlValueAccessor.
   * @param fn Callback to be registered.
   */
  registerOnChange(fn) {
    this._controlValueAccessorChangeFn = fn;
  }
  /**
   * Registers a callback to be triggered when the control is touched.
   * Implemented as part of ControlValueAccessor.
   * @param fn Callback to be registered.
   */
  registerOnTouched(fn) {
    this.onTouched = fn;
  }
  /**
   * Sets the disabled state of the control. Implemented as a part of ControlValueAccessor.
   * @param isDisabled Whether the control should be disabled.
   */
  setDisabledState(isDisabled) {
    this.disabled = isDisabled;
    this._changeDetector.markForCheck();
  }
}
_MatRadioGroupBase.ɵfac = function _MatRadioGroupBase_Factory(t) {
  return new (t || _MatRadioGroupBase)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__.ChangeDetectorRef));
};
_MatRadioGroupBase.ɵdir = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
  type: _MatRadioGroupBase,
  inputs: {
    color: "color",
    name: "name",
    labelPosition: "labelPosition",
    value: "value",
    selected: "selected",
    disabled: "disabled",
    required: "required"
  },
  outputs: {
    change: "change"
  }
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](_MatRadioGroupBase, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Directive
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.ChangeDetectorRef
    }];
  }, {
    change: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Output
    }],
    color: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    name: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    labelPosition: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    value: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    selected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    disabled: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    required: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }]
  });
})();
// Boilerplate for applying mixins to MatRadioButton.
/** @docs-private */
class MatRadioButtonBase {
  constructor(_elementRef) {
    this._elementRef = _elementRef;
  }
}
const _MatRadioButtonMixinBase = (0,_angular_material_core__WEBPACK_IMPORTED_MODULE_3__.mixinDisableRipple)((0,_angular_material_core__WEBPACK_IMPORTED_MODULE_3__.mixinTabIndex)(MatRadioButtonBase));
/**
 * Base class with all of the `MatRadioButton` functionality.
 * @docs-private
 */
class _MatRadioButtonBase extends _MatRadioButtonMixinBase {
  /** Whether this radio button is checked. */
  get checked() {
    return this._checked;
  }
  set checked(value) {
    const newCheckedState = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_2__.coerceBooleanProperty)(value);
    if (this._checked !== newCheckedState) {
      this._checked = newCheckedState;
      if (newCheckedState && this.radioGroup && this.radioGroup.value !== this.value) {
        this.radioGroup.selected = this;
      } else if (!newCheckedState && this.radioGroup && this.radioGroup.value === this.value) {
        // When unchecking the selected radio button, update the selected radio
        // property on the group.
        this.radioGroup.selected = null;
      }
      if (newCheckedState) {
        // Notify all radio buttons with the same name to un-check.
        this._radioDispatcher.notify(this.id, this.name);
      }
      this._changeDetector.markForCheck();
    }
  }
  /** The value of this radio button. */
  get value() {
    return this._value;
  }
  set value(value) {
    if (this._value !== value) {
      this._value = value;
      if (this.radioGroup !== null) {
        if (!this.checked) {
          // Update checked when the value changed to match the radio group's value
          this.checked = this.radioGroup.value === value;
        }
        if (this.checked) {
          this.radioGroup.selected = this;
        }
      }
    }
  }
  /** Whether the label should appear after or before the radio button. Defaults to 'after' */
  get labelPosition() {
    return this._labelPosition || this.radioGroup && this.radioGroup.labelPosition || 'after';
  }
  set labelPosition(value) {
    this._labelPosition = value;
  }
  /** Whether the radio button is disabled. */
  get disabled() {
    return this._disabled || this.radioGroup !== null && this.radioGroup.disabled;
  }
  set disabled(value) {
    this._setDisabled((0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_2__.coerceBooleanProperty)(value));
  }
  /** Whether the radio button is required. */
  get required() {
    return this._required || this.radioGroup && this.radioGroup.required;
  }
  set required(value) {
    this._required = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_2__.coerceBooleanProperty)(value);
  }
  /** Theme color of the radio button. */
  get color() {
    // As per Material design specifications the selection control radio should use the accent color
    // palette by default. https://material.io/guidelines/components/selection-controls.html
    return this._color || this.radioGroup && this.radioGroup.color || this._providerOverride && this._providerOverride.color || 'accent';
  }
  set color(newValue) {
    this._color = newValue;
  }
  /** ID of the native input element inside `<mat-radio-button>` */
  get inputId() {
    return `${this.id || this._uniqueId}-input`;
  }
  constructor(radioGroup, elementRef, _changeDetector, _focusMonitor, _radioDispatcher, animationMode, _providerOverride, tabIndex) {
    super(elementRef);
    this._changeDetector = _changeDetector;
    this._focusMonitor = _focusMonitor;
    this._radioDispatcher = _radioDispatcher;
    this._providerOverride = _providerOverride;
    this._uniqueId = `mat-radio-${++nextUniqueId}`;
    /** The unique ID for the radio button. */
    this.id = this._uniqueId;
    /**
     * Event emitted when the checked state of this radio button changes.
     * Change events are only emitted when the value changes due to user interaction with
     * the radio button (the same behavior as `<input type-"radio">`).
     */
    this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_1__.EventEmitter();
    /** Whether this radio is checked. */
    this._checked = false;
    /** Value assigned to this radio. */
    this._value = null;
    /** Unregister function for _radioDispatcher */
    this._removeUniqueSelectionListener = () => {};
    // Assertions. Ideally these should be stripped out by the compiler.
    // TODO(jelbourn): Assert that there's no name binding AND a parent radio group.
    this.radioGroup = radioGroup;
    this._noopAnimations = animationMode === 'NoopAnimations';
    if (tabIndex) {
      this.tabIndex = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_2__.coerceNumberProperty)(tabIndex, 0);
    }
  }
  /** Focuses the radio button. */
  focus(options, origin) {
    if (origin) {
      this._focusMonitor.focusVia(this._inputElement, origin, options);
    } else {
      this._inputElement.nativeElement.focus(options);
    }
  }
  /**
   * Marks the radio button as needing checking for change detection.
   * This method is exposed because the parent radio group will directly
   * update bound properties of the radio button.
   */
  _markForCheck() {
    // When group value changes, the button will not be notified. Use `markForCheck` to explicit
    // update radio button's status
    this._changeDetector.markForCheck();
  }
  ngOnInit() {
    if (this.radioGroup) {
      // If the radio is inside a radio group, determine if it should be checked
      this.checked = this.radioGroup.value === this._value;
      if (this.checked) {
        this.radioGroup.selected = this;
      }
      // Copy name from parent radio group
      this.name = this.radioGroup.name;
    }
    this._removeUniqueSelectionListener = this._radioDispatcher.listen((id, name) => {
      if (id !== this.id && name === this.name) {
        this.checked = false;
      }
    });
  }
  ngDoCheck() {
    this._updateTabIndex();
  }
  ngAfterViewInit() {
    this._updateTabIndex();
    this._focusMonitor.monitor(this._elementRef, true).subscribe(focusOrigin => {
      if (!focusOrigin && this.radioGroup) {
        this.radioGroup._touch();
      }
    });
  }
  ngOnDestroy() {
    this._focusMonitor.stopMonitoring(this._elementRef);
    this._removeUniqueSelectionListener();
  }
  /** Dispatch change event with current value. */
  _emitChangeEvent() {
    this.change.emit(new MatRadioChange(this, this._value));
  }
  _isRippleDisabled() {
    return this.disableRipple || this.disabled;
  }
  _onInputClick(event) {
    // We have to stop propagation for click events on the visual hidden input element.
    // By default, when a user clicks on a label element, a generated click event will be
    // dispatched on the associated input element. Since we are using a label element as our
    // root container, the click event on the `radio-button` will be executed twice.
    // The real click event will bubble up, and the generated click event also tries to bubble up.
    // This will lead to multiple click events.
    // Preventing bubbling for the second event will solve that issue.
    event.stopPropagation();
  }
  /** Triggered when the radio button receives an interaction from the user. */
  _onInputInteraction(event) {
    // We always have to stop propagation on the change event.
    // Otherwise the change event, from the input element, will bubble up and
    // emit its event object to the `change` output.
    event.stopPropagation();
    if (!this.checked && !this.disabled) {
      const groupValueChanged = this.radioGroup && this.value !== this.radioGroup.value;
      this.checked = true;
      this._emitChangeEvent();
      if (this.radioGroup) {
        this.radioGroup._controlValueAccessorChangeFn(this.value);
        if (groupValueChanged) {
          this.radioGroup._emitChangeEvent();
        }
      }
    }
  }
  /** Triggered when the user clicks on the touch target. */
  _onTouchTargetClick(event) {
    this._onInputInteraction(event);
    if (!this.disabled) {
      // Normally the input should be focused already, but if the click
      // comes from the touch target, then we might have to focus it ourselves.
      this._inputElement.nativeElement.focus();
    }
  }
  /** Sets the disabled state and marks for check if a change occurred. */
  _setDisabled(value) {
    if (this._disabled !== value) {
      this._disabled = value;
      this._changeDetector.markForCheck();
    }
  }
  /** Gets the tabindex for the underlying input element. */
  _updateTabIndex() {
    const group = this.radioGroup;
    let value;
    // Implement a roving tabindex if the button is inside a group. For most cases this isn't
    // necessary, because the browser handles the tab order for inputs inside a group automatically,
    // but we need an explicitly higher tabindex for the selected button in order for things like
    // the focus trap to pick it up correctly.
    if (!group || !group.selected || this.disabled) {
      value = this.tabIndex;
    } else {
      value = group.selected === this ? this.tabIndex : -1;
    }
    if (value !== this._previousTabIndex) {
      // We have to set the tabindex directly on the DOM node, because it depends on
      // the selected state which is prone to "changed after checked errors".
      const input = this._inputElement?.nativeElement;
      if (input) {
        input.setAttribute('tabindex', value + '');
        this._previousTabIndex = value;
      }
    }
  }
}
_MatRadioButtonBase.ɵfac = function _MatRadioButtonBase_Factory(t) {
  _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinvalidFactory"]();
};
_MatRadioButtonBase.ɵdir = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
  type: _MatRadioButtonBase,
  viewQuery: function _MatRadioButtonBase_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, 5);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx._inputElement = _t.first);
    }
  },
  inputs: {
    id: "id",
    name: "name",
    ariaLabel: ["aria-label", "ariaLabel"],
    ariaLabelledby: ["aria-labelledby", "ariaLabelledby"],
    ariaDescribedby: ["aria-describedby", "ariaDescribedby"],
    checked: "checked",
    value: "value",
    labelPosition: "labelPosition",
    disabled: "disabled",
    required: "required",
    color: "color"
  },
  outputs: {
    change: "change"
  },
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵInheritDefinitionFeature"]]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](_MatRadioButtonBase, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Directive
  }], function () {
    return [{
      type: _MatRadioGroupBase
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.ElementRef
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.ChangeDetectorRef
    }, {
      type: _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_4__.FocusMonitor
    }, {
      type: _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__.UniqueSelectionDispatcher
    }, {
      type: undefined
    }, {
      type: undefined
    }, {
      type: undefined
    }];
  }, {
    id: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    name: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    ariaLabel: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input,
      args: ['aria-label']
    }],
    ariaLabelledby: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input,
      args: ['aria-labelledby']
    }],
    ariaDescribedby: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input,
      args: ['aria-describedby']
    }],
    checked: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    value: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    labelPosition: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    disabled: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    required: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    color: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Input
    }],
    change: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Output
    }],
    _inputElement: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.ViewChild,
      args: ['input']
    }]
  });
})();
/**
 * A group of radio buttons. May contain one or more `<mat-radio-button>` elements.
 */
class MatRadioGroup extends _MatRadioGroupBase {}
MatRadioGroup.ɵfac = /* @__PURE__ */function () {
  let ɵMatRadioGroup_BaseFactory;
  return function MatRadioGroup_Factory(t) {
    return (ɵMatRadioGroup_BaseFactory || (ɵMatRadioGroup_BaseFactory = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](MatRadioGroup)))(t || MatRadioGroup);
  };
}();
MatRadioGroup.ɵdir = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
  type: MatRadioGroup,
  selectors: [["mat-radio-group"]],
  contentQueries: function MatRadioGroup_ContentQueries(rf, ctx, dirIndex) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵcontentQuery"](dirIndex, MatRadioButton, 5);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx._radios = _t);
    }
  },
  hostAttrs: ["role", "radiogroup", 1, "mat-mdc-radio-group"],
  exportAs: ["matRadioGroup"],
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([MAT_RADIO_GROUP_CONTROL_VALUE_ACCESSOR, {
    provide: MAT_RADIO_GROUP,
    useExisting: MatRadioGroup
  }]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵInheritDefinitionFeature"]]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](MatRadioGroup, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Directive,
    args: [{
      selector: 'mat-radio-group',
      exportAs: 'matRadioGroup',
      providers: [MAT_RADIO_GROUP_CONTROL_VALUE_ACCESSOR, {
        provide: MAT_RADIO_GROUP,
        useExisting: MatRadioGroup
      }],
      host: {
        'role': 'radiogroup',
        'class': 'mat-mdc-radio-group'
      }
    }]
  }], null, {
    _radios: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.ContentChildren,
      args: [(0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.forwardRef)(() => MatRadioButton), {
        descendants: true
      }]
    }]
  });
})();
class MatRadioButton extends _MatRadioButtonBase {
  constructor(radioGroup, elementRef, _changeDetector, _focusMonitor, _radioDispatcher, animationMode, _providerOverride, tabIndex) {
    super(radioGroup, elementRef, _changeDetector, _focusMonitor, _radioDispatcher, animationMode, _providerOverride, tabIndex);
  }
}
MatRadioButton.ɵfac = function MatRadioButton_Factory(t) {
  return new (t || MatRadioButton)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](MAT_RADIO_GROUP, 8), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_4__.FocusMonitor), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__.UniqueSelectionDispatcher), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__.ANIMATION_MODULE_TYPE, 8), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](MAT_RADIO_DEFAULT_OPTIONS, 8), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinjectAttribute"]('tabindex'));
};
MatRadioButton.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
  type: MatRadioButton,
  selectors: [["mat-radio-button"]],
  hostAttrs: [1, "mat-mdc-radio-button"],
  hostVars: 15,
  hostBindings: function MatRadioButton_HostBindings(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("focus", function MatRadioButton_focus_HostBindingHandler() {
        return ctx._inputElement.nativeElement.focus();
      });
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵattribute"]("id", ctx.id)("tabindex", null)("aria-label", null)("aria-labelledby", null)("aria-describedby", null);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("mat-primary", ctx.color === "primary")("mat-accent", ctx.color === "accent")("mat-warn", ctx.color === "warn")("mat-mdc-radio-checked", ctx.checked)("_mat-animation-noopable", ctx._noopAnimations);
    }
  },
  inputs: {
    disableRipple: "disableRipple",
    tabIndex: "tabIndex"
  },
  exportAs: ["matRadioButton"],
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵInheritDefinitionFeature"]],
  ngContentSelectors: _c1,
  decls: 13,
  vars: 17,
  consts: [[1, "mdc-form-field"], ["formField", ""], [1, "mdc-radio"], [1, "mat-mdc-radio-touch-target", 3, "click"], ["type", "radio", 1, "mdc-radio__native-control", 3, "id", "checked", "disabled", "required", "change"], ["input", ""], [1, "mdc-radio__background"], [1, "mdc-radio__outer-circle"], [1, "mdc-radio__inner-circle"], ["mat-ripple", "", 1, "mat-radio-ripple", "mat-mdc-focus-indicator", 3, "matRippleTrigger", "matRippleDisabled", "matRippleCentered"], [1, "mat-ripple-element", "mat-radio-persistent-ripple"], [3, "for"]],
  template: function MatRadioButton_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojectionDef"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0, 1)(2, "div", 2)(3, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function MatRadioButton_Template_div_click_3_listener($event) {
        return ctx._onTouchTargetClick($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "input", 4, 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function MatRadioButton_Template_input_change_4_listener($event) {
        return ctx._onInputInteraction($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "div", 7)(8, "div", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](10, "div", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "label", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojection"](12);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    }
    if (rf & 2) {
      const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("mdc-form-field--align-end", ctx.labelPosition == "before");
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("mdc-radio--disabled", ctx.disabled);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("id", ctx.inputId)("checked", ctx.checked)("disabled", ctx.disabled)("required", ctx.required);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵattribute"]("name", ctx.name)("value", ctx.value)("aria-label", ctx.ariaLabel)("aria-labelledby", ctx.ariaLabelledby)("aria-describedby", ctx.ariaDescribedby);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("matRippleTrigger", _r0)("matRippleDisabled", ctx._isRippleDisabled())("matRippleCentered", true);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("for", ctx.inputId);
    }
  },
  dependencies: [_angular_material_core__WEBPACK_IMPORTED_MODULE_3__.MatRipple],
  styles: [".mdc-radio{display:inline-block;position:relative;flex:0 0 auto;box-sizing:content-box;width:20px;height:20px;cursor:pointer;will-change:opacity,transform,border-color,color}.mdc-radio[hidden]{display:none}.mdc-radio__background{display:inline-block;position:relative;box-sizing:border-box;width:20px;height:20px}.mdc-radio__background::before{position:absolute;transform:scale(0, 0);border-radius:50%;opacity:0;pointer-events:none;content:\"\";transition:opacity 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1),transform 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1)}.mdc-radio__outer-circle{position:absolute;top:0;left:0;box-sizing:border-box;width:100%;height:100%;border-width:2px;border-style:solid;border-radius:50%;transition:border-color 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1)}.mdc-radio__inner-circle{position:absolute;top:0;left:0;box-sizing:border-box;width:100%;height:100%;transform:scale(0, 0);border-width:10px;border-style:solid;border-radius:50%;transition:transform 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1),border-color 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1)}.mdc-radio__native-control{position:absolute;margin:0;padding:0;opacity:0;cursor:inherit;z-index:1}.mdc-radio--touch{margin-top:4px;margin-bottom:4px;margin-right:4px;margin-left:4px}.mdc-radio--touch .mdc-radio__native-control{top:calc((40px - 48px) / 2);right:calc((40px - 48px) / 2);left:calc((40px - 48px) / 2);width:48px;height:48px}.mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__focus-ring,.mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__focus-ring{pointer-events:none;border:2px solid rgba(0,0,0,0);border-radius:6px;box-sizing:content-box;position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);height:100%;width:100%}@media screen and (forced-colors: active){.mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__focus-ring,.mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__focus-ring{border-color:CanvasText}}.mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__focus-ring::after,.mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__focus-ring::after{content:\"\";border:2px solid rgba(0,0,0,0);border-radius:8px;display:block;position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);height:calc(100% + 4px);width:calc(100% + 4px)}@media screen and (forced-colors: active){.mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__focus-ring::after,.mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__focus-ring::after{border-color:CanvasText}}.mdc-radio__native-control:checked+.mdc-radio__background,.mdc-radio__native-control:disabled+.mdc-radio__background{transition:opacity 120ms 0ms cubic-bezier(0, 0, 0.2, 1),transform 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__outer-circle,.mdc-radio__native-control:disabled+.mdc-radio__background .mdc-radio__outer-circle{transition:border-color 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__inner-circle,.mdc-radio__native-control:disabled+.mdc-radio__background .mdc-radio__inner-circle{transition:transform 120ms 0ms cubic-bezier(0, 0, 0.2, 1),border-color 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-radio--disabled{cursor:default;pointer-events:none}.mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__inner-circle{transform:scale(0.5);transition:transform 120ms 0ms cubic-bezier(0, 0, 0.2, 1),border-color 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-radio__native-control:disabled+.mdc-radio__background,[aria-disabled=true] .mdc-radio__native-control+.mdc-radio__background{cursor:default}.mdc-radio__native-control:focus+.mdc-radio__background::before{transform:scale(1);opacity:.12;transition:opacity 120ms 0ms cubic-bezier(0, 0, 0.2, 1),transform 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-form-field{display:inline-flex;align-items:center;vertical-align:middle}.mdc-form-field[hidden]{display:none}.mdc-form-field>label{margin-left:0;margin-right:auto;padding-left:4px;padding-right:0;order:0}[dir=rtl] .mdc-form-field>label,.mdc-form-field>label[dir=rtl]{margin-left:auto;margin-right:0}[dir=rtl] .mdc-form-field>label,.mdc-form-field>label[dir=rtl]{padding-left:0;padding-right:4px}.mdc-form-field--nowrap>label{text-overflow:ellipsis;overflow:hidden;white-space:nowrap}.mdc-form-field--align-end>label{margin-left:auto;margin-right:0;padding-left:0;padding-right:4px;order:-1}[dir=rtl] .mdc-form-field--align-end>label,.mdc-form-field--align-end>label[dir=rtl]{margin-left:0;margin-right:auto}[dir=rtl] .mdc-form-field--align-end>label,.mdc-form-field--align-end>label[dir=rtl]{padding-left:4px;padding-right:0}.mdc-form-field--space-between{justify-content:space-between}.mdc-form-field--space-between>label{margin:0}[dir=rtl] .mdc-form-field--space-between>label,.mdc-form-field--space-between>label[dir=rtl]{margin:0}.mat-mdc-radio-button{-webkit-tap-highlight-color:rgba(0,0,0,0)}.mat-mdc-radio-button .mdc-radio{padding:calc((var(--mdc-radio-state-layer-size, 40px) - 20px) / 2)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-disabled-selected-icon-color, #000)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control+.mdc-radio__background .mdc-radio__inner-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-disabled-selected-icon-color, #000)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled:checked+.mdc-radio__background .mdc-radio__outer-circle{opacity:var(--mdc-radio-disabled-selected-icon-opacity, 0.38)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control+.mdc-radio__background .mdc-radio__inner-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled+.mdc-radio__background .mdc-radio__inner-circle{opacity:var(--mdc-radio-disabled-selected-icon-opacity, 0.38)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-disabled-unselected-icon-color, #000)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{opacity:var(--mdc-radio-disabled-unselected-icon-opacity, 0.38)}.mat-mdc-radio-button .mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-selected-focus-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle,.mat-mdc-radio-button .mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-selected-focus-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:hover .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-selected-hover-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:hover .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-selected-hover-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-selected-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-selected-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:not(:disabled):active .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-selected-pressed-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:not(:disabled):active .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-selected-pressed-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:hover .mdc-radio__native-control:enabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-unselected-hover-icon-color, #000)}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:enabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-unselected-icon-color, #000)}.mat-mdc-radio-button .mdc-radio:not(:disabled):active .mdc-radio__native-control:enabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-unselected-pressed-icon-color, #000)}.mat-mdc-radio-button .mdc-radio .mdc-radio__background::before{top:calc(-1 * (var(--mdc-radio-state-layer-size, 40px) - 20px) / 2);left:calc(-1 * (var(--mdc-radio-state-layer-size, 40px) - 20px) / 2);width:var(--mdc-radio-state-layer-size, 40px);height:var(--mdc-radio-state-layer-size, 40px)}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control{top:calc((var(--mdc-radio-state-layer-size, 40px) - var(--mdc-radio-state-layer-size, 40px)) / 2);right:calc((var(--mdc-radio-state-layer-size, 40px) - var(--mdc-radio-state-layer-size, 40px)) / 2);left:calc((var(--mdc-radio-state-layer-size, 40px) - var(--mdc-radio-state-layer-size, 40px)) / 2);width:var(--mdc-radio-state-layer-size, 40px);height:var(--mdc-radio-state-layer-size, 40px)}.mat-mdc-radio-button .mdc-radio .mdc-radio__background::before{background-color:var(--mat-mdc-radio-ripple-color, transparent)}.mat-mdc-radio-button .mdc-radio:hover .mdc-radio__native-control:not([disabled]):not(:focus)~.mdc-radio__background::before{opacity:.04;transform:scale(1)}.mat-mdc-radio-button.mat-mdc-radio-checked .mdc-radio__background::before{background-color:var(--mat-mdc-radio-checked-ripple-color, transparent)}.mat-mdc-radio-button.mat-mdc-radio-checked .mat-ripple-element{background-color:var(--mat-mdc-radio-checked-ripple-color, transparent)}.mat-mdc-radio-button .mat-radio-ripple{top:0;left:0;right:0;bottom:0;position:absolute;pointer-events:none;border-radius:50%}.mat-mdc-radio-button .mat-radio-ripple .mat-ripple-element{opacity:.14}.mat-mdc-radio-button .mat-radio-ripple::before{border-radius:50%}.mat-mdc-radio-button._mat-animation-noopable .mdc-radio__background::before,.mat-mdc-radio-button._mat-animation-noopable .mdc-radio__outer-circle,.mat-mdc-radio-button._mat-animation-noopable .mdc-radio__inner-circle{transition:none !important}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:focus:enabled:not(:checked)~.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-unselected-focus-icon-color, black)}.mat-mdc-radio-button.cdk-focused .mat-mdc-focus-indicator::before{content:\"\"}.mat-mdc-radio-touch-target{position:absolute;top:50%;height:48px;left:50%;width:48px;transform:translate(-50%, -50%)}[dir=rtl] .mat-mdc-radio-touch-target{left:0;right:50%;transform:translate(50%, -50%)}"],
  encapsulation: 2,
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](MatRadioButton, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Component,
    args: [{
      selector: 'mat-radio-button',
      host: {
        'class': 'mat-mdc-radio-button',
        '[attr.id]': 'id',
        '[class.mat-primary]': 'color === "primary"',
        '[class.mat-accent]': 'color === "accent"',
        '[class.mat-warn]': 'color === "warn"',
        '[class.mat-mdc-radio-checked]': 'checked',
        '[class._mat-animation-noopable]': '_noopAnimations',
        // Needs to be removed since it causes some a11y issues (see #21266).
        '[attr.tabindex]': 'null',
        '[attr.aria-label]': 'null',
        '[attr.aria-labelledby]': 'null',
        '[attr.aria-describedby]': 'null',
        // Note: under normal conditions focus shouldn't land on this element, however it may be
        // programmatically set, for example inside of a focus trap, in this case we want to forward
        // the focus to the native element.
        '(focus)': '_inputElement.nativeElement.focus()'
      },
      inputs: ['disableRipple', 'tabIndex'],
      exportAs: 'matRadioButton',
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__.ViewEncapsulation.None,
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__.ChangeDetectionStrategy.OnPush,
      template: "<div class=\"mdc-form-field\" #formField\n     [class.mdc-form-field--align-end]=\"labelPosition == 'before'\">\n  <div class=\"mdc-radio\" [class.mdc-radio--disabled]=\"disabled\">\n    <!-- Render this element first so the input is on top. -->\n    <div class=\"mat-mdc-radio-touch-target\" (click)=\"_onTouchTargetClick($event)\"></div>\n    <input #input class=\"mdc-radio__native-control\" type=\"radio\"\n           [id]=\"inputId\"\n           [checked]=\"checked\"\n           [disabled]=\"disabled\"\n           [attr.name]=\"name\"\n           [attr.value]=\"value\"\n           [required]=\"required\"\n           [attr.aria-label]=\"ariaLabel\"\n           [attr.aria-labelledby]=\"ariaLabelledby\"\n           [attr.aria-describedby]=\"ariaDescribedby\"\n           (change)=\"_onInputInteraction($event)\">\n    <div class=\"mdc-radio__background\">\n      <div class=\"mdc-radio__outer-circle\"></div>\n      <div class=\"mdc-radio__inner-circle\"></div>\n    </div>\n    <div mat-ripple class=\"mat-radio-ripple mat-mdc-focus-indicator\"\n         [matRippleTrigger]=\"formField\"\n         [matRippleDisabled]=\"_isRippleDisabled()\"\n         [matRippleCentered]=\"true\">\n      <div class=\"mat-ripple-element mat-radio-persistent-ripple\"></div>\n    </div>\n  </div>\n  <label [for]=\"inputId\">\n    <ng-content></ng-content>\n  </label>\n</div>\n",
      styles: [".mdc-radio{display:inline-block;position:relative;flex:0 0 auto;box-sizing:content-box;width:20px;height:20px;cursor:pointer;will-change:opacity,transform,border-color,color}.mdc-radio[hidden]{display:none}.mdc-radio__background{display:inline-block;position:relative;box-sizing:border-box;width:20px;height:20px}.mdc-radio__background::before{position:absolute;transform:scale(0, 0);border-radius:50%;opacity:0;pointer-events:none;content:\"\";transition:opacity 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1),transform 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1)}.mdc-radio__outer-circle{position:absolute;top:0;left:0;box-sizing:border-box;width:100%;height:100%;border-width:2px;border-style:solid;border-radius:50%;transition:border-color 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1)}.mdc-radio__inner-circle{position:absolute;top:0;left:0;box-sizing:border-box;width:100%;height:100%;transform:scale(0, 0);border-width:10px;border-style:solid;border-radius:50%;transition:transform 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1),border-color 120ms 0ms cubic-bezier(0.4, 0, 0.6, 1)}.mdc-radio__native-control{position:absolute;margin:0;padding:0;opacity:0;cursor:inherit;z-index:1}.mdc-radio--touch{margin-top:4px;margin-bottom:4px;margin-right:4px;margin-left:4px}.mdc-radio--touch .mdc-radio__native-control{top:calc((40px - 48px) / 2);right:calc((40px - 48px) / 2);left:calc((40px - 48px) / 2);width:48px;height:48px}.mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__focus-ring,.mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__focus-ring{pointer-events:none;border:2px solid rgba(0,0,0,0);border-radius:6px;box-sizing:content-box;position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);height:100%;width:100%}@media screen and (forced-colors: active){.mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__focus-ring,.mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__focus-ring{border-color:CanvasText}}.mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__focus-ring::after,.mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__focus-ring::after{content:\"\";border:2px solid rgba(0,0,0,0);border-radius:8px;display:block;position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);height:calc(100% + 4px);width:calc(100% + 4px)}@media screen and (forced-colors: active){.mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__focus-ring::after,.mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__focus-ring::after{border-color:CanvasText}}.mdc-radio__native-control:checked+.mdc-radio__background,.mdc-radio__native-control:disabled+.mdc-radio__background{transition:opacity 120ms 0ms cubic-bezier(0, 0, 0.2, 1),transform 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__outer-circle,.mdc-radio__native-control:disabled+.mdc-radio__background .mdc-radio__outer-circle{transition:border-color 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__inner-circle,.mdc-radio__native-control:disabled+.mdc-radio__background .mdc-radio__inner-circle{transition:transform 120ms 0ms cubic-bezier(0, 0, 0.2, 1),border-color 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-radio--disabled{cursor:default;pointer-events:none}.mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__inner-circle{transform:scale(0.5);transition:transform 120ms 0ms cubic-bezier(0, 0, 0.2, 1),border-color 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-radio__native-control:disabled+.mdc-radio__background,[aria-disabled=true] .mdc-radio__native-control+.mdc-radio__background{cursor:default}.mdc-radio__native-control:focus+.mdc-radio__background::before{transform:scale(1);opacity:.12;transition:opacity 120ms 0ms cubic-bezier(0, 0, 0.2, 1),transform 120ms 0ms cubic-bezier(0, 0, 0.2, 1)}.mdc-form-field{display:inline-flex;align-items:center;vertical-align:middle}.mdc-form-field[hidden]{display:none}.mdc-form-field>label{margin-left:0;margin-right:auto;padding-left:4px;padding-right:0;order:0}[dir=rtl] .mdc-form-field>label,.mdc-form-field>label[dir=rtl]{margin-left:auto;margin-right:0}[dir=rtl] .mdc-form-field>label,.mdc-form-field>label[dir=rtl]{padding-left:0;padding-right:4px}.mdc-form-field--nowrap>label{text-overflow:ellipsis;overflow:hidden;white-space:nowrap}.mdc-form-field--align-end>label{margin-left:auto;margin-right:0;padding-left:0;padding-right:4px;order:-1}[dir=rtl] .mdc-form-field--align-end>label,.mdc-form-field--align-end>label[dir=rtl]{margin-left:0;margin-right:auto}[dir=rtl] .mdc-form-field--align-end>label,.mdc-form-field--align-end>label[dir=rtl]{padding-left:4px;padding-right:0}.mdc-form-field--space-between{justify-content:space-between}.mdc-form-field--space-between>label{margin:0}[dir=rtl] .mdc-form-field--space-between>label,.mdc-form-field--space-between>label[dir=rtl]{margin:0}.mat-mdc-radio-button{-webkit-tap-highlight-color:rgba(0,0,0,0)}.mat-mdc-radio-button .mdc-radio{padding:calc((var(--mdc-radio-state-layer-size, 40px) - 20px) / 2)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-disabled-selected-icon-color, #000)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control+.mdc-radio__background .mdc-radio__inner-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-disabled-selected-icon-color, #000)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control:checked+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled:checked+.mdc-radio__background .mdc-radio__outer-circle{opacity:var(--mdc-radio-disabled-selected-icon-opacity, 0.38)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control+.mdc-radio__background .mdc-radio__inner-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled+.mdc-radio__background .mdc-radio__inner-circle{opacity:var(--mdc-radio-disabled-selected-icon-opacity, 0.38)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-disabled-unselected-icon-color, #000)}.mat-mdc-radio-button .mdc-radio [aria-disabled=true] .mdc-radio__native-control:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:disabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{opacity:var(--mdc-radio-disabled-unselected-icon-opacity, 0.38)}.mat-mdc-radio-button .mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle,.mat-mdc-radio-button .mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-selected-focus-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio.mdc-ripple-upgraded--background-focused .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle,.mat-mdc-radio-button .mdc-radio:not(.mdc-ripple-upgraded):focus .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-selected-focus-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:hover .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-selected-hover-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:hover .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-selected-hover-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-selected-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-selected-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:not(:disabled):active .mdc-radio__native-control:enabled:checked+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-selected-pressed-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:not(:disabled):active .mdc-radio__native-control:enabled+.mdc-radio__background .mdc-radio__inner-circle{border-color:var(--mdc-radio-selected-pressed-icon-color, #6200ee)}.mat-mdc-radio-button .mdc-radio:hover .mdc-radio__native-control:enabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-unselected-hover-icon-color, #000)}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:enabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-unselected-icon-color, #000)}.mat-mdc-radio-button .mdc-radio:not(:disabled):active .mdc-radio__native-control:enabled:not(:checked)+.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-unselected-pressed-icon-color, #000)}.mat-mdc-radio-button .mdc-radio .mdc-radio__background::before{top:calc(-1 * (var(--mdc-radio-state-layer-size, 40px) - 20px) / 2);left:calc(-1 * (var(--mdc-radio-state-layer-size, 40px) - 20px) / 2);width:var(--mdc-radio-state-layer-size, 40px);height:var(--mdc-radio-state-layer-size, 40px)}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control{top:calc((var(--mdc-radio-state-layer-size, 40px) - var(--mdc-radio-state-layer-size, 40px)) / 2);right:calc((var(--mdc-radio-state-layer-size, 40px) - var(--mdc-radio-state-layer-size, 40px)) / 2);left:calc((var(--mdc-radio-state-layer-size, 40px) - var(--mdc-radio-state-layer-size, 40px)) / 2);width:var(--mdc-radio-state-layer-size, 40px);height:var(--mdc-radio-state-layer-size, 40px)}.mat-mdc-radio-button .mdc-radio .mdc-radio__background::before{background-color:var(--mat-mdc-radio-ripple-color, transparent)}.mat-mdc-radio-button .mdc-radio:hover .mdc-radio__native-control:not([disabled]):not(:focus)~.mdc-radio__background::before{opacity:.04;transform:scale(1)}.mat-mdc-radio-button.mat-mdc-radio-checked .mdc-radio__background::before{background-color:var(--mat-mdc-radio-checked-ripple-color, transparent)}.mat-mdc-radio-button.mat-mdc-radio-checked .mat-ripple-element{background-color:var(--mat-mdc-radio-checked-ripple-color, transparent)}.mat-mdc-radio-button .mat-radio-ripple{top:0;left:0;right:0;bottom:0;position:absolute;pointer-events:none;border-radius:50%}.mat-mdc-radio-button .mat-radio-ripple .mat-ripple-element{opacity:.14}.mat-mdc-radio-button .mat-radio-ripple::before{border-radius:50%}.mat-mdc-radio-button._mat-animation-noopable .mdc-radio__background::before,.mat-mdc-radio-button._mat-animation-noopable .mdc-radio__outer-circle,.mat-mdc-radio-button._mat-animation-noopable .mdc-radio__inner-circle{transition:none !important}.mat-mdc-radio-button .mdc-radio .mdc-radio__native-control:focus:enabled:not(:checked)~.mdc-radio__background .mdc-radio__outer-circle{border-color:var(--mdc-radio-unselected-focus-icon-color, black)}.mat-mdc-radio-button.cdk-focused .mat-mdc-focus-indicator::before{content:\"\"}.mat-mdc-radio-touch-target{position:absolute;top:50%;height:48px;left:50%;width:48px;transform:translate(-50%, -50%)}[dir=rtl] .mat-mdc-radio-touch-target{left:0;right:50%;transform:translate(50%, -50%)}"]
    }]
  }], function () {
    return [{
      type: MatRadioGroup,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Inject,
        args: [MAT_RADIO_GROUP]
      }]
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.ElementRef
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.ChangeDetectorRef
    }, {
      type: _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_4__.FocusMonitor
    }, {
      type: _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__.UniqueSelectionDispatcher
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Inject,
        args: [_angular_core__WEBPACK_IMPORTED_MODULE_1__.ANIMATION_MODULE_TYPE]
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Inject,
        args: [MAT_RADIO_DEFAULT_OPTIONS]
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.Attribute,
        args: ['tabindex']
      }]
    }];
  }, null);
})();

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
class MatRadioModule {}
MatRadioModule.ɵfac = function MatRadioModule_Factory(t) {
  return new (t || MatRadioModule)();
};
MatRadioModule.ɵmod = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
  type: MatRadioModule
});
MatRadioModule.ɵinj = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
  imports: [_angular_material_core__WEBPACK_IMPORTED_MODULE_3__.MatCommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule, _angular_material_core__WEBPACK_IMPORTED_MODULE_3__.MatRippleModule, _angular_material_core__WEBPACK_IMPORTED_MODULE_3__.MatCommonModule]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](MatRadioModule, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule,
    args: [{
      imports: [_angular_material_core__WEBPACK_IMPORTED_MODULE_3__.MatCommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule, _angular_material_core__WEBPACK_IMPORTED_MODULE_3__.MatRippleModule],
      exports: [_angular_material_core__WEBPACK_IMPORTED_MODULE_3__.MatCommonModule, MatRadioGroup, MatRadioButton],
      declarations: [MatRadioGroup, MatRadioButton]
    }]
  }], null, null);
})();

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

/**
 * Generated bundle index. Do not edit.
 */



/***/ }),

/***/ 35139:
/*!***************************************************************************!*\
  !*** ./node_modules/ng-pick-datetime-ex/fesm2020/ng-pick-datetime-ex.mjs ***!
  \***************************************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DateTimeAdapter": () => (/* binding */ DateTimeAdapter),
/* harmony export */   "OWL_DATE_TIME_FORMATS": () => (/* binding */ OWL_DATE_TIME_FORMATS),
/* harmony export */   "OWL_DATE_TIME_LOCALE": () => (/* binding */ OWL_DATE_TIME_LOCALE),
/* harmony export */   "OWL_DATE_TIME_LOCALE_PROVIDER": () => (/* binding */ OWL_DATE_TIME_LOCALE_PROVIDER),
/* harmony export */   "OwlCalendarComponent": () => (/* binding */ OwlCalendarComponent),
/* harmony export */   "OwlDateTimeComponent": () => (/* binding */ OwlDateTimeComponent),
/* harmony export */   "OwlDateTimeInlineComponent": () => (/* binding */ OwlDateTimeInlineComponent),
/* harmony export */   "OwlDateTimeInputDirective": () => (/* binding */ OwlDateTimeInputDirective),
/* harmony export */   "OwlDateTimeIntl": () => (/* binding */ OwlDateTimeIntl),
/* harmony export */   "OwlDateTimeModule": () => (/* binding */ OwlDateTimeModule),
/* harmony export */   "OwlDateTimeTriggerDirective": () => (/* binding */ OwlDateTimeTriggerDirective),
/* harmony export */   "OwlDayjsDateTimeModule": () => (/* binding */ OwlDayjsDateTimeModule),
/* harmony export */   "OwlMonthViewComponent": () => (/* binding */ OwlMonthViewComponent),
/* harmony export */   "OwlMultiYearViewComponent": () => (/* binding */ OwlMultiYearViewComponent),
/* harmony export */   "OwlNativeDateTimeModule": () => (/* binding */ OwlNativeDateTimeModule),
/* harmony export */   "OwlTimerComponent": () => (/* binding */ OwlTimerComponent),
/* harmony export */   "OwlYearViewComponent": () => (/* binding */ OwlYearViewComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/cdk/a11y */ 24218);
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/cdk/overlay */ 25895);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 26078);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 10745);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 36646);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 80228);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! rxjs */ 21954);
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/cdk/portal */ 17520);
/* harmony import */ var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/cdk/keycodes */ 28456);
/* harmony import */ var _angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/cdk/coercion */ 48971);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 59295);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! rxjs/operators */ 98977);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! rxjs/operators */ 60116);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! rxjs/operators */ 44874);
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/animations */ 24851);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/cdk/platform */ 89107);
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! dayjs */ 16901);
/* harmony import */ var dayjs_plugin_localeData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! dayjs/plugin/localeData */ 45965);
/* harmony import */ var dayjs_plugin_objectSupport__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dayjs/plugin/objectSupport */ 34596);
/* harmony import */ var dayjs_plugin_localizedFormat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! dayjs/plugin/localizedFormat */ 87772);
/* harmony import */ var dayjs_plugin_utc__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! dayjs/plugin/utc */ 21859);
























/**
 * date-time-picker-trigger.directive
 */
const _c0 = ["owl-date-time-calendar-body", ""];
const _c1 = function (a0, a1, a2) {
  return {
    "owl-dt-calendar-cell-out": a0,
    "owl-dt-calendar-cell-today": a1,
    "owl-dt-calendar-cell-selected": a2
  };
};
function OwlCalendarBodyComponent_tr_0_td_1_Template(rf, ctx) {
  if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "td", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlCalendarBodyComponent_tr_0_td_1_Template_td_click_0_listener() {
      const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r7);
      const item_r4 = restoredCtx.$implicit;
      const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"](2);
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r6.selectCell(item_r4));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "span", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const item_r4 = ctx.$implicit;
    const colIndex_r5 = ctx.index;
    const rowIndex_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]().index;
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassMapInterpolate1"]("owl-dt-calendar-cell ", item_r4.cellClass, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵstyleProp"]("width", 100 / ctx_r3.numCols, "%")("padding-top", 50 * ctx_r3.cellRatio / ctx_r3.numCols, "%")("padding-bottom", 50 * ctx_r3.cellRatio / ctx_r3.numCols, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-calendar-cell-active", ctx_r3.isActiveCell(rowIndex_r2, colIndex_r5))("owl-dt-calendar-cell-disabled", !item_r4.enabled)("owl-dt-calendar-cell-in-range", ctx_r3.isInRange(item_r4.value))("owl-dt-calendar-cell-range-from", ctx_r3.isRangeFrom(item_r4.value))("owl-dt-calendar-cell-range-to", ctx_r3.isRangeTo(item_r4.value));
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("tabindex", ctx_r3.isActiveCell(rowIndex_r2, colIndex_r5) ? 0 : -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-label", item_r4.ariaLabel)("aria-disabled", !item_r4.enabled || null);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵpureFunction3"](24, _c1, item_r4.out, item_r4.value === ctx_r3.todayValue, ctx_r3.isSelected(item_r4.value)));
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate1"](" ", item_r4.displayValue, " ");
  }
}
function OwlCalendarBodyComponent_tr_0_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "tr", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](1, OwlCalendarBodyComponent_tr_0_td_1_Template, 3, 28, "td", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const row_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngForOf", row_r1);
  }
}
function OwlMonthViewComponent_th_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "th", 6)(1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const weekday_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-label", weekday_r1.long);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"](weekday_r1.short);
  }
}
function OwlCalendarComponent_owl_date_time_month_view_18_Template(rf, ctx) {
  if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "owl-date-time-month-view", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("pickerDayjsChange", function OwlCalendarComponent_owl_date_time_month_view_18_Template_owl_date_time_month_view_pickerDayjsChange_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r4);
      const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r3.handlePickerDayjsChange($event));
    })("selectedChange", function OwlCalendarComponent_owl_date_time_month_view_18_Template_owl_date_time_month_view_selectedChange_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r4);
      const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r5.dateSelected($event));
    })("userSelection", function OwlCalendarComponent_owl_date_time_month_view_18_Template_owl_date_time_month_view_userSelection_0_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r4);
      const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r6.userSelected());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("pickerDayjs", ctx_r0.pickerDayjs)("firstDayOfWeek", ctx_r0.firstDayOfWeek)("selected", ctx_r0.selected)("selecteds", ctx_r0.selecteds)("selectMode", ctx_r0.selectMode)("minDate", ctx_r0.minDate)("maxDate", ctx_r0.maxDate)("dateFilter", ctx_r0.dateFilter)("hideOtherMonths", ctx_r0.hideOtherMonths);
  }
}
function OwlCalendarComponent_owl_date_time_year_view_19_Template(rf, ctx) {
  if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "owl-date-time-year-view", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("keyboardEnter", function OwlCalendarComponent_owl_date_time_year_view_19_Template_owl_date_time_year_view_keyboardEnter_0_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r8);
      const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r7.focusActiveCell());
    })("pickerDayjsChange", function OwlCalendarComponent_owl_date_time_year_view_19_Template_owl_date_time_year_view_pickerDayjsChange_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r8);
      const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r9.handlePickerDayjsChange($event));
    })("monthSelected", function OwlCalendarComponent_owl_date_time_year_view_19_Template_owl_date_time_year_view_monthSelected_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r8);
      const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r10.selectMonthInYearView($event));
    })("change", function OwlCalendarComponent_owl_date_time_year_view_19_Template_owl_date_time_year_view_change_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r8);
      const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r11.goToDateInView($event, "month"));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("pickerDayjs", ctx_r1.pickerDayjs)("selected", ctx_r1.selected)("selecteds", ctx_r1.selecteds)("selectMode", ctx_r1.selectMode)("minDate", ctx_r1.minDate)("maxDate", ctx_r1.maxDate)("dateFilter", ctx_r1.dateFilter);
  }
}
function OwlCalendarComponent_owl_date_time_multi_year_view_20_Template(rf, ctx) {
  if (rf & 1) {
    const _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "owl-date-time-multi-year-view", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("keyboardEnter", function OwlCalendarComponent_owl_date_time_multi_year_view_20_Template_owl_date_time_multi_year_view_keyboardEnter_0_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r13);
      const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r12.focusActiveCell());
    })("pickerDayjsChange", function OwlCalendarComponent_owl_date_time_multi_year_view_20_Template_owl_date_time_multi_year_view_pickerDayjsChange_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r13);
      const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r14.handlePickerDayjsChange($event));
    })("yearSelected", function OwlCalendarComponent_owl_date_time_multi_year_view_20_Template_owl_date_time_multi_year_view_yearSelected_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r13);
      const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r15.selectYearInMultiYearView($event));
    })("change", function OwlCalendarComponent_owl_date_time_multi_year_view_20_Template_owl_date_time_multi_year_view_change_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r13);
      const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r16.goToDateInView($event, "year"));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("pickerDayjs", ctx_r2.pickerDayjs)("selected", ctx_r2.selected)("selecteds", ctx_r2.selecteds)("selectMode", ctx_r2.selectMode)("minDate", ctx_r2.minDate)("maxDate", ctx_r2.maxDate)("dateFilter", ctx_r2.dateFilter);
  }
}
const _c2 = ["valueInput"];
function OwlTimerBoxComponent_div_0_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](0, "div", 10);
  }
}
function OwlTimerComponent_owl_date_time_timer_box_2_Template(rf, ctx) {
  if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "owl-date-time-timer-box", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("inputChange", function OwlTimerComponent_owl_date_time_timer_box_2_Template_owl_date_time_timer_box_inputChange_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r3);
      const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r2.setSecondValue($event));
    })("valueChange", function OwlTimerComponent_owl_date_time_timer_box_2_Template_owl_date_time_timer_box_valueChange_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r3);
      const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r4.setSecondValue($event));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("showDivider", true)("upBtnAriaLabel", ctx_r0.upSecondButtonLabel)("downBtnAriaLabel", ctx_r0.downSecondButtonLabel)("upBtnDisabled", !ctx_r0.upSecondEnabled())("downBtnDisabled", !ctx_r0.downSecondEnabled())("value", ctx_r0.secondValue)("min", 0)("max", 59)("step", ctx_r0.stepSecond)("inputLabel", "Second");
  }
}
function OwlTimerComponent_div_3_Template(rf, ctx) {
  if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 4)(1, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlTimerComponent_div_3_Template_button_click_1_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r6);
      const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r5.setMeridiem($event));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](2, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
  }
  if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate1"](" ", ctx_r1.hour12ButtonLabel, " ");
  }
}
function OwlDateTimeContainerComponent_owl_date_time_calendar_1_Template(rf, ctx) {
  if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "owl-date-time-calendar", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("pickerDayjsChange", function OwlDateTimeContainerComponent_owl_date_time_calendar_1_Template_owl_date_time_calendar_pickerDayjsChange_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r5);
      const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r4.pickerDayjs = $event);
    })("yearSelected", function OwlDateTimeContainerComponent_owl_date_time_calendar_1_Template_owl_date_time_calendar_yearSelected_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r5);
      const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r6.picker.selectYear($event));
    })("monthSelected", function OwlDateTimeContainerComponent_owl_date_time_calendar_1_Template_owl_date_time_calendar_monthSelected_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r5);
      const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r7.picker.selectMonth($event));
    })("selectedChange", function OwlDateTimeContainerComponent_owl_date_time_calendar_1_Template_owl_date_time_calendar_selectedChange_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r5);
      const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r8.dateSelected($event));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("firstDayOfWeek", ctx_r0.picker.firstDayOfWeek)("pickerDayjs", ctx_r0.pickerDayjs)("selected", ctx_r0.picker.selected)("selecteds", ctx_r0.picker.selecteds)("selectMode", ctx_r0.picker.selectMode)("minDate", ctx_r0.picker.minDateTime)("maxDate", ctx_r0.picker.maxDateTime)("dateFilter", ctx_r0.picker.dateTimeFilter)("startView", ctx_r0.picker.startView)("hideOtherMonths", ctx_r0.picker.hideOtherMonths);
  }
}
function OwlDateTimeContainerComponent_owl_date_time_timer_2_Template(rf, ctx) {
  if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "owl-date-time-timer", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("selectedChange", function OwlDateTimeContainerComponent_owl_date_time_timer_2_Template_owl_date_time_timer_selectedChange_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r10);
      const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r9.timeSelected($event));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("pickerDayjs", ctx_r1.pickerDayjs)("minDateTime", ctx_r1.picker.minDateTime)("maxDateTime", ctx_r1.picker.maxDateTime)("showSecondsTimer", ctx_r1.picker.showSecondsTimer)("hour12Timer", ctx_r1.picker.hour12Timer)("stepHour", ctx_r1.picker.stepHour)("stepMinute", ctx_r1.picker.stepMinute)("stepSecond", ctx_r1.picker.stepSecond);
  }
}
const _c3 = function (a0) {
  return {
    "owl-dt-container-info-active": a0
  };
};
function OwlDateTimeContainerComponent_div_3_Template(rf, ctx) {
  if (rf & 1) {
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 7)(1, "div", 8, 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlDateTimeContainerComponent_div_3_Template_div_click_1_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r14);
      const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r13.handleClickOnInfoGroup($event, 0));
    })("keydown", function OwlDateTimeContainerComponent_div_3_Template_div_keydown_1_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r14);
      const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵreference"](9);
      const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r15.handleKeydownOnInfoGroup($event, _r12, 0));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](3, "span", 10)(4, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](6, "span", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](8, "div", 13, 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlDateTimeContainerComponent_div_3_Template_div_click_8_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r14);
      const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r16.handleClickOnInfoGroup($event, 1));
    })("keydown", function OwlDateTimeContainerComponent_div_3_Template_div_keydown_8_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r14);
      const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵreference"](2);
      const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r17.handleKeydownOnInfoGroup($event, _r11, 1));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](10, "span", 10)(11, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](13, "span", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()()();
  }
  if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("tabindex", ctx_r2.activeSelectedIndex === 0 ? 0 : -1)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵpureFunction1"](10, _c3, ctx_r2.activeSelectedIndex === 0));
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-checked", ctx_r2.activeSelectedIndex === 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate1"]("", ctx_r2.fromLabel, ":");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"](ctx_r2.fromFormattedValue);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("tabindex", ctx_r2.activeSelectedIndex === 1 ? 0 : -1)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵpureFunction1"](12, _c3, ctx_r2.activeSelectedIndex === 1));
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-checked", ctx_r2.activeSelectedIndex === 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate1"]("", ctx_r2.toLabel, ":");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"](ctx_r2.toFormattedValue);
  }
}
function OwlDateTimeContainerComponent_div_4_Template(rf, ctx) {
  if (rf & 1) {
    const _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 15)(1, "button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlDateTimeContainerComponent_div_4_Template_button_click_1_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r19);
      const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r18.onCancelClicked($event));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](2, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlDateTimeContainerComponent_div_4_Template_button_click_4_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r19);
      const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx_r20.onSetClicked($event));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](5, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
  }
  if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate1"](" ", ctx_r3.cancelLabel, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate1"](" ", ctx_r3.setLabel, " ");
  }
}
function OwlDialogContainerComponent_ng_template_0_Template(rf, ctx) {}
class OwlDateTimeTriggerDirective {
  constructor(changeDetector) {
    this.changeDetector = changeDetector;
    this.stateChanges = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
  }
  get disabled() {
    return this._disabled === undefined ? this.dtPicker.disabled : !!this._disabled;
  }
  set disabled(value) {
    this._disabled = value;
  }
  get owlDTTriggerDisabledClass() {
    return this.disabled;
  }
  ngOnInit() {}
  ngOnChanges(changes) {
    if (changes.datepicker) {
      this.watchStateChanges();
    }
  }
  ngAfterContentInit() {
    this.watchStateChanges();
  }
  ngOnDestroy() {
    this.stateChanges.unsubscribe();
  }
  handleClickOnHost(event) {
    if (this.dtPicker) {
      this.dtPicker.open();
      event.stopPropagation();
    }
  }
  watchStateChanges() {
    this.stateChanges.unsubscribe();
    const inputDisabled = this.dtPicker && this.dtPicker.dtInput ? this.dtPicker.dtInput.disabledChange : (0,rxjs__WEBPACK_IMPORTED_MODULE_7__.of)();
    const pickerDisabled = this.dtPicker ? this.dtPicker.disabledChange : (0,rxjs__WEBPACK_IMPORTED_MODULE_7__.of)();
    this.stateChanges = (0,rxjs__WEBPACK_IMPORTED_MODULE_8__.merge)(pickerDisabled, inputDisabled).subscribe(() => {
      this.changeDetector.markForCheck();
    });
  }
}
OwlDateTimeTriggerDirective.ɵfac = function OwlDateTimeTriggerDirective_Factory(t) {
  return new (t || OwlDateTimeTriggerDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef));
};
OwlDateTimeTriggerDirective.ɵdir = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineDirective"]({
  type: OwlDateTimeTriggerDirective,
  selectors: [["", "owlDateTimeTrigger", ""]],
  hostVars: 2,
  hostBindings: function OwlDateTimeTriggerDirective_HostBindings(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlDateTimeTriggerDirective_click_HostBindingHandler($event) {
        return ctx.handleClickOnHost($event);
      });
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-trigger-disabled", ctx.owlDTTriggerDisabledClass);
    }
  },
  inputs: {
    dtPicker: ["owlDateTimeTrigger", "dtPicker"],
    disabled: "disabled"
  },
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵNgOnChangesFeature"]]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDateTimeTriggerDirective, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Directive,
    args: [{
      selector: '[owlDateTimeTrigger]',
      host: {
        '(click)': 'handleClickOnHost($event)',
        '[class.owl-dt-trigger-disabled]': 'owlDTTriggerDisabledClass'
      }
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }];
  }, {
    dtPicker: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input,
      args: ['owlDateTimeTrigger']
    }],
    disabled: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }]
  });
})();

/**
 * date-time-format.class
 */
/** InjectionToken for date time picker that can be used to override default format. */
const OWL_DATE_TIME_FORMATS = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.InjectionToken('OWL_DATE_TIME_FORMATS');

/**
 * date-time-picker-intl.service
 */
class OwlDateTimeIntl {
  constructor() {
    /**
     * Stream that emits whenever the labels here are changed. Use this to notify
     * components if the labels have changed after initialization.
     */
    this.changes = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    /** A label for the up second button (used by screen readers).  */
    this.upSecondLabel = 'Add a second';
    /** A label for the down second button (used by screen readers).  */
    this.downSecondLabel = 'Minus a second';
    /** A label for the up minute button (used by screen readers).  */
    this.upMinuteLabel = 'Add a minute';
    /** A label for the down minute button (used by screen readers).  */
    this.downMinuteLabel = 'Minus a minute';
    /** A label for the up hour button (used by screen readers).  */
    this.upHourLabel = 'Add a hour';
    /** A label for the down hour button (used by screen readers).  */
    this.downHourLabel = 'Minus a hour';
    /** A label for the previous month button (used by screen readers). */
    this.prevMonthLabel = 'Previous month';
    /** A label for the next month button (used by screen readers). */
    this.nextMonthLabel = 'Next month';
    /** A label for the previous year button (used by screen readers). */
    this.prevYearLabel = 'Previous year';
    /** A label for the next year button (used by screen readers). */
    this.nextYearLabel = 'Next year';
    /** A label for the previous multi-year button (used by screen readers). */
    this.prevMultiYearLabel = 'Previous 21 years';
    /** A label for the next multi-year button (used by screen readers). */
    this.nextMultiYearLabel = 'Next 21 years';
    /** A label for the 'switch to month view' button (used by screen readers). */
    this.switchToMonthViewLabel = 'Change to month view';
    /** A label for the 'switch to year view' button (used by screen readers). */
    this.switchToMultiYearViewLabel = 'Choose month and year';
    /** A label for the cancel button */
    this.cancelBtnLabel = 'Cancel';
    /** A label for the set button */
    this.setBtnLabel = 'Set';
    /** A label for the range 'from' in picker info */
    this.rangeFromLabel = 'From';
    /** A label for the range 'to' in picker info */
    this.rangeToLabel = 'To';
    /** A label for the hour12 button (AM) */
    this.hour12AMLabel = 'AM';
    /** A label for the hour12 button (PM) */
    this.hour12PMLabel = 'PM';
  }
}
OwlDateTimeIntl.ɵfac = function OwlDateTimeIntl_Factory(t) {
  return new (t || OwlDateTimeIntl)();
};
OwlDateTimeIntl.ɵprov = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjectable"]({
  token: OwlDateTimeIntl,
  factory: OwlDateTimeIntl.ɵfac,
  providedIn: 'root'
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDateTimeIntl, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable,
    args: [{
      providedIn: 'root'
    }]
  }], null, null);
})();

/**
 * date-time-adapter.class
 */
/** InjectionToken for date time picker that can be used to override default locale code. */
const OWL_DATE_TIME_LOCALE = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.InjectionToken('OWL_DATE_TIME_LOCALE', {
  providedIn: 'root',
  factory: OWL_DATE_TIME_LOCALE_FACTORY
});
/** @docs-private */
function OWL_DATE_TIME_LOCALE_FACTORY() {
  return (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.inject)(_angular_core__WEBPACK_IMPORTED_MODULE_5__.LOCALE_ID);
}
/** Provider for OWL_DATE_TIME_LOCALE injection token. */
const OWL_DATE_TIME_LOCALE_PROVIDER = {
  provide: OWL_DATE_TIME_LOCALE,
  useExisting: _angular_core__WEBPACK_IMPORTED_MODULE_5__.LOCALE_ID
};
class DateTimeAdapter {
  constructor() {
    /** A stream that emits when the locale changes. */
    this._localeChanges = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    /** total milliseconds in a day. */
    this.millisecondsInDay = 86400000;
    /** total milliseconds in a minute. */
    this.milliseondsInMinute = 60000;
  }
  get localeChanges() {
    return this._localeChanges;
  }
  /**
   * Compare two given dates
   * 1 if the first date is after the second,
   * -1 if the first date is before the second
   * 0 if dates are equal.
   * */
  compare(first, second) {
    if (!this.isValid(first) || !this.isValid(second)) {
      throw Error('JSNativeDate: Cannot compare invalid dates.');
    }
    const dateFirst = this.clone(first);
    const dateSecond = this.clone(second);
    const diff = this.getTime(dateFirst) - this.getTime(dateSecond);
    if (diff < 0) {
      return -1;
    } else if (diff > 0) {
      return 1;
    } else {
      // Return 0 if diff is 0; return NaN if diff is NaN
      return diff;
    }
  }
  /**
   * Check if two given dates are in the same year
   * 1 if the first date's year is after the second,
   * -1 if the first date's year is before the second
   * 0 if two given dates are in the same year
   * */
  compareYear(first, second) {
    if (!this.isValid(first) || !this.isValid(second)) {
      throw Error('JSNativeDate: Cannot compare invalid dates.');
    }
    const yearLeft = this.getYear(first);
    const yearRight = this.getYear(second);
    const diff = yearLeft - yearRight;
    if (diff < 0) {
      return -1;
    } else if (diff > 0) {
      return 1;
    } else {
      return 0;
    }
  }
  /**
   * Attempts to deserialize a value to a valid date object. This is different from parsing in that
   * deserialize should only accept non-ambiguous, locale-independent formats (e.g. a ISO 8601
   * string). The default implementation does not allow any deserialization, it simply checks that
   * the given value is already a valid date object or null. The `<mat-datepicker>` will call this
   * method on all of it's `@Input()` properties that accept dates. It is therefore possible to
   * support passing values from your backend directly to these properties by overriding this method
   * to also deserialize the format used by your backend.
   */
  deserialize(value) {
    if (value == null || this.isDateInstance(value) && this.isValid(value)) {
      return value;
    }
    return this.invalid();
  }
  /**
   * Sets the locale used for all dates.
   */
  setLocale(locale) {
    this.locale = locale;
    this._localeChanges.next();
  }
  /**
   * Clamp the given date between min and max dates.
   */
  clampDate(date, min, max) {
    if (min && this.compare(date, min) < 0) {
      return min;
    }
    if (max && this.compare(date, max) > 0) {
      return max;
    }
    return date;
  }
}

/**
 * calendar-body.component
 */
class CalendarCell {
  constructor(value, displayValue, ariaLabel, enabled, out = false, cellClass = '') {
    this.value = value;
    this.displayValue = displayValue;
    this.ariaLabel = ariaLabel;
    this.enabled = enabled;
    this.out = out;
    this.cellClass = cellClass;
  }
}
class OwlCalendarBodyComponent {
  constructor(elmRef, ngZone) {
    this.elmRef = elmRef;
    this.ngZone = ngZone;
    /**
     * The cell number of the active cell in the table.
     */
    this.activeCell = 0;
    /**
     * The number of columns in the table.
     * */
    this.numCols = 7;
    /**
     * The ratio (width / height) to use for the cells in the table.
     */
    this.cellRatio = 1;
    /**
     * Emit when a calendar cell is selected
     * */
    this.select = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
  }
  get owlDTCalendarBodyClass() {
    return true;
  }
  get isInSingleMode() {
    return this.selectMode === 'single';
  }
  get isInRangeMode() {
    return this.selectMode === 'range' || this.selectMode === 'rangeFrom' || this.selectMode === 'rangeTo';
  }
  ngOnInit() {}
  selectCell(cell) {
    this.select.emit(cell);
  }
  isActiveCell(rowIndex, colIndex) {
    const cellNumber = rowIndex * this.numCols + colIndex;
    return cellNumber === this.activeCell;
  }
  /**
   * Check if the cell is selected
   */
  isSelected(value) {
    if (!this.selectedValues || this.selectedValues.length === 0) {
      return false;
    }
    if (this.isInSingleMode) {
      return value === this.selectedValues[0];
    }
    if (this.isInRangeMode) {
      const fromValue = this.selectedValues[0];
      const toValue = this.selectedValues[1];
      return value === fromValue || value === toValue;
    }
  }
  /**
   * Check if the cell in the range
   * */
  isInRange(value) {
    if (this.isInRangeMode) {
      const fromValue = this.selectedValues[0];
      const toValue = this.selectedValues[1];
      if (fromValue !== null && toValue !== null) {
        return value >= fromValue && value <= toValue;
      } else {
        return value === fromValue || value === toValue;
      }
    }
  }
  /**
   * Check if the cell is the range from
   * */
  isRangeFrom(value) {
    if (this.isInRangeMode) {
      const fromValue = this.selectedValues[0];
      return fromValue !== null && value === fromValue;
    }
  }
  /**
   * Check if the cell is the range to
   * */
  isRangeTo(value) {
    if (this.isInRangeMode) {
      const toValue = this.selectedValues[1];
      return toValue !== null && value === toValue;
    }
  }
  /**
   * Focus to a active cell
   * */
  focusActiveCell() {
    this.ngZone.runOutsideAngular(() => {
      this.ngZone.onStable.asObservable().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1)).subscribe(() => {
        this.elmRef.nativeElement.querySelector('.owl-dt-calendar-cell-active').focus();
      });
    });
  }
}
OwlCalendarBodyComponent.ɵfac = function OwlCalendarBodyComponent_Factory(t) {
  return new (t || OwlCalendarBodyComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgZone));
};
OwlCalendarBodyComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlCalendarBodyComponent,
  selectors: [["", "owl-date-time-calendar-body", ""]],
  hostVars: 2,
  hostBindings: function OwlCalendarBodyComponent_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-calendar-body", ctx.owlDTCalendarBodyClass);
    }
  },
  inputs: {
    activeCell: "activeCell",
    rows: "rows",
    numCols: "numCols",
    cellRatio: "cellRatio",
    todayValue: "todayValue",
    selectedValues: "selectedValues",
    selectMode: "selectMode"
  },
  outputs: {
    select: "select"
  },
  exportAs: ["owlDateTimeCalendarBody"],
  attrs: _c0,
  decls: 1,
  vars: 1,
  consts: [["role", "row", 4, "ngFor", "ngForOf"], ["role", "row"], [3, "class", "tabindex", "owl-dt-calendar-cell-active", "owl-dt-calendar-cell-disabled", "owl-dt-calendar-cell-in-range", "owl-dt-calendar-cell-range-from", "owl-dt-calendar-cell-range-to", "width", "paddingTop", "paddingBottom", "click", 4, "ngFor", "ngForOf"], [3, "tabindex", "click"], [1, "owl-dt-calendar-cell-content", 3, "ngClass"]],
  template: function OwlCalendarBodyComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](0, OwlCalendarBodyComponent_tr_0_Template, 2, 1, "tr", 0);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngForOf", ctx.rows);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_11__.NgForOf],
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlCalendarBodyComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      selector: '[owl-date-time-calendar-body]',
      exportAs: 'owlDateTimeCalendarBody',
      host: {
        '[class.owl-dt-calendar-body]': 'owlDTCalendarBodyClass'
      },
      preserveWhitespaces: false,
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      template: "<tr *ngFor=\"let row of rows; let rowIndex = index\" role=\"row\">\n    <td *ngFor=\"let item of row; let colIndex = index\"\n        class=\"owl-dt-calendar-cell {{item.cellClass}}\"\n        [tabindex]=\"isActiveCell(rowIndex, colIndex) ? 0 : -1\"\n        [class.owl-dt-calendar-cell-active]=\"isActiveCell(rowIndex, colIndex)\"\n        [class.owl-dt-calendar-cell-disabled]=\"!item.enabled\"\n        [class.owl-dt-calendar-cell-in-range]=\"isInRange(item.value)\"\n        [class.owl-dt-calendar-cell-range-from]=\"isRangeFrom(item.value)\"\n        [class.owl-dt-calendar-cell-range-to]=\"isRangeTo(item.value)\"\n        [attr.aria-label]=\"item.ariaLabel\"\n        [attr.aria-disabled]=\"!item.enabled || null\"\n        [style.width.%]=\"100 / numCols\"\n        [style.paddingTop.%]=\"50 * cellRatio / numCols\"\n        [style.paddingBottom.%]=\"50 * cellRatio / numCols\"\n        (click)=\"selectCell(item)\">\n        <span class=\"owl-dt-calendar-cell-content\"\n              [ngClass]=\"{\n                'owl-dt-calendar-cell-out': item.out,\n                'owl-dt-calendar-cell-today': item.value === todayValue,\n                'owl-dt-calendar-cell-selected': isSelected(item.value)\n              }\">\n            {{item.displayValue}}\n        </span>\n    </td>\n</tr>\n"
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgZone
    }];
  }, {
    activeCell: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    rows: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    numCols: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    cellRatio: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    todayValue: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selectedValues: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selectMode: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    select: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }]
  });
})();

/**
 * calendar-multi-year-view.component
 */
const YEARS_PER_ROW = 3;
const YEAR_ROWS = 7;
class OwlMultiYearViewComponent {
  constructor(cdRef, pickerIntl, dateTimeAdapter) {
    this.cdRef = cdRef;
    this.pickerIntl = pickerIntl;
    this.dateTimeAdapter = dateTimeAdapter;
    /**
     * The select mode of the picker;
     * */
    this._selectMode = 'single';
    this._selecteds = [];
    this.initiated = false;
    /**
     * Callback to invoke when a new month is selected
     * */
    this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Emits the selected year. This doesn't imply a change on the selected date
     * */
    this.yearSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /** Emits when any date is activated. */
    this.pickerDayjsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /** Emits when use keyboard enter to select a calendar cell */
    this.keyboardEnter = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
  }
  get selectMode() {
    return this._selectMode;
  }
  set selectMode(val) {
    this._selectMode = val;
    if (this.initiated) {
      this.setSelectedYears();
      this.cdRef.markForCheck();
    }
  }
  get selected() {
    return this._selected;
  }
  set selected(value) {
    const oldSelected = this._selected;
    value = this.dateTimeAdapter.deserialize(value);
    this._selected = this.getValidDate(value);
    if (!this.dateTimeAdapter.isSameDay(oldSelected, this._selected)) {
      this.setSelectedYears();
    }
  }
  get selecteds() {
    return this._selecteds;
  }
  set selecteds(values) {
    this._selecteds = values.map(v => {
      v = this.dateTimeAdapter.deserialize(v);
      return this.getValidDate(v);
    });
    this.setSelectedYears();
  }
  get pickerDayjs() {
    return this._pickerDayjs;
  }
  set pickerDayjs(value) {
    const oldDayjs = this._pickerDayjs;
    value = this.dateTimeAdapter.deserialize(value);
    this._pickerDayjs = this.getValidDate(value) || this.dateTimeAdapter.now();
    if (oldDayjs && this._pickerDayjs && !this.isSameYearList(oldDayjs, this._pickerDayjs)) {
      this.generateYearList();
    }
  }
  get dateFilter() {
    return this._dateFilter;
  }
  set dateFilter(filter) {
    this._dateFilter = filter;
    if (this.initiated) {
      this.generateYearList();
    }
  }
  get minDate() {
    return this._minDate;
  }
  set minDate(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._minDate = this.getValidDate(value);
    if (this.initiated) {
      this.generateYearList();
    }
  }
  get maxDate() {
    return this._maxDate;
  }
  set maxDate(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._maxDate = this.getValidDate(value);
    if (this.initiated) {
      this.generateYearList();
    }
  }
  get todayYear() {
    return this._todayYear;
  }
  get years() {
    return this._years;
  }
  get selectedYears() {
    return this._selectedYears;
  }
  get isInSingleMode() {
    return this.selectMode === 'single';
  }
  get isInRangeMode() {
    return this.selectMode === 'range' || this.selectMode === 'rangeFrom' || this.selectMode === 'rangeTo';
  }
  get activeCell() {
    if (this._pickerDayjs) {
      return this.dateTimeAdapter.getYear(this._pickerDayjs) % (YEARS_PER_ROW * YEAR_ROWS);
    }
  }
  get tableHeader() {
    if (this._years && this._years.length > 0) {
      return `${this._years[0][0].displayValue} ~ ${this._years[YEAR_ROWS - 1][YEARS_PER_ROW - 1].displayValue}`;
    }
  }
  get prevButtonLabel() {
    return this.pickerIntl.prevMultiYearLabel;
  }
  get nextButtonLabel() {
    return this.pickerIntl.nextMultiYearLabel;
  }
  get owlDTCalendarView() {
    return true;
  }
  get owlDTCalendarMultiYearView() {
    return true;
  }
  ngOnInit() {}
  ngAfterContentInit() {
    this._todayYear = this.dateTimeAdapter.getYear(this.dateTimeAdapter.now());
    this.generateYearList();
    this.initiated = true;
  }
  /**
   * Handle a calendarCell selected
   */
  selectCalendarCell(cell) {
    this.selectYear(cell.value);
  }
  selectYear(year) {
    this.yearSelected.emit(this.dateTimeAdapter.createDate(year, 0, 1));
    const firstDateOfMonth = this.dateTimeAdapter.createDate(year, this.dateTimeAdapter.getMonth(this.pickerDayjs), 1);
    const daysInMonth = this.dateTimeAdapter.getNumDaysInMonth(firstDateOfMonth);
    const selected = this.dateTimeAdapter.createDate(year, this.dateTimeAdapter.getMonth(this.pickerDayjs), Math.min(daysInMonth, this.dateTimeAdapter.getDate(this.pickerDayjs)), this.dateTimeAdapter.getHours(this.pickerDayjs), this.dateTimeAdapter.getMinutes(this.pickerDayjs), this.dateTimeAdapter.getSeconds(this.pickerDayjs));
    this.change.emit(selected);
  }
  /**
   * Generate the previous year list
   * */
  prevYearList(event) {
    this._pickerDayjs = this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, -1 * YEAR_ROWS * YEARS_PER_ROW);
    this.generateYearList();
    event.preventDefault();
  }
  /**
   * Generate the next year list
   * */
  nextYearList(event) {
    this._pickerDayjs = this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, YEAR_ROWS * YEARS_PER_ROW);
    this.generateYearList();
    event.preventDefault();
  }
  generateYearList() {
    this._years = [];
    const pickerDayjsYear = this.dateTimeAdapter.getYear(this._pickerDayjs);
    const offset = pickerDayjsYear % (YEARS_PER_ROW * YEAR_ROWS);
    for (let i = 0; i < YEAR_ROWS; i++) {
      const row = [];
      for (let j = 0; j < YEARS_PER_ROW; j++) {
        const year = pickerDayjsYear - offset + (j + i * YEARS_PER_ROW);
        const yearCell = this.createYearCell(year);
        row.push(yearCell);
      }
      this._years.push(row);
    }
    return;
  }
  /** Whether the previous period button is enabled. */
  previousEnabled() {
    if (!this.minDate) {
      return true;
    }
    return !this.minDate || !this.isSameYearList(this._pickerDayjs, this.minDate);
  }
  /** Whether the next period button is enabled. */
  nextEnabled() {
    return !this.maxDate || !this.isSameYearList(this._pickerDayjs, this.maxDate);
  }
  handleCalendarKeydown(event) {
    let dayjs;
    switch (event.keyCode) {
      // minus 1 year
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.LEFT_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarYears(this._pickerDayjs, -1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // add 1 year
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.RIGHT_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarYears(this._pickerDayjs, 1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // minus 3 years
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.UP_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarYears(this._pickerDayjs, -1 * YEARS_PER_ROW);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // add 3 years
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.DOWN_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarYears(this._pickerDayjs, YEARS_PER_ROW);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // go to the first year of the year page
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.HOME:
        dayjs = this.dateTimeAdapter.addCalendarYears(this._pickerDayjs, -this.dateTimeAdapter.getYear(this._pickerDayjs) % (YEARS_PER_ROW * YEAR_ROWS));
        this.pickerDayjsChange.emit(dayjs);
        break;
      // go to the last year of the year page
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.END:
        dayjs = this.dateTimeAdapter.addCalendarYears(this._pickerDayjs, YEARS_PER_ROW * YEAR_ROWS - this.dateTimeAdapter.getYear(this._pickerDayjs) % (YEARS_PER_ROW * YEAR_ROWS) - 1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // minus 1 year page (or 10 year pages)
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.PAGE_UP:
        dayjs = this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, event.altKey ? -10 * (YEARS_PER_ROW * YEAR_ROWS) : -1 * (YEARS_PER_ROW * YEAR_ROWS));
        this.pickerDayjsChange.emit(dayjs);
        break;
      // add 1 year page (or 10 year pages)
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.PAGE_DOWN:
        dayjs = this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, event.altKey ? 10 * (YEARS_PER_ROW * YEAR_ROWS) : YEARS_PER_ROW * YEAR_ROWS);
        this.pickerDayjsChange.emit(dayjs);
        break;
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.ENTER:
        this.selectYear(this.dateTimeAdapter.getYear(this._pickerDayjs));
        this.keyboardEnter.emit();
        break;
      default:
        return;
    }
    this.focusActiveCell();
    event.preventDefault();
  }
  /**
   * Creates an CalendarCell for the given year.
   */
  createYearCell(year) {
    const startDateOfYear = this.dateTimeAdapter.createDate(year, 0, 1);
    const ariaLabel = this.dateTimeAdapter.getYearName(startDateOfYear);
    const cellClass = 'owl-dt-year-' + year;
    return new CalendarCell(year, year.toString(), ariaLabel, this.isYearEnabled(year), false, cellClass);
  }
  setSelectedYears() {
    this._selectedYears = [];
    if (this.isInSingleMode && this.selected) {
      this._selectedYears[0] = this.dateTimeAdapter.getYear(this.selected);
    }
    if (this.isInRangeMode && this.selecteds) {
      this._selectedYears = this.selecteds.map(selected => {
        if (this.dateTimeAdapter.isValid(selected)) {
          return this.dateTimeAdapter.getYear(selected);
        } else {
          return null;
        }
      });
    }
  }
  /** Whether the given year is enabled. */
  isYearEnabled(year) {
    // disable if the year is greater than maxDate lower than minDate
    if (year === undefined || year === null || this.maxDate && year > this.dateTimeAdapter.getYear(this.maxDate) || this.minDate && year < this.dateTimeAdapter.getYear(this.minDate)) {
      return false;
    }
    // enable if it reaches here and there's no filter defined
    if (!this.dateFilter) {
      return true;
    }
    const firstOfYear = this.dateTimeAdapter.createDate(year, 0, 1);
    // If any date in the year is enabled count the year as enabled.
    for (let date = firstOfYear; this.dateTimeAdapter.getYear(date) == year; date = this.dateTimeAdapter.addCalendarDays(date, 1)) {
      if (this.dateFilter(date)) {
        return true;
      }
    }
    return false;
  }
  isSameYearList(date1, date2) {
    return Math.floor(this.dateTimeAdapter.getYear(date1) / (YEARS_PER_ROW * YEAR_ROWS)) === Math.floor(this.dateTimeAdapter.getYear(date2) / (YEARS_PER_ROW * YEAR_ROWS));
  }
  /**
   * Get a valid date object
   */
  getValidDate(obj) {
    return this.dateTimeAdapter.isDateInstance(obj) && this.dateTimeAdapter.isValid(obj) ? obj : null;
  }
  focusActiveCell() {
    this.calendarBodyElm.focusActiveCell();
  }
}
OwlMultiYearViewComponent.ɵfac = function OwlMultiYearViewComponent_Factory(t) {
  return new (t || OwlMultiYearViewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OwlDateTimeIntl), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8));
};
OwlMultiYearViewComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlMultiYearViewComponent,
  selectors: [["owl-date-time-multi-year-view"]],
  viewQuery: function OwlMultiYearViewComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵviewQuery"](OwlCalendarBodyComponent, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵloadQuery"]()) && (ctx.calendarBodyElm = _t.first);
    }
  },
  hostVars: 4,
  hostBindings: function OwlMultiYearViewComponent_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-calendar-view", ctx.owlDTCalendarView)("owl-dt-calendar-multi-year-view", ctx.owlDTCalendarMultiYearView);
    }
  },
  inputs: {
    selectMode: "selectMode",
    selected: "selected",
    selecteds: "selecteds",
    pickerDayjs: "pickerDayjs",
    dateFilter: "dateFilter",
    minDate: "minDate",
    maxDate: "maxDate"
  },
  outputs: {
    change: "change",
    yearSelected: "yearSelected",
    pickerDayjsChange: "pickerDayjsChange",
    keyboardEnter: "keyboardEnter"
  },
  decls: 14,
  vars: 12,
  consts: [["type", "button", "tabindex", "0", 1, "owl-dt-control-button", "owl-dt-control-arrow-button", 3, "disabled", "click"], ["tabindex", "-1", 1, "owl-dt-control-button-content"], ["xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "version", "1.1", "x", "0px", "y", "0px", "viewBox", "0 0 250.738 250.738", 0, "xml", "space", "preserve", "width", "100%", "height", "100%", 2, "enable-background", "new 0 0 250.738 250.738"], ["d", "M96.633,125.369l95.053-94.533c7.101-7.055,7.101-18.492,0-25.546   c-7.1-7.054-18.613-7.054-25.714,0L58.989,111.689c-3.784,3.759-5.487,8.759-5.238,13.68c-0.249,4.922,1.454,9.921,5.238,13.681   l106.983,106.398c7.101,7.055,18.613,7.055,25.714,0c7.101-7.054,7.101-18.491,0-25.544L96.633,125.369z", 2, "fill-rule", "evenodd", "clip-rule", "evenodd"], [1, "owl-dt-calendar-table", "owl-dt-calendar-multi-year-table"], [1, "owl-dt-calendar-header"], ["colspan", "3"], ["owl-date-time-calendar-body", "", "role", "grid", 3, "rows", "numCols", "cellRatio", "activeCell", "todayValue", "selectedValues", "selectMode", "keydown", "select"], ["version", "1.1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 250.738 250.738", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 250.738 250.738"], ["d", "M191.75,111.689L84.766,5.291c-7.1-7.055-18.613-7.055-25.713,0\n                c-7.101,7.054-7.101,18.49,0,25.544l95.053,94.534l-95.053,94.533c-7.101,7.054-7.101,18.491,0,25.545\n                c7.1,7.054,18.613,7.054,25.713,0L191.75,139.05c3.784-3.759,5.487-8.759,5.238-13.681\n                C197.237,120.447,195.534,115.448,191.75,111.689z", 2, "fill-rule", "evenodd", "clip-rule", "evenodd"]],
  template: function OwlMultiYearViewComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "button", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlMultiYearViewComponent_Template_button_click_0_listener($event) {
        return ctx.prevYearList($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "span", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceSVG"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](2, "svg", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](3, "path", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceHTML"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "table", 4)(5, "thead", 5)(6, "tr")(7, "th", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](8);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](9, "tbody", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("keydown", function OwlMultiYearViewComponent_Template_tbody_keydown_9_listener($event) {
        return ctx.handleCalendarKeydown($event);
      })("select", function OwlMultiYearViewComponent_Template_tbody_select_9_listener($event) {
        return ctx.selectCalendarCell($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](10, "button", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlMultiYearViewComponent_Template_button_click_10_listener($event) {
        return ctx.nextYearList($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](11, "span", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceSVG"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](12, "svg", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](13, "path", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("disabled", !ctx.previousEnabled());
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-label", ctx.prevButtonLabel);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](8);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"](ctx.tableHeader);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("rows", ctx.years)("numCols", 3)("cellRatio", 3 / 7)("activeCell", ctx.activeCell)("todayValue", ctx.todayYear)("selectedValues", ctx.selectedYears)("selectMode", ctx.selectMode);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("disabled", !ctx.nextEnabled());
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-label", ctx.nextButtonLabel);
    }
  },
  dependencies: [OwlCalendarBodyComponent],
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlMultiYearViewComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      selector: 'owl-date-time-multi-year-view',
      host: {
        '[class.owl-dt-calendar-view]': 'owlDTCalendarView',
        '[class.owl-dt-calendar-multi-year-view]': 'owlDTCalendarMultiYearView'
      },
      preserveWhitespaces: false,
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      template: "<button class=\"owl-dt-control-button owl-dt-control-arrow-button\"\n        [disabled]=\"!previousEnabled()\" [attr.aria-label]=\"prevButtonLabel\"\n        type=\"button\" tabindex=\"0\" (click)=\"prevYearList($event)\">\n    <span class=\"owl-dt-control-button-content\" tabindex=\"-1\">\n        <!-- <editor-fold desc=\"SVG Arrow Left\"> -->\n    <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n             version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 250.738 250.738\"\n             style=\"enable-background:new 0 0 250.738 250.738;\" xml:space=\"preserve\"\n             width=\"100%\" height=\"100%\">\n            <path style=\"fill-rule: evenodd; clip-rule: evenodd;\" d=\"M96.633,125.369l95.053-94.533c7.101-7.055,7.101-18.492,0-25.546   c-7.1-7.054-18.613-7.054-25.714,0L58.989,111.689c-3.784,3.759-5.487,8.759-5.238,13.68c-0.249,4.922,1.454,9.921,5.238,13.681   l106.983,106.398c7.101,7.055,18.613,7.055,25.714,0c7.101-7.054,7.101-18.491,0-25.544L96.633,125.369z\"/>\n        </svg>\n        <!-- </editor-fold> -->\n    </span>\n</button>\n<table class=\"owl-dt-calendar-table owl-dt-calendar-multi-year-table\">\n    <thead class=\"owl-dt-calendar-header\">\n    <tr>\n        <th colspan=\"3\">{{tableHeader}}</th>\n    </tr>\n    </thead>\n    <tbody owl-date-time-calendar-body role=\"grid\"\n           [rows]=\"years\" [numCols]=\"3\" [cellRatio]=\"3 / 7\"\n           [activeCell]=\"activeCell\"\n           [todayValue]=\"todayYear\"\n           [selectedValues]=\"selectedYears\"\n           [selectMode]=\"selectMode\"\n           (keydown)=\"handleCalendarKeydown($event)\"\n           (select)=\"selectCalendarCell($event)\"></tbody>\n</table>\n<button class=\"owl-dt-control-button owl-dt-control-arrow-button\"\n        [disabled]=\"!nextEnabled()\" [attr.aria-label]=\"nextButtonLabel\"\n        type=\"button\" tabindex=\"0\" (click)=\"nextYearList($event)\">\n    <span class=\"owl-dt-control-button-content\" tabindex=\"-1\">\n        <!-- <editor-fold desc=\"SVG Arrow Right\"> -->\n    <svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n             viewBox=\"0 0 250.738 250.738\" style=\"enable-background:new 0 0 250.738 250.738;\" xml:space=\"preserve\">\n            <path style=\"fill-rule:evenodd;clip-rule:evenodd;\" d=\"M191.75,111.689L84.766,5.291c-7.1-7.055-18.613-7.055-25.713,0\n                c-7.101,7.054-7.101,18.49,0,25.544l95.053,94.534l-95.053,94.533c-7.101,7.054-7.101,18.491,0,25.545\n                c7.1,7.054,18.613,7.054,25.713,0L191.75,139.05c3.784-3.759,5.487-8.759,5.238-13.681\n                C197.237,120.447,195.534,115.448,191.75,111.689z\"/>\n        </svg>\n        <!-- </editor-fold> -->\n    </span>\n</button>\n"
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }, {
      type: OwlDateTimeIntl
    }, {
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }];
  }, {
    selectMode: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selecteds: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    pickerDayjs: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    dateFilter: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    minDate: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    maxDate: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    change: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    yearSelected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    pickerDayjsChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    keyboardEnter: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    calendarBodyElm: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild,
      args: [OwlCalendarBodyComponent, {
        static: true
      }]
    }]
  });
})();

/**
 * calendar-year-view.component
 */
const MONTHS_PER_YEAR = 12;
const MONTHS_PER_ROW = 3;
class OwlYearViewComponent {
  constructor(cdRef, dateTimeAdapter, dateTimeFormats) {
    this.cdRef = cdRef;
    this.dateTimeAdapter = dateTimeAdapter;
    this.dateTimeFormats = dateTimeFormats;
    /**
     * The select mode of the picker;
     * */
    this._selectMode = 'single';
    this._selecteds = [];
    this.localeSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    this.initiated = false;
    /**
     * An array to hold all selectedDates' month value
     * the value is the month number in current year
     * */
    this.selectedMonths = [];
    /**
     * Callback to invoke when a new month is selected
     * */
    this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Emits the selected year. This doesn't imply a change on the selected date
     * */
    this.monthSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /** Emits when any date is activated. */
    this.pickerDayjsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /** Emits when use keyboard enter to select a calendar cell */
    this.keyboardEnter = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this.monthNames = this.dateTimeAdapter.getMonthNames('short');
  }
  get selectMode() {
    return this._selectMode;
  }
  set selectMode(val) {
    this._selectMode = val;
    if (this.initiated) {
      this.generateMonthList();
      this.cdRef.markForCheck();
    }
  }
  get selected() {
    return this._selected;
  }
  set selected(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._selected = this.getValidDate(value);
    this.setSelectedMonths();
  }
  get selecteds() {
    return this._selecteds;
  }
  set selecteds(values) {
    this._selecteds = [];
    for (let i = 0; i < values.length; i++) {
      const value = this.dateTimeAdapter.deserialize(values[i]);
      this._selecteds.push(this.getValidDate(value));
    }
    this.setSelectedMonths();
  }
  get pickerDayjs() {
    return this._pickerDayjs;
  }
  set pickerDayjs(value) {
    const oldDayjs = this._pickerDayjs;
    value = this.dateTimeAdapter.deserialize(value);
    this._pickerDayjs = this.getValidDate(value) || this.dateTimeAdapter.now();
    if (!this.hasSameYear(oldDayjs, this._pickerDayjs) && this.initiated) {
      this.generateMonthList();
    }
  }
  get dateFilter() {
    return this._dateFilter;
  }
  set dateFilter(filter) {
    this._dateFilter = filter;
    if (this.initiated) {
      this.generateMonthList();
    }
  }
  get minDate() {
    return this._minDate;
  }
  set minDate(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._minDate = this.getValidDate(value);
    if (this.initiated) {
      this.generateMonthList();
    }
  }
  get maxDate() {
    return this._maxDate;
  }
  set maxDate(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._maxDate = this.getValidDate(value);
    if (this.initiated) {
      this.generateMonthList();
    }
  }
  get months() {
    return this._months;
  }
  get activeCell() {
    if (this._pickerDayjs) {
      return this.dateTimeAdapter.getMonth(this._pickerDayjs);
    }
  }
  get isInSingleMode() {
    return this.selectMode === 'single';
  }
  get isInRangeMode() {
    return this.selectMode === 'range' || this.selectMode === 'rangeFrom' || this.selectMode === 'rangeTo';
  }
  get owlDTCalendarView() {
    return true;
  }
  ngOnInit() {
    this.localeSub = this.dateTimeAdapter.localeChanges.subscribe(() => {
      this.generateMonthList();
      this.cdRef.markForCheck();
    });
  }
  ngAfterContentInit() {
    this.generateMonthList();
    this.initiated = true;
  }
  ngOnDestroy() {
    this.localeSub.unsubscribe();
  }
  /**
   * Handle a calendarCell selected
   */
  selectCalendarCell(cell) {
    this.selectMonth(cell.value);
  }
  /**
   * Handle a new month selected
   */
  selectMonth(month) {
    const firstDateOfMonth = this.dateTimeAdapter.createDate(this.dateTimeAdapter.getYear(this.pickerDayjs), month, 1);
    this.monthSelected.emit(firstDateOfMonth);
    const daysInMonth = this.dateTimeAdapter.getNumDaysInMonth(firstDateOfMonth);
    const result = this.dateTimeAdapter.createDate(this.dateTimeAdapter.getYear(this.pickerDayjs), month, Math.min(daysInMonth, this.dateTimeAdapter.getDate(this.pickerDayjs)), this.dateTimeAdapter.getHours(this.pickerDayjs), this.dateTimeAdapter.getMinutes(this.pickerDayjs), this.dateTimeAdapter.getSeconds(this.pickerDayjs));
    this.change.emit(result);
  }
  /**
   * Handle keydown event on calendar body
   */
  handleCalendarKeydown(event) {
    let dayjs;
    switch (event.keyCode) {
      // minus 1 month
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.LEFT_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, -1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // add 1 month
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.RIGHT_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, 1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // minus 3 months
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.UP_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, -3);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // add 3 months
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.DOWN_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, 3);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // move to first month of current year
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.HOME:
        dayjs = this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, -this.dateTimeAdapter.getMonth(this.pickerDayjs));
        this.pickerDayjsChange.emit(dayjs);
        break;
      // move to last month of current year
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.END:
        dayjs = this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, 11 - this.dateTimeAdapter.getMonth(this.pickerDayjs));
        this.pickerDayjsChange.emit(dayjs);
        break;
      // minus 1 year (or 10 year)
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.PAGE_UP:
        dayjs = this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, event.altKey ? -10 : -1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // add 1 year (or 10 year)
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.PAGE_DOWN:
        dayjs = this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, event.altKey ? 10 : 1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // Select current month
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.ENTER:
        this.selectMonth(this.dateTimeAdapter.getMonth(this.pickerDayjs));
        this.keyboardEnter.emit();
        break;
      default:
        return;
    }
    this.focusActiveCell();
    event.preventDefault();
  }
  /**
   * Generate the calendar month list
   * */
  generateMonthList() {
    if (!this.pickerDayjs) {
      return;
    }
    this.setSelectedMonths();
    this.todayMonth = this.getMonthInCurrentYear(this.dateTimeAdapter.now());
    this._months = [];
    for (let i = 0; i < MONTHS_PER_YEAR / MONTHS_PER_ROW; i++) {
      const row = [];
      for (let j = 0; j < MONTHS_PER_ROW; j++) {
        const month = j + i * MONTHS_PER_ROW;
        const monthCell = this.createMonthCell(month);
        row.push(monthCell);
      }
      this._months.push(row);
    }
    return;
  }
  /**
   * Creates an CalendarCell for the given month.
   */
  createMonthCell(month) {
    const startDateOfMonth = this.dateTimeAdapter.createDate(this.dateTimeAdapter.getYear(this.pickerDayjs), month, 1);
    const ariaLabel = this.dateTimeAdapter.format(startDateOfMonth, this.dateTimeFormats.monthYearA11yLabel);
    const cellClass = 'owl-dt-month-' + month;
    return new CalendarCell(month, this.monthNames[month], ariaLabel, this.isMonthEnabled(month), false, cellClass);
  }
  /**
   * Check if the given month is enable
   */
  isMonthEnabled(month) {
    const firstDateOfMonth = this.dateTimeAdapter.createDate(this.dateTimeAdapter.getYear(this.pickerDayjs), month, 1);
    // If any date in the month is selectable,
    // we count the month as enable
    for (let date = firstDateOfMonth; this.dateTimeAdapter.getMonth(date) === month; date = this.dateTimeAdapter.addCalendarDays(date, 1)) {
      if (!!date && (!this.dateFilter || this.dateFilter(date)) && (!this.minDate || this.dateTimeAdapter.compare(date, this.minDate) >= 0) && (!this.maxDate || this.dateTimeAdapter.compare(date, this.maxDate) <= 0)) {
        return true;
      }
    }
    return false;
  }
  /**
   * Gets the month in this year that the given Date falls on.
   * Returns null if the given Date is in another year.
   */
  getMonthInCurrentYear(date) {
    if (this.getValidDate(date) && this.getValidDate(this._pickerDayjs)) {
      const result = this.dateTimeAdapter.compareYear(date, this._pickerDayjs);
      // < 0 : the given date's year is before pickerDayjs's year, we return -1 as selected month value.
      // > 0 : the given date's year is after pickerDayjs's year, we return 12 as selected month value.
      // 0 : the give date's year is same as the pickerDayjs's year, we return the actual month value.
      if (result < 0) {
        return -1;
      } else if (result > 0) {
        return 12;
      } else {
        return this.dateTimeAdapter.getMonth(date);
      }
    } else {
      return null;
    }
  }
  /**
   * Set the selectedMonths value
   * In single mode, it has only one value which represent the month the selected date in
   * In range mode, it would has two values, one for the month the fromValue in and the other for the month the toValue in
   * */
  setSelectedMonths() {
    this.selectedMonths = [];
    if (this.isInSingleMode && this.selected) {
      this.selectedMonths[0] = this.getMonthInCurrentYear(this.selected);
    }
    if (this.isInRangeMode && this.selecteds) {
      this.selectedMonths[0] = this.getMonthInCurrentYear(this.selecteds[0]);
      this.selectedMonths[1] = this.getMonthInCurrentYear(this.selecteds[1]);
    }
  }
  /**
   * Check the given dates are in the same year
   */
  hasSameYear(dateLeft, dateRight) {
    return !!(dateLeft && dateRight && this.dateTimeAdapter.getYear(dateLeft) === this.dateTimeAdapter.getYear(dateRight));
  }
  /**
   * Get a valid date object
   */
  getValidDate(obj) {
    return this.dateTimeAdapter.isDateInstance(obj) && this.dateTimeAdapter.isValid(obj) ? obj : null;
  }
  focusActiveCell() {
    this.calendarBodyElm.focusActiveCell();
  }
}
OwlYearViewComponent.ɵfac = function OwlYearViewComponent_Factory(t) {
  return new (t || OwlYearViewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OWL_DATE_TIME_FORMATS, 8));
};
OwlYearViewComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlYearViewComponent,
  selectors: [["owl-date-time-year-view"]],
  viewQuery: function OwlYearViewComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵviewQuery"](OwlCalendarBodyComponent, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵloadQuery"]()) && (ctx.calendarBodyElm = _t.first);
    }
  },
  hostVars: 2,
  hostBindings: function OwlYearViewComponent_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-calendar-view", ctx.owlDTCalendarView);
    }
  },
  inputs: {
    selectMode: "selectMode",
    selected: "selected",
    selecteds: "selecteds",
    pickerDayjs: "pickerDayjs",
    dateFilter: "dateFilter",
    minDate: "minDate",
    maxDate: "maxDate"
  },
  outputs: {
    change: "change",
    monthSelected: "monthSelected",
    pickerDayjsChange: "pickerDayjsChange",
    keyboardEnter: "keyboardEnter"
  },
  exportAs: ["owlMonthView"],
  decls: 5,
  vars: 7,
  consts: [[1, "owl-dt-calendar-table", "owl-dt-calendar-year-table"], [1, "owl-dt-calendar-header"], ["aria-hidden", "true", "colspan", "3", 1, "owl-dt-calendar-table-divider"], ["owl-date-time-calendar-body", "", "role", "grid", 3, "rows", "numCols", "cellRatio", "activeCell", "todayValue", "selectedValues", "selectMode", "keydown", "select"]],
  template: function OwlYearViewComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "table", 0)(1, "thead", 1)(2, "tr");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](3, "th", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "tbody", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("keydown", function OwlYearViewComponent_Template_tbody_keydown_4_listener($event) {
        return ctx.handleCalendarKeydown($event);
      })("select", function OwlYearViewComponent_Template_tbody_select_4_listener($event) {
        return ctx.selectCalendarCell($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("rows", ctx.months)("numCols", 3)("cellRatio", 3 / 7)("activeCell", ctx.activeCell)("todayValue", ctx.todayMonth)("selectedValues", ctx.selectedMonths)("selectMode", ctx.selectMode);
    }
  },
  dependencies: [OwlCalendarBodyComponent],
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlYearViewComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      selector: 'owl-date-time-year-view',
      exportAs: 'owlMonthView',
      host: {
        '[class.owl-dt-calendar-view]': 'owlDTCalendarView'
      },
      preserveWhitespaces: false,
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      template: "<table class=\"owl-dt-calendar-table owl-dt-calendar-year-table\">\n    <thead class=\"owl-dt-calendar-header\">\n    <tr>\n        <th class=\"owl-dt-calendar-table-divider\" aria-hidden=\"true\" colspan=\"3\"></th>\n    </tr>\n    </thead>\n    <tbody owl-date-time-calendar-body role=\"grid\"\n           [rows]=\"months\" [numCols]=\"3\" [cellRatio]=\"3 / 7\"\n           [activeCell]=\"activeCell\"\n           [todayValue]=\"todayMonth\"\n           [selectedValues]=\"selectedMonths\"\n           [selectMode]=\"selectMode\"\n           (keydown)=\"handleCalendarKeydown($event)\"\n           (select)=\"selectCalendarCell($event)\">\n    </tbody>\n</table>\n"
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }, {
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DATE_TIME_FORMATS]
      }]
    }];
  }, {
    selectMode: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selecteds: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    pickerDayjs: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    dateFilter: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    minDate: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    maxDate: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    change: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    monthSelected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    pickerDayjsChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    keyboardEnter: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    calendarBodyElm: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild,
      args: [OwlCalendarBodyComponent, {
        static: true
      }]
    }]
  });
})();

/**
 * calendar-month-view.component
 */
const DAYS_PER_WEEK = 7;
const WEEKS_PER_VIEW = 6;
class OwlMonthViewComponent {
  constructor(cdRef, dateTimeAdapter, dateTimeFormats) {
    this.cdRef = cdRef;
    this.dateTimeAdapter = dateTimeAdapter;
    this.dateTimeFormats = dateTimeFormats;
    /**
     * Whether to hide dates in other months at the start or end of the current month.
     * */
    this.hideOtherMonths = false;
    /**
     * Define the first day of a week
     * Sunday: 0 ~ Saturday: 6
     * */
    this._firstDayOfWeek = 0;
    /**
     * The select mode of the picker;
     * */
    this._selectMode = 'single';
    this._selecteds = [];
    this.localeSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    this.initiated = false;
    /**
     * An array to hold all selectedDates' value
     * the value is the day number in current month
     * */
    this.selectedDates = [];
    /**
     * Callback to invoke when a new date is selected
     * */
    this.selectedChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Callback to invoke when any date is selected.
     * */
    this.userSelection = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /** Emits when any date is activated. */
    this.pickerDayjsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
  }
  get firstDayOfWeek() {
    return this._firstDayOfWeek;
  }
  set firstDayOfWeek(val) {
    val = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceNumberProperty)(val);
    if (val >= 0 && val <= 6 && val !== this._firstDayOfWeek) {
      this._firstDayOfWeek = val;
      if (this.initiated) {
        this.generateWeekDays();
        this.generateCalendar();
        this.cdRef.markForCheck();
      }
    }
  }
  get selectMode() {
    return this._selectMode;
  }
  set selectMode(val) {
    this._selectMode = val;
    if (this.initiated) {
      this.generateCalendar();
      this.cdRef.markForCheck();
    }
  }
  get selected() {
    return this._selected;
  }
  set selected(value) {
    const oldSelected = this._selected;
    value = this.dateTimeAdapter.deserialize(value);
    this._selected = this.getValidDate(value);
    if (!this.dateTimeAdapter.isSameDay(oldSelected, this._selected)) {
      this.setSelectedDates();
    }
  }
  get selecteds() {
    return this._selecteds;
  }
  set selecteds(values) {
    this._selecteds = values.map(v => {
      v = this.dateTimeAdapter.deserialize(v);
      return this.getValidDate(v);
    });
    this.setSelectedDates();
  }
  get pickerDayjs() {
    return this._pickerDayjs;
  }
  set pickerDayjs(value) {
    const oldDayjs = this._pickerDayjs;
    value = this.dateTimeAdapter.deserialize(value);
    this._pickerDayjs = this.getValidDate(value) || this.dateTimeAdapter.now();
    this.firstDateOfMonth = this.dateTimeAdapter.createDate(this.dateTimeAdapter.getYear(this._pickerDayjs), this.dateTimeAdapter.getMonth(this._pickerDayjs), 1);
    if (!this.isSameMonth(oldDayjs, this._pickerDayjs) && this.initiated) {
      this.generateCalendar();
    }
  }
  get dateFilter() {
    return this._dateFilter;
  }
  set dateFilter(filter) {
    this._dateFilter = filter;
    if (this.initiated) {
      this.generateCalendar();
      this.cdRef.markForCheck();
    }
  }
  get minDate() {
    return this._minDate;
  }
  set minDate(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._minDate = this.getValidDate(value);
    if (this.initiated) {
      this.generateCalendar();
      this.cdRef.markForCheck();
    }
  }
  get maxDate() {
    return this._maxDate;
  }
  set maxDate(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._maxDate = this.getValidDate(value);
    if (this.initiated) {
      this.generateCalendar();
      this.cdRef.markForCheck();
    }
  }
  get weekdays() {
    return this._weekdays;
  }
  get days() {
    return this._days;
  }
  get activeCell() {
    if (this.pickerDayjs) {
      return this.dateTimeAdapter.getDate(this.pickerDayjs) + this.firstRowOffset - 1;
    }
  }
  get isInSingleMode() {
    return this.selectMode === 'single';
  }
  get isInRangeMode() {
    return this.selectMode === 'range' || this.selectMode === 'rangeFrom' || this.selectMode === 'rangeTo';
  }
  get owlDTCalendarView() {
    return true;
  }
  ngOnInit() {
    this.generateWeekDays();
    this.localeSub = this.dateTimeAdapter.localeChanges.subscribe(() => {
      this.generateWeekDays();
      this.generateCalendar();
      this.cdRef.markForCheck();
    });
  }
  ngAfterContentInit() {
    this.generateCalendar();
    this.initiated = true;
  }
  ngOnDestroy() {
    this.localeSub.unsubscribe();
  }
  /**
   * Handle a calendarCell selected
   */
  selectCalendarCell(cell) {
    // Cases in which the date would not be selected
    // 1, the calendar cell is NOT enabled (is NOT valid)
    // 2, the selected date is NOT in current picker's month and the hideOtherMonths is enabled
    if (!cell.enabled || this.hideOtherMonths && cell.out) {
      return;
    }
    this.selectDate(cell.value);
  }
  /**
   * Handle a new date selected
   */
  selectDate(date) {
    const daysDiff = date - 1;
    const selected = this.dateTimeAdapter.addCalendarDays(this.firstDateOfMonth, daysDiff);
    this.selectedChange.emit(selected);
    this.userSelection.emit();
  }
  /**
   * Handle keydown event on calendar body
   */
  handleCalendarKeydown(event) {
    let dayjs;
    switch (event.keyCode) {
      // minus 1 day
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.LEFT_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarDays(this.pickerDayjs, -1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // add 1 day
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.RIGHT_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarDays(this.pickerDayjs, 1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // minus 1 week
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.UP_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarDays(this.pickerDayjs, -7);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // add 1 week
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.DOWN_ARROW:
        dayjs = this.dateTimeAdapter.addCalendarDays(this.pickerDayjs, 7);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // move to first day of current month
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.HOME:
        dayjs = this.dateTimeAdapter.addCalendarDays(this.pickerDayjs, 1 - this.dateTimeAdapter.getDate(this.pickerDayjs));
        this.pickerDayjsChange.emit(dayjs);
        break;
      // move to last day of current month
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.END:
        dayjs = this.dateTimeAdapter.addCalendarDays(this.pickerDayjs, this.dateTimeAdapter.getNumDaysInMonth(this.pickerDayjs) - this.dateTimeAdapter.getDate(this.pickerDayjs));
        this.pickerDayjsChange.emit(dayjs);
        break;
      // minus 1 month (or 1 year)
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.PAGE_UP:
        dayjs = event.altKey ? this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, -1) : this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, -1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // add 1 month (or 1 year)
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.PAGE_DOWN:
        dayjs = event.altKey ? this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, 1) : this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, 1);
        this.pickerDayjsChange.emit(dayjs);
        break;
      // select the pickerDayjs
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.ENTER:
        if (!this.dateFilter || this.dateFilter(this.pickerDayjs)) {
          this.selectDate(this.dateTimeAdapter.getDate(this.pickerDayjs));
        }
        break;
      default:
        return;
    }
    this.focusActiveCell();
    event.preventDefault();
  }
  /**
   * Generate the calendar weekdays array
   * */
  generateWeekDays() {
    const longWeekdays = this.dateTimeAdapter.getDayOfWeekNames('long');
    const shortWeekdays = this.dateTimeAdapter.getDayOfWeekNames('short');
    const narrowWeekdays = this.dateTimeAdapter.getDayOfWeekNames('narrow');
    const firstDayOfWeek = this.firstDayOfWeek;
    const weekdays = longWeekdays.map((long, i) => {
      return {
        long,
        short: shortWeekdays[i],
        narrow: narrowWeekdays[i]
      };
    });
    this._weekdays = weekdays.slice(firstDayOfWeek).concat(weekdays.slice(0, firstDayOfWeek));
    this.dateNames = this.dateTimeAdapter.getDateNames();
    return;
  }
  /**
   * Generate the calendar days array
   * */
  generateCalendar() {
    if (!this.pickerDayjs) {
      return;
    }
    this.todayDate = null;
    // the first weekday of the month
    const startWeekdayOfMonth = this.dateTimeAdapter.getDay(this.firstDateOfMonth);
    const firstDayOfWeek = this.firstDayOfWeek;
    // the amount of days from the first date of the month
    // if it is < 0, it means the date is in previous month
    let daysDiff = 0 - (startWeekdayOfMonth + (DAYS_PER_WEEK - firstDayOfWeek)) % DAYS_PER_WEEK;
    // the index of cell that contains the first date of the month
    this.firstRowOffset = Math.abs(daysDiff);
    this._days = [];
    for (let i = 0; i < WEEKS_PER_VIEW; i++) {
      const week = [];
      for (let j = 0; j < DAYS_PER_WEEK; j++) {
        const date = this.dateTimeAdapter.addCalendarDays(this.firstDateOfMonth, daysDiff);
        const dateCell = this.createDateCell(date, daysDiff);
        // check if the date is today
        if (this.dateTimeAdapter.isSameDay(this.dateTimeAdapter.now(), date)) {
          this.todayDate = daysDiff + 1;
        }
        week.push(dateCell);
        daysDiff += 1;
      }
      this._days.push(week);
    }
    this.setSelectedDates();
  }
  /**
   * Creates CalendarCell for days.
   */
  createDateCell(date, daysDiff) {
    // total days of the month
    const daysInMonth = this.dateTimeAdapter.getNumDaysInMonth(this.pickerDayjs);
    const dateNum = this.dateTimeAdapter.getDate(date);
    // const dateName = this.dateNames[dateNum - 1];
    const dateName = dateNum.toString();
    const ariaLabel = this.dateTimeAdapter.format(date, this.dateTimeFormats.dateA11yLabel);
    // check if the date if selectable
    const enabled = this.isDateEnabled(date);
    // check if date is not in current month
    const dayValue = daysDiff + 1;
    const out = dayValue < 1 || dayValue > daysInMonth;
    const cellClass = 'owl-dt-day-' + this.dateTimeAdapter.getDay(date);
    return new CalendarCell(dayValue, dateName, ariaLabel, enabled, out, cellClass);
  }
  /**
   * Check if the date is valid
   */
  isDateEnabled(date) {
    return !!date && (!this.dateFilter || this.dateFilter(date)) && (!this.minDate || this.dateTimeAdapter.compare(date, this.minDate) >= 0) && (!this.maxDate || this.dateTimeAdapter.compare(date, this.maxDate) <= 0);
  }
  /**
   * Get a valid date object
   */
  getValidDate(obj) {
    return this.dateTimeAdapter.isDateInstance(obj) && this.dateTimeAdapter.isValid(obj) ? obj : null;
  }
  /**
   * Check if the give dates are none-null and in the same month
   */
  isSameMonth(dateLeft, dateRight) {
    return !!(dateLeft && dateRight && this.dateTimeAdapter.isValid(dateLeft) && this.dateTimeAdapter.isValid(dateRight) && this.dateTimeAdapter.getYear(dateLeft) === this.dateTimeAdapter.getYear(dateRight) && this.dateTimeAdapter.getMonth(dateLeft) === this.dateTimeAdapter.getMonth(dateRight));
  }
  /**
   * Set the selectedDates value.
   * In single mode, it has only one value which represent the selected date
   * In range mode, it would has two values, one for the fromValue and the other for the toValue
   * */
  setSelectedDates() {
    this.selectedDates = [];
    if (!this.firstDateOfMonth) {
      return;
    }
    if (this.isInSingleMode && this.selected) {
      const dayDiff = this.dateTimeAdapter.differenceInCalendarDays(this.selected, this.firstDateOfMonth);
      this.selectedDates[0] = dayDiff + 1;
      return;
    }
    if (this.isInRangeMode && this.selecteds) {
      this.selectedDates = this.selecteds.map(selected => {
        if (this.dateTimeAdapter.isValid(selected)) {
          const dayDiff = this.dateTimeAdapter.differenceInCalendarDays(selected, this.firstDateOfMonth);
          return dayDiff + 1;
        } else {
          return null;
        }
      });
    }
  }
  focusActiveCell() {
    this.calendarBodyElm.focusActiveCell();
  }
}
OwlMonthViewComponent.ɵfac = function OwlMonthViewComponent_Factory(t) {
  return new (t || OwlMonthViewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OWL_DATE_TIME_FORMATS, 8));
};
OwlMonthViewComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlMonthViewComponent,
  selectors: [["owl-date-time-month-view"]],
  viewQuery: function OwlMonthViewComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵviewQuery"](OwlCalendarBodyComponent, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵloadQuery"]()) && (ctx.calendarBodyElm = _t.first);
    }
  },
  hostVars: 2,
  hostBindings: function OwlMonthViewComponent_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-calendar-view", ctx.owlDTCalendarView);
    }
  },
  inputs: {
    hideOtherMonths: "hideOtherMonths",
    firstDayOfWeek: "firstDayOfWeek",
    selectMode: "selectMode",
    selected: "selected",
    selecteds: "selecteds",
    pickerDayjs: "pickerDayjs",
    dateFilter: "dateFilter",
    minDate: "minDate",
    maxDate: "maxDate"
  },
  outputs: {
    selectedChange: "selectedChange",
    userSelection: "userSelection",
    pickerDayjsChange: "pickerDayjsChange"
  },
  exportAs: ["owlYearView"],
  decls: 7,
  vars: 8,
  consts: [[1, "owl-dt-calendar-table", "owl-dt-calendar-month-table"], [1, "owl-dt-calendar-header"], [1, "owl-dt-weekdays"], ["class", "owl-dt-weekday", "scope", "col", 4, "ngFor", "ngForOf"], ["aria-hidden", "true", "colspan", "7", 1, "owl-dt-calendar-table-divider"], ["owl-date-time-calendar-body", "", "role", "grid", 3, "rows", "todayValue", "selectedValues", "selectMode", "activeCell", "keydown", "select"], ["scope", "col", 1, "owl-dt-weekday"]],
  template: function OwlMonthViewComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "table", 0)(1, "thead", 1)(2, "tr", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](3, OwlMonthViewComponent_th_3_Template, 3, 2, "th", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "tr");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](5, "th", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](6, "tbody", 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("keydown", function OwlMonthViewComponent_Template_tbody_keydown_6_listener($event) {
        return ctx.handleCalendarKeydown($event);
      })("select", function OwlMonthViewComponent_Template_tbody_select_6_listener($event) {
        return ctx.selectCalendarCell($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-calendar-only-current-month", ctx.hideOtherMonths);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngForOf", ctx.weekdays);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("rows", ctx.days)("todayValue", ctx.todayDate)("selectedValues", ctx.selectedDates)("selectMode", ctx.selectMode)("activeCell", ctx.activeCell);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.NgForOf, OwlCalendarBodyComponent],
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlMonthViewComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      selector: 'owl-date-time-month-view',
      exportAs: 'owlYearView',
      host: {
        '[class.owl-dt-calendar-view]': 'owlDTCalendarView'
      },
      preserveWhitespaces: false,
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      template: "<table class=\"owl-dt-calendar-table owl-dt-calendar-month-table\"\n       [class.owl-dt-calendar-only-current-month]=\"hideOtherMonths\">\n    <thead class=\"owl-dt-calendar-header\">\n    <tr class=\"owl-dt-weekdays\">\n        <th *ngFor=\"let weekday of weekdays\"\n            [attr.aria-label]=\"weekday.long\"\n            class=\"owl-dt-weekday\" scope=\"col\">\n            <span>{{weekday.short}}</span>\n        </th>\n    </tr>\n    <tr>\n        <th class=\"owl-dt-calendar-table-divider\" aria-hidden=\"true\" colspan=\"7\"></th>\n    </tr>\n    </thead>\n    <tbody owl-date-time-calendar-body role=\"grid\"\n           [rows]=\"days\" [todayValue]=\"todayDate\"\n           [selectedValues]=\"selectedDates\"\n           [selectMode]=\"selectMode\"\n           [activeCell]=\"activeCell\"\n           (keydown)=\"handleCalendarKeydown($event)\"\n           (select)=\"selectCalendarCell($event)\">\n    </tbody>\n</table>\n"
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }, {
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DATE_TIME_FORMATS]
      }]
    }];
  }, {
    hideOtherMonths: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    firstDayOfWeek: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selectMode: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selecteds: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    pickerDayjs: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    dateFilter: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    minDate: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    maxDate: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selectedChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    userSelection: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    pickerDayjsChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    calendarBodyElm: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild,
      args: [OwlCalendarBodyComponent, {
        static: true
      }]
    }]
  });
})();

/**
 * calendar.component
 */
class OwlCalendarComponent {
  constructor(elmRef, pickerIntl, ngZone, cdRef, dateTimeAdapter, dateTimeFormats) {
    this.elmRef = elmRef;
    this.pickerIntl = pickerIntl;
    this.ngZone = ngZone;
    this.cdRef = cdRef;
    this.dateTimeAdapter = dateTimeAdapter;
    this.dateTimeFormats = dateTimeFormats;
    /**
     * Set the first day of week
     */
    this.firstDayOfWeek = 0;
    this._selecteds = [];
    /**
     * The view that the calendar should start in.
     */
    this.startView = 'month';
    /** Emits when the currently picker dayjs changes. */
    this.pickerDayjsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /** Emits when the currently selected date changes. */
    this.selectedChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /** Emits when any date is selected. */
    this.userSelection = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Emits the selected year. This doesn't imply a change on the selected date
     * */
    this.yearSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Emits the selected month. This doesn't imply a change on the selected date
     * */
    this.monthSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Date filter for the month and year view
     */
    this.dateFilterForViews = date => {
      return !!date && (!this.dateFilter || this.dateFilter(date)) && (!this.minDate || this.dateTimeAdapter.compare(date, this.minDate) >= 0) && (!this.maxDate || this.dateTimeAdapter.compare(date, this.maxDate) <= 0);
    };
    this.intlChangesSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    /**
     * Used for scheduling that focus should be moved to the active cell on the next tick.
     * We need to schedule it, rather than do it immediately, because we have to wait
     * for Angular to re-evaluate the view children.
     */
    this.moveFocusOnNextTick = false;
    this.intlChangesSub = this.pickerIntl.changes.subscribe(() => {
      this.cdRef.markForCheck();
    });
  }
  get minDate() {
    return this._minDate;
  }
  set minDate(value) {
    value = this.dateTimeAdapter.deserialize(value);
    value = this.getValidDate(value);
    this._minDate = value ? this.dateTimeAdapter.createDate(this.dateTimeAdapter.getYear(value), this.dateTimeAdapter.getMonth(value), this.dateTimeAdapter.getDate(value)) : null;
  }
  get maxDate() {
    return this._maxDate;
  }
  set maxDate(value) {
    value = this.dateTimeAdapter.deserialize(value);
    value = this.getValidDate(value);
    this._maxDate = value ? this.dateTimeAdapter.createDate(this.dateTimeAdapter.getYear(value), this.dateTimeAdapter.getMonth(value), this.dateTimeAdapter.getDate(value)) : null;
  }
  get pickerDayjs() {
    return this._pickerDayjs;
  }
  set pickerDayjs(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._pickerDayjs = this.getValidDate(value) || this.dateTimeAdapter.now();
  }
  get selected() {
    return this._selected;
  }
  set selected(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._selected = this.getValidDate(value);
  }
  get selecteds() {
    return this._selecteds;
  }
  set selecteds(values) {
    this._selecteds = values.map(v => {
      v = this.dateTimeAdapter.deserialize(v);
      return this.getValidDate(v);
    });
  }
  get periodButtonText() {
    return this.isMonthView ? this.dateTimeAdapter.format(this.pickerDayjs, this.dateTimeFormats.monthYearLabel) : this.dateTimeAdapter.getYearName(this.pickerDayjs);
  }
  get periodButtonLabel() {
    return this.isMonthView ? this.pickerIntl.switchToMultiYearViewLabel : this.pickerIntl.switchToMonthViewLabel;
  }
  get prevButtonLabel() {
    if (this._currentView === 'month') {
      return this.pickerIntl.prevMonthLabel;
    } else if (this._currentView === 'year') {
      return this.pickerIntl.prevYearLabel;
    } else {
      return null;
    }
  }
  get nextButtonLabel() {
    if (this._currentView === 'month') {
      return this.pickerIntl.nextMonthLabel;
    } else if (this._currentView === 'year') {
      return this.pickerIntl.nextYearLabel;
    } else {
      return null;
    }
  }
  get currentView() {
    return this._currentView;
  }
  set currentView(view) {
    this._currentView = view;
    this.moveFocusOnNextTick = true;
  }
  get isInSingleMode() {
    return this.selectMode === 'single';
  }
  get isInRangeMode() {
    return this.selectMode === 'range' || this.selectMode === 'rangeFrom' || this.selectMode === 'rangeTo';
  }
  get showControlArrows() {
    return this._currentView !== 'multi-years';
  }
  get isMonthView() {
    return this._currentView === 'month';
  }
  /**
   * Bind class 'owl-dt-calendar' to host
   * */
  get owlDTCalendarClass() {
    return true;
  }
  ngOnInit() {}
  ngAfterContentInit() {
    this._currentView = this.startView;
  }
  ngAfterViewChecked() {
    if (this.moveFocusOnNextTick) {
      this.moveFocusOnNextTick = false;
      this.focusActiveCell();
    }
  }
  ngOnDestroy() {
    this.intlChangesSub.unsubscribe();
  }
  /**
   * Toggle between month view and year view
   */
  toggleViews() {
    this.currentView = this._currentView == 'month' ? 'multi-years' : 'month';
  }
  /**
   * Handles user clicks on the previous button.
   * */
  previousClicked() {
    this.pickerDayjs = this.isMonthView ? this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, -1) : this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, -1);
    this.pickerDayjsChange.emit(this.pickerDayjs);
  }
  /**
   * Handles user clicks on the next button.
   * */
  nextClicked() {
    this.pickerDayjs = this.isMonthView ? this.dateTimeAdapter.addCalendarMonths(this.pickerDayjs, 1) : this.dateTimeAdapter.addCalendarYears(this.pickerDayjs, 1);
    this.pickerDayjsChange.emit(this.pickerDayjs);
  }
  dateSelected(date) {
    if (!this.dateFilterForViews(date)) {
      return;
    }
    this.selectedChange.emit(date);
    /*if ((this.isInSingleMode && !this.dateTimeAdapter.isSameDay(date, this.selected)) ||
        this.isInRangeMode) {
        this.selectedChange.emit(date);
    }*/
  }
  /**
   * Change the pickerDayjs value and switch to a specific view
   */
  goToDateInView(date, view) {
    this.handlePickerDayjsChange(date);
    this.currentView = view;
    return;
  }
  /**
   * Change the pickerDayjs value
   */
  handlePickerDayjsChange(date) {
    this.pickerDayjs = this.dateTimeAdapter.clampDate(date, this.minDate, this.maxDate);
    this.pickerDayjsChange.emit(this.pickerDayjs);
    return;
  }
  userSelected() {
    this.userSelection.emit();
  }
  /**
   * Whether the previous period button is enabled.
   */
  prevButtonEnabled() {
    return !this.minDate || !this.isSameView(this.pickerDayjs, this.minDate);
  }
  /**
   * Whether the next period button is enabled.
   */
  nextButtonEnabled() {
    return !this.maxDate || !this.isSameView(this.pickerDayjs, this.maxDate);
  }
  /**
   * Focus to the host element
   * */
  focusActiveCell() {
    this.ngZone.runOutsideAngular(() => {
      this.ngZone.onStable.asObservable().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1)).subscribe(() => {
        this.elmRef.nativeElement.querySelector('.owl-dt-calendar-cell-active').focus();
      });
    });
  }
  selectYearInMultiYearView(normalizedYear) {
    this.yearSelected.emit(normalizedYear);
  }
  selectMonthInYearView(normalizedMonth) {
    this.monthSelected.emit(normalizedMonth);
  }
  /**
   * Whether the two dates represent the same view in the current view mode (month or year).
   */
  isSameView(date1, date2) {
    if (this._currentView === 'month') {
      return !!(date1 && date2 && this.dateTimeAdapter.getYear(date1) === this.dateTimeAdapter.getYear(date2) && this.dateTimeAdapter.getMonth(date1) === this.dateTimeAdapter.getMonth(date2));
    } else if (this._currentView === 'year') {
      return !!(date1 && date2 && this.dateTimeAdapter.getYear(date1) === this.dateTimeAdapter.getYear(date2));
    } else {
      return false;
    }
  }
  /**
   * Get a valid date object
   */
  getValidDate(obj) {
    return this.dateTimeAdapter.isDateInstance(obj) && this.dateTimeAdapter.isValid(obj) ? obj : null;
  }
}
OwlCalendarComponent.ɵfac = function OwlCalendarComponent_Factory(t) {
  return new (t || OwlCalendarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OwlDateTimeIntl), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgZone), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OWL_DATE_TIME_FORMATS, 8));
};
OwlCalendarComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlCalendarComponent,
  selectors: [["owl-date-time-calendar"]],
  hostVars: 2,
  hostBindings: function OwlCalendarComponent_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-calendar", ctx.owlDTCalendarClass);
    }
  },
  inputs: {
    dateFilter: "dateFilter",
    firstDayOfWeek: "firstDayOfWeek",
    minDate: "minDate",
    maxDate: "maxDate",
    pickerDayjs: "pickerDayjs",
    selectMode: "selectMode",
    selected: "selected",
    selecteds: "selecteds",
    startView: "startView",
    hideOtherMonths: "hideOtherMonths"
  },
  outputs: {
    pickerDayjsChange: "pickerDayjsChange",
    selectedChange: "selectedChange",
    userSelection: "userSelection",
    yearSelected: "yearSelected",
    monthSelected: "monthSelected"
  },
  exportAs: ["owlDateTimeCalendar"],
  decls: 21,
  vars: 16,
  consts: [[1, "owl-dt-calendar-control"], ["type", "button", "tabindex", "0", 1, "owl-dt-control", "owl-dt-control-button", "owl-dt-control-arrow-button", 3, "disabled", "click"], ["tabindex", "-1", 1, "owl-dt-control-content", "owl-dt-control-button-content"], ["xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "version", "1.1", "x", "0px", "y", "0px", "viewBox", "0 0 250.738 250.738", 0, "xml", "space", "preserve", "width", "100%", "height", "100%", 2, "enable-background", "new 0 0 250.738 250.738"], ["d", "M96.633,125.369l95.053-94.533c7.101-7.055,7.101-18.492,0-25.546   c-7.1-7.054-18.613-7.054-25.714,0L58.989,111.689c-3.784,3.759-5.487,8.759-5.238,13.68c-0.249,4.922,1.454,9.921,5.238,13.681   l106.983,106.398c7.101,7.055,18.613,7.055,25.714,0c7.101-7.054,7.101-18.491,0-25.544L96.633,125.369z", 2, "fill-rule", "evenodd", "clip-rule", "evenodd"], [1, "owl-dt-calendar-control-content"], ["type", "button", "tabindex", "0", 1, "owl-dt-control", "owl-dt-control-button", "owl-dt-control-period-button", 3, "click"], [1, "owl-dt-control-button-arrow"], ["version", "1.1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "width", "50%", "height", "50%", "viewBox", "0 0 292.362 292.362", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 292.362 292.362"], ["d", "M286.935,69.377c-3.614-3.617-7.898-5.424-12.848-5.424H18.274c-4.952,0-9.233,1.807-12.85,5.424\n                                C1.807,72.998,0,77.279,0,82.228c0,4.948,1.807,9.229,5.424,12.847l127.907,127.907c3.621,3.617,7.902,5.428,12.85,5.428\n                                s9.233-1.811,12.847-5.428L286.935,95.074c3.613-3.617,5.427-7.898,5.427-12.847C292.362,77.279,290.548,72.998,286.935,69.377z"], ["version", "1.1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 250.738 250.738", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 250.738 250.738"], ["d", "M191.75,111.689L84.766,5.291c-7.1-7.055-18.613-7.055-25.713,0\n                    c-7.101,7.054-7.101,18.49,0,25.544l95.053,94.534l-95.053,94.533c-7.101,7.054-7.101,18.491,0,25.545\n                    c7.1,7.054,18.613,7.054,25.713,0L191.75,139.05c3.784-3.759,5.487-8.759,5.238-13.681\n                    C197.237,120.447,195.534,115.448,191.75,111.689z", 2, "fill-rule", "evenodd", "clip-rule", "evenodd"], ["cdkMonitorSubtreeFocus", "", "tabindex", "-1", 1, "owl-dt-calendar-main", 3, "ngSwitch"], [3, "pickerDayjs", "firstDayOfWeek", "selected", "selecteds", "selectMode", "minDate", "maxDate", "dateFilter", "hideOtherMonths", "pickerDayjsChange", "selectedChange", "userSelection", 4, "ngSwitchCase"], [3, "pickerDayjs", "selected", "selecteds", "selectMode", "minDate", "maxDate", "dateFilter", "keyboardEnter", "pickerDayjsChange", "monthSelected", "change", 4, "ngSwitchCase"], [3, "pickerDayjs", "selected", "selecteds", "selectMode", "minDate", "maxDate", "dateFilter", "keyboardEnter", "pickerDayjsChange", "yearSelected", "change", 4, "ngSwitchCase"], [3, "pickerDayjs", "firstDayOfWeek", "selected", "selecteds", "selectMode", "minDate", "maxDate", "dateFilter", "hideOtherMonths", "pickerDayjsChange", "selectedChange", "userSelection"], [3, "pickerDayjs", "selected", "selecteds", "selectMode", "minDate", "maxDate", "dateFilter", "keyboardEnter", "pickerDayjsChange", "monthSelected", "change"], [3, "pickerDayjs", "selected", "selecteds", "selectMode", "minDate", "maxDate", "dateFilter", "keyboardEnter", "pickerDayjsChange", "yearSelected", "change"]],
  template: function OwlCalendarComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 0)(1, "button", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlCalendarComponent_Template_button_click_1_listener() {
        return ctx.previousClicked();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](2, "span", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceSVG"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](3, "svg", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](4, "path", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceHTML"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](5, "div", 5)(6, "button", 6);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlCalendarComponent_Template_button_click_6_listener() {
        return ctx.toggleViews();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](7, "span", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](8);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](9, "span", 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceSVG"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](10, "svg", 8)(11, "g");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](12, "path", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()()()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceHTML"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](13, "button", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlCalendarComponent_Template_button_click_13_listener() {
        return ctx.nextClicked();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](14, "span", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceSVG"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](15, "svg", 10);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](16, "path", 11);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceHTML"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](17, "div", 12);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](18, OwlCalendarComponent_owl_date_time_month_view_18_Template, 1, 9, "owl-date-time-month-view", 13);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](19, OwlCalendarComponent_owl_date_time_year_view_19_Template, 1, 7, "owl-date-time-year-view", 14);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](20, OwlCalendarComponent_owl_date_time_multi_year_view_20_Template, 1, 7, "owl-date-time-multi-year-view", 15);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵstyleProp"]("visibility", ctx.showControlArrows ? "visible" : "hidden");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("disabled", !ctx.prevButtonEnabled());
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-label", ctx.prevButtonLabel);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-label", ctx.periodButtonLabel);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate1"](" ", ctx.periodButtonText, " ");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵstyleProp"]("transform", "rotate(" + (ctx.isMonthView ? 0 : 180) + "deg)");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵstyleProp"]("visibility", ctx.showControlArrows ? "visible" : "hidden");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("disabled", !ctx.nextButtonEnabled());
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-label", ctx.nextButtonLabel);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](4);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngSwitch", ctx.currentView);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngSwitchCase", "month");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngSwitchCase", "year");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngSwitchCase", "multi-years");
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.NgSwitch, _angular_common__WEBPACK_IMPORTED_MODULE_11__.NgSwitchCase, _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_14__.CdkMonitorFocus, OwlMultiYearViewComponent, OwlYearViewComponent, OwlMonthViewComponent],
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlCalendarComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      selector: 'owl-date-time-calendar',
      exportAs: 'owlDateTimeCalendar',
      host: {
        '[class.owl-dt-calendar]': 'owlDTCalendarClass'
      },
      preserveWhitespaces: false,
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      template: "<div class=\"owl-dt-calendar-control\">\n    <!-- focus when keyboard tab (http://kizu.ru/en/blog/keyboard-only-focus/#x) -->\n    <button class=\"owl-dt-control owl-dt-control-button owl-dt-control-arrow-button\"\n            type=\"button\" tabindex=\"0\"\n            [style.visibility]=\"showControlArrows? 'visible': 'hidden'\"\n            [disabled]=\"!prevButtonEnabled()\"\n            [attr.aria-label]=\"prevButtonLabel\"\n            (click)=\"previousClicked()\">\n        <span class=\"owl-dt-control-content owl-dt-control-button-content\" tabindex=\"-1\">\n            <!-- <editor-fold desc=\"SVG Arrow Left\"> -->\n        <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n                 version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 250.738 250.738\"\n                 style=\"enable-background:new 0 0 250.738 250.738;\" xml:space=\"preserve\"\n                 width=\"100%\" height=\"100%\">\n                <path style=\"fill-rule: evenodd; clip-rule: evenodd;\" d=\"M96.633,125.369l95.053-94.533c7.101-7.055,7.101-18.492,0-25.546   c-7.1-7.054-18.613-7.054-25.714,0L58.989,111.689c-3.784,3.759-5.487,8.759-5.238,13.68c-0.249,4.922,1.454,9.921,5.238,13.681   l106.983,106.398c7.101,7.055,18.613,7.055,25.714,0c7.101-7.054,7.101-18.491,0-25.544L96.633,125.369z\"/>\n            </svg>\n            <!-- </editor-fold> -->\n        </span>\n    </button>\n    <div class=\"owl-dt-calendar-control-content\">\n        <button class=\"owl-dt-control owl-dt-control-button owl-dt-control-period-button\"\n                type=\"button\" tabindex=\"0\"\n                [attr.aria-label]=\"periodButtonLabel\"\n                (click)=\"toggleViews()\">\n            <span class=\"owl-dt-control-content owl-dt-control-button-content\" tabindex=\"-1\">\n                {{periodButtonText}}\n\n                <span class=\"owl-dt-control-button-arrow\"\n                      [style.transform]=\"'rotate(' + (isMonthView? 0 : 180) +'deg)'\">\n                    <!-- <editor-fold desc=\"SVG Arrow\"> -->\n                    <svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n                         width=\"50%\" height=\"50%\" viewBox=\"0 0 292.362 292.362\" style=\"enable-background:new 0 0 292.362 292.362;\"\n                         xml:space=\"preserve\">\n                        <g>\n                            <path d=\"M286.935,69.377c-3.614-3.617-7.898-5.424-12.848-5.424H18.274c-4.952,0-9.233,1.807-12.85,5.424\n                                C1.807,72.998,0,77.279,0,82.228c0,4.948,1.807,9.229,5.424,12.847l127.907,127.907c3.621,3.617,7.902,5.428,12.85,5.428\n                                s9.233-1.811,12.847-5.428L286.935,95.074c3.613-3.617,5.427-7.898,5.427-12.847C292.362,77.279,290.548,72.998,286.935,69.377z\"/>\n                        </g>\n                    </svg>\n                    <!-- </editor-fold> -->\n                </span>\n            </span>\n        </button>\n    </div>\n    <button class=\"owl-dt-control owl-dt-control-button owl-dt-control-arrow-button\"\n            type=\"button\" tabindex=\"0\"\n            [style.visibility]=\"showControlArrows? 'visible': 'hidden'\"\n            [disabled]=\"!nextButtonEnabled()\"\n            [attr.aria-label]=\"nextButtonLabel\"\n            (click)=\"nextClicked()\">\n        <span class=\"owl-dt-control-content owl-dt-control-button-content\" tabindex=\"-1\">\n            <!-- <editor-fold desc=\"SVG Arrow Right\"> -->\n        <svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n                 viewBox=\"0 0 250.738 250.738\" style=\"enable-background:new 0 0 250.738 250.738;\" xml:space=\"preserve\">\n                <path style=\"fill-rule:evenodd;clip-rule:evenodd;\" d=\"M191.75,111.689L84.766,5.291c-7.1-7.055-18.613-7.055-25.713,0\n                    c-7.101,7.054-7.101,18.49,0,25.544l95.053,94.534l-95.053,94.533c-7.101,7.054-7.101,18.491,0,25.545\n                    c7.1,7.054,18.613,7.054,25.713,0L191.75,139.05c3.784-3.759,5.487-8.759,5.238-13.681\n                    C197.237,120.447,195.534,115.448,191.75,111.689z\"/>\n            </svg>\n            <!-- </editor-fold> -->\n        </span>\n    </button>\n</div>\n<div class=\"owl-dt-calendar-main\" cdkMonitorSubtreeFocus [ngSwitch]=\"currentView\" tabindex=\"-1\">\n    <owl-date-time-month-view\n            *ngSwitchCase=\"'month'\"\n            [pickerDayjs]=\"pickerDayjs\"\n            [firstDayOfWeek]=\"firstDayOfWeek\"\n            [selected]=\"selected\"\n            [selecteds]=\"selecteds\"\n            [selectMode]=\"selectMode\"\n            [minDate]=\"minDate\"\n            [maxDate]=\"maxDate\"\n            [dateFilter]=\"dateFilter\"\n            [hideOtherMonths]=\"hideOtherMonths\"\n            (pickerDayjsChange)=\"handlePickerDayjsChange($event)\"\n            (selectedChange)=\"dateSelected($event)\"\n            (userSelection)=\"userSelected()\"></owl-date-time-month-view>\n\n    <owl-date-time-year-view\n            *ngSwitchCase=\"'year'\"\n            [pickerDayjs]=\"pickerDayjs\"\n            [selected]=\"selected\"\n            [selecteds]=\"selecteds\"\n            [selectMode]=\"selectMode\"\n            [minDate]=\"minDate\"\n            [maxDate]=\"maxDate\"\n            [dateFilter]=\"dateFilter\"\n            (keyboardEnter)=\"focusActiveCell()\"\n            (pickerDayjsChange)=\"handlePickerDayjsChange($event)\"\n            (monthSelected)=\"selectMonthInYearView($event)\"\n            (change)=\"goToDateInView($event, 'month')\"></owl-date-time-year-view>\n\n    <owl-date-time-multi-year-view\n            *ngSwitchCase=\"'multi-years'\"\n            [pickerDayjs]=\"pickerDayjs\"\n            [selected]=\"selected\"\n            [selecteds]=\"selecteds\"\n            [selectMode]=\"selectMode\"\n            [minDate]=\"minDate\"\n            [maxDate]=\"maxDate\"\n            [dateFilter]=\"dateFilter\"\n            (keyboardEnter)=\"focusActiveCell()\"\n            (pickerDayjsChange)=\"handlePickerDayjsChange($event)\"\n            (yearSelected)=\"selectYearInMultiYearView($event)\"\n            (change)=\"goToDateInView($event, 'year')\"></owl-date-time-multi-year-view>\n</div>\n"
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef
    }, {
      type: OwlDateTimeIntl
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgZone
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }, {
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DATE_TIME_FORMATS]
      }]
    }];
  }, {
    dateFilter: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    firstDayOfWeek: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    minDate: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    maxDate: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    pickerDayjs: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selectMode: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selecteds: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    startView: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    hideOtherMonths: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    pickerDayjsChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    selectedChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    userSelection: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    yearSelected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    monthSelected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }]
  });
})();

/**
 * timer-box.component
 */
class OwlTimerBoxComponent {
  constructor() {
    this.showDivider = false;
    this.step = 1;
    this.valueChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this.inputChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this.inputStream = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    this.inputStreamSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
  }
  get displayValue() {
    return this.boxValue || this.value;
  }
  get owlDTTimerBoxClass() {
    return true;
  }
  ngOnInit() {
    this.inputStreamSub = this.inputStream.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_15__.distinctUntilChanged)()).subscribe(val => {
      if (val) {
        const inputValue = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceNumberProperty)(val, 0);
        this.updateValueViaInput(inputValue);
      }
    });
  }
  ngAfterViewInit() {
    if (this.input.nativeElement.value.length < 2) {
      this.input.nativeElement.value = `0${this.input.nativeElement.value}`;
    }
  }
  ngOnDestroy() {
    this.inputStreamSub.unsubscribe();
  }
  upBtnClicked() {
    this.updateValue(this.value + this.step);
  }
  downBtnClicked() {
    this.updateValue(this.value - this.step);
  }
  handleInputChange(val, input) {
    let num = '0';
    const number = Number(val);
    if (!isNaN(number)) {
      num = number.toFixed(0);
    }
    input.value = num;
    this.inputStream.next(num);
  }
  afterInputChange(input) {
    while (input.value.length < 2) {
      input.value = '0' + input.value;
    }
  }
  updateValue(value) {
    this.valueChange.emit(value);
  }
  updateValueViaInput(value) {
    if (value > this.max || value < this.min) {
      return;
    }
    this.inputChange.emit(value);
  }
}
OwlTimerBoxComponent.ɵfac = function OwlTimerBoxComponent_Factory(t) {
  return new (t || OwlTimerBoxComponent)();
};
OwlTimerBoxComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlTimerBoxComponent,
  selectors: [["owl-date-time-timer-box"]],
  viewQuery: function OwlTimerBoxComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵviewQuery"](_c2, 5);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵloadQuery"]()) && (ctx.input = _t.first);
    }
  },
  hostVars: 2,
  hostBindings: function OwlTimerBoxComponent_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-timer-box", ctx.owlDTTimerBoxClass);
    }
  },
  inputs: {
    showDivider: "showDivider",
    upBtnAriaLabel: "upBtnAriaLabel",
    upBtnDisabled: "upBtnDisabled",
    downBtnAriaLabel: "downBtnAriaLabel",
    downBtnDisabled: "downBtnDisabled",
    boxValue: "boxValue",
    value: "value",
    min: "min",
    max: "max",
    step: "step",
    inputLabel: "inputLabel"
  },
  outputs: {
    valueChange: "valueChange",
    inputChange: "inputChange"
  },
  exportAs: ["owlDateTimeTimerBox"],
  decls: 14,
  vars: 7,
  consts: [["class", "owl-dt-timer-divider", "aria-hidden", "true", 4, "ngIf"], ["type", "button", "tabindex", "-1", 1, "owl-dt-control-button", "owl-dt-control-arrow-button", 3, "disabled", "click"], ["tabindex", "-1", 1, "owl-dt-control-button-content"], ["xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "version", "1.1", "x", "0px", "y", "0px", "viewBox", "0 0 451.847 451.846", 0, "xml", "space", "preserve", "width", "100%", "height", "100%", 2, "enable-background", "new 0 0 451.847 451.846"], ["d", "M248.292,106.406l194.281,194.29c12.365,12.359,12.365,32.391,0,44.744c-12.354,12.354-32.391,12.354-44.744,0\n                        L225.923,173.529L54.018,345.44c-12.36,12.354-32.395,12.354-44.748,0c-12.359-12.354-12.359-32.391,0-44.75L203.554,106.4\n                        c6.18-6.174,14.271-9.259,22.369-9.259C234.018,97.141,242.115,100.232,248.292,106.406z"], [1, "owl-dt-timer-content"], ["maxlength", "2", 1, "owl-dt-timer-input", 3, "value", "click", "change", "input"], ["valueInput", ""], [1, "owl-hidden-accessible"], ["d", "M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751\n                        c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0\n                        c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"], ["aria-hidden", "true", 1, "owl-dt-timer-divider"]],
  template: function OwlTimerBoxComponent_Template(rf, ctx) {
    if (rf & 1) {
      const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](0, OwlTimerBoxComponent_div_0_Template, 1, 0, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "button", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlTimerBoxComponent_Template_button_click_1_listener() {
        return ctx.upBtnClicked();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](2, "span", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceSVG"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](3, "svg", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](4, "path", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceHTML"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](5, "label", 5)(6, "input", 6, 7);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlTimerBoxComponent_Template_input_click_6_listener() {
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r2);
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵreference"](7);
        return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](_r1.select());
      })("change", function OwlTimerBoxComponent_Template_input_change_6_listener() {
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r2);
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵreference"](7);
        return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx.afterInputChange(_r1));
      })("input", function OwlTimerBoxComponent_Template_input_input_6_listener() {
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r2);
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵreference"](7);
        return _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵresetView"](ctx.handleInputChange(_r1.value, _r1));
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](8, "span", 8);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](9);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](10, "button", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function OwlTimerBoxComponent_Template_button_click_10_listener() {
        return ctx.downBtnClicked();
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](11, "span", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnamespaceSVG"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](12, "svg", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](13, "path", 9);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]()()();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.showDivider);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("disabled", ctx.upBtnDisabled);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-label", ctx.upBtnAriaLabel);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](5);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("value", ctx.displayValue);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"](ctx.inputLabel);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("disabled", ctx.downBtnDisabled);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-label", ctx.downBtnAriaLabel);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.NgIf],
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlTimerBoxComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      exportAs: 'owlDateTimeTimerBox',
      selector: 'owl-date-time-timer-box',
      preserveWhitespaces: false,
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      host: {
        '[class.owl-dt-timer-box]': 'owlDTTimerBoxClass'
      },
      template: "<div *ngIf=\"showDivider\" class=\"owl-dt-timer-divider\" aria-hidden=\"true\"></div>\n<button class=\"owl-dt-control-button owl-dt-control-arrow-button\"\n        type=\"button\" tabindex=\"-1\"\n        [disabled]=\"upBtnDisabled\"\n        [attr.aria-label]=\"upBtnAriaLabel\"\n        (click)=\"upBtnClicked()\">\n    <span class=\"owl-dt-control-button-content\" tabindex=\"-1\">\n        <!-- <editor-fold desc=\"SVG Arrow Up\"> -->\n    <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n                 version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451.847 451.846\"\n                 style=\"enable-background:new 0 0 451.847 451.846;\" xml:space=\"preserve\"\n                 width=\"100%\" height=\"100%\">\n                    <path d=\"M248.292,106.406l194.281,194.29c12.365,12.359,12.365,32.391,0,44.744c-12.354,12.354-32.391,12.354-44.744,0\n                        L225.923,173.529L54.018,345.44c-12.36,12.354-32.395,12.354-44.748,0c-12.359-12.354-12.359-32.391,0-44.75L203.554,106.4\n                        c6.18-6.174,14.271-9.259,22.369-9.259C234.018,97.141,242.115,100.232,248.292,106.406z\"/>\n                </svg>\n        <!-- </editor-fold> -->\n    </span>\n</button>\n<label class=\"owl-dt-timer-content\">\n    <input class=\"owl-dt-timer-input\" maxlength=\"2\"\n           [value]=\"displayValue\"\n           (click)=\"valueInput.select()\"\n           (change)=\"afterInputChange(valueInput)\"\n           (input)=\"handleInputChange(valueInput.value, valueInput)\" #valueInput>\n    <span class=\"owl-hidden-accessible\">{{inputLabel}}</span>\n</label>\n<button class=\"owl-dt-control-button owl-dt-control-arrow-button\"\n        type=\"button\" tabindex=\"-1\"\n        [disabled]=\"downBtnDisabled\"\n        [attr.aria-label]=\"downBtnAriaLabel\"\n        (click)=\"downBtnClicked()\">\n    <span class=\"owl-dt-control-button-content\" tabindex=\"-1\">\n        <!-- <editor-fold desc=\"SVG Arrow Down\"> -->\n    <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n                 version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 451.847 451.846\"\n                 style=\"enable-background:new 0 0 451.847 451.846;\" xml:space=\"preserve\"\n                 width=\"100%\" height=\"100%\">\n                    <path d=\"M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751\n                        c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0\n                        c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z\"/>\n                </svg>\n        <!-- </editor-fold> -->\n    </span>\n</button>\n"
    }]
  }], function () {
    return [];
  }, {
    showDivider: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    upBtnAriaLabel: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    upBtnDisabled: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    downBtnAriaLabel: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    downBtnDisabled: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    boxValue: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    value: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    min: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    max: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    step: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    inputLabel: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    valueChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    inputChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    input: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild,
      args: ['valueInput']
    }]
  });
})();

/**
 * timer.component
 */
class OwlTimerComponent {
  constructor(ngZone, elmRef, pickerIntl, cdRef, dateTimeAdapter) {
    this.ngZone = ngZone;
    this.elmRef = elmRef;
    this.pickerIntl = pickerIntl;
    this.cdRef = cdRef;
    this.dateTimeAdapter = dateTimeAdapter;
    this.isPM = false; // a flag indicates the current timer dayjs is in PM or AM
    /**
     * Hours to change per step
     */
    this.stepHour = 1;
    /**
     * Minutes to change per step
     */
    this.stepMinute = 1;
    /**
     * Seconds to change per step
     */
    this.stepSecond = 1;
    this.selectedChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
  }
  get pickerDayjs() {
    return this._pickerDayjs;
  }
  set pickerDayjs(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._pickerDayjs = this.getValidDate(value) || this.dateTimeAdapter.now();
  }
  get minDateTime() {
    return this._minDateTime;
  }
  set minDateTime(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._minDateTime = this.getValidDate(value);
  }
  get maxDateTime() {
    return this._maxDateTime;
  }
  set maxDateTime(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this._maxDateTime = this.getValidDate(value);
  }
  get hourValue() {
    return this.dateTimeAdapter.getHours(this.pickerDayjs);
  }
  /**
   * The value would be displayed in hourBox.
   * We need this because the value displayed in hourBox it not
   * the same as the hourValue when the timer is in hour12Timer mode.
   * */
  get hourBoxValue() {
    let hours = this.hourValue;
    if (!this.hour12Timer) {
      return hours;
    } else {
      if (hours === 0) {
        hours = 12;
        this.isPM = false;
      } else if (hours > 0 && hours < 12) {
        this.isPM = false;
      } else if (hours === 12) {
        this.isPM = true;
      } else if (hours > 12 && hours < 24) {
        hours = hours - 12;
        this.isPM = true;
      }
      return hours;
    }
  }
  get minuteValue() {
    return this.dateTimeAdapter.getMinutes(this.pickerDayjs);
  }
  get secondValue() {
    return this.dateTimeAdapter.getSeconds(this.pickerDayjs);
  }
  get upHourButtonLabel() {
    return this.pickerIntl.upHourLabel;
  }
  get downHourButtonLabel() {
    return this.pickerIntl.downHourLabel;
  }
  get upMinuteButtonLabel() {
    return this.pickerIntl.upMinuteLabel;
  }
  get downMinuteButtonLabel() {
    return this.pickerIntl.downMinuteLabel;
  }
  get upSecondButtonLabel() {
    return this.pickerIntl.upSecondLabel;
  }
  get downSecondButtonLabel() {
    return this.pickerIntl.downSecondLabel;
  }
  get hour12ButtonLabel() {
    return this.isPM ? this.pickerIntl.hour12PMLabel : this.pickerIntl.hour12AMLabel;
  }
  get owlDTTimerClass() {
    return true;
  }
  get owlDTTimeTabIndex() {
    return -1;
  }
  ngOnInit() {}
  /**
   * Focus to the host element
   * */
  focus() {
    this.ngZone.runOutsideAngular(() => {
      this.ngZone.onStable.asObservable().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1)).subscribe(() => {
        this.elmRef.nativeElement.focus();
      });
    });
  }
  /**
   * Set the hour value via typing into timer box input
   * We need this to handle the hour value when the timer is in hour12 mode
   * */
  setHourValueViaInput(hours) {
    if (this.hour12Timer && this.isPM && hours >= 1 && hours <= 11) {
      hours = hours + 12;
    } else if (this.hour12Timer && !this.isPM && hours === 12) {
      hours = 0;
    }
    this.setHourValue(hours);
  }
  setHourValue(hours) {
    const m = this.dateTimeAdapter.setHours(this.pickerDayjs, hours);
    this.selectedChange.emit(m);
    this.cdRef.markForCheck();
  }
  setMinuteValue(minutes) {
    const m = this.dateTimeAdapter.setMinutes(this.pickerDayjs, minutes);
    this.selectedChange.emit(m);
    this.cdRef.markForCheck();
  }
  setSecondValue(seconds) {
    const m = this.dateTimeAdapter.setSeconds(this.pickerDayjs, seconds);
    this.selectedChange.emit(m);
    this.cdRef.markForCheck();
  }
  setMeridiem(event) {
    this.isPM = !this.isPM;
    let hours = this.hourValue;
    if (this.isPM) {
      hours = hours + 12;
    } else {
      hours = hours - 12;
    }
    if (hours >= 0 && hours <= 23) {
      this.setHourValue(hours);
    }
    this.cdRef.markForCheck();
    event.preventDefault();
  }
  /**
   * Check if the up hour button is enabled
   */
  upHourEnabled() {
    return !this.maxDateTime || this.compareHours(this.stepHour, this.maxDateTime) < 1;
  }
  /**
   * Check if the down hour button is enabled
   */
  downHourEnabled() {
    return !this.minDateTime || this.compareHours(-this.stepHour, this.minDateTime) > -1;
  }
  /**
   * Check if the up minute button is enabled
   */
  upMinuteEnabled() {
    return !this.maxDateTime || this.compareMinutes(this.stepMinute, this.maxDateTime) < 1;
  }
  /**
   * Check if the down minute button is enabled
   */
  downMinuteEnabled() {
    return !this.minDateTime || this.compareMinutes(-this.stepMinute, this.minDateTime) > -1;
  }
  /**
   * Check if the up second button is enabled
   */
  upSecondEnabled() {
    return !this.maxDateTime || this.compareSeconds(this.stepSecond, this.maxDateTime) < 1;
  }
  /**
   * Check if the down second button is enabled
   */
  downSecondEnabled() {
    return !this.minDateTime || this.compareSeconds(-this.stepSecond, this.minDateTime) > -1;
  }
  /**
   * PickerDayjs's hour value +/- certain amount and compare it to the give date
   * 1 is after the comparedDate
   * -1 is before the comparedDate
   * 0 is equal the comparedDate
   * */
  compareHours(amount, comparedDate) {
    const hours = this.dateTimeAdapter.getHours(this.pickerDayjs) + amount;
    const result = this.dateTimeAdapter.setHours(this.pickerDayjs, hours);
    return this.dateTimeAdapter.compare(result, comparedDate);
  }
  /**
   * PickerDayjs's minute value +/- certain amount and compare it to the give date
   * 1 is after the comparedDate
   * -1 is before the comparedDate
   * 0 is equal the comparedDate
   * */
  compareMinutes(amount, comparedDate) {
    const minutes = this.dateTimeAdapter.getMinutes(this.pickerDayjs) + amount;
    const result = this.dateTimeAdapter.setMinutes(this.pickerDayjs, minutes);
    return this.dateTimeAdapter.compare(result, comparedDate);
  }
  /**
   * PickerDayjs's second value +/- certain amount and compare it to the give date
   * 1 is after the comparedDate
   * -1 is before the comparedDate
   * 0 is equal the comparedDate
   * */
  compareSeconds(amount, comparedDate) {
    const seconds = this.dateTimeAdapter.getSeconds(this.pickerDayjs) + amount;
    const result = this.dateTimeAdapter.setSeconds(this.pickerDayjs, seconds);
    return this.dateTimeAdapter.compare(result, comparedDate);
  }
  /**
   * Get a valid date object
   */
  getValidDate(obj) {
    return this.dateTimeAdapter.isDateInstance(obj) && this.dateTimeAdapter.isValid(obj) ? obj : null;
  }
}
OwlTimerComponent.ɵfac = function OwlTimerComponent_Factory(t) {
  return new (t || OwlTimerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgZone), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OwlDateTimeIntl), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8));
};
OwlTimerComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlTimerComponent,
  selectors: [["owl-date-time-timer"]],
  hostVars: 3,
  hostBindings: function OwlTimerComponent_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("tabindex", ctx.owlDTTimeTabIndex);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-timer", ctx.owlDTTimerClass);
    }
  },
  inputs: {
    pickerDayjs: "pickerDayjs",
    minDateTime: "minDateTime",
    maxDateTime: "maxDateTime",
    showSecondsTimer: "showSecondsTimer",
    hour12Timer: "hour12Timer",
    stepHour: "stepHour",
    stepMinute: "stepMinute",
    stepSecond: "stepSecond"
  },
  outputs: {
    selectedChange: "selectedChange"
  },
  exportAs: ["owlDateTimeTimer"],
  decls: 4,
  vars: 22,
  consts: [[3, "upBtnAriaLabel", "downBtnAriaLabel", "upBtnDisabled", "downBtnDisabled", "boxValue", "value", "min", "max", "step", "inputLabel", "inputChange", "valueChange"], [3, "showDivider", "upBtnAriaLabel", "downBtnAriaLabel", "upBtnDisabled", "downBtnDisabled", "value", "min", "max", "step", "inputLabel", "inputChange", "valueChange"], [3, "showDivider", "upBtnAriaLabel", "downBtnAriaLabel", "upBtnDisabled", "downBtnDisabled", "value", "min", "max", "step", "inputLabel", "inputChange", "valueChange", 4, "ngIf"], ["class", "owl-dt-timer-hour12", 4, "ngIf"], [1, "owl-dt-timer-hour12"], ["type", "button", "tabindex", "0", 1, "owl-dt-control-button", "owl-dt-timer-hour12-box", 3, "click"], ["tabindex", "-1", 1, "owl-dt-control-button-content"]],
  template: function OwlTimerComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "owl-date-time-timer-box", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("inputChange", function OwlTimerComponent_Template_owl_date_time_timer_box_inputChange_0_listener($event) {
        return ctx.setHourValueViaInput($event);
      })("valueChange", function OwlTimerComponent_Template_owl_date_time_timer_box_valueChange_0_listener($event) {
        return ctx.setHourValue($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "owl-date-time-timer-box", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("inputChange", function OwlTimerComponent_Template_owl_date_time_timer_box_inputChange_1_listener($event) {
        return ctx.setMinuteValue($event);
      })("valueChange", function OwlTimerComponent_Template_owl_date_time_timer_box_valueChange_1_listener($event) {
        return ctx.setMinuteValue($event);
      });
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](2, OwlTimerComponent_owl_date_time_timer_box_2_Template, 1, 10, "owl-date-time-timer-box", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](3, OwlTimerComponent_div_3_Template, 4, 1, "div", 3);
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("upBtnAriaLabel", ctx.upHourButtonLabel)("downBtnAriaLabel", ctx.downHourButtonLabel)("upBtnDisabled", !ctx.upHourEnabled())("downBtnDisabled", !ctx.downHourEnabled())("boxValue", ctx.hourBoxValue)("value", ctx.hourValue)("min", 0)("max", 23)("step", ctx.stepHour)("inputLabel", "Hour");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("showDivider", true)("upBtnAriaLabel", ctx.upMinuteButtonLabel)("downBtnAriaLabel", ctx.downMinuteButtonLabel)("upBtnDisabled", !ctx.upMinuteEnabled())("downBtnDisabled", !ctx.downMinuteEnabled())("value", ctx.minuteValue)("min", 0)("max", 59)("step", ctx.stepMinute)("inputLabel", "Minute");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.showSecondsTimer);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.hour12Timer);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.NgIf, OwlTimerBoxComponent],
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlTimerComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      exportAs: 'owlDateTimeTimer',
      selector: 'owl-date-time-timer',
      preserveWhitespaces: false,
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      host: {
        '[class.owl-dt-timer]': 'owlDTTimerClass',
        '[attr.tabindex]': 'owlDTTimeTabIndex'
      },
      template: "<owl-date-time-timer-box\n        [upBtnAriaLabel]=\"upHourButtonLabel\"\n        [downBtnAriaLabel]=\"downHourButtonLabel\"\n        [upBtnDisabled]=\"!upHourEnabled()\"\n        [downBtnDisabled]=\"!downHourEnabled()\"\n        [boxValue]=\"hourBoxValue\"\n        [value]=\"hourValue\" [min]=\"0\" [max]=\"23\"\n        [step]=\"stepHour\" [inputLabel]=\"'Hour'\"\n        (inputChange)=\"setHourValueViaInput($event)\"\n        (valueChange)=\"setHourValue($event)\"></owl-date-time-timer-box>\n<owl-date-time-timer-box\n        [showDivider]=\"true\"\n        [upBtnAriaLabel]=\"upMinuteButtonLabel\"\n        [downBtnAriaLabel]=\"downMinuteButtonLabel\"\n        [upBtnDisabled]=\"!upMinuteEnabled()\"\n        [downBtnDisabled]=\"!downMinuteEnabled()\"\n        [value]=\"minuteValue\" [min]=\"0\" [max]=\"59\"\n        [step]=\"stepMinute\" [inputLabel]=\"'Minute'\"\n        (inputChange)=\"setMinuteValue($event)\"\n        (valueChange)=\"setMinuteValue($event)\"></owl-date-time-timer-box>\n<owl-date-time-timer-box\n        *ngIf=\"showSecondsTimer\"\n        [showDivider]=\"true\"\n        [upBtnAriaLabel]=\"upSecondButtonLabel\"\n        [downBtnAriaLabel]=\"downSecondButtonLabel\"\n        [upBtnDisabled]=\"!upSecondEnabled()\"\n        [downBtnDisabled]=\"!downSecondEnabled()\"\n        [value]=\"secondValue\" [min]=\"0\" [max]=\"59\"\n        [step]=\"stepSecond\" [inputLabel]=\"'Second'\"\n        (inputChange)=\"setSecondValue($event)\"\n        (valueChange)=\"setSecondValue($event)\"></owl-date-time-timer-box>\n\n<div *ngIf=\"hour12Timer\" class=\"owl-dt-timer-hour12\">\n    <button class=\"owl-dt-control-button owl-dt-timer-hour12-box\"\n            type=\"button\" tabindex=\"0\"\n            (click)=\"setMeridiem($event)\">\n        <span class=\"owl-dt-control-button-content\" tabindex=\"-1\">\n            {{hour12ButtonLabel}}\n        </span>\n    </button>\n</div>\n"
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgZone
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef
    }, {
      type: OwlDateTimeIntl
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }, {
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }];
  }, {
    pickerDayjs: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    minDateTime: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    maxDateTime: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    showSecondsTimer: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    hour12Timer: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    stepHour: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    stepMinute: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    stepSecond: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selectedChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }]
  });
})();

/**
 * date-time-picker.animations
 */
const owlDateTimePickerAnimations = {
  transformPicker: (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.trigger)('transformPicker', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.state)('void', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
    opacity: 0,
    transform: 'scale(1, 0)'
  })), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.state)('enter', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
    opacity: 1,
    transform: 'scale(1, 1)'
  })), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.transition)('void => enter', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.group)([(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.query)('@fadeInPicker', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animateChild)(), {
    optional: true
  }), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animate)('400ms cubic-bezier(0.25, 0.8, 0.25, 1)')])), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.transition)('enter => void', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animate)('100ms linear', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
    opacity: 0
  })))]),
  fadeInPicker: (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.trigger)('fadeInPicker', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.state)('enter', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
    opacity: 1
  })), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.state)('void', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
    opacity: 0
  })), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.transition)('void => enter', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animate)('400ms 100ms cubic-bezier(0.55, 0, 0.55, 0.2)'))])
};

/**
 * date-time-picker-container.component
 */
class OwlDateTimeContainerComponent {
  constructor(cdRef, elmRef, pickerIntl, dateTimeAdapter) {
    this.cdRef = cdRef;
    this.elmRef = elmRef;
    this.pickerIntl = pickerIntl;
    this.dateTimeAdapter = dateTimeAdapter;
    this.activeSelectedIndex = 0; // The current active SelectedIndex in range select mode (0: 'from', 1: 'to')
    /**
     * Stream emits when try to hide picker
     * */
    this.hidePicker$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    /**
     * Stream emits when try to confirm the selected value
     * */
    this.confirmSelected$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    this.pickerOpened$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
  }
  get hidePickerStream() {
    return this.hidePicker$.asObservable();
  }
  get confirmSelectedStream() {
    return this.confirmSelected$.asObservable();
  }
  get pickerOpenedStream() {
    return this.pickerOpened$.asObservable();
  }
  get pickerDayjs() {
    return this._clamPickerDayjs;
  }
  set pickerDayjs(value) {
    if (value) {
      this._clamPickerDayjs = this.dateTimeAdapter.clampDate(value, this.picker.minDateTime, this.picker.maxDateTime);
    }
    this.cdRef.markForCheck();
  }
  get pickerType() {
    return this.picker.pickerType;
  }
  get cancelLabel() {
    return this.pickerIntl.cancelBtnLabel;
  }
  get setLabel() {
    return this.pickerIntl.setBtnLabel;
  }
  /**
   * The range 'from' label
   * */
  get fromLabel() {
    return this.pickerIntl.rangeFromLabel;
  }
  /**
   * The range 'to' label
   * */
  get toLabel() {
    return this.pickerIntl.rangeToLabel;
  }
  /**
   * The range 'from' formatted value
   * */
  get fromFormattedValue() {
    const value = this.picker.selecteds[0];
    return value ? this.dateTimeAdapter.format(value, this.picker.formatString) : '';
  }
  /**
   * The range 'to' formatted value
   * */
  get toFormattedValue() {
    const value = this.picker.selecteds[1];
    return value ? this.dateTimeAdapter.format(value, this.picker.formatString) : '';
  }
  /**
   * Cases in which the control buttons show in the picker
   * 1) picker mode is 'dialog'
   * 2) picker type is NOT 'calendar' and the picker mode is NOT 'inline'
   * */
  get showControlButtons() {
    return this.picker.pickerMode === 'dialog' || this.picker.pickerType !== 'calendar' && this.picker.pickerMode !== 'inline';
  }
  get containerElm() {
    return this.elmRef.nativeElement;
  }
  get owlDTContainerClass() {
    return true;
  }
  get owlDTPopupContainerClass() {
    return this.picker.pickerMode === 'popup';
  }
  get owlDTDialogContainerClass() {
    return this.picker.pickerMode === 'dialog';
  }
  get owlDTInlineContainerClass() {
    return this.picker.pickerMode === 'inline';
  }
  get owlDTContainerDisabledClass() {
    return this.picker.disabled;
  }
  get owlDTContainerId() {
    return this.picker.id;
  }
  get owlDTContainerAnimation() {
    return this.picker.pickerMode === 'inline' ? '' : 'enter';
  }
  ngOnInit() {}
  ngAfterContentInit() {
    this.initPicker();
  }
  ngAfterViewInit() {
    this.focusPicker();
  }
  handleContainerAnimationDone(event) {
    const toState = event.toState;
    if (toState === 'enter') {
      this.pickerOpened$.next();
    }
  }
  dateSelected(date) {
    let result;
    if (this.picker.isInSingleMode) {
      result = this.dateSelectedInSingleMode(date);
      if (result) {
        this.pickerDayjs = result;
        this.picker.select(result);
      } else {
        // we close the picker when result is null and pickerType is calendar.
        if (this.pickerType === 'calendar') {
          this.hidePicker$.next(null);
        }
      }
      return;
    }
    if (this.picker.isInRangeMode) {
      result = this.dateSelectedInRangeMode(date);
      if (result) {
        this.pickerDayjs = result[this.activeSelectedIndex];
        this.picker.select(result);
      }
    }
  }
  timeSelected(time) {
    this.pickerDayjs = this.dateTimeAdapter.clone(time);
    if (!this.picker.dateTimeChecker(this.pickerDayjs)) {
      return;
    }
    if (this.picker.isInSingleMode) {
      this.picker.select(this.pickerDayjs);
      return;
    }
    if (this.picker.isInRangeMode) {
      const selecteds = [...this.picker.selecteds];
      // check if the 'from' is after 'to' or 'to'is before 'from'
      // In this case, we set both the 'from' and 'to' the same value
      if (this.activeSelectedIndex === 0 && selecteds[1] && this.dateTimeAdapter.compare(this.pickerDayjs, selecteds[1]) === 1 || this.activeSelectedIndex === 1 && selecteds[0] && this.dateTimeAdapter.compare(this.pickerDayjs, selecteds[0]) === -1) {
        selecteds[0] = this.pickerDayjs;
        selecteds[1] = this.pickerDayjs;
      } else {
        selecteds[this.activeSelectedIndex] = this.pickerDayjs;
      }
      this.picker.select(selecteds);
    }
  }
  /**
   * Handle click on cancel button
   */
  onCancelClicked(event) {
    this.hidePicker$.next(null);
    event.preventDefault();
    return;
  }
  /**
   * Handle click on set button
   */
  onSetClicked(event) {
    if (!this.picker.dateTimeChecker(this.pickerDayjs)) {
      this.hidePicker$.next(null);
      event.preventDefault();
      return;
    }
    this.confirmSelected$.next(event);
    event.preventDefault();
    return;
  }
  /**
   * Handle click on inform radio group
   */
  handleClickOnInfoGroup(event, index) {
    this.setActiveSelectedIndex(index);
    event.preventDefault();
    event.stopPropagation();
  }
  /**
   * Handle click on inform radio group
   */
  handleKeydownOnInfoGroup(event, next, index) {
    switch (event.keyCode) {
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.DOWN_ARROW:
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.RIGHT_ARROW:
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.UP_ARROW:
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.LEFT_ARROW:
        next.focus();
        this.setActiveSelectedIndex(index === 0 ? 1 : 0);
        event.preventDefault();
        event.stopPropagation();
        break;
      case _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.SPACE:
        this.setActiveSelectedIndex(index);
        event.preventDefault();
        event.stopPropagation();
        break;
      default:
        return;
    }
  }
  /**
   * Set the value of activeSelectedIndex
   */
  setActiveSelectedIndex(index) {
    if (this.picker.selectMode === 'range' && this.activeSelectedIndex !== index) {
      this.activeSelectedIndex = index;
      const selected = this.picker.selecteds[this.activeSelectedIndex];
      if (this.picker.selecteds && selected) {
        this.pickerDayjs = this.dateTimeAdapter.clone(selected);
      }
    }
    return;
  }
  initPicker() {
    this.pickerDayjs = this.picker.startAt || this.dateTimeAdapter.now();
    this.activeSelectedIndex = this.picker.selectMode === 'rangeTo' ? 1 : 0;
  }
  /**
   * Select calendar date in single mode,
   * it returns null when date is not selected.
   */
  dateSelectedInSingleMode(date) {
    if (this.dateTimeAdapter.isSameDay(date, this.picker.selected)) {
      return null;
    }
    return this.updateAndCheckCalendarDate(date);
  }
  /**
   * Select dates in range Mode
   */
  dateSelectedInRangeMode(date) {
    let from = this.picker.selecteds[0];
    let to = this.picker.selecteds[1];
    const result = this.updateAndCheckCalendarDate(date);
    if (!result) {
      return null;
    }
    // if the given calendar day is after or equal to 'from',
    // set ths given date as 'to'
    // otherwise, set it as 'from' and set 'to' to null
    if (this.picker.selectMode === 'range') {
      if (this.picker.selecteds && this.picker.selecteds.length && !to && from && this.dateTimeAdapter.differenceInCalendarDays(result, from) >= 0) {
        to = result;
        this.activeSelectedIndex = 1;
      } else {
        from = result;
        to = null;
        this.activeSelectedIndex = 0;
      }
    } else if (this.picker.selectMode === 'rangeFrom') {
      from = result;
      // if the from value is after the to value, set the to value as null
      if (to && this.dateTimeAdapter.compare(from, to) > 0) {
        to = null;
      }
    } else if (this.picker.selectMode === 'rangeTo') {
      to = result;
      // if the from value is after the to value, set the from value as null
      if (from && this.dateTimeAdapter.compare(from, to) > 0) {
        from = null;
      }
    }
    return [from, to];
  }
  /**
   * Update the given calendar date's time and check if it is valid
   * Because the calendar date has 00:00:00 as default time, if the picker type is 'both',
   * we need to update the given calendar date's time before selecting it.
   * if it is valid, return the updated dateTime
   * if it is not valid, return null
   */
  updateAndCheckCalendarDate(date) {
    let result;
    // if the picker is 'both', update the calendar date's time value
    if (this.picker.pickerType === 'both') {
      result = this.dateTimeAdapter.createDate(this.dateTimeAdapter.getYear(date), this.dateTimeAdapter.getMonth(date), this.dateTimeAdapter.getDate(date), this.dateTimeAdapter.getHours(this.pickerDayjs), this.dateTimeAdapter.getMinutes(this.pickerDayjs), this.dateTimeAdapter.getSeconds(this.pickerDayjs));
      result = this.dateTimeAdapter.clampDate(result, this.picker.minDateTime, this.picker.maxDateTime);
    } else {
      result = this.dateTimeAdapter.clone(date);
    }
    // check the updated dateTime
    return this.picker.dateTimeChecker(result) ? result : null;
  }
  /**
   * Focus to the picker
   * */
  focusPicker() {
    if (this.picker.pickerMode === 'inline') {
      return;
    }
    if (this.calendar) {
      this.calendar.focusActiveCell();
    } else if (this.timer) {
      this.timer.focus();
    }
  }
}
OwlDateTimeContainerComponent.ɵfac = function OwlDateTimeContainerComponent_Factory(t) {
  return new (t || OwlDateTimeContainerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OwlDateTimeIntl), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8));
};
OwlDateTimeContainerComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlDateTimeContainerComponent,
  selectors: [["owl-date-time-container"]],
  viewQuery: function OwlDateTimeContainerComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵviewQuery"](OwlCalendarComponent, 5);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵviewQuery"](OwlTimerComponent, 5);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵloadQuery"]()) && (ctx.calendar = _t.first);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵloadQuery"]()) && (ctx.timer = _t.first);
    }
  },
  hostVars: 12,
  hostBindings: function OwlDateTimeContainerComponent_HostBindings(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsyntheticHostListener"]("@transformPicker.done", function OwlDateTimeContainerComponent_animation_transformPicker_done_HostBindingHandler($event) {
        return ctx.handleContainerAnimationDone($event);
      });
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("id", ctx.owlDTContainerId);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsyntheticHostProperty"]("@transformPicker", ctx.owlDTContainerAnimation);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-container", ctx.owlDTContainerClass)("owl-dt-popup-container", ctx.owlDTPopupContainerClass)("owl-dt-dialog-container", ctx.owlDTDialogContainerClass)("owl-dt-inline-container", ctx.owlDTInlineContainerClass)("owl-dt-container-disabled", ctx.owlDTContainerDisabledClass);
    }
  },
  exportAs: ["owlDateTimeContainer"],
  decls: 5,
  vars: 6,
  consts: [[1, "owl-dt-container-inner", 3, "cdkTrapFocus"], ["class", "owl-dt-container-row", 3, "firstDayOfWeek", "pickerDayjs", "selected", "selecteds", "selectMode", "minDate", "maxDate", "dateFilter", "startView", "hideOtherMonths", "pickerDayjsChange", "yearSelected", "monthSelected", "selectedChange", 4, "ngIf"], ["class", "owl-dt-container-row", 3, "pickerDayjs", "minDateTime", "maxDateTime", "showSecondsTimer", "hour12Timer", "stepHour", "stepMinute", "stepSecond", "selectedChange", 4, "ngIf"], ["role", "radiogroup", "class", "owl-dt-container-info owl-dt-container-row", 4, "ngIf"], ["class", "owl-dt-container-buttons owl-dt-container-row", 4, "ngIf"], [1, "owl-dt-container-row", 3, "firstDayOfWeek", "pickerDayjs", "selected", "selecteds", "selectMode", "minDate", "maxDate", "dateFilter", "startView", "hideOtherMonths", "pickerDayjsChange", "yearSelected", "monthSelected", "selectedChange"], [1, "owl-dt-container-row", 3, "pickerDayjs", "minDateTime", "maxDateTime", "showSecondsTimer", "hour12Timer", "stepHour", "stepMinute", "stepSecond", "selectedChange"], ["role", "radiogroup", 1, "owl-dt-container-info", "owl-dt-container-row"], ["role", "radio", 1, "owl-dt-control", "owl-dt-container-range", "owl-dt-container-from", 3, "tabindex", "ngClass", "click", "keydown"], ["from", ""], ["tabindex", "-1", 1, "owl-dt-control-content", "owl-dt-container-range-content"], [1, "owl-dt-container-info-label"], [1, "owl-dt-container-info-value"], ["role", "radio", 1, "owl-dt-control", "owl-dt-container-range", "owl-dt-container-to", 3, "tabindex", "ngClass", "click", "keydown"], ["to", ""], [1, "owl-dt-container-buttons", "owl-dt-container-row"], ["type", "button", "tabindex", "0", 1, "owl-dt-control", "owl-dt-control-button", "owl-dt-container-control-button", 3, "click"], ["tabindex", "-1", 1, "owl-dt-control-content", "owl-dt-control-button-content"]],
  template: function OwlDateTimeContainerComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](1, OwlDateTimeContainerComponent_owl_date_time_calendar_1_Template, 1, 10, "owl-date-time-calendar", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](2, OwlDateTimeContainerComponent_owl_date_time_timer_2_Template, 1, 8, "owl-date-time-timer", 2);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](3, OwlDateTimeContainerComponent_div_3_Template, 15, 14, "div", 3);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](4, OwlDateTimeContainerComponent_div_4_Template, 7, 2, "div", 4);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("cdkTrapFocus", ctx.picker.pickerMode !== "inline")("@fadeInPicker", ctx.picker.pickerMode === "inline" ? "" : "enter");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.pickerType === "both" || ctx.pickerType === "calendar");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.pickerType === "both" || ctx.pickerType === "timer");
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.picker.isInRangeMode);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.showControlButtons);
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_11__.NgIf, _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_14__.CdkTrapFocus, OwlTimerComponent, OwlCalendarComponent],
  data: {
    animation: [owlDateTimePickerAnimations.transformPicker, owlDateTimePickerAnimations.fadeInPicker]
  },
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDateTimeContainerComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      exportAs: 'owlDateTimeContainer',
      selector: 'owl-date-time-container',
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      preserveWhitespaces: false,
      animations: [owlDateTimePickerAnimations.transformPicker, owlDateTimePickerAnimations.fadeInPicker],
      host: {
        '(@transformPicker.done)': 'handleContainerAnimationDone($event)',
        '[class.owl-dt-container]': 'owlDTContainerClass',
        '[class.owl-dt-popup-container]': 'owlDTPopupContainerClass',
        '[class.owl-dt-dialog-container]': 'owlDTDialogContainerClass',
        '[class.owl-dt-inline-container]': 'owlDTInlineContainerClass',
        '[class.owl-dt-container-disabled]': 'owlDTContainerDisabledClass',
        '[attr.id]': 'owlDTContainerId',
        '[@transformPicker]': 'owlDTContainerAnimation'
      },
      template: "<div [cdkTrapFocus]=\"picker.pickerMode !== 'inline'\"\n     [@fadeInPicker]=\"picker.pickerMode === 'inline'? '' : 'enter'\"\n     class=\"owl-dt-container-inner\">\n\n    <owl-date-time-calendar\n            *ngIf=\"pickerType === 'both' || pickerType === 'calendar'\"\n            class=\"owl-dt-container-row\"\n            [firstDayOfWeek]=\"picker.firstDayOfWeek\"\n            [(pickerDayjs)]=\"pickerDayjs\"\n            [selected]=\"picker.selected\"\n            [selecteds]=\"picker.selecteds\"\n            [selectMode]=\"picker.selectMode\"\n            [minDate]=\"picker.minDateTime\"\n            [maxDate]=\"picker.maxDateTime\"\n            [dateFilter]=\"picker.dateTimeFilter\"\n            [startView]=\"picker.startView\"\n            [hideOtherMonths]=\"picker.hideOtherMonths\"\n            (yearSelected)=\"picker.selectYear($event)\"\n            (monthSelected)=\"picker.selectMonth($event)\"\n            (selectedChange)=\"dateSelected($event)\"></owl-date-time-calendar>\n\n    <owl-date-time-timer\n            *ngIf=\"pickerType === 'both' || pickerType === 'timer'\"\n            class=\"owl-dt-container-row\"\n            [pickerDayjs]=\"pickerDayjs\"\n            [minDateTime]=\"picker.minDateTime\"\n            [maxDateTime]=\"picker.maxDateTime\"\n            [showSecondsTimer]=\"picker.showSecondsTimer\"\n            [hour12Timer]=\"picker.hour12Timer\"\n            [stepHour]=\"picker.stepHour\"\n            [stepMinute]=\"picker.stepMinute\"\n            [stepSecond]=\"picker.stepSecond\"\n            (selectedChange)=\"timeSelected($event)\"></owl-date-time-timer>\n\n    <div *ngIf=\"picker.isInRangeMode\"\n         role=\"radiogroup\"\n         class=\"owl-dt-container-info owl-dt-container-row\">\n        <div role=\"radio\" [tabindex]=\"activeSelectedIndex === 0 ? 0 : -1\"\n             [attr.aria-checked]=\"activeSelectedIndex === 0\"\n             class=\"owl-dt-control owl-dt-container-range owl-dt-container-from\"\n             [ngClass]=\"{'owl-dt-container-info-active': activeSelectedIndex === 0}\"\n             (click)=\"handleClickOnInfoGroup($event, 0)\"\n             (keydown)=\"handleKeydownOnInfoGroup($event, to, 0)\" #from>\n            <span class=\"owl-dt-control-content owl-dt-container-range-content\" tabindex=\"-1\">\n                <span class=\"owl-dt-container-info-label\">{{fromLabel}}:</span>\n                <span class=\"owl-dt-container-info-value\">{{fromFormattedValue}}</span>\n            </span>\n        </div>\n        <div role=\"radio\" [tabindex]=\"activeSelectedIndex === 1 ? 0 : -1\"\n             [attr.aria-checked]=\"activeSelectedIndex === 1\"\n             class=\"owl-dt-control owl-dt-container-range owl-dt-container-to\"\n             [ngClass]=\"{'owl-dt-container-info-active': activeSelectedIndex === 1}\"\n             (click)=\"handleClickOnInfoGroup($event, 1)\"\n             (keydown)=\"handleKeydownOnInfoGroup($event, from, 1)\" #to>\n            <span class=\"owl-dt-control-content owl-dt-container-range-content\" tabindex=\"-1\">\n                <span class=\"owl-dt-container-info-label\">{{toLabel}}:</span>\n                <span class=\"owl-dt-container-info-value\">{{toFormattedValue}}</span>\n            </span>\n        </div>\n    </div>\n\n    <div *ngIf=\"showControlButtons\" class=\"owl-dt-container-buttons owl-dt-container-row\">\n        <button class=\"owl-dt-control owl-dt-control-button owl-dt-container-control-button\"\n                type=\"button\" tabindex=\"0\"\n                (click)=\"onCancelClicked($event)\">\n            <span class=\"owl-dt-control-content owl-dt-control-button-content\" tabindex=\"-1\">\n                {{cancelLabel}}\n            </span>\n        </button>\n        <button class=\"owl-dt-control owl-dt-control-button owl-dt-container-control-button\"\n                type=\"button\" tabindex=\"0\"\n                (click)=\"onSetClicked($event)\">\n            <span class=\"owl-dt-control-content owl-dt-control-button-content\" tabindex=\"-1\">\n                {{setLabel}}\n            </span>\n        </button>\n    </div>\n</div>\n"
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef
    }, {
      type: OwlDateTimeIntl
    }, {
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }];
  }, {
    calendar: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild,
      args: [OwlCalendarComponent]
    }],
    timer: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild,
      args: [OwlTimerComponent]
    }]
  });
})();

/**
 * date-time.class
 */
let nextUniqueId = 0;
class OwlDateTime {
  constructor(dateTimeAdapter, dateTimeFormats) {
    this.dateTimeAdapter = dateTimeAdapter;
    this.dateTimeFormats = dateTimeFormats;
    /**
     * Whether to show the second's timer
     */
    this._showSecondsTimer = false;
    /**
     * Whether the timer is in hour12 format
     */
    this._hour12Timer = false;
    /**
     * The view that the calendar should start in.
     */
    this.startView = 'month';
    /**
     * Hours to change per step
     */
    this._stepHour = 1;
    /**
     * Minutes to change per step
     */
    this._stepMinute = 1;
    /**
     * Seconds to change per step
     */
    this._stepSecond = 1;
    /**
     * Set the first day of week
     */
    this._firstDayOfWeek = 0;
    /**
     * Whether to hide dates in other months at the start or end of the current month.
     */
    this._hideOtherMonths = false;
    /**
     * Date Time Checker to check if the give dateTime is selectable
     */
    this.dateTimeChecker = dateTime => {
      return !!dateTime && (!this.dateTimeFilter || this.dateTimeFilter(dateTime)) && (!this.minDateTime || this.dateTimeAdapter.compare(dateTime, this.minDateTime) >= 0) && (!this.maxDateTime || this.dateTimeAdapter.compare(dateTime, this.maxDateTime) <= 0);
    };
    if (!this.dateTimeAdapter) {
      throw Error(`OwlDateTimePicker: No provider found for DateTimeAdapter. You must import one of the following ` + `modules at your application root: OwlNativeDateTimeModule, OwlDayjsDateTimeModule, or provide a ` + `custom implementation.`);
    }
    if (!this.dateTimeFormats) {
      throw Error(`OwlDateTimePicker: No provider found for OWL_DATE_TIME_FORMATS. You must import one of the following ` + `modules at your application root: OwlNativeDateTimeModule, OwlDayjsDateTimeModule, or provide a ` + `custom implementation.`);
    }
    this._id = `owl-dt-picker-${nextUniqueId++}`;
  }
  get showSecondsTimer() {
    return this._showSecondsTimer;
  }
  set showSecondsTimer(val) {
    this._showSecondsTimer = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceBooleanProperty)(val);
  }
  get hour12Timer() {
    return this._hour12Timer;
  }
  set hour12Timer(val) {
    this._hour12Timer = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceBooleanProperty)(val);
  }
  get stepHour() {
    return this._stepHour;
  }
  set stepHour(val) {
    this._stepHour = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceNumberProperty)(val, 1);
  }
  get stepMinute() {
    return this._stepMinute;
  }
  set stepMinute(val) {
    this._stepMinute = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceNumberProperty)(val, 1);
  }
  get stepSecond() {
    return this._stepSecond;
  }
  set stepSecond(val) {
    this._stepSecond = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceNumberProperty)(val, 1);
  }
  get firstDayOfWeek() {
    return this._firstDayOfWeek;
  }
  set firstDayOfWeek(value) {
    value = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceNumberProperty)(value, 0);
    if (value > 6 || value < 0) {
      this._firstDayOfWeek = 0;
    } else {
      this._firstDayOfWeek = value;
    }
  }
  get hideOtherMonths() {
    return this._hideOtherMonths;
  }
  set hideOtherMonths(val) {
    this._hideOtherMonths = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceBooleanProperty)(val);
  }
  get id() {
    return this._id;
  }
  get formatString() {
    return this.pickerType === 'both' ? this.dateTimeFormats.fullPickerInput : this.pickerType === 'calendar' ? this.dateTimeFormats.datePickerInput : this.dateTimeFormats.timePickerInput;
  }
  get disabled() {
    return false;
  }
  getValidDate(obj) {
    return this.dateTimeAdapter.isDateInstance(obj) && this.dateTimeAdapter.isValid(obj) ? obj : null;
  }
}
OwlDateTime.ɵfac = function OwlDateTime_Factory(t) {
  return new (t || OwlDateTime)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OWL_DATE_TIME_FORMATS, 8));
};
OwlDateTime.ɵdir = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineDirective"]({
  type: OwlDateTime,
  inputs: {
    showSecondsTimer: "showSecondsTimer",
    hour12Timer: "hour12Timer",
    startView: "startView",
    stepHour: "stepHour",
    stepMinute: "stepMinute",
    stepSecond: "stepSecond",
    firstDayOfWeek: "firstDayOfWeek",
    hideOtherMonths: "hideOtherMonths"
  }
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDateTime, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Directive
  }], function () {
    return [{
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DATE_TIME_FORMATS]
      }]
    }];
  }, {
    showSecondsTimer: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    hour12Timer: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    startView: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    stepHour: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    stepMinute: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    stepSecond: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    firstDayOfWeek: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    hideOtherMonths: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }]
  });
})();
let uniqueId = 0;
class OwlDialogConfig {
  constructor() {
    /**
     * ID of the element that describes the dialog.
     */
    this.ariaDescribedBy = null;
    /**
     * Whether to focus the dialog when the dialog is opened
     */
    this.autoFocus = true;
    /** Whether the dialog has a backdrop. */
    this.hasBackdrop = true;
    /** Data being injected into the child component. */
    this.data = null;
    /** Whether the user can use escape or clicking outside to close a modal. */
    this.disableClose = false;
    /**
     * The ARIA role of the dialog element.
     */
    this.role = 'dialog';
    /**
     * Custom class for the pane
     * */
    this.paneClass = '';
    /**
     * Mouse Event
     * */
    this.event = null;
    /**
     * Custom class for the backdrop
     * */
    this.backdropClass = '';
    /**
     * Whether the dialog should close when the user goes backwards/forwards in history.
     * */
    this.closeOnNavigation = true;
    /** Width of the dialog. */
    this.width = '';
    /** Height of the dialog. */
    this.height = '';
    /**
     * The max-width of the overlay panel.
     * If a number is provided, pixel units are assumed.
     * */
    this.maxWidth = '85vw';
    /**
     * The scroll strategy when the dialog is open
     * Learn more this from https://material.angular.io/cdk/overlay/overview#scroll-strategies
     * */
    this.scrollStrategy = new _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.NoopScrollStrategy();
    this.id = `owl-dialog-${uniqueId++}`;
  }
}
class OwlDialogRef {
  constructor(overlayRef, container, id, location) {
    this.overlayRef = overlayRef;
    this.container = container;
    this.id = id;
    this._beforeClose$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    this._afterOpen$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    this._afterClosed$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    /** Subscription to changes in the user's location. */
    this.locationChanged = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    /** Whether the user is allowed to close the dialog. */
    this.disableClose = this.container.config.disableClose;
    this.container.animationStateChanged.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_18__.filter)(event => event.phaseName === 'done' && event.toState === 'enter'), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1)).subscribe(() => {
      this._afterOpen$.next();
      this._afterOpen$.complete();
    });
    this.container.animationStateChanged.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_18__.filter)(event => event.phaseName === 'done' && event.toState === 'exit'), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1)).subscribe(() => {
      this.overlayRef.dispose();
      this.locationChanged.unsubscribe();
      this._afterClosed$.next(this.result);
      this._afterClosed$.complete();
      this.componentInstance = null;
    });
    this.overlayRef.keydownEvents().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_18__.filter)(event => event.keyCode === _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.ESCAPE && !this.disableClose)).subscribe(() => this.close());
    if (location) {
      this.locationChanged = location.subscribe(() => {
        if (this.container.config.closeOnNavigation) {
          this.close();
        }
      });
    }
  }
  close(dialogResult) {
    this.result = dialogResult;
    this.container.animationStateChanged.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_18__.filter)(event => event.phaseName === 'start'), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1)).subscribe(() => {
      this._beforeClose$.next(dialogResult);
      this._beforeClose$.complete();
      this.overlayRef.detachBackdrop();
    });
    this.container.startExitAnimation();
  }
  /**
   * Gets an observable that emits when the overlay's backdrop has been clicked.
   */
  backdropClick() {
    return this.overlayRef.backdropClick();
  }
  /**
   * Gets an observable that emits when keydown events are targeted on the overlay.
   */
  keydownEvents() {
    return this.overlayRef.keydownEvents();
  }
  /**
   * Updates the dialog's position.
   * @param position New dialog position.
   */
  updatePosition(position) {
    let strategy = this.getPositionStrategy();
    if (position && (position.left || position.right)) {
      position.left ? strategy.left(position.left) : strategy.right(position.right);
    } else {
      strategy.centerHorizontally();
    }
    if (position && (position.top || position.bottom)) {
      position.top ? strategy.top(position.top) : strategy.bottom(position.bottom);
    } else {
      strategy.centerVertically();
    }
    this.overlayRef.updatePosition();
    return this;
  }
  /**
   * Updates the dialog's width and height.
   * @param width New width of the dialog.
   * @param height New height of the dialog.
   */
  updateSize(width = 'auto', height = 'auto') {
    this.getPositionStrategy().width(width).height(height);
    this.overlayRef.updatePosition();
    return this;
  }
  isAnimating() {
    return this.container.isAnimating;
  }
  afterOpen() {
    return this._afterOpen$.asObservable();
  }
  beforeClose() {
    return this._beforeClose$.asObservable();
  }
  afterClosed() {
    return this._afterClosed$.asObservable();
  }
  /** Fetches the position strategy object from the overlay ref. */
  getPositionStrategy() {
    return this.overlayRef.getConfig().positionStrategy;
  }
}

/**
 * dialog-container.component
 */
const zoomFadeIn = {
  opacity: 0,
  transform: 'translateX({{ x }}) translateY({{ y }}) scale({{scale}})'
};
const zoomFadeInFrom = {
  opacity: 0,
  transform: 'translateX({{ x }}) translateY({{ y }}) scale({{scale}})',
  transformOrigin: '{{ ox }} {{ oy }}'
};
class OwlDialogContainerComponent extends _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.BasePortalOutlet {
  constructor(changeDetector, elementRef, focusTrapFactory, document) {
    super();
    this.changeDetector = changeDetector;
    this.elementRef = elementRef;
    this.focusTrapFactory = focusTrapFactory;
    this.document = document;
    /** ID of the element that should be considered as the dialog's label. */
    this.ariaLabelledBy = null;
    /** Emits when an animation state changes. */
    this.animationStateChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this.isAnimating = false;
    this.state = 'enter';
    // for animation purpose
    this.params = {
      x: '0px',
      y: '0px',
      ox: '50%',
      oy: '50%',
      scale: 0
    };
    // A variable to hold the focused element before the dialog was open.
    // This would help us to refocus back to element when the dialog was closed.
    this.elementFocusedBeforeDialogWasOpened = null;
  }
  get config() {
    return this._config;
  }
  get owlDialogContainerClass() {
    return true;
  }
  get owlDialogContainerTabIndex() {
    return -1;
  }
  get owlDialogContainerId() {
    return this._config.id;
  }
  get owlDialogContainerRole() {
    return this._config.role || null;
  }
  get owlDialogContainerAriaLabelledby() {
    return this.ariaLabelledBy;
  }
  get owlDialogContainerAriaDescribedby() {
    return this._config.ariaDescribedBy || null;
  }
  get owlDialogContainerAnimation() {
    return {
      value: this.state,
      params: this.params
    };
  }
  ngOnInit() {}
  /**
   * Attach a ComponentPortal as content to this dialog container.
   */
  attachComponentPortal(portal) {
    if (this.portalOutlet.hasAttached()) {
      throw Error('Attempting to attach dialog content after content is already attached');
    }
    this.savePreviouslyFocusedElement();
    return this.portalOutlet.attachComponentPortal(portal);
  }
  attachTemplatePortal(portal) {
    throw new Error('Method not implemented.');
  }
  setConfig(config) {
    this._config = config;
    if (config.event) {
      this.calculateZoomOrigin(event);
    }
  }
  onAnimationStart(event) {
    this.isAnimating = true;
    this.animationStateChanged.emit(event);
  }
  onAnimationDone(event) {
    if (event.toState === 'enter') {
      this.trapFocus();
    } else if (event.toState === 'exit') {
      this.restoreFocus();
    }
    this.animationStateChanged.emit(event);
    this.isAnimating = false;
  }
  startExitAnimation() {
    this.state = 'exit';
    this.changeDetector.markForCheck();
  }
  /**
   * Calculate origin used in the `zoomFadeInFrom()`
   * for animation purpose
   */
  calculateZoomOrigin(event) {
    if (!event) {
      return;
    }
    const clientX = event.clientX;
    const clientY = event.clientY;
    const wh = window.innerWidth / 2;
    const hh = window.innerHeight / 2;
    const x = clientX - wh;
    const y = clientY - hh;
    const ox = clientX / window.innerWidth;
    const oy = clientY / window.innerHeight;
    this.params.x = `${x}px`;
    this.params.y = `${y}px`;
    this.params.ox = `${ox * 100}%`;
    this.params.oy = `${oy * 100}%`;
    this.params.scale = 0;
    return;
  }
  /**
   * Save the focused element before dialog was open
   */
  savePreviouslyFocusedElement() {
    if (this.document) {
      this.elementFocusedBeforeDialogWasOpened = this.document.activeElement;
      Promise.resolve().then(() => this.elementRef.nativeElement.focus());
    }
  }
  trapFocus() {
    if (!this.focusTrap) {
      this.focusTrap = this.focusTrapFactory.create(this.elementRef.nativeElement);
    }
    if (this._config.autoFocus) {
      this.focusTrap.focusInitialElementWhenReady();
    }
  }
  restoreFocus() {
    const toFocus = this.elementFocusedBeforeDialogWasOpened;
    // We need the extra check, because IE can set the `activeElement` to null in some cases.
    if (toFocus && typeof toFocus.focus === 'function') {
      toFocus.focus();
    }
    if (this.focusTrap) {
      this.focusTrap.destroy();
    }
  }
}
OwlDialogContainerComponent.ɵfac = function OwlDialogContainerComponent_Factory(t) {
  return new (t || OwlDialogContainerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_14__.FocusTrapFactory), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_11__.DOCUMENT, 8));
};
OwlDialogContainerComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlDialogContainerComponent,
  selectors: [["owl-dialog-container"]],
  viewQuery: function OwlDialogContainerComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵviewQuery"](_angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.CdkPortalOutlet, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵloadQuery"]()) && (ctx.portalOutlet = _t.first);
    }
  },
  hostVars: 8,
  hostBindings: function OwlDialogContainerComponent_HostBindings(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsyntheticHostListener"]("@slideModal.start", function OwlDialogContainerComponent_animation_slideModal_start_HostBindingHandler($event) {
        return ctx.onAnimationStart($event);
      })("@slideModal.done", function OwlDialogContainerComponent_animation_slideModal_done_HostBindingHandler($event) {
        return ctx.onAnimationDone($event);
      });
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("tabindex", ctx.owlDialogContainerTabIndex)("id", ctx.owlDialogContainerId)("role", ctx.owlDialogContainerRole)("aria-labelledby", ctx.owlDialogContainerAriaLabelledby)("aria-describedby", ctx.owlDialogContainerAriaDescribedby);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsyntheticHostProperty"]("@slideModal", ctx.owlDialogContainerAnimation);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dialog-container", ctx.owlDialogContainerClass);
    }
  },
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵInheritDefinitionFeature"]],
  decls: 1,
  vars: 0,
  consts: [["cdkPortalOutlet", ""]],
  template: function OwlDialogContainerComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](0, OwlDialogContainerComponent_ng_template_0_Template, 0, 0, "ng-template", 0);
    }
  },
  dependencies: [_angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.CdkPortalOutlet],
  encapsulation: 2,
  data: {
    animation: [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.trigger)('slideModal', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.transition)('void => enter', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)(zoomFadeInFrom), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animate)('300ms cubic-bezier(0.35, 0, 0.25, 1)', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)('*')), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animate)('150ms', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.keyframes)([(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
      transform: 'scale(1)',
      offset: 0
    }), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
      transform: 'scale(1.05)',
      offset: 0.3
    }), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
      transform: 'scale(.95)',
      offset: 0.8
    }), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
      transform: 'scale(1)',
      offset: 1.0
    })])), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animateChild)()], {
      params: {
        x: '0px',
        y: '0px',
        ox: '50%',
        oy: '50%',
        scale: 1
      }
    }), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.transition)('enter => exit', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animateChild)(), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animate)(200, (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)(zoomFadeIn))], {
      params: {
        x: '0px',
        y: '0px',
        ox: '50%',
        oy: '50%'
      }
    })])]
  }
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDialogContainerComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      selector: 'owl-dialog-container',
      animations: [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.trigger)('slideModal', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.transition)('void => enter', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)(zoomFadeInFrom), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animate)('300ms cubic-bezier(0.35, 0, 0.25, 1)', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)('*')), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animate)('150ms', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.keyframes)([(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
        transform: 'scale(1)',
        offset: 0
      }), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
        transform: 'scale(1.05)',
        offset: 0.3
      }), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
        transform: 'scale(.95)',
        offset: 0.8
      }), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)({
        transform: 'scale(1)',
        offset: 1.0
      })])), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animateChild)()], {
        params: {
          x: '0px',
          y: '0px',
          ox: '50%',
          oy: '50%',
          scale: 1
        }
      }), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.transition)('enter => exit', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animateChild)(), (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.animate)(200, (0,_angular_animations__WEBPACK_IMPORTED_MODULE_16__.style)(zoomFadeIn))], {
        params: {
          x: '0px',
          y: '0px',
          ox: '50%',
          oy: '50%'
        }
      })])],
      host: {
        '(@slideModal.start)': 'onAnimationStart($event)',
        '(@slideModal.done)': 'onAnimationDone($event)',
        '[class.owl-dialog-container]': 'owlDialogContainerClass',
        '[attr.tabindex]': 'owlDialogContainerTabIndex',
        '[attr.id]': 'owlDialogContainerId',
        '[attr.role]': 'owlDialogContainerRole',
        '[attr.aria-labelledby]': 'owlDialogContainerAriaLabelledby',
        '[attr.aria-describedby]': 'owlDialogContainerAriaDescribedby',
        '[@slideModal]': 'owlDialogContainerAnimation'
      },
      template: "<ng-template cdkPortalOutlet></ng-template>\n"
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef
    }, {
      type: _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_14__.FocusTrapFactory
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.DOCUMENT]
      }]
    }];
  }, {
    portalOutlet: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild,
      args: [_angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.CdkPortalOutlet, {
        static: true
      }]
    }]
  });
})();

/**
 * object.utils
 */
/**
 * Extends an object with the *enumerable* and *own* properties of one or more source objects,
 * similar to Object.assign.
 *
 * @param dest The object which will have properties copied to it.
 * @param sources The source objects from which properties will be copied.
 */
function extendObject(dest, ...sources) {
  if (dest == null) {
    throw TypeError('Cannot convert undefined or null to object');
  }
  for (const source of sources) {
    if (source != null) {
      for (const key in source) {
        if (source.hasOwnProperty(key)) {
          dest[key] = source[key];
        }
      }
    }
  }
  return dest;
}

/**
 * index
 */

/**
 * dialog.service
 */
const OWL_DIALOG_DATA = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.InjectionToken('OwlDialogData');
/**
 * Injection token that determines the scroll handling while the dialog is open.
 * */
const OWL_DIALOG_SCROLL_STRATEGY = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.InjectionToken('owl-dialog-scroll-strategy');
function OWL_DIALOG_SCROLL_STRATEGY_PROVIDER_FACTORY(overlay) {
  const fn = () => overlay.scrollStrategies.block();
  return fn;
}
/** @docs-private */
const OWL_DIALOG_SCROLL_STRATEGY_PROVIDER = {
  provide: OWL_DIALOG_SCROLL_STRATEGY,
  deps: [_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.Overlay],
  useFactory: OWL_DIALOG_SCROLL_STRATEGY_PROVIDER_FACTORY
};
/** I
 * njection token that can be used to specify default dialog options.
 * */
const OWL_DIALOG_DEFAULT_OPTIONS = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.InjectionToken('owl-dialog-default-options');
class OwlDialogService {
  constructor(overlay, injector, location, scrollStrategy, defaultOptions, parentDialog, overlayContainer) {
    this.overlay = overlay;
    this.injector = injector;
    this.location = location;
    this.defaultOptions = defaultOptions;
    this.parentDialog = parentDialog;
    this.overlayContainer = overlayContainer;
    this.ariaHiddenElements = new Map();
    this._openDialogsAtThisLevel = [];
    this._afterOpenAtThisLevel = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    this._afterAllClosedAtThisLevel = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    /**
     * Stream that emits when all open dialog have finished closing.
     * Will emit on subscribe if there are no open dialogs to begin with.
     */
    this.afterAllClosed = (0,rxjs__WEBPACK_IMPORTED_MODULE_20__.defer)(() => this._openDialogsAtThisLevel.length ? this._afterAllClosed : this._afterAllClosed.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_21__.startWith)(undefined)));
    this.scrollStrategy = scrollStrategy;
    if (!parentDialog && location) {
      location.subscribe(() => this.closeAll());
    }
  }
  /** Keeps track of the currently-open dialogs. */
  get openDialogs() {
    return this.parentDialog ? this.parentDialog.openDialogs : this._openDialogsAtThisLevel;
  }
  /** Stream that emits when a dialog has been opened. */
  get afterOpen() {
    return this.parentDialog ? this.parentDialog.afterOpen : this._afterOpenAtThisLevel;
  }
  get _afterAllClosed() {
    const parent = this.parentDialog;
    return parent ? parent._afterAllClosed : this._afterAllClosedAtThisLevel;
  }
  open(componentOrTemplateRef, config) {
    config = applyConfigDefaults(config, this.defaultOptions);
    if (config.id && this.getDialogById(config.id)) {
      throw Error(`Dialog with id "${config.id}" exists already. The dialog id must be unique.`);
    }
    const overlayRef = this.createOverlay(config);
    const dialogContainer = this.attachDialogContainer(overlayRef, config);
    const dialogRef = this.attachDialogContent(componentOrTemplateRef, dialogContainer, overlayRef, config);
    if (!this.openDialogs.length) {
      this.hideNonDialogContentFromAssistiveTechnology();
    }
    this.openDialogs.push(dialogRef);
    dialogRef.afterClosed().subscribe(() => this.removeOpenDialog(dialogRef));
    this.afterOpen.next(dialogRef);
    return dialogRef;
  }
  /**
   * Closes all of the currently-open dialogs.
   */
  closeAll() {
    let i = this.openDialogs.length;
    while (i--) {
      this.openDialogs[i].close();
    }
  }
  /**
   * Finds an open dialog by its id.
   * @param id ID to use when looking up the dialog.
   */
  getDialogById(id) {
    return this.openDialogs.find(dialog => dialog.id === id);
  }
  attachDialogContent(componentOrTemplateRef, dialogContainer, overlayRef, config) {
    const dialogRef = new OwlDialogRef(overlayRef, dialogContainer, config.id, this.location);
    if (config.hasBackdrop) {
      overlayRef.backdropClick().subscribe(() => {
        if (!dialogRef.disableClose) {
          dialogRef.close();
        }
      });
    }
    if (componentOrTemplateRef instanceof _angular_core__WEBPACK_IMPORTED_MODULE_5__.TemplateRef) {} else {
      const injector = this.createInjector(config, dialogRef, dialogContainer);
      const contentRef = dialogContainer.attachComponentPortal(new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.ComponentPortal(componentOrTemplateRef, undefined, injector));
      dialogRef.componentInstance = contentRef.instance;
    }
    dialogRef.updateSize(config.width, config.height).updatePosition(config.position);
    return dialogRef;
  }
  createInjector(config, dialogRef, dialogContainer) {
    const userInjector = config && config.viewContainerRef && config.viewContainerRef.injector;
    const injectionTokens = new WeakMap();
    injectionTokens.set(OwlDialogRef, dialogRef);
    injectionTokens.set(OwlDialogContainerComponent, dialogContainer);
    injectionTokens.set(OWL_DIALOG_DATA, config.data);
    return new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.PortalInjector(userInjector || this.injector, injectionTokens);
  }
  createOverlay(config) {
    const overlayConfig = this.getOverlayConfig(config);
    return this.overlay.create(overlayConfig);
  }
  attachDialogContainer(overlayRef, config) {
    const containerPortal = new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.ComponentPortal(OwlDialogContainerComponent, config.viewContainerRef);
    const containerRef = overlayRef.attach(containerPortal);
    containerRef.instance.setConfig(config);
    return containerRef.instance;
  }
  getOverlayConfig(dialogConfig) {
    const state = new _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.OverlayConfig({
      positionStrategy: this.overlay.position().global(),
      scrollStrategy: dialogConfig.scrollStrategy || this.scrollStrategy(),
      panelClass: dialogConfig.paneClass,
      hasBackdrop: dialogConfig.hasBackdrop,
      minWidth: dialogConfig.minWidth,
      minHeight: dialogConfig.minHeight,
      maxWidth: dialogConfig.maxWidth,
      maxHeight: dialogConfig.maxHeight
    });
    if (dialogConfig.backdropClass) {
      state.backdropClass = dialogConfig.backdropClass;
    }
    return state;
  }
  removeOpenDialog(dialogRef) {
    const index = this._openDialogsAtThisLevel.indexOf(dialogRef);
    if (index > -1) {
      this.openDialogs.splice(index, 1);
      // If all the dialogs were closed, remove/restore the `aria-hidden`
      // to a the siblings and emit to the `afterAllClosed` stream.
      if (!this.openDialogs.length) {
        this.ariaHiddenElements.forEach((previousValue, element) => {
          if (previousValue) {
            element.setAttribute('aria-hidden', previousValue);
          } else {
            element.removeAttribute('aria-hidden');
          }
        });
        this.ariaHiddenElements.clear();
        this._afterAllClosed.next();
      }
    }
  }
  /**
   * Hides all of the content that isn't an overlay from assistive technology.
   */
  hideNonDialogContentFromAssistiveTechnology() {
    const overlayContainer = this.overlayContainer.getContainerElement();
    // Ensure that the overlay container is attached to the DOM.
    if (overlayContainer.parentElement) {
      const siblings = overlayContainer.parentElement.children;
      for (let i = siblings.length - 1; i > -1; i--) {
        let sibling = siblings[i];
        if (sibling !== overlayContainer && sibling.nodeName !== 'SCRIPT' && sibling.nodeName !== 'STYLE' && !sibling.hasAttribute('aria-live')) {
          this.ariaHiddenElements.set(sibling, sibling.getAttribute('aria-hidden'));
          sibling.setAttribute('aria-hidden', 'true');
        }
      }
    }
  }
}
OwlDialogService.ɵfac = function OwlDialogService_Factory(t) {
  return new (t || OwlDialogService)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.Overlay), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](_angular_common__WEBPACK_IMPORTED_MODULE_11__.Location, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](OWL_DIALOG_SCROLL_STRATEGY), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](OWL_DIALOG_DEFAULT_OPTIONS, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](OwlDialogService, 12), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.OverlayContainer));
};
OwlDialogService.ɵprov = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjectable"]({
  token: OwlDialogService,
  factory: OwlDialogService.ɵfac
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDialogService, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable
  }], function () {
    return [{
      type: _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.Overlay
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector
    }, {
      type: _angular_common__WEBPACK_IMPORTED_MODULE_11__.Location,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DIALOG_SCROLL_STRATEGY]
      }]
    }, {
      type: OwlDialogConfig,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DIALOG_DEFAULT_OPTIONS]
      }]
    }, {
      type: OwlDialogService,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.SkipSelf
      }]
    }, {
      type: _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.OverlayContainer
    }];
  }, null);
})();
/**
 * Applies default options to the dialog config.
 * @param config Config to be modified.
 * @param defaultOptions Default config setting
 * @returns The new configuration object.
 */
function applyConfigDefaults(config, defaultOptions) {
  return extendObject(new OwlDialogConfig(), config, defaultOptions);
}

/**
 * date-time-picker.component
 */
/** Injection token that determines the scroll handling while the dtPicker is open. */
const OWL_DTPICKER_SCROLL_STRATEGY = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.InjectionToken('owl-dtpicker-scroll-strategy');
/** @docs-private */
function OWL_DTPICKER_SCROLL_STRATEGY_PROVIDER_FACTORY(overlay) {
  const fn = () => overlay.scrollStrategies.block();
  return fn;
}
/** @docs-private */
const OWL_DTPICKER_SCROLL_STRATEGY_PROVIDER = {
  provide: OWL_DTPICKER_SCROLL_STRATEGY,
  deps: [_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.Overlay],
  useFactory: OWL_DTPICKER_SCROLL_STRATEGY_PROVIDER_FACTORY
};
class OwlDateTimeComponent extends OwlDateTime {
  constructor(overlay, viewContainerRef, dialogService, ngZone, changeDetector, dateTimeAdapter, defaultScrollStrategy, dateTimeFormats, document) {
    super(dateTimeAdapter, dateTimeFormats);
    this.overlay = overlay;
    this.viewContainerRef = viewContainerRef;
    this.dialogService = dialogService;
    this.ngZone = ngZone;
    this.changeDetector = changeDetector;
    this.dateTimeAdapter = dateTimeAdapter;
    this.dateTimeFormats = dateTimeFormats;
    this.document = document;
    /** Custom class for the picker backdrop. */
    this.backdropClass = [];
    /** Custom class for the picker overlay pane. */
    this.panelClass = [];
    /**
     * Set the type of the dateTime picker
     *      'both' -- show both calendar and timer
     *      'calendar' -- show only calendar
     *      'timer' -- show only timer
     */
    this._pickerType = 'both';
    /**
     * Whether the picker open as a dialog
     */
    this._pickerMode = 'popup';
    /** Whether the calendar is open. */
    this._opened = false;
    /**
     * Callback when the picker is closed
     * */
    this.afterPickerClosed = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Callback when the picker is open
     * */
    this.afterPickerOpen = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Emits selected year in multi-year view
     * This doesn't imply a change on the selected date.
     * */
    this.yearSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Emits selected month in year view
     * This doesn't imply a change on the selected date.
     * */
    this.monthSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Emit when the selected value has been confirmed
     * */
    this.confirmSelectedChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Emits when the date time picker is disabled.
     * */
    this.disabledChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this.dtInputSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    this.hidePickerStreamSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    this.confirmSelectedStreamSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    this.pickerOpenedStreamSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    /** The element that was focused before the date time picker was opened. */
    this.focusedElementBeforeOpen = null;
    this._selecteds = [];
    this.defaultScrollStrategy = defaultScrollStrategy;
  }
  get startAt() {
    // If an explicit startAt is set we start there, otherwise we start at whatever the currently
    // selected value is.
    if (this._startAt) {
      return this._startAt;
    }
    if (this._dtInput) {
      if (this._dtInput.selectMode === 'single') {
        return this._dtInput.value || null;
      } else if (this._dtInput.selectMode === 'range' || this._dtInput.selectMode === 'rangeFrom') {
        return this._dtInput.values[0] || null;
      } else if (this._dtInput.selectMode === 'rangeTo') {
        return this._dtInput.values[1] || null;
      }
    } else {
      return null;
    }
  }
  set startAt(date) {
    this._startAt = this.getValidDate(this.dateTimeAdapter.deserialize(date));
  }
  get pickerType() {
    return this._pickerType;
  }
  set pickerType(val) {
    if (val !== this._pickerType) {
      this._pickerType = val;
      if (this._dtInput) {
        this._dtInput.formatNativeInputValue();
      }
    }
  }
  get pickerMode() {
    return this._pickerMode;
  }
  set pickerMode(mode) {
    if (mode === 'popup') {
      this._pickerMode = mode;
    } else {
      this._pickerMode = 'dialog';
    }
  }
  get disabled() {
    return this._disabled === undefined && this._dtInput ? this._dtInput.disabled : !!this._disabled;
  }
  set disabled(value) {
    value = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceBooleanProperty)(value);
    if (value !== this._disabled) {
      this._disabled = value;
      this.disabledChange.next(value);
    }
  }
  get opened() {
    return this._opened;
  }
  set opened(val) {
    val ? this.open() : this.close();
  }
  get dtInput() {
    return this._dtInput;
  }
  get selected() {
    return this._selected;
  }
  set selected(value) {
    this._selected = value;
    this.changeDetector.markForCheck();
  }
  get selecteds() {
    return this._selecteds;
  }
  set selecteds(values) {
    this._selecteds = values;
    this.changeDetector.markForCheck();
  }
  /** The minimum selectable date. */
  get minDateTime() {
    return this._dtInput && this._dtInput.min;
  }
  /** The maximum selectable date. */
  get maxDateTime() {
    return this._dtInput && this._dtInput.max;
  }
  get dateTimeFilter() {
    return this._dtInput && this._dtInput.dateTimeFilter;
  }
  get selectMode() {
    return this._dtInput.selectMode;
  }
  get isInSingleMode() {
    return this._dtInput.isInSingleMode;
  }
  get isInRangeMode() {
    return this._dtInput.isInRangeMode;
  }
  ngOnInit() {}
  ngOnDestroy() {
    this.close();
    this.dtInputSub.unsubscribe();
    this.disabledChange.complete();
    if (this.popupRef) {
      this.popupRef.dispose();
    }
  }
  registerInput(input) {
    if (this._dtInput) {
      throw Error('A Owl DateTimePicker can only be associated with a single input.');
    }
    this._dtInput = input;
    this.dtInputSub = this._dtInput.valueChange.subscribe(value => {
      if (Array.isArray(value)) {
        this.selecteds = value;
      } else {
        this.selected = value;
      }
    });
  }
  open() {
    if (this._opened || this.disabled) {
      return;
    }
    if (!this._dtInput) {
      throw Error('Attempted to open an DateTimePicker with no associated input.');
    }
    if (this.document) {
      this.focusedElementBeforeOpen = this.document.activeElement;
    }
    // reset the picker selected value
    if (this.isInSingleMode) {
      this.selected = this._dtInput.value;
    } else if (this.isInRangeMode) {
      this.selecteds = this._dtInput.values;
    }
    // when the picker is open , we make sure the picker's current selected time value
    // is the same as the _startAt time value.
    if (this.selected && this.pickerType !== 'calendar' && this._startAt) {
      this.selected = this.dateTimeAdapter.createDate(this.dateTimeAdapter.getYear(this.selected), this.dateTimeAdapter.getMonth(this.selected), this.dateTimeAdapter.getDate(this.selected), this.dateTimeAdapter.getHours(this._startAt), this.dateTimeAdapter.getMinutes(this._startAt), this.dateTimeAdapter.getSeconds(this._startAt));
    }
    this.pickerMode === 'dialog' ? this.openAsDialog() : this.openAsPopup();
    this.pickerContainer.picker = this;
    // Listen to picker container's hidePickerStream
    this.hidePickerStreamSub = this.pickerContainer.hidePickerStream.subscribe(() => {
      this.close();
    });
    // Listen to picker container's confirmSelectedStream
    this.confirmSelectedStreamSub = this.pickerContainer.confirmSelectedStream.subscribe(event => {
      this.confirmSelect(event);
    });
  }
  /**
   * Selects the given date
   */
  select(date) {
    if (Array.isArray(date)) {
      this.selecteds = [...date];
    } else {
      this.selected = date;
    }
    /**
     * Cases in which automatically confirm the select when date or dates are selected:
     * 1) picker mode is NOT 'dialog'
     * 2) picker type is 'calendar' and selectMode is 'single'.
     * 3) picker type is 'calendar' and selectMode is 'range' and
     *    the 'selecteds' has 'from'(selecteds[0]) and 'to'(selecteds[1]) values.
     * 4) selectMode is 'rangeFrom' and selecteds[0] has value.
     * 5) selectMode is 'rangeTo' and selecteds[1] has value.
     * */
    if (this.pickerMode !== 'dialog' && this.pickerType === 'calendar' && (this.selectMode === 'single' && this.selected || this.selectMode === 'rangeFrom' && this.selecteds[0] || this.selectMode === 'rangeTo' && this.selecteds[1] || this.selectMode === 'range' && this.selecteds[0] && this.selecteds[1])) {
      this.confirmSelect();
    }
  }
  /**
   * Emits the selected year in multi-year view
   * */
  selectYear(normalizedYear) {
    this.yearSelected.emit(normalizedYear);
  }
  /**
   * Emits selected month in year view
   * */
  selectMonth(normalizedMonth) {
    this.monthSelected.emit(normalizedMonth);
  }
  /**
   * Hide the picker
   */
  close() {
    if (!this._opened) {
      return;
    }
    if (this.popupRef && this.popupRef.hasAttached()) {
      this.popupRef.detach();
    }
    if (this.pickerContainerPortal && this.pickerContainerPortal.isAttached) {
      this.pickerContainerPortal.detach();
    }
    if (this.hidePickerStreamSub) {
      this.hidePickerStreamSub.unsubscribe();
      this.hidePickerStreamSub = null;
    }
    if (this.confirmSelectedStreamSub) {
      this.confirmSelectedStreamSub.unsubscribe();
      this.confirmSelectedStreamSub = null;
    }
    if (this.pickerOpenedStreamSub) {
      this.pickerOpenedStreamSub.unsubscribe();
      this.pickerOpenedStreamSub = null;
    }
    if (this.dialogRef) {
      this.dialogRef.close();
      this.dialogRef = null;
    }
    const completeClose = () => {
      if (this._opened) {
        this._opened = false;
        this.afterPickerClosed.emit(null);
        this.focusedElementBeforeOpen = null;
      }
    };
    if (this.focusedElementBeforeOpen && typeof this.focusedElementBeforeOpen.focus === 'function') {
      // Because IE moves focus asynchronously, we can't count on it being restored before we've
      // marked the datepicker as closed. If the event fires out of sequence and the element that
      // we're refocusing opens the datepicker on focus, the user could be stuck with not being
      // able to close the calendar at all. We work around it by making the logic, that marks
      // the datepicker as closed, async as well.
      this.focusedElementBeforeOpen.focus();
      setTimeout(completeClose);
    } else {
      completeClose();
    }
  }
  /**
   * Confirm the selected value
   */
  confirmSelect(event) {
    if (this.isInSingleMode) {
      const selected = this.selected || this.startAt || this.dateTimeAdapter.now();
      this.confirmSelectedChange.emit(selected);
    } else if (this.isInRangeMode) {
      this.confirmSelectedChange.emit(this.selecteds);
    }
    this.close();
    return;
  }
  /**
   * Open the picker as a dialog
   */
  openAsDialog() {
    this.dialogRef = this.dialogService.open(OwlDateTimeContainerComponent, {
      autoFocus: false,
      backdropClass: ['cdk-overlay-dark-backdrop', ...(0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceArray)(this.backdropClass)],
      paneClass: ['owl-dt-dialog', ...(0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceArray)(this.panelClass)],
      viewContainerRef: this.viewContainerRef,
      scrollStrategy: this.scrollStrategy || this.defaultScrollStrategy()
    });
    this.pickerContainer = this.dialogRef.componentInstance;
    this.dialogRef.afterOpen().subscribe(() => {
      this.afterPickerOpen.emit(null);
      this._opened = true;
    });
    this.dialogRef.afterClosed().subscribe(() => this.close());
  }
  /**
   * Open the picker as popup
   */
  openAsPopup() {
    if (!this.pickerContainerPortal) {
      this.pickerContainerPortal = new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.ComponentPortal(OwlDateTimeContainerComponent, this.viewContainerRef);
    }
    if (!this.popupRef) {
      this.createPopup();
    }
    if (!this.popupRef.hasAttached()) {
      const componentRef = this.popupRef.attach(this.pickerContainerPortal);
      this.pickerContainer = componentRef.instance;
      // Update the position once the calendar has rendered.
      this.ngZone.onStable.asObservable().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1)).subscribe(() => {
        this.popupRef.updatePosition();
      });
      // emit open stream
      this.pickerOpenedStreamSub = this.pickerContainer.pickerOpenedStream.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1)).subscribe(() => {
        this.afterPickerOpen.emit(null);
        this._opened = true;
      });
    }
  }
  createPopup() {
    const overlayConfig = new _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.OverlayConfig({
      positionStrategy: this.createPopupPositionStrategy(),
      hasBackdrop: true,
      backdropClass: ['cdk-overlay-transparent-backdrop', ...(0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceArray)(this.backdropClass)],
      scrollStrategy: this.scrollStrategy || this.defaultScrollStrategy(),
      panelClass: ['owl-dt-popup', ...(0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceArray)(this.panelClass)]
    });
    this.popupRef = this.overlay.create(overlayConfig);
    (0,rxjs__WEBPACK_IMPORTED_MODULE_8__.merge)(this.popupRef.backdropClick(), this.popupRef.detachments(), this.popupRef.keydownEvents().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_18__.filter)(event => event.keyCode === _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.ESCAPE || this._dtInput && event.altKey && event.keyCode === _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.UP_ARROW))).subscribe(() => this.close());
  }
  /**
   * Create the popup PositionStrategy.
   * */
  createPopupPositionStrategy() {
    return this.overlay.position().flexibleConnectedTo(this._dtInput.elementRef).withTransformOriginOn('.owl-dt-container').withFlexibleDimensions(false).withPush(false).withPositions([{
      originX: 'start',
      originY: 'bottom',
      overlayX: 'start',
      overlayY: 'top'
    }, {
      originX: 'start',
      originY: 'top',
      overlayX: 'start',
      overlayY: 'bottom'
    }, {
      originX: 'end',
      originY: 'bottom',
      overlayX: 'end',
      overlayY: 'top'
    }, {
      originX: 'end',
      originY: 'top',
      overlayX: 'end',
      overlayY: 'bottom'
    }, {
      originX: 'start',
      originY: 'top',
      overlayX: 'start',
      overlayY: 'top',
      offsetY: -176
    }, {
      originX: 'start',
      originY: 'top',
      overlayX: 'start',
      overlayY: 'top',
      offsetY: -352
    }]);
  }
}
OwlDateTimeComponent.ɵfac = function OwlDateTimeComponent_Factory(t) {
  return new (t || OwlDateTimeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.Overlay), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewContainerRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OwlDialogService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgZone), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OWL_DTPICKER_SCROLL_STRATEGY), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OWL_DATE_TIME_FORMATS, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_11__.DOCUMENT, 8));
};
OwlDateTimeComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlDateTimeComponent,
  selectors: [["owl-date-time"]],
  inputs: {
    backdropClass: "backdropClass",
    panelClass: "panelClass",
    startAt: "startAt",
    pickerType: "pickerType",
    pickerMode: "pickerMode",
    disabled: "disabled",
    opened: "opened",
    scrollStrategy: "scrollStrategy"
  },
  outputs: {
    afterPickerClosed: "afterPickerClosed",
    afterPickerOpen: "afterPickerOpen",
    yearSelected: "yearSelected",
    monthSelected: "monthSelected"
  },
  exportAs: ["owlDateTime"],
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵInheritDefinitionFeature"]],
  decls: 0,
  vars: 0,
  template: function OwlDateTimeComponent_Template(rf, ctx) {},
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDateTimeComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      selector: 'owl-date-time',
      exportAs: 'owlDateTime',
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      preserveWhitespaces: false,
      template: ""
    }]
  }], function () {
    return [{
      type: _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.Overlay
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewContainerRef
    }, {
      type: OwlDialogService
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgZone
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }, {
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DTPICKER_SCROLL_STRATEGY]
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DATE_TIME_FORMATS]
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.DOCUMENT]
      }]
    }];
  }, {
    backdropClass: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    panelClass: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    startAt: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    pickerType: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    pickerMode: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    disabled: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    opened: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    scrollStrategy: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    afterPickerClosed: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    afterPickerOpen: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    yearSelected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    monthSelected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }]
  });
})();

/**
 * date-time-picker-input.directive
 */
const OWL_DATETIME_VALUE_ACCESSOR$1 = {
  provide: _angular_forms__WEBPACK_IMPORTED_MODULE_22__.NG_VALUE_ACCESSOR,
  useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.forwardRef)(() => OwlDateTimeInputDirective),
  multi: true
};
const OWL_DATETIME_VALIDATORS = {
  provide: _angular_forms__WEBPACK_IMPORTED_MODULE_22__.NG_VALIDATORS,
  useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.forwardRef)(() => OwlDateTimeInputDirective),
  multi: true
};
class OwlDateTimeInputDirective {
  constructor(elmRef, renderer, dateTimeAdapter, dateTimeFormats) {
    this.elmRef = elmRef;
    this.renderer = renderer;
    this.dateTimeAdapter = dateTimeAdapter;
    this.dateTimeFormats = dateTimeFormats;
    /**
     * The picker's select mode
     */
    this._selectMode = 'single';
    /**
     * The character to separate the 'from' and 'to' in input value
     */
    this.rangeSeparator = '~';
    this._values = [];
    /**
     * Callback to invoke when `change` event is fired on this `<input>`
     * */
    this.dateTimeChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Callback to invoke when an `input` event is fired on this `<input>`.
     * */
    this.dateTimeInput = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this.dtPickerSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    this.localeSub = rxjs__WEBPACK_IMPORTED_MODULE_6__.Subscription.EMPTY;
    this.lastValueValid = true;
    this.onModelChange = () => {};
    this.onModelTouched = () => {};
    this.validatorOnChange = () => {};
    /** The form control validator for whether the input parses. */
    this.parseValidator = () => {
      return this.lastValueValid ? null : {
        owlDateTimeParse: {
          text: this.elmRef.nativeElement.value
        }
      };
    };
    /** The form control validator for the min date. */
    this.minValidator = control => {
      if (this.isInSingleMode) {
        const controlValue = this.getValidDate(this.dateTimeAdapter.deserialize(control.value));
        return !this.min || !controlValue || this.dateTimeAdapter.compare(this.min, controlValue) <= 0 ? null : {
          owlDateTimeMin: {
            min: this.min,
            actual: controlValue
          }
        };
      } else if (this.isInRangeMode && control.value) {
        const controlValueFrom = this.getValidDate(this.dateTimeAdapter.deserialize(control.value[0]));
        const controlValueTo = this.getValidDate(this.dateTimeAdapter.deserialize(control.value[1]));
        return !this.min || !controlValueFrom || !controlValueTo || this.dateTimeAdapter.compare(this.min, controlValueFrom) <= 0 ? null : {
          owlDateTimeMin: {
            min: this.min,
            actual: [controlValueFrom, controlValueTo]
          }
        };
      }
    };
    /** The form control validator for the max date. */
    this.maxValidator = control => {
      if (this.isInSingleMode) {
        const controlValue = this.getValidDate(this.dateTimeAdapter.deserialize(control.value));
        return !this.max || !controlValue || this.dateTimeAdapter.compare(this.max, controlValue) >= 0 ? null : {
          owlDateTimeMax: {
            max: this.max,
            actual: controlValue
          }
        };
      } else if (this.isInRangeMode && control.value) {
        const controlValueFrom = this.getValidDate(this.dateTimeAdapter.deserialize(control.value[0]));
        const controlValueTo = this.getValidDate(this.dateTimeAdapter.deserialize(control.value[1]));
        return !this.max || !controlValueFrom || !controlValueTo || this.dateTimeAdapter.compare(this.max, controlValueTo) >= 0 ? null : {
          owlDateTimeMax: {
            max: this.max,
            actual: [controlValueFrom, controlValueTo]
          }
        };
      }
    };
    /** The form control validator for the date filter. */
    this.filterValidator = control => {
      const controlValue = this.getValidDate(this.dateTimeAdapter.deserialize(control.value));
      return !this._dateTimeFilter || !controlValue || this._dateTimeFilter(controlValue) ? null : {
        owlDateTimeFilter: true
      };
    };
    /**
     * The form control validator for the range.
     * Check whether the 'before' value is before the 'to' value
     * */
    this.rangeValidator = control => {
      if (this.isInSingleMode || !control.value) {
        return null;
      }
      const controlValueFrom = this.getValidDate(this.dateTimeAdapter.deserialize(control.value[0]));
      const controlValueTo = this.getValidDate(this.dateTimeAdapter.deserialize(control.value[1]));
      return !controlValueFrom || !controlValueTo || this.dateTimeAdapter.compare(controlValueFrom, controlValueTo) <= 0 ? null : {
        owlDateTimeRange: true
      };
    };
    /** The combined form control validator for this input. */
    this.validator = _angular_forms__WEBPACK_IMPORTED_MODULE_22__.Validators.compose([this.parseValidator, this.minValidator, this.maxValidator, this.filterValidator, this.rangeValidator]);
    /** Emits when the value changes (either due to user input or programmatic change). */
    this.valueChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /** Emits when the disabled state has changed */
    this.disabledChange = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    if (!this.dateTimeAdapter) {
      throw Error(`OwlDateTimePicker: No provider found for DateTimePicker. You must import one of the following ` + `modules at your application root: OwlNativeDateTimeModule, OwlDayjsDateTimeModule, or provide a ` + `custom implementation.`);
    }
    if (!this.dateTimeFormats) {
      throw Error(`OwlDateTimePicker: No provider found for OWL_DATE_TIME_FORMATS. You must import one of the following ` + `modules at your application root: OwlNativeDateTimeModule, OwlDayjsDateTimeModule, or provide a ` + `custom implementation.`);
    }
    this.localeSub = this.dateTimeAdapter.localeChanges.subscribe(() => {
      this.value = this.value;
    });
  }
  /**
   * The date time picker that this input is associated with.
   * */
  set owlDateTime(value) {
    this.registerDateTimePicker(value);
  }
  /**
   * A function to filter date time
   */
  set owlDateTimeFilter(filter) {
    this._dateTimeFilter = filter;
    this.validatorOnChange();
  }
  get dateTimeFilter() {
    return this._dateTimeFilter;
  }
  get disabled() {
    return !!this._disabled;
  }
  set disabled(value) {
    const newValue = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceBooleanProperty)(value);
    const element = this.elmRef.nativeElement;
    if (this._disabled !== newValue) {
      this._disabled = newValue;
      this.disabledChange.emit(newValue);
    }
    // We need to null check the `blur` method, because it's undefined during SSR.
    if (newValue && element.blur) {
      // Normally, native input elements automatically blur if they turn disabled. This behavior
      // is problematic, because it would mean that it triggers another change detection cycle,
      // which then causes a changed after checked error if the input element was focused before.
      element.blur();
    }
  }
  get min() {
    return this._min;
  }
  set min(value) {
    this._min = this.getValidDate(this.dateTimeAdapter.deserialize(value));
    this.validatorOnChange();
  }
  get max() {
    return this._max;
  }
  set max(value) {
    this._max = this.getValidDate(this.dateTimeAdapter.deserialize(value));
    this.validatorOnChange();
  }
  get selectMode() {
    return this._selectMode;
  }
  set selectMode(mode) {
    if (mode !== 'single' && mode !== 'range' && mode !== 'rangeFrom' && mode !== 'rangeTo') {
      throw Error('OwlDateTime Error: invalid selectMode value!');
    }
    this._selectMode = mode;
  }
  get value() {
    return this._value;
  }
  set value(value) {
    value = this.dateTimeAdapter.deserialize(value);
    this.lastValueValid = !value || this.dateTimeAdapter.isValid(value);
    value = this.getValidDate(value);
    const oldDate = this._value;
    this._value = value;
    // set the input property 'value'
    this.formatNativeInputValue();
    // check if the input value changed
    if (!this.dateTimeAdapter.isEqual(oldDate, value)) {
      this.valueChange.emit(value);
    }
  }
  get values() {
    return this._values;
  }
  set values(values) {
    if (values && values.length > 0) {
      this._values = values.map(v => {
        v = this.dateTimeAdapter.deserialize(v);
        return this.getValidDate(v);
      });
      this.lastValueValid = (!this._values[0] || this.dateTimeAdapter.isValid(this._values[0])) && (!this._values[1] || this.dateTimeAdapter.isValid(this._values[1]));
    } else {
      this._values = [];
      this.lastValueValid = true;
    }
    // set the input property 'value'
    this.formatNativeInputValue();
    this.valueChange.emit(this._values);
  }
  get elementRef() {
    return this.elmRef;
  }
  get isInSingleMode() {
    return this._selectMode === 'single';
  }
  get isInRangeMode() {
    return this._selectMode === 'range' || this._selectMode === 'rangeFrom' || this._selectMode === 'rangeTo';
  }
  get owlDateTimeInputAriaHaspopup() {
    return true;
  }
  get owlDateTimeInputAriaOwns() {
    return this.dtPicker.opened && this.dtPicker.id || null;
  }
  get minIso8601() {
    return this.min ? this.dateTimeAdapter.toIso8601(this.min) : null;
  }
  get maxIso8601() {
    return this.max ? this.dateTimeAdapter.toIso8601(this.max) : null;
  }
  get owlDateTimeInputDisabled() {
    return this.disabled;
  }
  ngOnInit() {
    if (!this.dtPicker) {
      throw Error(`OwlDateTimePicker: the picker input doesn't have any associated owl-date-time component`);
    }
  }
  ngAfterContentInit() {
    this.dtPickerSub = this.dtPicker.confirmSelectedChange.subscribe(selecteds => {
      if (Array.isArray(selecteds)) {
        this.values = selecteds;
      } else {
        this.value = selecteds;
      }
      this.onModelChange(selecteds);
      this.onModelTouched();
      this.dateTimeChange.emit({
        source: this,
        value: selecteds,
        input: this.elmRef.nativeElement
      });
      this.dateTimeInput.emit({
        source: this,
        value: selecteds,
        input: this.elmRef.nativeElement
      });
    });
  }
  ngOnDestroy() {
    this.dtPickerSub.unsubscribe();
    this.localeSub.unsubscribe();
    this.valueChange.complete();
    this.disabledChange.complete();
  }
  writeValue(value) {
    if (this.isInSingleMode) {
      this.value = value;
    } else {
      this.values = value;
    }
  }
  registerOnChange(fn) {
    this.onModelChange = fn;
  }
  registerOnTouched(fn) {
    this.onModelTouched = fn;
  }
  setDisabledState(isDisabled) {
    this.disabled = isDisabled;
  }
  validate(c) {
    return this.validator ? this.validator(c) : null;
  }
  registerOnValidatorChange(fn) {
    this.validatorOnChange = fn;
  }
  /**
   * Open the picker when user hold alt + DOWN_ARROW
   * */
  handleKeydownOnHost(event) {
    if (event.altKey && event.keyCode === _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_12__.DOWN_ARROW) {
      this.dtPicker.open();
      event.preventDefault();
    }
  }
  handleBlurOnHost(event) {
    this.onModelTouched();
  }
  handleInputOnHost(event) {
    let value = event.target.value;
    if (this._selectMode === 'single') {
      this.changeInputInSingleMode(value);
    } else if (this._selectMode === 'range') {
      this.changeInputInRangeMode(value);
    } else {
      this.changeInputInRangeFromToMode(value);
    }
  }
  handleChangeOnHost(event) {
    let v;
    if (this.isInSingleMode) {
      v = this.value;
    } else if (this.isInRangeMode) {
      v = this.values;
    }
    this.dateTimeChange.emit({
      source: this,
      value: v,
      input: this.elmRef.nativeElement
    });
  }
  /**
   * Set the native input property 'value'
   */
  formatNativeInputValue() {
    if (this.isInSingleMode) {
      this.renderer.setProperty(this.elmRef.nativeElement, 'value', this._value ? this.dateTimeAdapter.format(this._value, this.dtPicker.formatString) : '');
    } else if (this.isInRangeMode) {
      if (this._values && this.values.length > 0) {
        const from = this._values[0];
        const to = this._values[1];
        const fromFormatted = from ? this.dateTimeAdapter.format(from, this.dtPicker.formatString) : '';
        const toFormatted = to ? this.dateTimeAdapter.format(to, this.dtPicker.formatString) : '';
        if (!fromFormatted && !toFormatted) {
          this.renderer.setProperty(this.elmRef.nativeElement, 'value', null);
        } else {
          if (this._selectMode === 'range') {
            this.renderer.setProperty(this.elmRef.nativeElement, 'value', fromFormatted + ' ' + this.rangeSeparator + ' ' + toFormatted);
          } else if (this._selectMode === 'rangeFrom') {
            this.renderer.setProperty(this.elmRef.nativeElement, 'value', fromFormatted);
          } else if (this._selectMode === 'rangeTo') {
            this.renderer.setProperty(this.elmRef.nativeElement, 'value', toFormatted);
          }
        }
      } else {
        this.renderer.setProperty(this.elmRef.nativeElement, 'value', '');
      }
    }
    return;
  }
  /**
   * Register the relationship between this input and its picker component
   */
  registerDateTimePicker(picker) {
    if (picker) {
      this.dtPicker = picker;
      this.dtPicker.registerInput(this);
    }
  }
  /**
   * Convert a given obj to a valid date object
   */
  getValidDate(obj) {
    return this.dateTimeAdapter.isDateInstance(obj) && this.dateTimeAdapter.isValid(obj) ? obj : null;
  }
  /**
   * Convert a time string to a date-time string
   * When pickerType is 'timer', the value in the picker's input is a time string.
   * The dateTimeAdapter parse fn could not parse a time string to a Date Object.
   * Therefore we need this fn to convert a time string to a date-time string.
   */
  convertTimeStringToDateTimeString(timeString, dateTime) {
    if (timeString) {
      const v = dateTime || this.dateTimeAdapter.now();
      const dateString = this.dateTimeAdapter.format(v, this.dateTimeFormats.datePickerInput);
      return dateString + ' ' + timeString;
    } else {
      return null;
    }
  }
  /**
   * Handle input change in single mode
   */
  changeInputInSingleMode(inputValue) {
    let value = inputValue;
    if (this.dtPicker.pickerType === 'timer') {
      value = this.convertTimeStringToDateTimeString(value, this.value);
    }
    let result = this.dateTimeAdapter.parse(value, this.dateTimeFormats.parseInput);
    this.lastValueValid = !result || this.dateTimeAdapter.isValid(result);
    result = this.getValidDate(result);
    // if the newValue is the same as the oldValue, we intend to not fire the valueChange event
    // result equals to null means there is input event, but the input value is invalid
    if (!this.isSameValue(result, this._value) || result === null) {
      this._value = result;
      this.valueChange.emit(result);
      this.onModelChange(result);
      this.dateTimeInput.emit({
        source: this,
        value: result,
        input: this.elmRef.nativeElement
      });
    }
  }
  /**
   * Handle input change in rangeFrom or rangeTo mode
   */
  changeInputInRangeFromToMode(inputValue) {
    let originalValue = this._selectMode === 'rangeFrom' ? this._values[0] : this._values[1];
    if (this.dtPicker.pickerType === 'timer') {
      inputValue = this.convertTimeStringToDateTimeString(inputValue, originalValue);
    }
    let result = this.dateTimeAdapter.parse(inputValue, this.dateTimeFormats.parseInput);
    this.lastValueValid = !result || this.dateTimeAdapter.isValid(result);
    result = this.getValidDate(result);
    // if the newValue is the same as the oldValue, we intend to not fire the valueChange event
    if (this._selectMode === 'rangeFrom' && this.isSameValue(result, this._values[0]) && result || this._selectMode === 'rangeTo' && this.isSameValue(result, this._values[1]) && result) {
      return;
    }
    this._values = this._selectMode === 'rangeFrom' ? [result, this._values[1]] : [this._values[0], result];
    this.valueChange.emit(this._values);
    this.onModelChange(this._values);
    this.dateTimeInput.emit({
      source: this,
      value: this._values,
      input: this.elmRef.nativeElement
    });
  }
  /**
   * Handle input change in range mode
   */
  changeInputInRangeMode(inputValue) {
    const selecteds = inputValue.split(this.rangeSeparator);
    let fromString = selecteds[0];
    let toString = selecteds[1];
    if (this.dtPicker.pickerType === 'timer') {
      fromString = this.convertTimeStringToDateTimeString(fromString, this.values[0]);
      toString = this.convertTimeStringToDateTimeString(toString, this.values[1]);
    }
    let from = this.dateTimeAdapter.parse(fromString, this.dateTimeFormats.parseInput);
    let to = this.dateTimeAdapter.parse(toString, this.dateTimeFormats.parseInput);
    this.lastValueValid = (!from || this.dateTimeAdapter.isValid(from)) && (!to || this.dateTimeAdapter.isValid(to));
    from = this.getValidDate(from);
    to = this.getValidDate(to);
    // if the newValue is the same as the oldValue, we intend to not fire the valueChange event
    if (!this.isSameValue(from, this._values[0]) || !this.isSameValue(to, this._values[1]) || from === null && to === null) {
      this._values = [from, to];
      this.valueChange.emit(this._values);
      this.onModelChange(this._values);
      this.dateTimeInput.emit({
        source: this,
        value: this._values,
        input: this.elmRef.nativeElement
      });
    }
  }
  /**
   * Check if the two value is the same
   */
  isSameValue(first, second) {
    if (first && second) {
      return this.dateTimeAdapter.compare(first, second) === 0;
    }
    return first == second;
  }
}
OwlDateTimeInputDirective.ɵfac = function OwlDateTimeInputDirective_Factory(t) {
  return new (t || OwlDateTimeInputDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.Renderer2), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OWL_DATE_TIME_FORMATS, 8));
};
OwlDateTimeInputDirective.ɵdir = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineDirective"]({
  type: OwlDateTimeInputDirective,
  selectors: [["input", "owlDateTime", ""]],
  hostVars: 5,
  hostBindings: function OwlDateTimeInputDirective_HostBindings(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("keydown", function OwlDateTimeInputDirective_keydown_HostBindingHandler($event) {
        return ctx.handleKeydownOnHost($event);
      })("blur", function OwlDateTimeInputDirective_blur_HostBindingHandler($event) {
        return ctx.handleBlurOnHost($event);
      })("input", function OwlDateTimeInputDirective_input_HostBindingHandler($event) {
        return ctx.handleInputOnHost($event);
      })("change", function OwlDateTimeInputDirective_change_HostBindingHandler($event) {
        return ctx.handleChangeOnHost($event);
      });
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵhostProperty"]("disabled", ctx.owlDateTimeInputDisabled);
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵattribute"]("aria-haspopup", ctx.owlDateTimeInputAriaHaspopup)("aria-owns", ctx.owlDateTimeInputAriaOwns)("min", ctx.minIso8601)("max", ctx.maxIso8601);
    }
  },
  inputs: {
    owlDateTime: "owlDateTime",
    owlDateTimeFilter: "owlDateTimeFilter",
    _disabled: "_disabled",
    min: "min",
    max: "max",
    selectMode: "selectMode",
    rangeSeparator: "rangeSeparator",
    value: "value",
    values: "values"
  },
  outputs: {
    dateTimeChange: "dateTimeChange",
    dateTimeInput: "dateTimeInput"
  },
  exportAs: ["owlDateTimeInput"],
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵProvidersFeature"]([OWL_DATETIME_VALUE_ACCESSOR$1, OWL_DATETIME_VALIDATORS])]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDateTimeInputDirective, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Directive,
    args: [{
      selector: 'input[owlDateTime]',
      exportAs: 'owlDateTimeInput',
      host: {
        '(keydown)': 'handleKeydownOnHost($event)',
        '(blur)': 'handleBlurOnHost($event)',
        '(input)': 'handleInputOnHost($event)',
        '(change)': 'handleChangeOnHost($event)',
        '[attr.aria-haspopup]': 'owlDateTimeInputAriaHaspopup',
        '[attr.aria-owns]': 'owlDateTimeInputAriaOwns',
        '[attr.min]': 'minIso8601',
        '[attr.max]': 'maxIso8601',
        '[disabled]': 'owlDateTimeInputDisabled'
      },
      providers: [OWL_DATETIME_VALUE_ACCESSOR$1, OWL_DATETIME_VALIDATORS]
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ElementRef
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Renderer2
    }, {
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DATE_TIME_FORMATS]
      }]
    }];
  }, {
    owlDateTime: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    owlDateTimeFilter: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    _disabled: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    min: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    max: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selectMode: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    rangeSeparator: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    value: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    values: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    dateTimeChange: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    dateTimeInput: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }]
  });
})();

/**
 * numberFixedLen.pipe
 */
class NumberFixedLenPipe {
  transform(num, len) {
    const number = Math.floor(num);
    const length = Math.floor(len);
    if (num === null || isNaN(number) || isNaN(length)) {
      return num;
    }
    let numString = number.toString();
    while (numString.length < length) {
      numString = '0' + numString;
    }
    return numString;
  }
}
NumberFixedLenPipe.ɵfac = function NumberFixedLenPipe_Factory(t) {
  return new (t || NumberFixedLenPipe)();
};
NumberFixedLenPipe.ɵpipe = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefinePipe"]({
  name: "numberFixedLen",
  type: NumberFixedLenPipe,
  pure: true
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](NumberFixedLenPipe, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Pipe,
    args: [{
      name: 'numberFixedLen'
    }]
  }], null, null);
})();

/**
 * date-time-inline.component
 */
const OWL_DATETIME_VALUE_ACCESSOR = {
  provide: _angular_forms__WEBPACK_IMPORTED_MODULE_22__.NG_VALUE_ACCESSOR,
  useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.forwardRef)(() => OwlDateTimeInlineComponent),
  multi: true
};
class OwlDateTimeInlineComponent extends OwlDateTime {
  constructor(changeDetector, dateTimeAdapter, dateTimeFormats) {
    super(dateTimeAdapter, dateTimeFormats);
    this.changeDetector = changeDetector;
    this.dateTimeAdapter = dateTimeAdapter;
    this.dateTimeFormats = dateTimeFormats;
    /**
     * Set the type of the dateTime picker
     *      'both' -- show both calendar and timer
     *      'calendar' -- show only calendar
     *      'timer' -- show only timer
     */
    this._pickerType = 'both';
    this._disabled = false;
    this._selectMode = 'single';
    this._values = [];
    /**
     * Emits selected year in multi-year view
     * This doesn't imply a change on the selected date.
     * */
    this.yearSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    /**
     * Emits selected month in year view
     * This doesn't imply a change on the selected date.
     * */
    this.monthSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this._selecteds = [];
    this.onModelChange = () => {};
    this.onModelTouched = () => {};
  }
  get pickerType() {
    return this._pickerType;
  }
  set pickerType(val) {
    if (val !== this._pickerType) {
      this._pickerType = val;
    }
  }
  get disabled() {
    return !!this._disabled;
  }
  set disabled(value) {
    this._disabled = (0,_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_13__.coerceBooleanProperty)(value);
  }
  get selectMode() {
    return this._selectMode;
  }
  set selectMode(mode) {
    if (mode !== 'single' && mode !== 'range' && mode !== 'rangeFrom' && mode !== 'rangeTo') {
      throw Error('OwlDateTime Error: invalid selectMode value!');
    }
    this._selectMode = mode;
  }
  get startAt() {
    if (this._startAt) {
      return this._startAt;
    }
    if (this.selectMode === 'single') {
      return this.value || null;
    } else if (this.selectMode === 'range' || this.selectMode === 'rangeFrom') {
      return this.values[0] || null;
    } else if (this.selectMode === 'rangeTo') {
      return this.values[1] || null;
    } else {
      return null;
    }
  }
  set startAt(date) {
    this._startAt = this.getValidDate(this.dateTimeAdapter.deserialize(date));
  }
  get dateTimeFilter() {
    return this._dateTimeFilter;
  }
  set dateTimeFilter(filter) {
    this._dateTimeFilter = filter;
  }
  get minDateTime() {
    return this._min || null;
  }
  set minDateTime(value) {
    this._min = this.getValidDate(this.dateTimeAdapter.deserialize(value));
    this.changeDetector.markForCheck();
  }
  get maxDateTime() {
    return this._max || null;
  }
  set maxDateTime(value) {
    this._max = this.getValidDate(this.dateTimeAdapter.deserialize(value));
    this.changeDetector.markForCheck();
  }
  get value() {
    return this._value;
  }
  set value(value) {
    value = this.dateTimeAdapter.deserialize(value);
    value = this.getValidDate(value);
    this._value = value;
    this.selected = value;
  }
  get values() {
    return this._values;
  }
  set values(values) {
    if (values && values.length > 0) {
      values = values.map(v => {
        v = this.dateTimeAdapter.deserialize(v);
        v = this.getValidDate(v);
        return v ? this.dateTimeAdapter.clone(v) : null;
      });
      this._values = [...values];
      this.selecteds = [...values];
    } else {
      this._values = [];
      this.selecteds = [];
    }
  }
  get selected() {
    return this._selected;
  }
  set selected(value) {
    this._selected = value;
    this.changeDetector.markForCheck();
  }
  get selecteds() {
    return this._selecteds;
  }
  set selecteds(values) {
    this._selecteds = values;
    this.changeDetector.markForCheck();
  }
  get opened() {
    return true;
  }
  get pickerMode() {
    return 'inline';
  }
  get isInSingleMode() {
    return this._selectMode === 'single';
  }
  get isInRangeMode() {
    return this._selectMode === 'range' || this._selectMode === 'rangeFrom' || this._selectMode === 'rangeTo';
  }
  get owlDTInlineClass() {
    return true;
  }
  ngOnInit() {
    this.container.picker = this;
  }
  writeValue(value) {
    if (this.isInSingleMode) {
      this.value = value;
      this.container.pickerDayjs = value;
    } else {
      this.values = value;
      this.container.pickerDayjs = this._values[this.container.activeSelectedIndex];
    }
  }
  registerOnChange(fn) {
    this.onModelChange = fn;
  }
  registerOnTouched(fn) {
    this.onModelTouched = fn;
  }
  setDisabledState(isDisabled) {
    this.disabled = isDisabled;
  }
  select(date) {
    if (this.disabled) {
      return;
    }
    if (Array.isArray(date)) {
      this.values = [...date];
    } else {
      this.value = date;
    }
    this.onModelChange(date);
    this.onModelTouched();
  }
  /**
   * Emits the selected year in multi-year view
   * */
  selectYear(normalizedYear) {
    this.yearSelected.emit(normalizedYear);
  }
  /**
   * Emits selected month in year view
   * */
  selectMonth(normalizedMonth) {
    this.monthSelected.emit(normalizedMonth);
  }
}
OwlDateTimeInlineComponent.ɵfac = function OwlDateTimeInlineComponent_Factory(t) {
  return new (t || OwlDateTimeInlineComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](DateTimeAdapter, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](OWL_DATE_TIME_FORMATS, 8));
};
OwlDateTimeInlineComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
  type: OwlDateTimeInlineComponent,
  selectors: [["owl-date-time-inline"]],
  viewQuery: function OwlDateTimeInlineComponent_Query(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵviewQuery"](OwlDateTimeContainerComponent, 7);
    }
    if (rf & 2) {
      let _t;
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵloadQuery"]()) && (ctx.container = _t.first);
    }
  },
  hostVars: 2,
  hostBindings: function OwlDateTimeInlineComponent_HostBindings(rf, ctx) {
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵclassProp"]("owl-dt-inline", ctx.owlDTInlineClass);
    }
  },
  inputs: {
    pickerType: "pickerType",
    disabled: "disabled",
    selectMode: "selectMode",
    startAt: "startAt",
    dateTimeFilter: ["owlDateTimeFilter", "dateTimeFilter"],
    minDateTime: ["min", "minDateTime"],
    maxDateTime: ["max", "maxDateTime"],
    value: "value",
    values: "values"
  },
  outputs: {
    yearSelected: "yearSelected",
    monthSelected: "monthSelected"
  },
  features: [_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵProvidersFeature"]([OWL_DATETIME_VALUE_ACCESSOR]), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵInheritDefinitionFeature"]],
  decls: 1,
  vars: 0,
  template: function OwlDateTimeInlineComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](0, "owl-date-time-container");
    }
  },
  dependencies: [OwlDateTimeContainerComponent],
  changeDetection: 0
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDateTimeInlineComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component,
    args: [{
      selector: 'owl-date-time-inline',
      host: {
        '[class.owl-dt-inline]': 'owlDTInlineClass'
      },
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectionStrategy.OnPush,
      preserveWhitespaces: false,
      providers: [OWL_DATETIME_VALUE_ACCESSOR],
      template: "<owl-date-time-container></owl-date-time-container>"
    }]
  }], function () {
    return [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef
    }, {
      type: DateTimeAdapter,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DATE_TIME_FORMATS]
      }]
    }];
  }, {
    container: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild,
      args: [OwlDateTimeContainerComponent, {
        static: true
      }]
    }],
    pickerType: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    disabled: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    selectMode: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    startAt: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    dateTimeFilter: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input,
      args: ['owlDateTimeFilter']
    }],
    minDateTime: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input,
      args: ['min']
    }],
    maxDateTime: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input,
      args: ['max']
    }],
    value: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    values: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
    }],
    yearSelected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }],
    monthSelected: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
    }]
  });
})();

/**
 * dialog.module
 */
class OwlDialogModule {}
OwlDialogModule.ɵfac = function OwlDialogModule_Factory(t) {
  return new (t || OwlDialogModule)();
};
OwlDialogModule.ɵmod = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
  type: OwlDialogModule
});
OwlDialogModule.ɵinj = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
  providers: [OWL_DIALOG_SCROLL_STRATEGY_PROVIDER, OwlDialogService],
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.CommonModule, _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_14__.A11yModule, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.OverlayModule, _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.PortalModule]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDialogModule, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule,
    args: [{
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.CommonModule, _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_14__.A11yModule, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.OverlayModule, _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_19__.PortalModule],
      exports: [],
      declarations: [OwlDialogContainerComponent],
      providers: [OWL_DIALOG_SCROLL_STRATEGY_PROVIDER, OwlDialogService]
    }]
  }], null, null);
})();

/**
 * date-time.module
 */
class OwlDateTimeModule {}
OwlDateTimeModule.ɵfac = function OwlDateTimeModule_Factory(t) {
  return new (t || OwlDateTimeModule)();
};
OwlDateTimeModule.ɵmod = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
  type: OwlDateTimeModule
});
OwlDateTimeModule.ɵinj = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
  providers: [OwlDateTimeIntl, OWL_DTPICKER_SCROLL_STRATEGY_PROVIDER],
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.CommonModule, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.OverlayModule, OwlDialogModule, _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_14__.A11yModule]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDateTimeModule, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule,
    args: [{
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.CommonModule, _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_17__.OverlayModule, OwlDialogModule, _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_14__.A11yModule],
      exports: [OwlCalendarComponent, OwlTimerComponent, OwlDateTimeTriggerDirective, OwlDateTimeInputDirective, OwlDateTimeComponent, OwlDateTimeInlineComponent, OwlMultiYearViewComponent, OwlYearViewComponent, OwlMonthViewComponent],
      declarations: [OwlDateTimeTriggerDirective, OwlDateTimeInputDirective, OwlDateTimeComponent, OwlDateTimeContainerComponent, OwlMultiYearViewComponent, OwlYearViewComponent, OwlMonthViewComponent, OwlTimerComponent, OwlTimerBoxComponent, OwlCalendarComponent, OwlCalendarBodyComponent, NumberFixedLenPipe, OwlDateTimeInlineComponent],
      providers: [OwlDateTimeIntl, OWL_DTPICKER_SCROLL_STRATEGY_PROVIDER]
    }]
  }], null, null);
})();

/**
 * native-date-time-adapter.class
 */
/** The default month names to use if Intl API is not available. */
const DEFAULT_MONTH_NAMES = {
  long: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  narrow: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']
};
/** The default day of the week names to use if Intl API is not available. */
const DEFAULT_DAY_OF_WEEK_NAMES = {
  long: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  short: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  narrow: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
};
/** The default date names to use if Intl API is not available. */
const DEFAULT_DATE_NAMES = range$1(31, i => String(i + 1));
/** Whether the browser supports the Intl API. */
const SUPPORTS_INTL_API = typeof Intl !== 'undefined';
/**
 * Matches strings that have the form of a valid RFC 3339 string
 * (https://tools.ietf.org/html/rfc3339). Note that the string may not actually be a valid date
 * because the regex will match strings an with out of bounds month, date, etc.
 */
const ISO_8601_REGEX = /^\d{4}-\d{2}-\d{2}(?:T\d{2}:\d{2}:\d{2}(?:\.\d+)?(?:Z|(?:(?:\+|-)\d{2}:\d{2}))?)?$/;
/** Creates an array and fills it with values. */
function range$1(length, valueFunction) {
  const valuesArray = Array(length);
  for (let i = 0; i < length; i++) {
    valuesArray[i] = valueFunction(i);
  }
  return valuesArray;
}
class NativeDateTimeAdapter extends DateTimeAdapter {
  constructor(owlDateTimeLocale, platform) {
    super();
    this.owlDateTimeLocale = owlDateTimeLocale;
    super.setLocale(owlDateTimeLocale);
    // IE does its own time zone correction, so we disable this on IE.
    this.useUtcForDisplay = !platform.TRIDENT;
    this._clampDate = platform.TRIDENT || platform.EDGE;
  }
  getYear(date) {
    return date.getFullYear();
  }
  getMonth(date) {
    return date.getMonth();
  }
  getDay(date) {
    return date.getDay();
  }
  getDate(date) {
    return date.getDate();
  }
  getHours(date) {
    return date.getHours();
  }
  getMinutes(date) {
    return date.getMinutes();
  }
  getSeconds(date) {
    return date.getSeconds();
  }
  getTime(date) {
    return date.getTime();
  }
  getNumDaysInMonth(date) {
    const lastDateOfMonth = this.createDateWithOverflow(this.getYear(date), this.getMonth(date) + 1, 0);
    return this.getDate(lastDateOfMonth);
  }
  differenceInCalendarDays(dateLeft, dateRight) {
    if (this.isValid(dateLeft) && this.isValid(dateRight)) {
      const dateLeftStartOfDay = this.createDate(this.getYear(dateLeft), this.getMonth(dateLeft), this.getDate(dateLeft));
      const dateRightStartOfDay = this.createDate(this.getYear(dateRight), this.getMonth(dateRight), this.getDate(dateRight));
      const timeStampLeft = this.getTime(dateLeftStartOfDay) - dateLeftStartOfDay.getTimezoneOffset() * this.milliseondsInMinute;
      const timeStampRight = this.getTime(dateRightStartOfDay) - dateRightStartOfDay.getTimezoneOffset() * this.milliseondsInMinute;
      return Math.round((timeStampLeft - timeStampRight) / this.millisecondsInDay);
    } else {
      return null;
    }
  }
  getYearName(date) {
    if (SUPPORTS_INTL_API) {
      const dtf = new Intl.DateTimeFormat(this.locale, {
        year: 'numeric',
        timeZone: 'utc'
      });
      return this.stripDirectionalityCharacters(this._format(dtf, date));
    }
    return String(this.getYear(date));
  }
  getMonthNames(style) {
    if (SUPPORTS_INTL_API) {
      const dtf = new Intl.DateTimeFormat(this.locale, {
        month: style,
        timeZone: 'utc'
      });
      return range$1(12, i => this.stripDirectionalityCharacters(this._format(dtf, new Date(2017, i, 1))));
    }
    return DEFAULT_MONTH_NAMES[style];
  }
  getDayOfWeekNames(style) {
    if (SUPPORTS_INTL_API) {
      const dtf = new Intl.DateTimeFormat(this.locale, {
        weekday: style,
        timeZone: 'utc'
      });
      return range$1(7, i => this.stripDirectionalityCharacters(this._format(dtf, new Date(2017, 0, i + 1))));
    }
    return DEFAULT_DAY_OF_WEEK_NAMES[style];
  }
  getDateNames() {
    if (SUPPORTS_INTL_API) {
      const dtf = new Intl.DateTimeFormat(this.locale, {
        day: 'numeric',
        timeZone: 'utc'
      });
      return range$1(31, i => this.stripDirectionalityCharacters(this._format(dtf, new Date(2017, 0, i + 1))));
    }
    return DEFAULT_DATE_NAMES;
  }
  toIso8601(date) {
    return date.toISOString();
  }
  isEqual(dateLeft, dateRight) {
    if (this.isValid(dateLeft) && this.isValid(dateRight)) {
      return dateLeft.getTime() === dateRight.getTime();
    } else {
      return false;
    }
  }
  isSameDay(dateLeft, dateRight) {
    if (this.isValid(dateLeft) && this.isValid(dateRight)) {
      const dateLeftStartOfDay = this.clone(dateLeft);
      const dateRightStartOfDay = this.clone(dateRight);
      dateLeftStartOfDay.setHours(0, 0, 0, 0);
      dateRightStartOfDay.setHours(0, 0, 0, 0);
      return dateLeftStartOfDay.getTime() === dateRightStartOfDay.getTime();
    } else {
      return false;
    }
  }
  isValid(date) {
    return date && !isNaN(date.getTime());
  }
  invalid() {
    return new Date(NaN);
  }
  isDateInstance(obj) {
    return obj instanceof Date;
  }
  addCalendarYears(date, amount) {
    return this.addCalendarMonths(date, amount * 12);
  }
  addCalendarMonths(date, amount) {
    const result = this.clone(date);
    amount = Number(amount);
    const desiredMonth = result.getMonth() + amount;
    const dateWithDesiredMonth = new Date(0);
    dateWithDesiredMonth.setFullYear(result.getFullYear(), desiredMonth, 1);
    dateWithDesiredMonth.setHours(0, 0, 0, 0);
    const daysInMonth = this.getNumDaysInMonth(dateWithDesiredMonth);
    // Set the last day of the new month
    // if the original date was the last day of the longer month
    result.setMonth(desiredMonth, Math.min(daysInMonth, result.getDate()));
    return result;
  }
  addCalendarDays(date, amount) {
    const result = this.clone(date);
    amount = Number(amount);
    result.setDate(result.getDate() + amount);
    return result;
  }
  setHours(date, amount) {
    const result = this.clone(date);
    result.setHours(amount);
    return result;
  }
  setMinutes(date, amount) {
    const result = this.clone(date);
    result.setMinutes(amount);
    return result;
  }
  setSeconds(date, amount) {
    const result = this.clone(date);
    result.setSeconds(amount);
    return result;
  }
  createDate(year, month, date, hours = 0, minutes = 0, seconds = 0) {
    if (month < 0 || month > 11) {
      throw Error(`Invalid month index "${month}". Month index has to be between 0 and 11.`);
    }
    if (date < 1) {
      throw Error(`Invalid date "${date}". Date has to be greater than 0.`);
    }
    if (hours < 0 || hours > 23) {
      throw Error(`Invalid hours "${hours}". Hours has to be between 0 and 23.`);
    }
    if (minutes < 0 || minutes > 59) {
      throw Error(`Invalid minutes "${minutes}". Minutes has to between 0 and 59.`);
    }
    if (seconds < 0 || seconds > 59) {
      throw Error(`Invalid seconds "${seconds}". Seconds has to be between 0 and 59.`);
    }
    const result = this.createDateWithOverflow(year, month, date, hours, minutes, seconds);
    // Check that the date wasn't above the upper bound for the month, causing the month to overflow
    // For example, createDate(2017, 1, 31) would try to create a date 2017/02/31 which is invalid
    if (result.getMonth() !== month) {
      throw Error(`Invalid date "${date}" for month with index "${month}".`);
    }
    return result;
  }
  clone(date) {
    return this.createDate(this.getYear(date), this.getMonth(date), this.getDate(date), this.getHours(date), this.getMinutes(date), this.getSeconds(date));
  }
  now() {
    return new Date();
  }
  format(date, displayFormat) {
    if (!this.isValid(date)) {
      throw Error('JSNativeDate: Cannot format invalid date.');
    }
    if (SUPPORTS_INTL_API) {
      if (this._clampDate && (date.getFullYear() < 1 || date.getFullYear() > 9999)) {
        date = this.clone(date);
        date.setFullYear(Math.max(1, Math.min(9999, date.getFullYear())));
      }
      displayFormat = {
        ...displayFormat,
        timeZone: 'utc'
      };
      const dtf = new Intl.DateTimeFormat(this.locale, displayFormat);
      return this.stripDirectionalityCharacters(this._format(dtf, date));
    }
    return this.stripDirectionalityCharacters(date.toDateString());
  }
  parse(value, parseFormat) {
    // There is no way using the native JS Date to set the parse format or locale
    if (typeof value === 'number') {
      return new Date(value);
    }
    return value ? new Date(Date.parse(value)) : null;
  }
  /**
   * Returns the given value if given a valid Date or null. Deserializes valid ISO 8601 strings
   * (https://www.ietf.org/rfc/rfc3339.txt) into valid Dates and empty string into null. Returns an
   * invalid date for all other values.
   */
  deserialize(value) {
    if (typeof value === 'string') {
      if (!value) {
        return null;
      }
      // The `Date` constructor accepts formats other than ISO 8601, so we need to make sure the
      // string is the right format first.
      if (ISO_8601_REGEX.test(value)) {
        const date = new Date(value);
        if (this.isValid(date)) {
          return date;
        }
      }
    }
    return super.deserialize(value);
  }
  /**
   * Creates a date but allows the month and date to overflow.
   */
  createDateWithOverflow(year, month, date, hours = 0, minutes = 0, seconds = 0) {
    const result = new Date(year, month, date, hours, minutes, seconds);
    if (year >= 0 && year < 100) {
      result.setFullYear(this.getYear(result) - 1900);
    }
    return result;
  }
  /**
   * Strip out unicode LTR and RTL characters. Edge and IE insert these into formatted dates while
   * other browsers do not. We remove them to make output consistent and because they interfere with
   * date parsing.
   */
  stripDirectionalityCharacters(str) {
    return str.replace(/[\u200e\u200f]/g, '');
  }
  /**
   * When converting Date object to string, javascript built-in functions may return wrong
   * results because it applies its internal DST rules. The DST rules around the world change
   * very frequently, and the current valid rule is not always valid in previous years though.
   * We work around this problem building a new Date object which has its internal UTC
   * representation with the local date and time.
   */
  _format(dtf, date) {
    const d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds()));
    return dtf.format(d);
  }
}
NativeDateTimeAdapter.ɵfac = function NativeDateTimeAdapter_Factory(t) {
  return new (t || NativeDateTimeAdapter)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](OWL_DATE_TIME_LOCALE, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](_angular_cdk_platform__WEBPACK_IMPORTED_MODULE_23__.Platform));
};
NativeDateTimeAdapter.ɵprov = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjectable"]({
  token: NativeDateTimeAdapter,
  factory: NativeDateTimeAdapter.ɵfac
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](NativeDateTimeAdapter, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable
  }], function () {
    return [{
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DATE_TIME_LOCALE]
      }]
    }, {
      type: _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_23__.Platform
    }];
  }, null);
})();
const OWL_NATIVE_DATE_TIME_FORMATS = {
  parseInput: null,
  fullPickerInput: {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric'
  },
  datePickerInput: {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric'
  },
  timePickerInput: {
    hour: 'numeric',
    minute: 'numeric'
  },
  monthYearLabel: {
    year: 'numeric',
    month: 'short'
  },
  dateA11yLabel: {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  },
  monthYearA11yLabel: {
    year: 'numeric',
    month: 'long'
  }
};

/**
 * native-date-time.module
 */
class NativeDateTimeModule {}
NativeDateTimeModule.ɵfac = function NativeDateTimeModule_Factory(t) {
  return new (t || NativeDateTimeModule)();
};
NativeDateTimeModule.ɵmod = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
  type: NativeDateTimeModule
});
NativeDateTimeModule.ɵinj = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
  providers: [{
    provide: DateTimeAdapter,
    useClass: NativeDateTimeAdapter
  }],
  imports: [_angular_cdk_platform__WEBPACK_IMPORTED_MODULE_23__.PlatformModule]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](NativeDateTimeModule, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule,
    args: [{
      imports: [_angular_cdk_platform__WEBPACK_IMPORTED_MODULE_23__.PlatformModule],
      providers: [{
        provide: DateTimeAdapter,
        useClass: NativeDateTimeAdapter
      }]
    }]
  }], null, null);
})();
class OwlNativeDateTimeModule {}
OwlNativeDateTimeModule.ɵfac = function OwlNativeDateTimeModule_Factory(t) {
  return new (t || OwlNativeDateTimeModule)();
};
OwlNativeDateTimeModule.ɵmod = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
  type: OwlNativeDateTimeModule
});
OwlNativeDateTimeModule.ɵinj = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
  providers: [{
    provide: OWL_DATE_TIME_FORMATS,
    useValue: OWL_NATIVE_DATE_TIME_FORMATS
  }],
  imports: [NativeDateTimeModule]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlNativeDateTimeModule, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule,
    args: [{
      imports: [NativeDateTimeModule],
      providers: [{
        provide: OWL_DATE_TIME_FORMATS,
        useValue: OWL_NATIVE_DATE_TIME_FORMATS
      }]
    }]
  }], null, null);
})();

/**
 * dayjs-date-time-adapter.class
 */
const dayjs = dayjs__WEBPACK_IMPORTED_MODULE_0__["default"] ? dayjs__WEBPACK_IMPORTED_MODULE_0__["default"] : dayjs__WEBPACK_IMPORTED_MODULE_0__;
/** InjectionToken for dayjs date adapter to configure options. */
const OWL_DAYJS_DATE_TIME_ADAPTER_OPTIONS = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.InjectionToken('OWL_MOMENT_DATE_TIME_ADAPTER_OPTIONS', {
  providedIn: 'root',
  factory: OWL_MOMENT_DATE_TIME_ADAPTER_OPTIONS_FACTORY
});
/** @docs-private */
function OWL_MOMENT_DATE_TIME_ADAPTER_OPTIONS_FACTORY() {
  return {
    useUtc: false
  };
}
/** Creates an array and fills it with values. */
function range(length, valueFunction) {
  const valuesArray = Array(length);
  for (let i = 0; i < length; i++) {
    valuesArray[i] = valueFunction(i);
  }
  return valuesArray;
}
class DayjsDateTimeAdapter extends DateTimeAdapter {
  constructor(owlDateTimeLocale, options) {
    super();
    this.owlDateTimeLocale = owlDateTimeLocale;
    this.options = options;
    dayjs.extend(dayjs_plugin_localeData__WEBPACK_IMPORTED_MODULE_1__);
    dayjs.extend(dayjs_plugin_utc__WEBPACK_IMPORTED_MODULE_4__);
    dayjs.extend(dayjs_plugin_objectSupport__WEBPACK_IMPORTED_MODULE_2__);
    dayjs.extend(dayjs_plugin_localizedFormat__WEBPACK_IMPORTED_MODULE_3__);
    this.setLocale(owlDateTimeLocale || dayjs.locale());
  }
  setLocale(locale) {
    super.setLocale(locale);
    const dayjsLocaleData = dayjs.localeData(locale);
    this._localeData = {
      longMonths: dayjsLocaleData.months(),
      shortMonths: dayjsLocaleData.monthsShort(),
      longDaysOfWeek: dayjsLocaleData.weekdays(),
      shortDaysOfWeek: dayjsLocaleData.weekdaysShort(),
      narrowDaysOfWeek: dayjsLocaleData.weekdaysMin(),
      dates: range(31, i => this.createDate(2017, 0, i + 1).format('D'))
    };
  }
  getYear(date) {
    return this.clone(date).year();
  }
  getMonth(date) {
    return this.clone(date).month();
  }
  getDay(date) {
    return this.clone(date).day();
  }
  getDate(date) {
    return this.clone(date).date();
  }
  getHours(date) {
    return this.clone(date).hour();
  }
  getMinutes(date) {
    return this.clone(date).minute();
  }
  getSeconds(date) {
    return this.clone(date).second();
  }
  getTime(date) {
    return this.clone(date).valueOf();
  }
  getNumDaysInMonth(date) {
    return this.clone(date).daysInMonth();
  }
  differenceInCalendarDays(dateLeft, dateRight) {
    return this.clone(dateLeft).diff(dateRight, 'days');
  }
  getYearName(date) {
    return this.clone(date).format('YYYY');
  }
  getMonthNames(style) {
    return style === 'long' ? this._localeData.longMonths : this._localeData.shortMonths;
  }
  getDayOfWeekNames(style) {
    if (style === 'long') {
      return this._localeData.longDaysOfWeek;
    }
    if (style === 'short') {
      return this._localeData.shortDaysOfWeek;
    }
    return this._localeData.narrowDaysOfWeek;
  }
  getDateNames() {
    return this._localeData.dates;
  }
  toIso8601(date) {
    return this.clone(date).toISOString();
  }
  isEqual(dateLeft, dateRight) {
    if (dateLeft && dateRight) {
      return this.clone(dateLeft).isSame(this.clone(dateRight));
    }
    return dateLeft === dateRight;
  }
  isSameDay(dateLeft, dateRight) {
    if (dateLeft && dateRight) {
      return this.clone(dateLeft).isSame(this.clone(dateRight), 'day');
    }
    return dateLeft === dateRight;
  }
  isValid(date) {
    return this.clone(date).isValid();
  }
  invalid() {
    return dayjs(null);
  }
  isDateInstance(obj) {
    return dayjs(obj).isValid();
  }
  addCalendarYears(date, amount) {
    return this.clone(date).add(amount, 'years');
  }
  addCalendarMonths(date, amount) {
    return this.clone(date).add(amount, 'months');
  }
  addCalendarDays(date, amount) {
    return this.clone(date).add(amount, 'days');
  }
  setHours(date, amount) {
    return this.clone(date).hour(amount);
  }
  setMinutes(date, amount) {
    return this.clone(date).minute(amount);
  }
  setSeconds(date, amount) {
    return this.clone(date).second(amount);
  }
  createDate(year, month, date, hours = 0, minutes = 0, seconds = 0) {
    if (month < 0 || month > 11) {
      throw Error(`Invalid month index "${month}". Month index has to be between 0 and 11.`);
    }
    if (date < 1) {
      throw Error(`Invalid date "${date}". Date has to be greater than 0.`);
    }
    if (hours < 0 || hours > 23) {
      throw Error(`Invalid hours "${hours}". Hours has to be between 0 and 23.`);
    }
    if (minutes < 0 || minutes > 59) {
      throw Error(`Invalid minutes "${minutes}". Minutes has to between 0 and 59.`);
    }
    if (seconds < 0 || seconds > 59) {
      throw Error(`Invalid seconds "${seconds}". Seconds has to be between 0 and 59.`);
    }
    const result = this.createDayjs({
      year,
      month,
      date,
      hours,
      minutes,
      seconds
    }).locale(this.locale);
    // If the result isn't valid, the date must have been out of bounds for this month.
    if (!result.isValid()) {
      throw Error(`Invalid date "${date}" for month with index "${month}".`);
    }
    return result;
  }
  clone(date) {
    return this.createDayjs(date).clone().locale(this.locale);
  }
  now() {
    return this.createDayjs().locale(this.locale);
  }
  format(date, displayFormat) {
    date = this.clone(date);
    if (!this.isValid(date)) {
      throw Error('DayjsDateTimeAdapter: Cannot format invalid date.');
    }
    return date.format(displayFormat);
  }
  parse(value, parseFormat) {
    if (value && typeof value === 'string') {
      return this.createDayjs(value, parseFormat, this.locale);
    }
    return value ? this.createDayjs(value).locale(this.locale) : null;
  }
  /**
   * Returns the given value if given a valid Dayjs or null. Deserializes valid ISO 8601 strings
   * (https://www.ietf.org/rfc/rfc3339.txt) and valid Date objects into valid Dayjss and empty
   * string into null. Returns an invalid date for all other values.
   */
  deserialize(value) {
    let date;
    if (value instanceof Date) {
      date = this.createDayjs(value);
    }
    if (typeof value === 'string') {
      if (!value) {
        return null;
      }
      date = this.createDayjs(value).locale(this.locale);
    }
    if (date && this.isValid(date)) {
      return date;
    }
    return super.deserialize(value);
  }
  /** Creates a Dayjs instance while respecting the current UTC settings. */
  createDayjs(...args) {
    return this.options && this.options.useUtc ? dayjs.utc(...args) : dayjs(...args);
  }
}
DayjsDateTimeAdapter.ɵfac = function DayjsDateTimeAdapter_Factory(t) {
  return new (t || DayjsDateTimeAdapter)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](OWL_DATE_TIME_LOCALE, 8), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵinject"](OWL_DAYJS_DATE_TIME_ADAPTER_OPTIONS, 8));
};
DayjsDateTimeAdapter.ɵprov = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjectable"]({
  token: DayjsDateTimeAdapter,
  factory: DayjsDateTimeAdapter.ɵfac
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](DayjsDateTimeAdapter, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable
  }], function () {
    return [{
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DATE_TIME_LOCALE]
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Optional
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Inject,
        args: [OWL_DAYJS_DATE_TIME_ADAPTER_OPTIONS]
      }]
    }];
  }, null);
})();

// import { OwlDateTimeFormats } from 'ng-pick-datetime';
const OWL_DAYJS_DATE_TIME_FORMATS = {
  parseInput: 'l LT',
  fullPickerInput: 'l LT',
  datePickerInput: 'l',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};

/**
 * dayjs-date-time.module
 */
// import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE_PROVIDER } from 'ng-pick-datetime';
class DayjsDateTimeModule {}
DayjsDateTimeModule.ɵfac = function DayjsDateTimeModule_Factory(t) {
  return new (t || DayjsDateTimeModule)();
};
DayjsDateTimeModule.ɵmod = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
  type: DayjsDateTimeModule
});
DayjsDateTimeModule.ɵinj = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
  providers: [{
    provide: DateTimeAdapter,
    useClass: DayjsDateTimeAdapter,
    deps: [OWL_DATE_TIME_LOCALE, OWL_DAYJS_DATE_TIME_ADAPTER_OPTIONS]
  }]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](DayjsDateTimeModule, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule,
    args: [{
      providers: [{
        provide: DateTimeAdapter,
        useClass: DayjsDateTimeAdapter,
        deps: [OWL_DATE_TIME_LOCALE, OWL_DAYJS_DATE_TIME_ADAPTER_OPTIONS]
      }]
    }]
  }], null, null);
})();
class OwlDayjsDateTimeModule {}
OwlDayjsDateTimeModule.ɵfac = function OwlDayjsDateTimeModule_Factory(t) {
  return new (t || OwlDayjsDateTimeModule)();
};
OwlDayjsDateTimeModule.ɵmod = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
  type: OwlDayjsDateTimeModule
});
OwlDayjsDateTimeModule.ɵinj = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
  providers: [{
    provide: OWL_DATE_TIME_FORMATS,
    useValue: OWL_DAYJS_DATE_TIME_FORMATS
  }],
  imports: [DayjsDateTimeModule]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](OwlDayjsDateTimeModule, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule,
    args: [{
      imports: [DayjsDateTimeModule],
      providers: [{
        provide: OWL_DATE_TIME_FORMATS,
        useValue: OWL_DAYJS_DATE_TIME_FORMATS
      }]
    }]
  }], null, null);
})();

/**
 * picker
 */

/**
 * Generated bundle index. Do not edit.
 */



/***/ })

}]);
//# sourceMappingURL=default-src_app_core_domain-classes_dayOfWeek_enum_ts-src_app_core_domain-classes_frequency_e-a11b28.js.map